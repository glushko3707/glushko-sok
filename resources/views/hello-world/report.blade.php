<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div style="width: 100vw; height: 100vh; margin: 0; padding: 0; overflow: hidden;">
    <iframe
            title="WH_D_3"
            src="{{$src}}"
            frameborder="0"
            allowfullscreen="true"
            style="width: 100%; height: 100%; border: none;">
    </iframe>
</div>
</body>
</html>