@extends('adminPanel.adminHW')

@section('content')


    <div class="content-wrapper">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <form method="POST" action="{{ route('hw.listUpdateStore') }}">
                    @csrf
                    <div class="row mb-3">
                        <label for="sheet" class="col-md-4 col-form-label text-md-end"> Дата начало периода </label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control" type="date" id="dateFrom" name="dateFrom" value="{{\Illuminate\Support\Carbon::today()->subDays(2)->format('Y-m-d')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="sheet" class="col-md-4 col-form-label text-md-end"> Дата конец периода </label>
                        <div class="col-md-6">
                            <div class="input-group">
                                <input class="form-control" type="date" id="dateTo" name="dateTo" value="{{\Illuminate\Support\Carbon::today()->format('Y-m-d')}}">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-12 d-flex justify-content-center">
                            <button style="width: 200px" class="btn btn-primary">Обновить</button>
                        </div>
                    </div>
                </form>

                @livewire('hw.hw-account-update-table')
            </div>
        </div>
    </div>


@endsection
