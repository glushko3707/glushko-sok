@extends('adminPanel.adminHW')

@section('content')


    <div class="content-wrapper">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">
                        Обновление данных по датам
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('hw.account.update.table.date.store',$account_update_id) }}">
                          @csrf
                            <div class="row mb-3">
                            <label for="sheet" class="col-md-4 col-form-label text-md-end"> Дата начало периода </label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input class="form-control" type="date" id="dateFrom" name="dateFrom" value="{{\Illuminate\Support\Carbon::today()->subDays(2)->format('Y-m-d')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label for="sheet" class="col-md-4 col-form-label text-md-end"> Дата конец периода </label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input class="form-control" type="date" id="dateTo" name="dateTo" value="{{\Illuminate\Support\Carbon::today()->format('Y-m-d')}}">
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-12 d-flex justify-content-center">
                                <button style="width: 200px" class="btn btn-primary">Обновить</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>


@endsection
