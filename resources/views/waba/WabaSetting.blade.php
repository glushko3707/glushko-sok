@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <div class="row justify-content-center">
            <div class="col-md-10" style="margin-top: 30px">
                @if (session()->has('success') || session()->has('error'))
                    <div id="allert" class="alert alert-{{ session()->has('success') ? 'success' : 'danger' }}">
                        {{ session()->get('success') ?? session()->get('error') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('SendMessageWaba.store',$id)}}">
                    @csrf

                <div class="card mt-4">
                    <div class="card-header d-flex justify-content-between">
                        <div>
                            <h4>
                                Настройки Waba
                            </h4>
                        </div>
                    </div>

                    <div class="card-body">

                        @foreach($wabaIntance as $intance_id)

                            <div class="row mb-3">
                                <label for="category" class="col-md-4 col-form-label text-md-end"> Номер телефона</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" value="{{$intance_id['phone']}}" disabled />

                                </div>
                                <div class="col-1 clickable-element zoom-effect text-2xl d-flex justify-content-start align-items-center" style="color: #727272FF">
                                      <a href="{{route('setBM.index',$id)}}"> + </a>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="category" class="col-md-4 col-form-label text-md-end"> Waba Phone Id</label>
                                <div class="col-md-6">
                                    <input class="form-control" type="text" value="{{$intance_id['waba_phone_id']}}" disabled />
                                </div>
                            </div>
                            @if($intance_id['active'] != 1)
                                <div class="row mb-3">
                                    <label for="category" class="col-md-4 col-form-label text-md-end"> Подтвердить номер</label>
                                    <div class="col-md-6">
                                        <a href="{{route('verificationCode.index',['id' => $id,'phone_id' => $intance_id['waba_phone_id']])}}" class="btn btn-success">Обновить</a>
                                    </div>
                                </div>
                            @endif
                        @endforeach



                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <a href="{{ route('facebookEnter.index',$id) }}" class="btn btn-primary">
                                        Обновить подключение
                                    </a>

                                </div>
                            </div>
                    </div>

                </div>
                </form>

            </div>
        </div>
    </div>


    <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })

        // Получаем элемент по id
        var alert = document.getElementById("allert");

        // Устанавливаем таймер на 3 секунды
        setTimeout(function() {
                alert.style.display = "none";
        }, 3000);
    </script>
@endsection
