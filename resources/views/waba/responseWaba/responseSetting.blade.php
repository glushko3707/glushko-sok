@extends('adminPanel.startAddProject')

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            @livewire('waba.response-setting',[
                           'id' => $id,
                                          ])
        </div>
    </div>
@endsection

<style>
    tfoot .custom-textarea {
        background-color: #ffffff;
        border: 1px solid rgba(17, 17, 17, 0.09);
        border-radius: 3px;
    }

    textarea {
        padding-left: 2px;
    }

    textarea:focus {
        border-radius: 3px;
    }

    input {
        padding-left: 2px;
    }

    .close-btn {
        position: absolute;
        top: 0;
        right: 5px;
        cursor: pointer;
        font-size: 18px;
        color: red;
    }

    .whatsapp-header-template {
        background-color: #f7f7f7;
        text-align: center;
        width: 400px;
        padding: 15px;
        font-weight: 800;
        border: 1px solid rgba(0, 0, 0, 0.2);
    }

    .whatsapp-message {
        margin-top: 12px;
        margin-bottom: 12px;
        margin-left: 12px;
        width: 280px;
        background-color: #f7f7f7;
        border-radius: 10px;
        padding: 3px;
    }
    .message-header {
        background-color: #f7f7f7;
        padding: 10px 15px;
        font-weight: bold;
        border-bottom: 1px solid #d9d9d9;
    }

    .message-body {
        /*padding: 5px;*/
        font-size: 14px;
        line-height: 1.5;
        background-color: rgba(0, 0, 0, 0);
        overflow: hidden; /* Скрытие полос прокрутки */
        resize: none; /* Отключаем возможность изменения размера вручную */
    }

    .message-body p {
        margin: 0 0 10px;
    }

    .message-body a {
        color: #1a73e8;
        text-decoration: none;
        word-break: break-word;
    }

    .message-body a:hover {
        text-decoration: underline;
    }

    .message-footer {
        position: relative;
        background-color: #f7f7f7;
        padding: 10px;
        text-align: center;
        border-top: 1px solid #d9d9d9;
    }


    .waba-button {
        color: #4f8ff1;
        background: transparent;
        border: none;
        /*padding: 10px 20px;*/
        border-radius: 5px;
        cursor: pointer;
    }

    .whatsapp-background {
        background-image: url('{{asset('images/waba/bg.jpg')}}');
        display: flex;
        align-items: flex-start;
        width: 400px;
        height: 700px;
        background-size: cover;
        background-position: center;
        border: 1px solid #d9d9d9;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        flex-direction: column-reverse;
    }
    ._6xe4 {
        color: #282828;
        font-size: 14px;
        /*white-space: pre-wrap;*/
        /*white-space-collapse: preserve;*/
        overflow-wrap: break-word;
        text-align: initial;
    }

    .custom-textarea {
        line-height: 1.5;
        background-color: rgba(0, 0, 0, 0);
        width: 250px;
        overflow: hidden; /* Скрытие полос прокрутки */
        resize: none; /* Отключаем возможность изменения размера вручную */
    }

    table tr {
        height: 40px; /* Используйте нужное вам значение высоты */
    }

    /* Стиль для ячеек таблицы, если хотите использовать line-height */
    tbody td, tbody th {
        line-height: 40px; /* значение должно совпадать или быть в соотношении с высотой строки */
        font-weight: 400;
    }
    .rounded-top-corners {
        border-top-left-radius: 10px; /* Скругление левого верхнего угла */
        border-top-right-radius: 10px; /* Скругление правого верхнего угла */
        display: block; /* Убедитесь, что изображение отображается как блок */
    }

    .large-icon {
        cursor: pointer;
        width: 30px;    /* Устанавливает ширину иконки */
        height: 30px;   /* Устанавливает высоту иконки */
        transition: transform 0.3s ease;
    }
    .large-icon:hover {
        transform: rotate(90deg);
    }
</style>




