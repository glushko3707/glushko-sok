<div class="container-fluid">

    <div class="row mt-3" style="justify-content: center">

        <div class="row" style="width: 90%;padding: 0">
            <div class="d-flex flex-wrap p-0">
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <div class="d-flex justify-content-between">
                            <div>
                                <p> Регистраций </p>
                            </div>
                            <div>
                                <button wire:click="save"  class="btn-sm btn-outline-primary">Скачать</button>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div>
                                <h1> {{$statistica['lead'] ?? ''}} </h1>

                            </div>
                            <div>
                                <div class="text-sm ml-1">
                                    {{$statistica['leadToday'] ?? ''}}
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <div class="d-flex justify-content-between">
                            <div>
                                <p> Затраты </p>
                            </div>
                            <div>
                                <button class="btn btn-sm" wire:click="updateStatistica" >Обновить</button>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div>
                                <h1> ${{$statistica['spend'] ?? ''}} </h1>
                            </div>
                            <div>


                                {{--                                <div class="input-group" id="addAccaunt" style="width: 175px;display: none">--}}
                                {{--                                    <input  type="text" class="form-control input-opacity"  wire:model.live="add_account_id" placeholder="Id аккаунта" />--}}
                                {{--                                    <span  class="input-group-append">--}}
                                {{--                                        <button wire:click="add_account" class="btn btn-sm">--}}
                                {{--                                            &#43;--}}
                                {{--                                        </button></span>--}}
                                {{--                                </div>--}}
                                <div>
                                    <a href="{{route('deleteAccountAdFacebook.index',$id)}}"  class="btn btn-sm">
                                        <span> - </span>
                                    </a>
                                    <a href="{{route('addAccountAdFacebook.index',$id)}}"  class="btn btn-sm">
                                        <span>&#43;</span>
                                    </a>
                                </div>


                            </div>

                        </div>



                    </div>
                </div>

                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Цена лида </p>
                        <h1> {{round($statistica['leadPrice'],2) ?? ''}} </h1>
                    </div>
                </div>

                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <h6 class="font-semibold"> Вебинар </h6>
                        <div class="row">
                            <div class="col-9">
                                Доходимость
                            </div>
                            <div class="col-3">
                                {{$statistica['webinarReach'] ?? ''}} %
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-9">
                                Цена вебинар
                            </div>
                            <div class="col-3">
                                {{round($statistica['webinarPrice'],2) ?? ''}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <div class="card" style="width: 90%;padding: 0">


            <div class="card-tools">
                <div class="row">
                    <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                        <div>
                            <div class="form-inline float-right mr-3"  style="padding: 14px">
                                <label  class="mr-3">От</label>
                                <input class="form-control mr-3" wire:model.live="date_from" type="date" name="date_from">
                                <label  class="mr-3">До</label>
                                <input class="form-control mr-3" wire:model.live="date_to" type="date"  name="date_to">

                                <input type="text" class="mr-2 form-control"  wire:model.live="search" placeholder="поиск" />
                                <select type="text" class="mr-2 form-control"  wire:model.live="date_preview">
                                    <option selected value="all">Все дни</option>
                                    <option value="today"> Сегодня</option>
                                    <option value="yesterday"> Вчера</option>
                                    <option value="2dayAgo"> Позовчера</option>
                                    <option value="las7Days"> Последние 7 дней</option>
                                    <option value="month"> Этот месяц</option>
                                    <option value="last30Days"> 30 дней</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-header -->
            <div class="card" style="margin-bottom: 0;box-shadow: unset">
                <div class="card-body table-responsive p-0" style="max-height: 450px;">
                    <table class="table table-head-fixed table-sm table-bordered text-nowrap">
                        <thead>
                        <tr>
                            <th rowspan="2">Название рекламных компаний</th>
                            <th rowspan="2">Расход</th>
                            <th colspan="2" style="text-align: center">Лиды</th>
                            <th colspan="3" style="text-align: center">Вебинар</th>
                            <th colspan="3" style="text-align: center">Оплаты сессий</th>
                        </tr>
                        <tr>
                            <th style="font-weight: 400;text-align: center;padding-left: 5px">кол-во</th>
                            <th style="font-weight: 400;text-align: center">цена</th>
                            <th style="font-weight: 400;text-align: center">кол-во</th>
                            <th style="font-weight: 400;;text-align: center">цена</th>
                            <th style="font-weight: 400;text-align: center;width: 50px">%</th>
                            <th style="text-align: center;font-weight: 400;">кол-во</th>
                            <th style="text-align: center;font-weight: 400;">цена</th>
                            <th style="font-weight: 400;text-align: center;width: 50px">%</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dayWebStatistice as $key => $accaunt)
                            <tr class="clickable" data-toggle="{{$accaunt['account_id']}}">
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div class="d-flex">
                                            <div class="mr-1">{{$accaunt['account_name']}}</div> <div class="text-sm"> ({{$key}})</div>
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            $ {{$accaunt['spend']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{$accaunt['lead']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            $ {{round($accaunt['leadPrice'],1) ?? 0}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{$accaunt['webinar']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            $ {{round($accaunt['webinarPrice'],1) ?? 0}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{round($accaunt['webinarPercent']) ?? 0}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{$accaunt['payment']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            @if($accaunt['payment'] > 0)
                                                {{round($accaunt['spend']/$accaunt['payment'])}}
                                            @else
                                                0
                                            @endif
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            @if($accaunt['webinar'] > 0)
                                                {{round($accaunt['payment']/$accaunt['webinar']*100,2)}}
                                            @else
                                                0
                                            @endif
                                        </div>
                                    </div>
                                </th>
                            </tr>
                            @foreach($accaunt['facebook_campaign_collect'] as $campaignKey => $campaign)
                                <tr class="clickable {{$accaunt['account_id']}}"  data-toggle="{{$campaign['campaign_id']}}" style="display: none;background: #e0f9ff">
                                    <th style="font-weight: normal;padding-left: 50px">
                                        <div class="d-flex justify-content-start">
                                            <div class="d-flex">
                                                <div class="mr-1">{{$campaign['campaign_name']}}</div> <div class="text-sm"> ({{$campaign['campaign_id']}})</div>
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$campaign['spend']}} $
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{$campaign['lead']}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{round($campaign['leadPrice'],1) ?? 0}} $
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{$campaign['webinar']}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{round($campaign['webinarPrice'],1) ?? 0}} $
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{round($campaign['webinarPercent']) ?? 0}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{$campaign['payment']}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                @if($campaign['payment'] > 0)
                                                    {{round($campaign['spend']/$campaign['payment'])}}
                                                @else
                                                    0
                                                @endif
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                @if($campaign['webinar'] > 0)
                                                    {{round($campaign['payment']/$campaign['webinar']*100,2)}}
                                                @else
                                                    0
                                                @endif
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                                @foreach($campaign['facebook_adset_collect'] as $adsetKey => $adset)
                                    <tr class="clickable {{$campaign['campaign_id']}}" data-toggle="{{$adset['adset_id']}}" style="display: none;background: #edffeb">
                                        <th style="font-weight: normal; padding-left: 70px">
                                            <div class="d-flex justify-content-start">
                                                <div class="d-flex">
                                                    <div class="mr-1">{{$adset['adset_name']}}</div> <div class="text-sm">({{$adset['adset_id']}})</div>
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$adset['spend']}} $
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{$adset['lead']}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{round($adset['leadPrice'],1) ?? 0}} $
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{$adset['webinar']}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{round($adset['webinarPrice'],1) ?? 0}} $
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{round($adset['webinarPercent']) ?? 0}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{$adset['payment']}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    @if($adset['payment'] > 0)
                                                        {{round($adset['spend']/$adset['payment'])}}
                                                    @else
                                                        0
                                                    @endif
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    @if($adset['webinar'] > 0)
                                                        {{round($adset['payment']/$adset['webinar']*100),2}}
                                                    @else
                                                        0
                                                    @endif
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    @foreach($adset['facebook_ad_collect'] as $adKey => $ad)
                                        <tr class="clickable {{$adset['adset_id']}}" style="display: none">
                                            <th style="font-weight: normal; padding-left: 100px">
                                                <div class="d-flex justify-content-start">
                                                    <div class="d-flex">
                                                        <div class="mr-1"> {{$ad['ad_name']}}</div> <div class="text-sm"> ({{$ad['ad_id']}})</div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-start">
                                                    <div>
                                                        {{$ad['spend']}} $
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{$ad['lead']}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{round($ad['leadPrice'],1) ?? 0}} $
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{$ad['webinar']}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{round($ad['webinarPrice'],1) ?? 0}} $
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{round($ad['webinarPercent']) ?? 0}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{$ad['payment']}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        @if($ad['payment'] > 0)
                                                            {{round($ad['spend']/$ad['payment'])}}
                                                        @else
                                                            0
                                                        @endif
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        @if($ad['webinar'] > 0)
                                                            {{round($ad['payment']/$ad['webinar'])}}
                                                        @else
                                                            0
                                                        @endif
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        </tbody>
                        <tfoot style="background: #ffffff;font-weight: bold;position: sticky; bottom: 0;">
                        <td>
                            Всего
                        </td>
                        <td>
                            $ {{$statistica['spend'] ?? ''}}
                        </td>
                        <td style="text-align: center">
                            {{$statistica['lead'] ?? ''}}
                        </td>
                        <td style="text-align: center">
                            $ {{$statistica['leadPrice'] ?? ''}}
                        </td>
                        <td style="text-align: center">
                            {{$statistica['webinar'] ?? ''}}
                        </td>
                        <td style="text-align: center">
                            $ {{$statistica['webinarPrice'] ?? ''}}
                        </td>
                        <td style="text-align: center;">
                            {{$statistica['webinarReach'] ?? ''}}
                        </td>
                        <td style="text-align: center;">
                            {{$statistica['payment'] ?? ''}}
                        </td>
                        <td style="text-align: center;">
                            $ {{$statistica['paymentPrice'] ?? ''}}
                        </td>
                        <td style="text-align: center">
                            {{$statistica['paymentPercent'] ?? ''}}
                        </td>
                        </tfoot>
                    </table>

                </div>

            </div>

        </div>
    </div>



</div>


@script
<script>
    $wire.watch('statistica', function () {
        setTimeout(function() {
            $('.clickable').off('click').on('click', function () {
                console.log(this);
                var id = $(this).data('toggle');
                console.log(id);

                var rows = $('.' + id);
                rows.toggle();

                // Скрыть все дочерние элементы при скрытии родителя
                if (!rows.is(':visible')) {
                    hideNested(rows);
                }
            });
        }, 2000); // Задержка 3 секунды (4000 миллисекунд)
    });
</script>



@endscript

