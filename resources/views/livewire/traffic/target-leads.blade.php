<div>


    <div class="row">
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <div class="d-flex justify-content-between">
                        <div>
                            <p> Лиды </p>
                        </div>
                        <div>
                            <a wire:click="save" type="button"  class=" period ">Скачать</a>
                        </div>
                    </div>

                    <h1> {{$stat ['count']}} </h1>
                </div>
            </div>
        </div>
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <div class="d-flex justify-content-between">
                        <div>
                            <p> Сегодня </p>
                        </div>
                    </div>

                    <h1> {{$stat ['countToday']}} </h1>
                </div>
            </div>
        </div>

    </div>



    <div>
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-3" style="justify-content: center">


                    <div class="card" style="padding: 0">


                        <div class="card-tools">
                            <div class="d-flex justify-content-between" style="padding: 10px">
                                <div>
                                    <div class="form-inline float-right">
                                        <label  class="mr-3">От</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_from" name="date_from">
                                        <label  class="mr-3">До</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_to" name="date_to">
                                        <input type="text" class="mr-2 form-control"  wire:model.live="search" placeholder="поиск">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card" style="margin-bottom: 0;box-shadow: unset">
                                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                                            <thead>
                                            <tr>
                                                <th style="width: 100px">Дата и время</th>
                                                <th style="width: 100px">Имя</th>
                                                <th style="width: 100px">Номер тел.</th>
                                                <th style="width: 125px">utm_source</th>
                                                <th style="width: 125px">utm_medium</th>
                                                <th style="width: 150px">utm_campaign</th>
                                                <th style="width: 100px">utm_content</th>
                                                <th style="width: 300px">utm_term</th>
                                                <th style="width: 400px">Доп. поле</th>

                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($leads as $lead)
                                                <tr >

                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{ $lead['created_at'] }}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{ $lead['name'] ?? '' }}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['phone']}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['utm_source'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['utm_medium']  ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['utm_campaign'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['utm_content'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['utm_term'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['value_1'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                        <!-- /.card-header -->
                    </div>
                    {{ $leads->links() }}
                </div>
            </div>
        </section>
    </div>



</div>

