<div class="container-fluid">

    <div class="row mt-3" style="justify-content: center">

        <div class="row" style="width: 90%;padding: 0">
            <div class="d-flex flex-wrap p-0">
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <div class="d-flex justify-content-between">
                            <div>
                                <p> Регистраций </p>
                            </div>
                            <div>
                                <button wire:click="save"  class="btn-sm btn-outline-primary">Скачать</button>
                            </div>
                        </div>
                        <h1> {{$statistica['lead'] ?? ''}} </h1>

                    </div>

                </div>


                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <h6 class="font-semibold"> Вебинар </h6>
                        <div class="row">
                            <div class="col-9">
                                Доходимость
                            </div>
                            <div class="col-3">
                                {{$statistica['webinarReach'] ?? ''}} %
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
        <div class="card" style="width: 90%;padding: 0">


            <div class="card-tools">
                <div class="row">
                    <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                        <div>
                            <div class="form-inline float-right mr-3"  style="padding: 14px">
                                <label  class="mr-3">От</label>
                                <input class="form-control mr-3" wire:model.live="date_from" type="date" name="date_from">
                                <label  class="mr-3">До</label>
                                <input class="form-control mr-3" wire:model.live="date_to" type="date"  name="date_to">

                                <input type="text" class="mr-2 form-control"  wire:model.live="search" placeholder="поиск" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /.card-header -->
            <div class="card" style="margin-bottom: 0;box-shadow: unset">
                <div class="card-body table-responsive p-0" style="max-height: 450px;">
                    <table class="table table-head-fixed table-sm table-bordered text-nowrap">
                        <thead>
                        <tr>
                            <th rowspan="2">Название рекламных компаний</th>
                            <th colspan="1" style="text-align: center">Лиды</th>
                            <th colspan="2" style="text-align: center">Вебинар</th>
                            <th colspan="2" style="text-align: center">Оплаты сессий</th>
                            <th colspan="1" style="text-align: center">Основной курс</th>
                        </tr>
                        <tr>
                            <th style="font-weight: 400;text-align: center;padding-left: 5px">кол-во</th>
                            <th style="font-weight: 400;text-align: center">кол-во</th>
                            <th style="font-weight: 400;text-align: center;width: 50px">%</th>
                            <th style="font-weight: 400;text-align: center;">кол-во</th>
                            <th style="font-weight: 400;text-align: center;width: 50px">%</th>
                            <th style="text-align: center;font-weight: 400;">кол-во</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($dayWebStatistice as $key => $accaunt)
                            <tr class="clickable" data-toggle="{{$accaunt['account_id']}}">
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div class="d-flex">
                                            <div class="mr-1">{{$accaunt['account_name']}}</div>
                                        </div>
                                    </div>
                                </th>

                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{$accaunt['lead']}}
                                        </div>
                                    </div>
                                </th>

                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{$accaunt['webinar']}}
                                        </div>
                                    </div>
                                </th>

                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{round($accaunt['webinarPercent']) ?? 0}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{$accaunt['payment']}}
                                        </div>
                                    </div>
                                </th>

                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            @if($accaunt['webinar'] > 0)
                                                {{round($accaunt['payment']/$accaunt['webinar'])}}
                                            @else
                                                0
                                            @endif
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-center">
                                        <div>
                                            {{$accaunt['paymentMain']}}
                                        </div>
                                    </div>
                                </th>

                            </tr>
                            @foreach($accaunt['facebook_campaign_collect'] as $campaignKey => $campaign)
                                <tr class="clickable {{$accaunt['account_id']}}"  data-toggle="{{$campaign['campaign_id']}}" style="display: none;background: #e0f9ff">
                                    <th style="font-weight: normal;padding-left: 50px">
                                        <div class="d-flex justify-content-start">
                                            <div class="d-flex">
                                                <div class="mr-1">{{$campaign['campaign_name']}}</div>
                                            </div>
                                        </div>
                                    </th>

                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{$campaign['lead']}}
                                            </div>
                                        </div>
                                    </th>

                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{$campaign['webinar']}}
                                            </div>
                                        </div>
                                    </th>


                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{$campaign['payment']}}
                                            </div>
                                        </div>
                                    </th>


                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-center">
                                            <div>
                                                {{$campaign['paymentMain']}}
                                            </div>
                                        </div>
                                    </th>

                                </tr>
                                @foreach($campaign['facebook_adset_collect'] as $adsetKey => $adset)
                                    <tr class="clickable {{$campaign['campaign_id']}}" data-toggle="{{$adset['adset_id']}}" style="display: none;background: #edffeb">
                                        <th style="font-weight: normal; padding-left: 70px">
                                            <div class="d-flex justify-content-start">
                                                <div class="d-flex">
                                                    <div class="mr-1">{{$adset['adset_name']}}</div>
                                                </div>
                                            </div>
                                        </th>

                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{$adset['lead']}}
                                                </div>
                                            </div>
                                        </th>

                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{$adset['webinar']}}
                                                </div>
                                            </div>
                                        </th>

                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{round($adset['webinarPercent']) ?? 0}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{$adset['payment']}}
                                                </div>
                                            </div>
                                        </th>


                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-center">
                                                <div>
                                                    {{$adset['paymentMain']}}
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                    @foreach($adset['facebook_ad_collect'] as $adKey => $ad)
                                        <tr class="clickable {{$adset['adset_id']}}" style="display: none">
                                            <th style="font-weight: normal; padding-left: 100px">
                                                <div class="d-flex justify-content-start">
                                                    <div class="d-flex">
                                                        <div class="mr-1"> {{$ad['ad_name']}}</div>
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{$ad['lead']}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{$ad['webinar']}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{round($ad['webinarPercent']) ?? 0}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{$ad['payment']}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        @if($ad['webinar'] > 0)
                                                            {{round($ad['payment']/$ad['webinar'])}}
                                                        @else
                                                            0
                                                        @endif
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-center">
                                                    <div>
                                                        {{$ad['paymentMain']}}
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach
                        </tbody>

                    </table>
                    <script>
                        $(document).ready(function () {
                            $('.clickable').on('click', function () {
                                var id = $(this).data('toggle');
                                var rows = $('.' + id);
                                rows.toggle();

                                // Скрыть все дочерние элементы при скрытии родителя
                                if (!rows.is(':visible')) {
                                    hideNested(rows);
                                }
                            });

                            function hideNested(rows) {
                                rows.each(function () {
                                    var nestedId = $(this).data('toggle');
                                    var nestedRows = $('.' + nestedId);
                                    nestedRows.hide();
                                    hideNested(nestedRows);
                                });
                            }
                        });
                    </script>
                </div>

            </div>

        </div>
    </div>
</div>

