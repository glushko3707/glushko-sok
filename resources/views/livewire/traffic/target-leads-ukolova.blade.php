<div>


    <div class="row">
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <p> Лиды </p>
                    <h1> {{$stat ['count']}} </h1>
                </div>
                <div class="icon">
                    <i class="ion ion-speedometer"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-5 col-6 mr-5">
            <!-- Виджет 1: Остаток бюджета -->
            <button wire:click="save" type="button"  class="btn period btn-outline-primary">Скачать файл</button>
        </div>
    </div>


    <div>
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-3" style="justify-content: center">


                    <div class="card" style="padding: 0">


                        <div class="card-tools">
                            <div class="d-flex justify-content-between" style="padding: 10px">
                                <div style="align-items: center;" class="d-flex">
                                    <div>
                                        <ul class="nav nav-pills">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#detal" data-toggle="tab">Лиды</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div>
                                    <div class="form-inline float-right">
                                        <input type="text" class="mr-2 form-control"  wire:model.live="searchPhone" placeholder="поиск по телефону" />

                                        <div class="mr-3">
                                            <select wire:model.live="dateTask" class="form-control input-opacity" name="dateTask" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                                <option  value="Все">Все задачи</option>
                                                <option  value="Сегодня">Сегодня</option>
                                                <option  value="Будущие">Будущие</option>
                                                <option  value="Просроченные">Просроченные</option>
                                            </select>
                                        </div>

                                        <div class="mr-3">
                                            <select wire:model.live="status" class="form-control input-opacity" name="status" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                                <option  value="all">Все статусы</option>
                                                <option  value="new">Новый лид</option>
                                                <option  value="noConnect">Недозвон</option>
                                                <option  value="bad">Негатив</option>
                                                <option  value="meet">Назначена встреча</option>
                                                <option  value="possibleSale">Заинтересован</option>
                                                <option  value="sales">Продажа</option>
                                                <option  value="no_success">Не успешно</option>
                                                <option  value="noTarget">Не целевой</option>
                                            </select>
                                        </div>
                                        <label  class="mr-3">От</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_from" name="date_from">
                                        <label  class="mr-3">До</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_to" name="date_to">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" id="detal">
                                <div class="card" style="margin-bottom: 0;box-shadow: unset">
                                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                                            <thead>
                                            <tr>
                                                <th style="width: 100px">Дата и время</th>
                                                <th style="width: 100px">Номер тел.</th>
                                                <th style="width: 125px">Источник</th>
                                                <th style="width: 125px">ADSET_ID</th>
                                                <th style="width: 150px">Статус лида</th>
                                                <th style="width: 100px">Дата задачи</th>
                                                <th style="width: 400px">Комментарий</th>
                                                <th style="width: 50px">WA</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($leads as $lead)
                                                <tr >

                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{ $lead['created_at'] }}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['phone']}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['utm_source']}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['utm_campaign']}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="day">
                                123
                            </div>
                        </div>

                        <!-- /.card-header -->

                    </div>
                    <div class="d-flex justify-content-center">
                        {{$leadses->links()}}
                    </div>
                </div>
            </div>
        </section>
    </div>



</div>

