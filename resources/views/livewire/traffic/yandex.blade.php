<div class="container-fluid">

    <div class="row mt-3" style="justify-content: center">

        <div class="row" style="width: 90%;padding: 0">
            <div class="d-flex flex-wrap p-0">
                <div class="small-box glushkoSmallBox col-12 col-md-6 col-lg-3 mb-3">
                    <div class="inner">
                        <div class="d-flex justify-content-between">
                            <div>
                                <p>Регистраций</p>
                            </div>
                            <div>
                                <button wire:click="save" class="btn btn-sm btn-outline-primary">Скачать</button>
                            </div>
                        </div>
                        <h1>{{$statistica['lead'] ?? ''}}</h1>
                    </div>
                </div>

                <div class="small-box glushkoSmallBox col-12 col-md-6 col-lg-3 mb-3">
                    <div class="inner">
                        <div class="d-flex justify-content-between">
                            <div>
                                <p>Затраты</p>
                            </div>
                            <div>
                                <button class="btn btn-sm btn-outline-primary" wire:click="updateStatistica">Обновить</button>
                            </div>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div>
                                <h1>{{$statistica['spend'] ?? ''}} ₽</h1>
                            </div>
                            <div>
                                <div class="input-group" id="add_account_name" style="width: 175px; display: none;">
                                    <input type="text" class="form-control input-opacity" wire:model.lazy="add_account_name" placeholder="account_name" />
                                    <span class="input-group-append">
                                  <button wire:click="add_account" class="btn btn-sm"> Добавить
                            </button>
                        </span>
                                </div>
                                <div id="showAddAccauntButton">
                                    <button onclick="toggleInputss()" class="btn btn-sm">
                                        <span>&#43;</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="small-box glushkoSmallBox col-12 col-md-6 col-lg-3 mb-3">
                    <div class="inner">
                        <p>Цена лида</p>
                        <h1>{{round($statistica['leadPrice']) ?? ''}}</h1>
                    </div>
                </div>

                <div class="small-box glushkoSmallBox col-12 col-md-6 col-lg-3 mb-3">
                    <div class="inner">
                        <h6 class="font-semibold">Вебинар</h6>
                        <div class="row">
                            <div class="col-9 text-left dot-leader ml-2 p-0">
                                Доходимость
                            </div>
                            <div class="col-2">
                                {{$statistica['webinarReach'] ?? ''}} %
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-9 text-left dot-leader  ml-2 p-0">
                                Цена вебинар
                            </div>
                            <div class="col-2">
                                {{round($statistica['webinarPrice']) ?? ''}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                function toggleInputss() {
                    document.getElementById('add_account_name').style.display = '';
                    document.getElementById('showAddAccauntButton').style.display = 'none';
                }
            </script>




        </div>
        <div class="card" style="width: 90%;padding: 0">


            <div class="card-tools">
                <div class="row">
                    <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                        <div class="w-100 row">
                            <div class="col-md-8 col-12">
                                <div class="row" style="padding: 15px 15px 0 15px">
                                    <div class="col-12 col-md-4 mb-2 d-flex align-items-center">
                                        <label class="mr-2" style="min-width: 35px">От</label>
                                        <input class="form-control" wire:model.lazy="date_from" type="date" name="date_from">
                                    </div>
                                    <div class="col-12 col-md-4 mb-2 d-flex align-items-center">
                                        <label class="mr-2" style="min-width: 35px">До</label>
                                        <input class="form-control" wire:model.lazy="date_to" type="date" name="date_to">
                                    </div>
                                    <div class="col-12 col-md-4">
                                        <input type="text" class="form-control" wire:model.lazy="search" placeholder="поиск" />
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-end col-12 col-md-4">
                                <div class="btn-group p-3 col-12 col-xl-10" role="group" aria-label="Basic radio toggle button group">
                                    <input wire:model.lazy="statisticaType" value="ads" type="radio" class="btn-check" name="btnradio" id="btnradio1">
                                    <label class="btn btn-outline-primary leftRadius5" for="btnradio1">Объявления</label>

                                    <input wire:model.lazy="statisticaType" value="date" type="radio" class="btn-check" name="btnradio" id="btnradio3">
                                    <label class="btn btn-outline-primary" for="btnradio3">По дате</label>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
            <!-- /.card-header -->
            <div class="card" style="margin-bottom: 0;box-shadow: unset">
                  @if($statisticaType != 'date')
                      @include('components.traffic.yandex.ads')
                  @endif
                  @if($statisticaType == 'date')
                      @include('components.traffic.yandex.days')
                  @endif
            </div>

        </div>
    </div>
</div>

@script
<script>

    $wire.watch('statistica', function () {
        setTimeout(function() {
            $('.clickable').off('click').on('click', function () {
                console.log(this);
                var id = $(this).data('toggle');
                console.log(id);

                var rows = $('.' + id);
                rows.toggle();

                // Скрыть все дочерние элементы при скрытии родителя
                if (!rows.is(':visible')) {
                    hideNested(rows);
                }
            });
        }, 2000); // Задержка 3 секунды (4000 миллисекунд)
    });
</script>
@endscript