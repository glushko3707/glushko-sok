<div class="container" >
    <div class="row">
        <div class="col-12 col-md-6 mx-auto">

            <!-- Main content -->
            <x-card>
                <x-card-header>
                    <h4 class="m-0">
                        Выберите статусы воронки
                    </h4>
                </x-card-header>

                <x-card-body>
                    <label class="my-1 mr-2" for="newleads">Новый лид</label>
                    <select wire:model.live="new_lead_status" class="custom-select my-1 mr-sm-2 mb-3" name="new_lead" id="newleads">
                        <option value="" >Выберите статус сделки нового лида</option>
                        @foreach($accountCrmStatus as $pipeline)
                            @foreach($pipeline['statuses'] as $status)
                                <option value="{{ $status['id'] }}">{{ $status['name'] }}</option>
                            @endforeach
                        @endforeach
                    </select>

                    <label class="my-1 mr-2"  for="CrmTargetStatuses">Качественный лид</label>
                    <select class="custom-select my-1 mr-sm-2 mb-3" name="CrmTargetStatuses" id="CrmTargetStatuses">
                        <option selected>Выберите статус сделки качественного лида</option>
                        @foreach($accountCrmStatus as $pipeline)
                            @foreach($pipeline['statuses'] as $status)
                                <option value="{{$status['id']}}">{{$status['name']}}</option>
                            @endforeach
                        @endforeach
                    </select>

                    <label class="my-1 mr-2"  for="crmSuccess"> Успешно реализовано </label>
                    <select class="custom-select my-1 mr-sm-2 mb-3" name="crmSuccess" id="crmSuccess">
                        <option selected>Выберите статус сделки успешного лида</option>
                        @foreach($accountCrmStatus as $pipeline)
                            @foreach($pipeline['statuses'] as $status)
                                <option value="{{$status['id']}}">{{$status['name']}}</option>
                            @endforeach
                        @endforeach
                    </select>
                </x-card-body>
            </x-card>
        </div>
    </div>

</div>
