<div class="row">
    <div class="col-12 col-md-8 mx-auto" style="height: 500px; {{$step == 1 ? '' : 'display: none'}}">

        <!-- Main content -->
        <div class="card shadow mt-3  border-0" style="height: 400px">
            <x-card-header>
                <h4 class="m-0">
                    Выберите CRM
                </h4>

            </x-card-header>

            <x-card-body>
                    <div class="form-check mb-1">
                        <input class="form-check-input" wire:model.live="crm_type" type="radio" id="exampleRadios1" value="Bitrix24" checked>
                        <label class="form-check-label" for="exampleRadios1">
                            Bitrix24
                        </label>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" wire:model.live="crm_type" id="exampleRadios2" value="AmoCrm">
                        <label class="form-check-label" for="exampleRadios2">
                            AmoCrm
                        </label>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" wire:model.live="crm_type" id="exampleRadios2" value="Commo">
                        <label class="form-check-label">
                            Commo
                        </label>
                    </div>
                    <div class="form-check mb-1">
                        <input class="form-check-input" type="radio" wire:model.live="crm_type" id="exampleRadios3" value="other" disabled>
                        <label class="form-check-label" for="exampleRadios3">
                            Другая
                        </label>
                    </div>
                    <div class="form-check mb-4">
                        <input class="form-check-input" type="radio" wire:model.live="crm_type" id="exampleRadios3" value="GoogleSheets" disabled>
                        <label class="form-check-label" for="exampleRadios3">
                            Нет CRM (в Google таблицы)
                        </label>
                    </div>
            </x-card-body>
            <div class="card-footer">
                <div class="d-grid gap-2">
                    <button wire:click="nextStep" color="primary">
                        Далее
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div style="{{$crm_type == 'AmoCrm' ? '' : 'display: none'}}">
        <div class="col-12 col-md-8 mx-auto" style="height: 500px; {{$step == 2 ? '' : 'display: none'}}">

            <!-- Main content -->
            <div class="card shadow mt-3  border-0" style="height: 400px">
                <x-card-header>
                    <h4 class="m-0">
                        Настройка подключения AMO
                    </h4>

                </x-card-header>


                <x-card-body>
                    <x-form-item>
                        <label required> Секретный ключ </label>
                        <input class="form-control" type="text" wire:model.live="clientSecret"  name="clientSecret" />
                    </x-form-item>

                    <x-form-item>
                        <label required> Id интеграции </label>
                        <input class="form-control" type="text" wire:model.live="clientIdAmo" name="clientIdAmo" />
                    </x-form-item>

                    <div class="d-grid gap-2 mb-2">
                        <x-button color="primary">
                            Добавить
                        </x-button>
                    </div>
                    <div>
                        Ссылка на перенаправление: https://glushko-sok.ru/AmoCrm/redirection/{{$id}}
                    </div>
                    <div>
                        Название интеграции: kupr
                    </div>
                    <div>
                        Описание: интеграция лидов из glushko-sok
                    </div>
                </x-card-body>
                <div class="card-footer">
                    <div class="d-grid gap-2">
                        <button wire:click="nextStep" color="primary">
                            Далее
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="{{$crm_type == 'Commo' ? '' : 'display: none'}}">
        <div class="col-12 col-md-8 mx-auto" style="height: 500px; {{$step == 2  ? '' : 'display: none'}}">

            <!-- Main content -->
            <div class="card shadow mt-3  border-0" style="height: 400px">
                <x-card-header>
                    <h4 class="m-0">
                        Настройка подключения Commo
                    </h4>

                </x-card-header>


                <x-card-body>
                    <x-form-item>
                        <label required> Секретный ключ </label>
                        <input class="form-control" type="text" wire:model.live="clientSecret"  name="clientSecret" />
                    </x-form-item>

                    <x-form-item>
                        <label required> Id интеграции </label>
                        <input class="form-control" type="text" wire:model.live="clientIdAmo" name="clientIdAmo" />
                    </x-form-item>

                    <div>
                        Ссылка на перенаправление: https://glushko-sok.ru/AmoCrm/redirection/{{$id}}
                    </div>
                    <div>
                        Название интеграции: kupr
                    </div>
                    <div>
                        Описание: интеграция лидов из glushko-sok
                    </div>
                </x-card-body>
                <div class="card-footer">
                    <div class="d-grid gap-2">
                        <button wire:click="nextStep" color="primary">
                            Далее
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8 mx-auto" style="height: 500px; {{$step == 3  ? '' : 'display: none'}}">
            <!-- Main content -->
            <div class="card shadow mt-3  border-0" style="height: 400px">
                <x-card-header>
                    <h4 class="m-0">
                        Настройка подключения Alister
                    </h4>

                </x-card-header>
                <x-card-body>
                    {{$clientIdAmo ?? ''}}

                    @script
                    <script
                            class="kommo_oauth"
                            charset="utf-8"
                            data-client-id="{{$clientIdAmo ?? ''}}"
                            data-title="Установить связь"
                            data-compact="false"
                            data-class-name="className"
                            data-theme="light"
                            data-locale="en"
                            data-state="state"
                            data-error-callback="functionName"
                            data-mode="popup"
                            src="https://www.kommo.com/auth/button.js"
                    >

                    </script>
                    @endscript

                </x-card-body>
                <div class="card-footer">
                    <div class="d-grid gap-2">
                        <button wire:click="nextStep" color="primary">
                            Далее
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
