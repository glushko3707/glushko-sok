<div class="container-fluid">

    <div class="row mt-3" style="justify-content: center">
        <div class="row" style="padding: 30px">
            <div class="d-flex flex-wrap p-0">
                <div class="small-box glushkoSmallBox">
                        <div class="inner">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <p> Регистраций </p>
                                </div>
                                <div>
                                    <button wire:click="save"  class="btn-sm btn-outline-primary">Скачать</button>
                                </div>
                            </div>
                            <h1> {{$statistica['userReg'] ?? ''}} </h1>

                        </div>

                    </div>
                <div class="small-box glushkoSmallBox">
                        <div class="inner">
                            <p> Доходимость до веба </p>
                            <h1> {{$statistica['visitConversion'] ?? ''}}% </h1>
                        </div>
                    </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Учестников веба </p>
                        <h1> {{$statistica['visitWeb'] ?? ''}} </h1>
                    </div>
                </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Конверсия веба </p>
                        <h1> {{$statistica['paymentConversion'] ?? ''}}% </h1>
                    </div>
                </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Оплат </p>
                        <h1> {{$statistica['paymentsWeb'] ?? ''}} </h1>
                    </div>
                </div>
            </div>
                <div class="d-flex flex-wrap p-0">
                    <div class="small-box glushkoSmallBox">
                        <div class="inner">
                            <p> Подписок в канал </p>
                            <h1> {{$statistica['channelUser'] ?? ''}} </h1>
                        </div>
                    </div>
                    <div class="small-box glushkoSmallBox">
                        <div class="inner">
                            <p> Конверсия в подписку </p>
                            <h1> {{$statistica['channelUserConversion'] ?? ''}}% </h1>
                        </div>
                    </div>
                </div>
                <div class="card p-0">
                    <div class="card-header pl-2 pr-2 pt-0 pb-0">
                        <div class="row">
                            <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                                <div>
                                    <div class="form-inline float-right mr-3"  style="padding: 14px">
                                        <label  class="mr-3">От</label>
                                        <input class="form-control mr-3" wire:model.live="date_from" type="date" name="date_from">
                                        <label  class="mr-3">До</label>
                                        <input class="form-control mr-3" wire:model.live="date_to" type="date"  name="date_to">
                                        <div class="ml-3">
                                            <select wire:model.live="warm" class="form-control input-opacity" name="warm" style="background-color: rgba(0, 0, 0, 0)">
                                                <option value="all">Все статусы</option>
                                                <option value="open">Открыта оплата</option>
                                                <option value="succeeded">Оплачено</option>
                                                <option value="cold">Не запустился</option>
                                                <option value="hot">Горячий</option>
                                                <option value="normal">Холодный</option>
                                            </select>
                                        </div>

                                        <div class="ml-3">
                                            <input type="text" class="mr-2 form-control"  wire:model.live="utm_medium" placeholder="поиск" />
                                        </div>

                                        <div class="ml-3">
                                            <select wire:model.live="utm_source" class="form-control input-opacity" name="utm_source" style="background-color: rgba(0, 0, 0, 0)">
                                                <option value="all">Все источники</option>
                                                @foreach($utm_source_all as $utm_sources)
                                                    <option value="{{$utm_sources}}">{{$utm_sources}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                            <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                                <thead>

                                    <tr>
                                        <th>Дата</th>
                                        <th>Регистрации</th>
                                        <th>Конверсия в веб</th>
                                        <th>Участники веб</th>
                                        <th>Конверсия в оплату</th>
                                        <th>Оплаты</th>
                                        <th>Подписок</th>
                                        <th>Конверсия в пдп</th>
                                    </tr>

                                </thead>
                                <tbody>
                                @foreach($dayWebStatistice as $key => $day)
                                    <tr >
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$key}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$day['userReg']}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$day['visitConversion']}}%
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$day['visitWeb']}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$day['paymentConversion']}}%
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$day['paymentsWeb']}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$day['channelUser']}}
                                                </div>
                                            </div>
                                        </th>
                                        <th style="font-weight: normal">
                                            <div class="d-flex justify-content-start">
                                                <div>
                                                    {{$day['channelUserConversion']}}%
                                                </div>
                                            </div>
                                        </th>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>


        </div>
         </div>
    </div>
