<div class="container-fluid">

    <div class="row mt-3" style="justify-content: center">

        <div class="row" style="width: 90%;padding: 0">
            <div class="col-2 p-0 mb-2">
                <a class="btn btn-outline-primary" href="{{route('getWebinarLint.store',$id)}}">
                    Список вебинаров
                </a>
            </div>
            <div class="col-12" style="justify-content: start;display: flex; padding: 0">
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <div class="d-flex justify-content-between">
                            <div>
                                <p> Участников </p>
                            </div>
                            <div>
                                <button wire:click="save"  class="btn-sm btn-outline-primary">Скачать</button>
                            </div>
                        </div>
                        <h1> {{$statistica['count']}} </h1>
                    </div>
                </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Оплаты </p>
                        <h1> {{$statistica['payment']}} </h1>
                    </div>
                </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Конверсия веба </p>
                        <h1> {{$statistica['PercentPayment']}}% </h1>
                    </div>
                </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Горячих </p>
                        <h1> {{$statistica['hot']}} </h1>
                    </div>
                </div>
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <p> Процент горячих </p>
                        <h1> {{$statistica['hotPercent']}}% </h1>
                    </div>
                </div>
            </div>



        </div>
        <div class="card" style="width: 90%;padding: 0px">


            <div class="card-tools">
                <div class="row">
                    <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                        <div>
                                <div class="form-inline float-right mr-3"  style="padding: 14px">
                                    <label  class="mr-3">От</label>
                                    <input class="form-control mr-3" wire:model.live="date_from" type="date" value="{{request('date_from')}}" name="date_from">
                                    <label  class="mr-3">До</label>
                                    <input class="form-control mr-3" wire:model.live="date_to" type="date" value="{{request('date_to')}}" name="date_to">
                                    <div class="ml-3">
                                        <select wire:model.live="warm" class="form-control input-opacity" name="warm" style="background-color: rgba(0, 0, 0, 0)">
                                            <option value="all">Все статусы</option>
                                            <option value="open">Открыта оплата</option>
                                            <option value="succeeded">Оплачено</option>
                                            <option value="cold">Не запустился</option>
                                            <option value="hot">Горячий</option>
                                            <option value="normal">Холодный</option>
                                        </select>
                                    </div>

                                    <div class="ml-3">
                                        <select wire:model.live="utm_source" class="form-control input-opacity" name="utm_source" style="background-color: rgba(0, 0, 0, 0)">
                                            <option value="all">Все источники</option>
                                        @foreach($utm_source_all as $utm_sources)
                                            <option value="{{$utm_sources}}">{{$utm_sources}}</option>
                                            @endforeach
                                        </select>
                                        <a href="{{route('WebinarStatUpdateUtm.update',$id)}}?date_from={{$date_from}}"  class="btn-sm btn-outline-primary">Обновить UTM</a>
                                    </div>

                                </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- /.card-header -->
            <div class="card" style="margin-bottom: 0;box-shadow: unset">
                <div class="card-body table-responsive p-0" style="max-height: 450px;">
                    <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                        <thead>
                            <tr>
                                <th>Дата</th>
                                <th>Номер телефона</th>
                                <th>Источник</th>
                                <th>Имя</th>
                                <th>Время на вебе</th>
                                <th>Кнопки</th>
                                <th>Статус</th>

                            </tr>
                        </thead>
                        <tbody>
                        @foreach($webinarUser as $key => $day)
                            <tr >

                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            {{$day['date']}} ₽
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            {{$day['phone']}}
                                        </div>

                                    </div>                                                        </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            {{$day['utm_source']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            {{$day['username']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            {{$day['timeWeb']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            {{$day['buttons']}}
                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            <form action="{{route('webinarStat.update',$id)}}" method="POST">
                                                @method('PATCH')
                                                @csrf
                                                <input type="hidden" name="id" value="{{$day['id']}}">
                                                <select onchange="this.form.submit()" class="form-control input-opacity" name="status" style="background-color: rgba(0, 0, 0, 0)">
                                                    <option {{($day['warm'] == 'open') ? 'selected="selected"' : '' }} value="open">Открыта оплата</option>
                                                    <option {{($day['warm'] == 'succeeded' || $day['warm'] == 'по карте') ? 'selected="selected"' : '' }} value="succeeded">Оплачено</option>
                                                    <option {{($day['warm'] == 'cold') ? 'selected="selected"' : '' }} value="cold">Не запустился</option>
                                                    <option {{($day['warm'] == 'hot') ? 'selected="selected"' : '' }} value="hot">Горячий</option>
                                                    <option {{($day['warm'] == 'normal') ? 'selected="selected"' : '' }} value="normal">Холодный</option>

                                                </select>

                                            </form>
                                        </div>
                                    </div>
                                </th>




                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>
