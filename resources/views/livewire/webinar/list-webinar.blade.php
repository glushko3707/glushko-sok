<div class="container-fluid">

    <div class="row mt-3" style="justify-content: center">

        <div class="row" style="width: 90%;padding: 0">
            <div class="col-12" style="justify-content: start;display: flex; padding: 0">
                <div class="small-box glushkoSmallBox">
                    <div class="inner">
                        <div class="d-flex justify-content-between">
                            <div>
                                <p> Вебинаров </p>
                            </div>
                            <div>
                                <button wire:click="save"  class="btn-sm btn-outline-primary">Скачать</button>
                            </div>
                        </div>
                        <h1> {{$statistica['count'] ?? ''}} </h1>
                    </div>
                </div>
            </div>



        </div>
        <div class="card" style="width: 90%;padding: 0px">


            <div class="card-tools">
                <div class="row">
                    <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                        <div>
                            <div class="form-inline float-right mr-3"  style="padding: 14px">
                                <label  class="mr-3">От</label>
                                <input class="form-control mr-3" wire:model.live="date_from" type="date" value="{{request('date_from')}}" name="date_from">
                                <label  class="mr-3">До</label>
                                <input class="form-control mr-3" wire:model.live="date_to" type="date" value="{{request('date_to')}}" name="date_to">
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <!-- /.card-header -->
            <div class="card" style="margin-bottom: 0;box-shadow: unset">
                <div class="card-body table-responsive p-0" style="max-height: 450px;">
                    <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                        <thead>
                        <tr>
                            <th>Дата</th>
                            <th>ID web</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($webinars as $key => $day)
                            <tr >

                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                             {{ Str::after(Str::before($day['webinarId'], 'T'), '*')  ?? now()->format('Y-m-d')}}  {{ \Carbon\Carbon::parse(Str::after($day['webinarId'], 'T'))->format('H:i')  ?? now()->format('Y-m-d')}}

                                        </div>
                                    </div>
                                </th>
                                <th style="font-weight: normal">
                                    <div class="d-flex justify-content-start">
                                        <div>
                                            <a href="{{route('webinar.update',['id' => $id,'webinar_id' => $day['webinarId']])}}">
                                                {{$day['webinarId']}}
                                            </a>
                                        </div>
                                    </div>                                                        </th>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>
