<div id="container">
    <h1>Текстовый редактор</h1>
    <textarea wire:model.live="text" class="editor mt-2" id="editor" name="text"></textarea>
    @if ($exit)
        <div class="row w-100">
            @foreach($exit as $index => $text)
            <div class="col-4">
                <textarea style="height: 400px;border-radius: 5px" class="w-100 ml-3 mr-3 mt-3 p-3 text-sm" id="exit{{ $index }}" name="exit">{{$text}}</textarea>
                <button class="copy-btn btn btn-primary ml-3 mr-3 w-100" data-target="exit{{ $index }}">Копировать</button>
            </div>
        @endforeach
        </div>
    @endif
</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        // Вместо прямой привязки, используем делегирование события
        document.addEventListener('click', function(e) {
            // Проверяем, что была нажата кнопка с необходимым классом
            if (e.target.classList.contains('copy-btn')) {
                var targetId = e.target.getAttribute('data-target');
                var textarea = document.getElementById(targetId);

                // Если textarea существует, выполняем копирование
                if (textarea) {
                    textarea.select(); // Выделяем текст в textarea
                    var successful = document.execCommand('copy'); // Пытаемся копировать текст в буфер обмена

                    // Если операция копирования завершилась успешно
                    if (successful) {
                        let originalText = e.target.innerText; // Сохраняем оригинальный текст кнопки
                        e.target.innerText = 'Скопировано'; // Меняем текст кнопки на "Скопировано"

                        // Возвращаем исходный текст кнопки через 2 секунды
                        setTimeout(() => {
                            e.target.innerText = originalText;
                        }, 2000); // 2000 миллисекунд = 2 секунды
                    }
                }
            }
        });
    });
</script>
