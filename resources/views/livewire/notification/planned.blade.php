<div class="container" >
    <div class="row">

        <div class="col-lg-3 col-6">
            <!-- Виджет 1: Остаток бюджета -->
            <div class="small-box">
                <div class="inner">
                    <p> Участников сегодня </p>
                    <div class="d-flex justify-between">
                        <div>
                            <h1> {{$stat ['countPeapleToday']}} </h1>
                        </div>
                        <div>
                            <a style="margin-top: 7px" href="{{route('addNotificationWeb.index',$id)}}" class="btn btn-outline-secondary">
                                Добавить
                            </a>
                        </div>

                    </div>

                </div>


            </div>
        </div>
        <div class="col-lg-3 col-6">
            <!-- Виджет 1: Остаток бюджета -->
            <div class="small-box" style="height: 116px">
                <div class="inner">
                    <div class="d-flex justify-content-between">
                        <div>
                            <p> Лидов на вебинар </p>
                        </div>
                        <div>
                            <a href="{{route('downloadShedule',$id)}}"> скачать </a>
                            <a href="{{route('addWebLead',$id)}}"> удалить </a>
                        </div>
                    </div>
                    <div class="row">
                        <div style="display: flex;flex-wrap: wrap; justify-content: space-between">
                            <h1> {{$stat ['countPeapleCanToday']}} </h1>
                            @csrf
                            <div class="icon">
                                <i class="ion ion-speedometer" style="position: relative;top:-10px;right: 0 "></i>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

        </div>
        <div class="col-lg-3 col-6">
            <!-- Виджет 1: Остаток бюджета -->
            <div class="small-box">
                <div class="inner">
                    <p class="mb-4"> Дата вебинара </p>
                    <form id="changeDateWeb"  action="{{route('changeDateWeb',$id)}}" method="post">
                        @csrf
                        <div class="input-group" style="margin-bottom: 5px">
                            <input style="background-color: rgba(0, 0, 0, 0);border-color: #c9c9c9;border-top-left-radius: 6px; border-bottom-left-radius: 6px;" type="date" name="changeDateWeb" id="changeDateWeb" value="{{$stat['dateWeb'] ?? ''}}">
                            <span class="input-group-append">
                                                   <button style="border-color: #c9c9c9" onClick="document.getElementById('changeDateWeb').submit();" class="btn btn-outline-secondary" type="submit"> Сохранить </button>
                                                </span>
                        </div>
                    </form>
                </div>


            </div>

        </div>

        {{--                        <div class="col-lg-3 col-6">--}}
        {{--                            <!-- Виджет 1: Остаток бюджета -->--}}
        {{--                            <a href="{{ route('saveFile.store',$id) }}" class="btn btn-outline-primary">Скачать файл</a>--}}

        {{--                        </div>--}}

    </div>
    <div class="card-tools mt-5">
        <div class="row">
            <div class="col">
                <div class="form-inline float-left mr-3"  style="padding: 14px">
                    <label  class="mr-3">От</label>
                    <input wire:model.live="date_from" style="background-color: rgba(0, 0, 0, 0);border-color: #c9c9c9" class="form-control mr-3 " type="date" value="{{request('date_from')}}" name="date_from">
                    <label  class="mr-3">До</label>

                    <input wire:model.live="date_to" style="background-color: rgba(0, 0, 0, 0);border-color: #c9c9c9"  class="form-control mr-3" type="date" value="{{request('date_to')}}" name="date_to">

                    <div class="mr-3">
                        <select class="form-control input-opacity" wire:model.live="search" name="search"  style="background-color: rgba(0, 0, 0, 0)">
                            <option selected disabled value="all">Все уведомления</option>
                            @foreach($contentType as $content)
                                <option value="{{$content}}">{{$content}}</option>
                            @endforeach
                        </select>
                        <input wire:model.live="searchUtm" style="background-color: rgba(0, 0, 0, 0);border-color: #c9c9c9"  class="form-control mr-3" type="text"  name="searchUtm">

                    </div>

                    {{--                        <input type="text" class="mr-2 form-control"  wire:model.live="search" placeholder="поиск" />--}}
                    <textarea class="textarea-input" wire:model.live="inputText" rows="1" cols="20" placeholder="вставьте номера"></textarea>

                    <button wire:click="downloadPlannedPhones" type="submit" class="btn ml-3 btn-outline-primary ">Скачать</button>
                    <button onclick="confirmDeletion()" wire:click="deleteNotification" class="btn  ml-3  btn-outline-danger">Удалить все</button>
                </div>
            </div>
            {{--                                            </div>--}}

        </div>
    </div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Время</th>
            <th scope="col">Тип</th>
            <th scope="col">Этап</th>
            <th scope="col">Телефон</th>
            <th scope="col">Действия</th>

        </tr>
        </thead>
        <tbody>
        @foreach($webNotificationPlaned as $step)
            <tr>
                <td>{{$step['timeSend']}}</td>
                <td>{{$step['type']}}</td>
                <td>{{$step['step_id']}}</td>
                <td>{{$step['phone']}}</td>
                <td>
                    <div class="d-flex">
                        <div class="mr-2">
                            <form action="{{ route('plannedNotificztion.action', $step['id']) }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" class="btn btn-outline-primary btn-sm">Выполнить</button>
                            </form>
                        </div>
                        <div>
                            <button wire:click="deleteNotification({{$step['id']}})" class="btn btn-outline-danger btn-sm">Delete</button>
                        </div>
                    </div>
                </td>


            </tr>
        @endforeach
        </tbody>
    </table>
    <script>
        function confirmDeletion() {
            if(confirm("Вы действительно хотите удалить?")) {
                @this.call('deleteNotification');
            }
        }
    </script>

</div>