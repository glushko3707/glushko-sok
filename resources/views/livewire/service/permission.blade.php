<div class="container mt-5">
    <div class="table-responsive">
        <table class="table">
            <tbody>
            @foreach($permissionUser as $user)
                <tr>
                    <th style="font-weight: 400">
                        {{$user->user->name}}
                    </th>
                    <th style="font-weight: 400">
                        {{$user->user->email}}
                    </th>
                    <th>
                        <button wire:click="deleteUser({{$user->user->id}})" class="btn btn-sm btn-outline-danger">
                            Удалить
                        </button>
                    </th>
                </tr>
            @endforeach
            <tr>
                <th style="font-weight: 400">
                    Добавить доступ
                </th>
                <th colspan="2">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="input-group">
                                <input
                                        class="form-control"
                                        type="email"
                                        wire:model="email"
                                        placeholder="Введите email"
                                >
                                <button
                                        wire:click="addPermisson"
                                        type="button"
                                        class="btn btn-outline-primary"
                                >
                                    Добавить
                                </button>
                            </div>
                        </div>
                    </div>
                </th>
            </tr>
            </tbody>
        </table>
    </div>
</div>
