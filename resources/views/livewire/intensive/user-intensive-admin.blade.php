<div>


    <div class="row">
        <div class="col-lg-2 col-6">
            <div class="small-box">
                <div class="inner">
                    <p> Участники </p>
                    <h1> {{$stat ['count'] ?? ''}} </h1>
                </div>
                <div class="icon">
                    <i class="ion ion-speedometer"></i>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex">
        <div class="mr-2">
            <a href="{{route('userIndex',['intensive_id' =>  $intensive_id . 'kpr32', 'id' => $id])}}" type="button"  class="btn period btn-outline-primary">Для участников</a>
        </div>

{{--        <div class="mr-2">--}}
{{--            <a href="{{route('adminIndexUpdate',['intensive_id' => $intensive_id, 'id' => $id])}}" type="button"  class="btn period btn-outline-primary">Обновить</a>--}}
{{--        </div>--}}

        <div class="mr-2">
            <a href="{{route('distributedByFaculty',['intensive_id' => $intensive_id, 'id' => $id])}}" type="button"  class="btn period btn-outline-primary">По факультетам</a>
        </div>
        <div class="mr-2">
            <button data-bs-toggle="modal" data-bs-target="#exampleModal"  class="btn period btn-outline-primary">Добавить участника</button>
        </div>
        <div class="mr-2">
            <button data-bs-toggle="modal" data-bs-target="#notification"  class="btn period btn-outline-primary">Добавить уведомления</button>
        </div>
        <div class="mr-2">
            <button wire:click="save" type="button"  class="btn period btn-outline-primary">Скачать</button>
        </div>




    </div>
    <div>
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-3" style="justify-content: center">


                    <div class="card" style="padding: 0">
                        <div class="card-tools">
                            <div class="d-flex justify-content-between" style="padding: 10px">
                                <div style="align-items: center;" class="d-flex">
                                    <div>
                                        <ul class="nav nav-pills">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#detal" data-toggle="tab">Список участников</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div>
                                    <div class="form-inline float-right">
                                        <input wire:model.live="search" type="search" class="mr-2 form-control" name="search" placeholder="поиск" />
                                        <div class="mr-3">
                                            <select wire:model.live="status" class="form-control input-opacity" name="status" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                                <option  value="all">Все статусы</option>
                                                <option  value="noChat">Не вступили в чат</option>
                                                <option  value="noMeet">Не записались на встречу</option>
                                                <option  value="meetToday">Встречи сегодня</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" id="detal">
                                <div class="card" style="margin-bottom: 0;box-shadow: unset">
                                    <div class="card-body table-responsive p-0" style="max-height: 800px;">
                                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                                            <thead>
                                            <tr>
                                                <th style="width: 100px">Имя</th>
                                                <th style="width: 50px">Номер тел.</th>
                                                <th style="width: 50px">TG</th>
                                                <th style="width: 120px">Факультет</th>
                                                <th class="checkboxFont" style="width: 60px">Чат 1</th>
                                                <th class="checkboxFont" style="width: 60px">Чат 2</th>
                                                <th style="width: 200px">Комментарий</th>
                                                <th class="checkboxFont" style="width: 50px">Урок 1</th>
                                                <th class="checkboxFont" style="width: 50px">Урок 2</th>
                                                <th class="checkboxFont" style="width: 50px">Домашка 1</th>
                                                <th class="checkboxFont" style="width: 50px">Домашка 2</th>
                                                <th class="checkboxFont" style="width: 50px">Встреча</th>
                                                <th style="width: 50px">Отзыв 1</th>
                                                <th style="width: 50px">Отзыв 2</th>
                                                <th style="width: 50px">Очки</th>
                                                <th style="width: 50px">Telegram Id</th>
                                                <th style="width: 50px">Дата и время встречи</th>
                                                <th style="width: 50px">Приглашение в TG</th>
                                                <th style="width: 50px">Источник</th>

                                                {{--                                                <th style="width: 50px">Действия</th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>



                                            @foreach($intensiveUser as $index => $lead)


                                                <tr>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div class="d-flex">

                                                                <div>
                                                                    <input wire:model.lazy="intensiveUser.{{ $index }}.name" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 120px" name="name{{ $index }}">
                                                                </div>
                                                                <div>
                                                                    @if(isset($lead['anketa']))
                                                                        <div class="popup-container d-flex align-items: center;">
                                                                            <span class="popup-trigger" style="font-size:20px;cursor: pointer"> <ion-icon name="document-lock-outline"></ion-icon> </span>
                                                                            <div class="popup-content text-xs" style="line-height: 1.5">
                                                                                @foreach($lead['anketa'] as $key => $value)
                                                                                    {{$key+1}}) {{$value}} <br>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div class="d-flex justify-start">
                                                                <div class="mr-1 d-flex align-items-center">
                                                                    <a target="_blank" href="https://api.whatsapp.com/send?phone={{$lead['phone']}}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#008000" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                                                            <path d="M13.601 2.326A7.85 7.85 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.9 7.9 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.9 7.9 0 0 0 13.6 2.326zM7.994 14.521a6.6 6.6 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.56 6.56 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592m3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.73.73 0 0 0-.529.247c-.182.198-.691.677-.691 1.654s.71 1.916.81 2.049c.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232"/>
                                                                        </svg>
                                                                    </a>
                                                                </div>
                                                                <div>
                                                                    <input wire:model.lazy="intensiveUser.{{ $index }}.phone" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 100px" name="phone{{ $index }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div class="d-flex justify-start">
                                                                <div class="mr-1 d-flex align-items-center">
                                                                    @if(isset($lead['telegram_username']))
                                                                    <a target="_blank" href="https://t.me/{{$lead['telegram_username']}}">
                                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                                                                            <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09"/>
                                                                        </svg>
                                                                    </a>
                                                                    @endif
                                                                </div>
                                                                <div>
                                                                    <input wire:model.lazy="intensiveUser.{{ $index }}.telegram_username" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 100px" name="telegram_username{{ $index }}">
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                <select wire:model.lazy="intensiveUser.{{ $index }}.faculty_name" class="input-opacity" style="
                                                                background-color: rgba(0, 0, 0, 0);
                                                                padding: 0;
                                                                width: 150px" name="faculty_name{{ $index }}">
                                                                    <option></option>
                                                                    @foreach($faculty_names as $facultet)
                                                                        <option value="{{$facultet}}">{{$facultet}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-start">
                                                            <div class="d-flex justify-content-center" style="width: 50px">
                                                                <input type="checkbox" @if($lead['chat_main_active']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.chat_main_active" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-start">
                                                            <div class="d-flex justify-content-center"  style="width: 50px">
                                                                <input type="checkbox" @if($lead['chat_second_active']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.chat_second_active" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th class="" style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-start">
                                                            <textarea rows="1" wire:model.lazy="intensiveUser.{{ $index }}.comment_actual" class="input-opacity text-sm custom-textarea" style="line-height: 1.5;background-color: rgba(0, 0, 0, 0);width: 250px" name="comment_actual{{ $index }}"> </textarea>
{{--                                                            <input wire:model.lazy="intensiveUser.{{ $index }}.comment_actual" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 250px" name="comment_actual{{ $index }}">--}}
                                                        </div>

                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <div>
                                                                <input type="checkbox" @if($lead['lesson_1_visit']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.lesson_1_visit" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <div>
                                                                <input type="checkbox" @if($lead['lesson_2_visit']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.lesson_2_visit" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <div>
                                                                <input type="checkbox" @if($lead['homeWork_1']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.homeWork_1" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <div>
                                                                <input type="checkbox" @if($lead['homeWork_2']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.homeWork_2" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <div>
                                                                <input type="checkbox" @if($lead['meet_visit']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.meet_visit" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <textarea row="1" wire:model.lazy="intensiveUser.{{ $index }}.feedback_1" class="input-opacity text-sm custom-textarea" style="line-height: 1.5;background-color: rgba(0, 0, 0, 0);width: 250px" name="feedback_1{{ $index }}"> </textarea>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <textarea row="1" wire:model.lazy="intensiveUser.{{ $index }}.feedback_2" class="input-opacity text-sm custom-textarea" style="line-height: 1.5;background-color: rgba(0, 0, 0, 0);width: 250px" name="feedback_2{{ $index }}"> </textarea>
                                                        </div>
                                                    </th>

                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-center">
                                                            <div>
                                                                <div>
                                                                    <!-- Внимание: предполагается, что у вас есть свойство name в вашем Livewire компоненте -->
                                                                    <input type="number" wire:model.lazy="intensiveUser.{{ $index }}.score" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 90px" name="score{{ $index }}">
                                                                </div>

                                                                {{--                                                            <input data-id="{{$lead['name']}}" onchange="updateLead(this)" style="background-color: rgba(0, 0, 0, 0)" class="input-opacity" type="date" value="{{$lead['dateTask'] ? $lead['dateTask'] : $lead['updated_at']}}" name="dateTask">--}}
                                                            </div>
                                                        </div>
                                                    </th>

                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                <div>
                                                                    <!-- Внимание: предполагается, что у вас есть свойство name в вашем Livewire компоненте -->
                                                                    <input wire:model.lazy="intensiveUser.{{ $index }}.telegram_id" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 120px" name="telegram_id{{ $index }}">
                                                                </div>

                                                                {{--                                                            <input data-id="{{$lead['name']}}" onchange="updateLead(this)" style="background-color: rgba(0, 0, 0, 0)" class="input-opacity" type="date" value="{{$lead['dateTask'] ? $lead['dateTask'] : $lead['updated_at']}}" name="dateTask">--}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                <div>
                                                                    <!-- Внимание: предполагается, что у вас есть свойство name в вашем Livewire компоненте -->
                                                                    <input type="datetime-local" wire:model.lazy="intensiveUser.{{ $index }}.meet_dateTime" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 180px" name="meet_dateTime{{ $index }}">
                                                                </div>

                                                                {{--                                                            <input data-id="{{$lead['name']}}" onchange="updateLead(this)" style="background-color: rgba(0, 0, 0, 0)" class="input-opacity" type="date" value="{{$lead['dateTask'] ? $lead['dateTask'] : $lead['updated_at']}}" name="dateTask">--}}
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            @if(isset($intensiveUsers_not_chat_active[$lead['phone']]))

                                                                <div class="d-flex justify-start">
                                                                    <div class="mr-1 d-flex align-items-center">
                                                                        <a target="_blank" href="https://api.whatsapp.com/send/?phone={{$lead['phone']}}&text=%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82%21+%D0%A1%D0%BF%D0%B0%D1%81%D0%B8%D0%B1%D0%BE+%D0%B7%D0%B0+%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%B8%D0%B5+%D0%B2+%D0%B8%D0%BD%D1%82%D0%B5%D0%BD%D1%81%D0%B8%D0%B2%D0%B5+%D0%AE%D0%BB%D0%B8%D0%B8+%D0%9A%D1%83%D0%BF%D1%80+%D0%BF%D0%BE+Telegram.+%D0%92%D1%81%D1%82%D1%83%D0%BF%D0%B0%D0%B9+%D0%B2+%D1%87%D0%B0%D1%82+https://api.whatsapp.com/send/?phone=79259126938&text=%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82%21+%D0%A1%D0%BF%D0%B0%D1%81%D0%B8%D0%B1%D0%BE+%D0%B7%D0%B0+%D1%83%D1%87%D0%B0%D1%81%D1%82%D0%B8%D0%B5+%D0%B2+%D0%B8%D0%BD%D1%82%D0%B5%D0%BD%D1%81%D0%B8%D0%B2%D0%B5+%D0%AE%D0%BB%D0%B8%D0%B8+%D0%9A%D1%83%D0%BF%D1%80+%D0%BF%D0%BE+Telegram.+%D0%92%D1%81%D1%82%D1%83%D0%BF%D0%B0%D0%B9+%D0%B2+%D1%87%D0%B0%D1%82+{{str_replace('+', '%2B', $intensiveUsers_not_chat_active[$lead['phone']]['link'])}}&type=phone_number&app_absent=0&type=phone_number&app_absent=0">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#008000" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                                                                <path d="M13.601 2.326A7.85 7.85 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.9 7.9 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.9 7.9 0 0 0 13.6 2.326zM7.994 14.521a6.6 6.6 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.56 6.56 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592m3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.73.73 0 0 0-.529.247c-.182.198-.691.677-.691 1.654s.71 1.916.81 2.049c.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232"/>
                                                                            </svg>
                                                                        </a>

                                                                        <a target="_blank" href="https://t.me/+{{$lead['phone']}}">
                                                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                                                                                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09"/>
                                                                            </svg>
                                                                        </a>
                                                                    </div>
                                                                    <div>
                                                                        {{$intensiveUsers_not_chat_active[$lead['phone']]['link']}}
                                                                    </div>

                                                                </div>
                                                            @endif

                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal;vertical-align: middle">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                {{$lead['source'] ?? ''}}
                                                            </div>
                                                        </div>
                                                    </th>



{{--                                                    <th style="font-weight: normal">--}}
{{--                                                        <div class="d-flex justify-content-start">--}}
{{--                                                            <div>--}}
{{--                                                                <div>--}}
{{--                                                                    <a href="{{route('deleteUserStore',$lead['id'])}}"> <i class="fas fa-times ml-2" style="color: red"></i></a>--}}
{{--                                                                </div>--}}

{{--                                                                --}}{{--                                                            <input data-id="{{$lead['name']}}" onchange="updateLead(this)" style="background-color: rgba(0, 0, 0, 0)" class="input-opacity" type="date" value="{{$lead['dateTask'] ? $lead['dateTask'] : $lead['updated_at']}}" name="dateTask">--}}
{{--                                                            </div>--}}
{{--                                                        </div>--}}
{{--                                                    </th>--}}
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <!-- /.card-header -->

                    </div>
                </div>
            </div>
        </section>
    </div>

{{--    Начало попапа--}}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="exampleModalLabel">Добавление участника</h5>
                </div>
                <div class="modal-body">
                    <div>

                        <form action="{{route('addUserStore',['intensive_id' => $intensive_id, 'id' => $id])}}" method="GET">
                            <x-form.input-nolabel  id="telegram_id" class="mt-4" type="text" name="telegram_id"  :value="old('telegram_id', $telegram_id ?? '')" placeholder="Введите telegram_id"/>
                            <x-input-error :messages="$errors->get('telegram_id')" class="mt-1" />

                            <x-form.input-nolabel  id="phone" class="mt-4" type="text" name="phone"  :value="old('phone', $phone ?? '')" placeholder="Введите ваш номер телефона" required/>
                            <x-input-error :messages="$errors->get('phone')" class="mt-1" />

                            <x-form.button data-bs-dismiss="modal" class="btn mt-4 w-100 shine-button" style="" type="submit"> Добавить </x-form.button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="notification" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header ">
                    <h5 class="modal-title" id="exampleModalLabel">Добавление участника</h5>
                </div>
                <div class="modal-body">
                    <div>
                        <form action="{{route('setIntensiveNotification',['intensive_id' => $intensive_id, 'id' => $id])}}" method="GET">
                            <x-form.label-input class="mb mt-4" name="nisha" />
                            <input style="background: transparent;border-radius: 5px;border-color:#bdbdbd;height:55px"  id="timeStart" class="mt-2 w-100" type="time" name="timeStart"  value="18:00" required/>

                            <x-form.input-nolabel list="options"  id="call_id" class="mt-2" type="text" name="campaign_id"  :value="old('campaign_id', $campaign_id ?? '')" placeholder="Введите ID аудио" required />
                            <datalist id="options">
                                <option value="3239001"> 1ый урок </option>
                                <option value="3240258"> 2ой урок </option>
                                <option value="3241470"> Бонусный </option>
                            </datalist>

                            <x-input-error :messages="$errors->get('call_id')" class="mt-1" />

                            <x-form.input-nolabel list="sms"  id="text" class="mt-4" type="text" name="text"  :value="old('text', $text ?? '')" placeholder="Введите текст SMS" required />
                            <x-input-error :messages="$errors->get('phone')" class="mt-1" />
                            <datalist id="sms">
                                <option value="Интенсив с Юлией Купр начинается. ССЫЛКА"> 1ый урок </option>
                                <option value="Второй день интенсива с Юлией Купр: ССЫЛКА"> 2ой урок </option>
                                <option value="Бонусный урок с Юлией Купр: ССЫЛКА"> Бонусный урок </option>
                            </datalist>
                            <x-form.button data-bs-dismiss="modal" class="btn mt-4 w-100 shine-button" style="" type="submit"> Установить </x-form.button>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
{{--    конец попапа--}}

</div>


<script>
    function autoResize(textarea) {
        textarea.style.height = 'auto'; // Сброс высоты
        textarea.style.height = (textarea.scrollHeight) + 'px'; // Установка новой высоты
    }

    function resetHeight(textarea) {
        textarea.style.height = 'auto'; // Сброс высоты
        textarea.rows = 1; // Возврат высоты к одной строке
    }

    document.querySelectorAll('.custom-textarea').forEach(textarea => {
        textarea.addEventListener('input', () => autoResize(textarea));
        textarea.addEventListener('focus', () => autoResize(textarea));
        textarea.addEventListener('blur', () => resetHeight(textarea)); // Обработчик для установки высоты при потере фокуса
    });
</script>



<script>
    document.addEventListener("DOMContentLoaded", function() {
        const popupTriggers = document.querySelectorAll(".popup-trigger");
        const popupContents = document.querySelectorAll(".popup-content");

        popupTriggers.forEach((popupTrigger, index) => {
            popupTrigger.addEventListener("click", function() {
                if (popupContents[index].style.display === "block") {
                    popupContents[index].style.display = "none";
                } else {
                    popupContents.forEach(content => {
                        content.style.display = "none";
                    });
                    popupContents[index].style.display = "block";
                }
            });

            document.addEventListener("click", function(event) {
                if (!popupTrigger.contains(event.target) && !popupContents[index].contains(event.target)) {
                    popupContents[index].style.display = "none";
                }
            });
        });
    });
</script>