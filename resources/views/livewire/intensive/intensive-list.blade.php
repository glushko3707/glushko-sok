<div>
    <div>
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-3" style="justify-content: center">
                    <div class="card" style="padding: 0">
                        <div class="card-tools">
                            <div class="d-flex justify-content-between" style="padding: 10px">
                                <div style="align-items: center;" class="d-flex">
                                    <div>
                                        <ul class="nav nav-pills">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#detal" data-toggle="tab">Лиды</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                <div>
                                    <div class="form-inline float-right">
                                        <input type="text" class="mr-2 form-control"  wire:model.live="searchPhone" placeholder="поиск по телефону" />
                                        <label  class="mr-3">От</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_from" name="date_from">
                                        <label  class="mr-3">До</label>
                                        <input class="form-control mr-3" type="date" wire:model.live="date_to" name="date_to">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" id="detal">
                                <div class="card" style="margin-bottom: 0;box-shadow: unset">
                                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                                            <thead>
                                            <tr>
                                                <th style="width: 100px">Дата начала</th>
                                                <th style="width: 100px">Участников</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($IntensiveList as $intensive)
                                                <tr >
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                <a href="{{route('listSettingIndex',[$intensive->id,$id])}}">
                                                                    {{ \Illuminate\Support\Carbon::parse($intensive['date_intensive'])->format('d.m.y') }}</a>
                                                            </div>
                                                        </div>
                                                    </th>
                                                    <th style="font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                <a href="{{route('IntensiveAdminIndex',[$intensive->id,$id])}}">
                                                                    {{ $intensive->getUsers->count() }}</a>
                                                            </div>
                                                        </div>
                                                    </th>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- /.card-header -->

                    </div>
                </div>
            </div>
        </section>
    </div>



</div>

