<div>
    <div>
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-3" style="justify-content: center">
                    <div class="text-center">
                        <h2>Интенсив с Юлией Купр по Telegram</h2>
                    </div>
                    <div class="card" style="padding: 0">
                        <div class="card-tools">
                            <div class="d-flex flex-column flex-md-row justify-content-between" style="padding: 10px">
                                <div class="d-flex align-items-center mb-2 mb-md-0 d-none d-md-flex">
                                    @if(empty(auth()->user()->telegram_id))
                                    <ul class="nav nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#detal" data-toggle="tab">Список участников</a>
                                        </li>
                                    </ul>
                                    @endif
                                    @if(!empty(auth()->user()->telegram_id))
                                    <div class="d-flex align-items-center">
                                        <input type="checkbox"  wire:model.change="onlyMyFaculties" class="form-check-input mt-0 ml-0 position-relative" style="width: 25px;height: 25px">
                                        <label class="ml-3 mt-1">Только мой факультет</label>
                                    </div>
                                    @endif
                                </div>

                                <div>
                                    <div class="form-inline">
                                        <input wire:model.live="search" type="search" class="mr-2 form-control" name="search" placeholder="поиск" />
                                    </div>
                                </div>



                            </div>                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="detal">
                                <div class="card" style="margin-bottom: 0;box-shadow: unset">
                                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">
                                            <thead>
                                            <tr>
                                                <th style="width: 100px">Имя</th>
                                                <th style="width: 50px;">Номер тел.</th>
                                                <th style="width: 50px">TG</th>
                                                <th style="width: 120px">Факультет</th>
                                                <th class="checkboxFont background-leson1" style="width: 50px;text-align: center">Урок 1</th>
                                                <th class="checkboxFont background-leson1" style="width: 50px;text-align: center">Урок 2</th>
                                                <th class="checkboxFont background-leson1" style="width: 50px;text-align: center">Урок 3</th>
                                                <th class="checkboxFont background-leson1" style="width: 50px;text-align: center">Урок 4</th>
                                                <th class="checkboxFont background-homework" style="width: 50px;text-align: center">Домашка 1</th>
                                                <th class="checkboxFont background-homework" style="width: 50px;text-align: center">Домашка 2</th>
                                                <th style="display: none;width: 50px;text-align: center">Очки</th>
                                            </tr>
                                            </thead>
                                            <tbody>



                                            @foreach($intensiveUser as $index => $lead)
                                            <tr>
                                                <th style="font-weight: normal">
                                                    <div class="d-flex justify-content-start">
                                                        <div>
                                                            <div>
                                                                <input wire:model.lazy="intensiveUser.{{ $index }}.name" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 120px" name="name{{ $index }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th style="font-weight: normal">
                                                    <div class="d-flex justify-content-start">
                                                        <div class="d-flex justify-start">
                                                        <div class="mr-1 d-flex align-items-center">
                                                            <a target="_blank" href="https://api.whatsapp.com/send?phone={{$lead['phone']}}">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#008000" class="bi bi-whatsapp" viewBox="0 0 16 16">
                                                                    <path d="M13.601 2.326A7.85 7.85 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.9 7.9 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.9 7.9 0 0 0 13.6 2.326zM7.994 14.521a6.6 6.6 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.56 6.56 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592m3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.73.73 0 0 0-.529.247c-.182.198-.691.677-.691 1.654s.71 1.916.81 2.049c.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232"/>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                        <div>
                                                            <input wire:model.lazy="intensiveUser.{{ $index }}.phone" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 100px" name="phone{{ $index }}">
                                                        </div>
                                                        </div>
                                                    </div>
                                                </th>
                                                <th style="font-weight: normal">
                                                    <div class="d-flex justify-content-start">
                                                        <div class="d-flex justify-start">
                                                            <div class="mr-1 d-flex align-items-center">
                                                                <a href="https://t.me/{{$lead['telegram_username']}}">
                                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telegram" viewBox="0 0 16 16">
                                                                        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0M8.287 5.906q-1.168.486-4.666 2.01-.567.225-.595.442c-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294q.39.01.868-.32 3.269-2.206 3.374-2.23c.05-.012.12-.026.166.016s.042.12.037.141c-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8 8 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629q.14.092.27.187c.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.4 1.4 0 0 0-.013-.315.34.34 0 0 0-.114-.217.53.53 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09"/>
                                                                    </svg>
                                                                </a>
                                                            </div>
                                                             <div>
                                                                 <input wire:model.lazy="intensiveUser.{{ $index }}.telegram_username" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 100px" name="telegram_username{{ $index }}">
                                                             </div>

                                                        </div>
                                                    </div>
                                                </th>
                                                <th style="font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-start">
                                                            <div>
                                                                <select wire:model.lazy="intensiveUser.{{ $index }}.faculty_name" class="input-opacity" style="
                                                                background-color: rgba(0, 0, 0, 0);
                                                                padding: 0;
                                                                width: 150px" name="faculty_name{{ $index }}">
                                                                    <option></option>
                                                                    @foreach($faculty_names as $facultet)
                                                                        <option value="{{$facultet}}">{{$facultet}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                    </div>
                                                </th>
                                                <th class="background-leson1"  style="font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-center">
                                                        <div class="d-flex">
                                                            <input type="checkbox" @if($lead['lesson_1_visit']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.lesson_1_visit" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="background-leson1" style="font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-center">
                                                        <div class="d-flex">
                                                            <input type="checkbox" @if($lead['lesson_2_visit']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.lesson_2_visit" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="background-leson1" style="font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-center">
                                                        <div class="d-flex">
                                                            <input type="checkbox"  class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="background-leson1" style="font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-center">
                                                        <div class="d-flex">
                                                            <input type="checkbox"  class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                        </div>
                                                    </div>
                                                </th>
                                                <th class="background-homework" style="font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-center">
                                                        <div class="d-flex">
                                                            <input type="checkbox" @if($lead['homeWork_1']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.homeWork_1" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                        </div>
                                                    </div>
                                                </th>

                                                <th class="background-homework" style="font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-center">
                                                        <div class="d-flex">
                                                            <input type="checkbox" @if($lead['homeWork_2']) checked @endif wire:model.lazy="intensiveUser.{{ $index }}.homeWork_2" class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px">
                                                        </div>
                                                    </div>
                                                </th>
                                                <th style="display: none;font-weight: normal;vertical-align: middle">
                                                    <div class="d-flex justify-content-center">
                                                        <div>
                                                            <div>
                                                                <!-- Внимание: предполагается, что у вас есть свойство name в вашем Livewire компоненте -->
                                                                <input type="number" wire:model.lazy="intensiveUser.{{ $index }}.score" class="input-opacity" style="background-color: rgba(0, 0, 0, 0);width: 90px" name="score{{ $index }}">
                                                            </div>

                                                            {{--                                                            <input data-id="{{$lead['name']}}" onchange="updateLead(this)" style="background-color: rgba(0, 0, 0, 0)" class="input-opacity" type="date" value="{{$lead['dateTask'] ? $lead['dateTask'] : $lead['updated_at']}}" name="dateTask">--}}
                                                        </div>
                                                    </div>
                                                </th>

                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- /.card-header -->

                    </div>
                </div>
                @auth


                @else
                    <div class="d-flex justify-content-center">
                        <script async src="https://telegram.org/js/telegram-widget.js?22" data-telegram-login="magic_kupr_bot" data-size="large" data-userpic="true" data-auth-url="https://glushko-sok.ru/register/telegram" data-request-access="write"></script>
                    </div>
                @endauth

            </div>
        </section>
    </div>
</div>
