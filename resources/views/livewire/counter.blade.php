<div>
    <h1>{{ $count }}</h1>

    <input type="text" class="mr-2 form-control" wire:model.lazy.debounce.150ms="count" />

    <button wire:click="increments">+</button>

    <button wire:click="decrement">-</button>
</div>