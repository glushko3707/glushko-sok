<div class="container-fluid">

    <div class="row mt-3" style="justify-content: center">

        <div class="row" style="width: 90%;padding: 0">
            <div class="col-lg-2 col-6">
                <div class="small-box">
                    <div class="inner">
                        <p> Оплаты </p>
                        <h1> {{$paymentRequestCount}} </h1>
                    </div>
                    <div class="icon">
                        <i class="ion ion-speedometer"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-6 mr-5">
                <!-- Виджет 1: Остаток бюджета -->
                <button wire:click="save" type="button"  class="btn period btn-outline-primary">Скачать файл</button>
                <a href="{{route('viewAdd.index',$id)}}" class="btn btn-outline-primary">Добавить</a>
            </div>
        </div>


        <div class="card mt-5" style="width: 90%;padding: 0">

            <div class="card-tools">
                <div class="row">
                    <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                        <div class="form-inline">

                            <div class="btn-group" style="border-radius: 15px">
                                <div class="btn-group">

                                    <div class="card-header">
                                        <ul class="nav nav-pills ml-auto">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#detal" data-toggle="tab">Оплаты</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#day" data-toggle="tab">По дням</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div>
                            <form action="{{route('paymentController.index',$id,request('period'))}}" id="filterDate" method="GET">
                                <div class="form-inline float-right mr-3"  style="padding: 14px">

                                    <div class="mr-3">
                                        <select class="form-control input-opacity" wire:model.live="product" name="status"  style="background-color: rgba(0, 0, 0, 0);max-width: 200px">
                                            <option  value="all">Все продукты</option>
                                            @foreach($products as $product)
                                                <option  value="{{$product}}">{{$product}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="mr-3">
                                        <select class="form-control input-opacity" wire:model.live="status" name="status"  style="background-color: rgba(0, 0, 0, 0)">
                                            <option value="all">Все статусы</option>
                                            <option value="allPayments">Все оплаты</option>
                                            <option value="по карте">Перевод</option>
                                            <option value="succeeded">С карты</option>
                                            <option value="open">Открыта оплата</option>
                                        </select>
                                    </div>

                                    <label  class="mr-3">От</label>
                                    <input class="form-control mr-3" wire:model.live="date_from" type="date" name="date_from">
                                    <label  class="mr-3">До</label>
                                    <input class="form-control mr-3" wire:model.live="date_to" type="date" name="date_to">
                                    <button type="submit" class="btn ml-3 btn-outline-primary ">Фильтр</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>


            <div class="tab-content p-0">
                <!-- /.card-header -->
                {{--по деталям--}}
                <div class="card tab-pane active" id="detal" style="margin-bottom: 0;box-shadow: unset">

                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                            <thead>
                            <tr>
                                <th>Дата</th>
                                <th>Номер телефона</th>
                                <th>Сумма</th>
                                <th>Название продукта</th>
                                <th>Source</th>
                                <th>Medium</th>
                                <th>Статус</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payment as $key => $day)
                                <tr >

                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['dateTime']}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['phone']}}
                                            </div>

                                        </div>                                                        </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{round($day['sum'])}} ₽
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['product']}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['utm_source']}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['utm_medium']}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                <form action="{{route('paymentControllerUpdate',$id)}}" method="POST">
                                                    @method('PATCH')
                                                    @csrf
                                                    <input type="hidden" name="id" value="{{$day['id']}}">
                                                    <select onchange="this.form.submit()" class="form-control input-opacity" name="status" style="background-color: rgba(0, 0, 0, 0)">
                                                        <option {{($day['status'] == 'open') ? 'selected="selected"' : '' }} value="open">open</option>
                                                        <option {{($day['status'] == 'succeeded') ? 'selected="selected"' : '' }} value="succeeded">succeeded</option>
                                                        <option {{($day['status'] == 'по карте') ? 'selected="selected"' : '' }} value="по карте">по карте</option>
                                                        <option {{($day['status'] == 'canceled') ? 'selected="selected"' : '' }} value="canceled">canceled</option>
                                                    </select>

                                                </form>
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal;vertical-align: middle;padding-right:0">
                                        <div class="d-flex justify-content-center">
                                            <div class="ml-1">
                                                <a href="https://wa.me/{{$day['phone']}}?text=%D0%94%D0%BE%D0%B1%D1%80%D1%8B%D0%B9%20%D0%B2%D0%B5%D1%87%D0%B5%D1%80.%20%D0%9C%D0%B5%D0%BD%D1%8F%20%D0%B7%D0%BE%D0%B2%D1%83%D1%82%20%D0%9C%D0%B0%D0%BA%D1%81%D0%B8%D0%BC%2C%20%D0%BF%D0%BE%D0%BC%D0%BE%D1%89%D0%BD%D0%B8%D0%BA%20%D0%AE%D0%BB%D0%B8%D0%B8%20%D0%9A%D1%83%D0%BF%D1%80.%20%D0%92%D0%B8%D0%B6%D1%83%20%D0%B1%D1%8B%D0%BB%D0%B8%20%D0%BD%D0%B0%20%D0%B2%D0%B5%D0%B1%D0%B8%D0%BD%D0%B0%D1%80%D0%B5%20%D0%B8%20%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D0%BE%D0%B2%D0%B0%D0%BB%D0%B8%D1%81%D1%8C%20%D0%BF%D1%80%D0%BE%D0%B4%D0%BE%D0%BB%D0%B6%D0%B8%D1%82%D1%8C%20%D0%BE%D0%B1%D1%83%D1%87%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%BD%D0%B0%20%D0%B6%D0%B8%D0%B2%D0%BE%D0%BC%20%D0%B8%D0%BD%D1%82%D0%B5%D0%BD%D1%81%D0%B8%D0%B2%D0%B5%3F%20%D0%92%D1%81%D0%B5%20%D0%BF%D1%80%D0%B0%D0%B2%D0%B8%D0%BB%D1%8C%D0%BD%D0%BE%3F" target="_blank">
                                                    <img src={{asset("dist/img/whatsapp.png")}} class="brand-image" style="width: 25px">
                                                </a>
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal;vertical-align: middle;padding-right:0">
                                        <div class="d-flex justify-content-center">
                                            <div class="ml-1">
                                                <a href="https://t.me/+{{$day['phone']}}" target="_blank">
                                                    Telegram
                                                </a>
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>

                {{--по дням--}}
                <div class="card tab-pane" id="day" style="margin-bottom: 0;box-shadow: unset">

                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                        <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                            <thead>
                            <tr>
                                <th>Дата</th>
                                <th>Оплат</th>
                                <th>Касса</th>
                                <th>По карте</th>
                                <th>Заказов</th>
                                <th>Не оплачено</th>
                                <th>Сумма</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($paymentStat as $key => $day)
                                <tr >

                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$key}}
                                            </div>
                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['paymentCount']}}
                                            </div>

                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['paymentKassa']}}
                                            </div>

                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['paymentCard']}}
                                            </div>

                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['paymentOrder']}}
                                            </div>

                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{$day['paymentOrder'] - $day['paymentCount'] }}
                                            </div>

                                        </div>
                                    </th>
                                    <th style="font-weight: normal">
                                        <div class="d-flex justify-content-start">
                                            <div>
                                                {{round($day['sum'])}} ₽
                                            </div>
                                        </div>
                                    </th>
                                </tr>
                            @endforeach
                            </tbody>
                            {{--                                                <tfoot style="background: #ffffff;font-weight: bold;position: sticky; bottom: 0px;">--}}
                            {{--                                                <td>--}}
                            {{--                                                    Всего--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="rub">--}}
                            {{--                                                    {{$all['rashod'] ?? ''}}--}}
                            {{--                                                </td>--}}
                            {{--                                                <td>--}}
                            {{--                                                    {{$all['newleads'] ?? ''}}--}}
                            {{--                                                </td>--}}
                            {{--                                                <td class="rub">--}}
                            {{--                                                    {{$all['leadPrice'] ?? ''}}--}}
                            {{--                                                </td>--}}

                            {{--                                                @if (request('period') == 'phone')--}}
                            {{--                                                    <td>--}}
                            {{--                                                    </td>--}}
                            {{--                                                    <td>--}}
                            {{--                                                    </td>--}}
                            {{--                                                @endif--}}

                            {{--                                                </tfoot>--}}
                        </table>
                    </div>

                </div>
            </div>
        </div>



    </div>





</div>

