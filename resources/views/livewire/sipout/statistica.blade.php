<div>
    <div class="row justify-content-between" style="display: flex; justify-content: center;background-color: #f4f6f9  ; padding: 0;">

        <div class="col-12 col-md-12 col-lg-12 col-xl-6 mt-2 p-2">
            <div style="height:185px; display: flex;flex-direction: column;padding: 15px;box-shadow: 0 0 5px 1px #d1d1d1 inset; margin-bottom: 10px">
                <div class="mb-2 mt-2 ml-1" style="display: flex;justify-content: start">
                    <h5> {{$lastStats['date'] ?? 'Сегодня'}} </h5>
                </div>
                <div style="display: flex;">
                    <div class="col-4">
                        <!-- Виджет 1: Остаток бюджета -->
                        <div class="maksCard small-box">
                            <div class="inner">
                                <p> Затраты </p>
                                <h1> {{$lastStats['todaySpend'] ?? '0'}} ₽ </h1>
                            </div>

                        </div>
                    </div>
                    <div class="col-4">
                        <!-- Виджет 1: Остаток бюджета -->
                        <div class="maksCard small-box">
                            <div class="inner">
                                <p> Лиды </p>
                                <h1> {{$lastStats['todayLeads'] ?? '0'}} </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <!-- Виджет 1: Остаток бюджета -->
                        <div class="maksCard small-box">
                            <div class="inner">
                                <p> Цена лидов </p>
                                <h1> {{$lastStats['todayLeadPrice'] ?? '0'}} ₽ </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    <div class="card" style="padding:0">
        <div class="card-tools">
            <div class="row">
                <div class="col-5 col-md-5 col-xl-5 form-inline"  style="padding-left: 14px">
                    <div class="btn-group" style="border-radius: 15px">
                        <div class="btn-group">

                            <div class="card-header">
                                <ul class="nav nav-pills ml-auto">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#phone" data-toggle="tab">Линии</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#days" data-toggle="tab">По дням</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="ml-3">
                        <select class="form-control input-opacity" wire:model.live="offer" name="offer"  style="background-color: rgba(0, 0, 0, 0)">
                            <option value="all">Все</option>
                            @foreach($offers as $offert)
                                <option value="{{$offert}}">{{$offert}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-7 col-md-7 col-xl-7">
                        <div class="form-inline float-right mr-3"  style="padding: 14px">
                            <label  class="mr-3">От</label>
                            <input class="form-control mr-3" type="date" wire:model.live="date_from" name="date_from">
                            <label  class="mr-3">До</label>
                            <input class="form-control mr-3" type="date" wire:model.live="date_to" name="date_to">
                            <button type="button"  wire:click="getSipoutPhone()" id="getSipoutPhone" class="btn btn-outline-primary">Скачать звонки</button>
                        </div>
                </div>

            </div>
        </div>

        <div class="card" style="margin-bottom: 0;box-shadow: unset">

            <div class="tab-content p-0">
                {{--по деталям--}}
                <div class="card tab-pane active" id="phone" style="margin-bottom: 0;box-shadow: unset">
                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                        @include('components.sipout.statistica.numbers')
                    </div>

                </div>
                {{--по дням--}}
                <div class="card tab-pane" id="days" style="margin-bottom: 0;box-shadow: unset">

                    <div class="card-body table-responsive p-0" style="max-height: 450px;">
                        @include('components.sipout.statistica.days')
                    </div>

                </div>
            </div>


    {{--    @if(request('period') != 'days' )--}}
    {{--            @include('components.sipout.statistica.numbers')--}}
    {{--        @elseif(request('period') == 'days')--}}
    {{--            @include('components.sipout.statistica.days')--}}
    {{--        @elseif(request('period') == 'offers')--}}
    {{--            @include('components.sipout.statistica.offers')--}}
    {{--        @endif--}}
        </div>

    </div>
</div>