<div class="table-responsive">
    <table class="table table-bordered">
        <thead class="table-dark">
        <tr>
            <th>Channel</th>
            <th>account_id</th>
            <th>UTM Source</th>
            <th>UTM Medium</th>
            <th>Sheet Name</th>
            <th>Currency</th>
            <th>Active</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($channels as $channel)
            <tr>
                <td>{{ $channel->channel }}</td>
                <td><a href="{{route('hw.account.update.table.date',$channel->id)}}">{{ $channel->account_id }}</a></td>
                <td>{{ $channel->utm_source }}</td>
                <td>{{ $channel->utm_medium }}</td>
                <td>{{ $channel->sheetName }}</td>
                <td>{{ $channel->currency }}</td>
                <td>{{ $channel->active ? 'Yes' : 'No' }}</td>
                <td>
                    <button wire:click="deleteChannel({{ $channel->id }})" class="btn btn-danger btn-sm">Delete</button>
                </td>
            </tr>
        @endforeach
        <tr>
            <td><input type="text" class="form-control" wire:model="newChannel.channel"></td>
            <td><input type="text" class="form-control" wire:model="newChannel.account_id"></td>
            <td><input type="text" class="form-control" wire:model="newChannel.utm_source"></td>
            <td><input type="text" class="form-control" wire:model="newChannel.utm_medium"></td>
            <td><input type="text" class="form-control" wire:model="newChannel.sheetName"></td>
            <td>
                <select class="form-select" wire:model="newChannel.currency">
                    <option value="">Валюта</option>
                    <option value="SGD">SGD</option>
                    <option value="RUB">RUB</option>
                    <option value="USD">USD</option>
                </select>
            </td>
            <td class="text-center align-middle">
                <input type="checkbox" class="" wire:model="newChannel.active" checked style="width: 20px;height: 20px">
            </td>
            <td>
                <button wire:click="addChannel" class="btn btn-outline-primary btn-sm">Добавить</button>
            </td>
        </tr>
        </tbody>
    </table>
</div>
