<div class="col-md-10" style="margin-top: 30px">
    @if (session()->has('success') || session()->has('error'))
        <div id="allert" class="alert alert-{{ session()->has('success') ? 'success' : 'danger' }}">
            {{ session()->get('success') ?? session()->get('error') }}
        </div>
    @endif


        <div class="row">
            <div class="col-lg-2 col-6">
                <div class="small-box">
                    <div class="inner">
                        <p> Переписки </p>
                        <h1> {{$statistic['count']}} </h1>
                    </div>
                    <div class="icon">
                        <i class="ion ion-speedometer"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-6 mr-5">
                <!-- Виджет 1: Остаток бюджета -->
                <button wire:click="save" type="button"  class="btn period btn-outline-primary">Скачать</button>
            </div>
        </div>


    <div class="card mt-4">
        <div class="d-flex justify-content-between border-bottom align-items-center p-3" style="height: 100px">
            <div class="col-2">
                <div>
                    Сообщения WABA
                </div>
            </div>
            <div class="col">
                <div>
                    <div class="form-inline float-right">
                        <input type="text" class="mr-2 form-control" style="width: 150px"  wire:model.live="search" placeholder="поиск" />



                        <div class="mr-3">
                            <select wire:model.live="filterTypeMessage" class="form-control input-opacity" name="filterTypeMessage" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                <option  value="outgoing"> Исходящие</option>
                                <option  value="in_button">Ответы кнопкой</option>
                                <option  value="in">Входящие</option>
                            </select>
                        </div>
                        <div class="">
                            <select wire:model.live="filterStatus" class="form-control input-opacity" name="filterStatus" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                <option  value="all"> Все</option>
                                <option  value="new"> Новые</option>
                                <option  value="fail"> Ошибка</option>
                                <option  value="deliver">Доставлено</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div style="margin-top: 45px">
                    <div class="form-inline float-right">
                        <div class="mr-3">
                            <select wire:model.live="filterTime" class="form-control input-opacity" name="filterTime" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                <option  value="updated_at"> По обновлению</option>
                                <option  value="created_at">По созданию</option>
                            </select>
                        </div>
                        <label  class="mr-3">От</label>
                        <input class="form-control mr-3" type="date" wire:model.live="date_from" name="date_from">
                        <label  class="mr-3">До</label>
                        <input class="form-control" type="date" wire:model.live="date_to" name="date_to">
                    </div>

                </div>
            </div>

        </div>
        <div class="card-body p-0">
            <div class=d-flex>
                <div  class="col-2 overflow-auto" style="height: 600px">
                 @foreach($phones as $phone)
                    <div wire:click="dialogs({{ $phone }})" class="border-bottom d-flex" style="cursor: pointer;{{ $phonePointer == $phone ? 'background: #f3f3f3;' : '' }}">
                        <div class="p-2 fs-5 fw-normal">
                            {{ $phone['phone'] }}
                        </div>
                        @if($phone['status'] == 'in')
                        <div class="fw-light">new</div>
                        @endif
                    </div>
                 @endforeach
                </div>
                <div class="col" style="height: 600px;  display: inline-flex; flex-direction: column;
">
                    <div class="row overflow-auto" style="background: rgb(253,248,236);height: 88%">
                        @foreach($dialog as $message)
                            @if ($message->type == 'outgoing')
                            <div class="row p-2  justify-content-end">
                                <div class="w-50 mr-2 fw-normal d-flex justify-content-end">
                                   <div class="waba_outgoing">
                                        <div>
                                            {{ $message->message }}
                                        </div>
                                        <div class="ml-1" style="font-size: 10px;color: #525252">
                                            {{ \Carbon\Carbon::parse($message->created_at)->format('H:i') }}
                                        </div>
                                   </div>
                                </div>
                            </div>
                            @else
                                <div class="row p-2 ">
                                    <div class="w-50 mr-3 fw-normal d-flex">
                                        <div class="waba_in">
                                            <div>
                                                {{ $message->message }}
                                            </div>
                                            <div class="ml-1" style="font-size: 10px;color: #525252">
                                                {{ \Carbon\Carbon::parse($message->created_at)->format('H:i') }}
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div class="row mt-2">
                        <div>
                            <div class="d-flex align-items-end">
                                <div class="p-1 w-100">
                                    <textarea wire:model.live="text" style="width: 100%;line-height: 15px; resize: none;" type="text" class="form-control" id="text" placeholder="Введите сообщение"> {{$text}} </textarea>
                                </div>
                                <div class="p-1">
                                    <button wire:click="sendMessageStore" type="button" class="btn btn-primary ml-1">Отправить</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
</div>

<script>
    const textarea = document.querySelector('textarea');

    textarea.addEventListener('input', function() {
        const maxHeight = 300;
        const height = 20 * this.value.split('\n').length;
        this.style.height = `${Math.min(height, maxHeight)}px`;
    });

    textarea.dispatchEvent(new Event('input'));
</script>


