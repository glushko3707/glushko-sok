<div class="row" style="justify-content: start">

    <div class="col-8">
        <div class="pl-3">
            <div class="col-11">
                <div  class="card" style="padding:0;margin-top: 100px">

                    <div class="card" style="margin-bottom: 0;box-shadow: unset">

                        <div class="card-body table-responsive p-0" style="max-height: 600px;">
                            <table class="table table-head-fixed table-sm  text-nowrap">
                                <thead style="height:50px;vertical-align: middle">
                                <tr>
                                    <th style="vertical-align: middle">Входящий текст</th>
                                    <th style="vertical-align: middle;width: 320px">Ответ</th>
                                    <th style="vertical-align: middle">Действия</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($WabaMessage as $index => $message)
                                    <tr>
                                        <th>
                                            <div>
                                              <textarea rows="1"  wire:model.lazy="WabaMessage.{{ $index }}.callback" class="input-opacity custom-textarea" style="background-color: rgba(0, 0, 0, 0);width: 250px" name="callback{{ $index }}"></textarea>
                                            </div>
                                        </th>
                                        <th>
                                              <textarea rows="1" wire:model.lazy="WabaMessage.{{ $index }}.text" class="input-opacity text-sm custom-textarea" style="line-height: 1.5;background-color: rgba(0, 0, 0, 0);width: 250px" name="comment_actual{{ $index }}">  </textarea>
                                        </th>
                                        <th>
                                            <button wire:click="preview({{$message['id']}})" class="btn btn-sm btn-success">Показать</button>
                                            <button wire:confirm="Хотите удалить?" wire:click="delete({{$message['id']}})" type="button">
                                                <i class="fas fa-times ml-2" style="color: red"></i>
                                            </button>
                                        </th>

                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot style="background:#fff7f3">
                                    <tr>
                                        <th>
                                            <div>
                                            <textarea rows="1" wire:model.lazy="WabaMessageNew.callback" class="text-sm custom-textarea" style="width: 250px">  </textarea>
                                            </div>
                                        </th>
                                        <th>
                                            <textarea rows="1" wire:model.lazy="WabaMessageNew.text" class="text-sm custom-textarea" style="width: 250px">  </textarea>
                                        </th>
                                        <th>
                                            <button wire:click="save" class="btn btn-sm btn-outline-success">Добавить</button>
                                        </th>

                                      </tr>
                                    </tfoot>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div style="margin-top: 100px">
            <div>
                <div class="whatsapp-header-template">
                    Превью шаблона
                </div>
                <div class="whatsapp-background">
                    <div class="row mt-2 p-3">
                        @if(isset($WabaMessagePreview['text']) || (isset($WabaMessagePreview['url_type']) && $WabaMessagePreview['url_type'] != '') )
                        <div class="col">
                            <img data-bs-toggle="modal" data-bs-target="#modalInage" src="{{ asset('images/waba/attach-outline.svg') }}" class="large-icon rotatable" alt="Attach Icon">
                        </div>

                        @endif
                    </div>
                    <div class="whatsapp-message">
                            @if(isset($WabaMessagePreview['url_type']))
                                <div class="message-header" style="padding: 0">
                                    @if($WabaMessagePreview['url_type'] == 'image')
                                        <img src={{$WabaMessagePreview['url_media']}} class="rounded-top-corners">
                                    @endif
                                </div>
                            @endif
                            <div class="message-body _6xe4">
                                <textarea id="WabaMessagePreviewText" wire:model.lazy="WabaMessagePreview.text" class="input-opacity auto-resize" style="height:auto;line-height: 1.5;background-color: rgba(0, 0, 0, 0);width: 100%" name="WabaMessagePreviewText"> </textarea>
                            </div>
                            @if(isset($WabaMessagePreview['button']) && count($WabaMessagePreview['button']) > 0)
                                    @foreach($WabaMessagePreview['button'] as $button)
                                    <div class="message-footer">
                                        <button class="waba-button">{{$button['text']}}</button>
                                        <button wire:click="deleteButton({{$button['id']}})"><span class="close-btn">×</span></button>
                                    </div>
                                    @endforeach
                            @endif
                                    <div class="message-footer">
                                    <input wire:model.live.lazy.debounce.200="newButton" class="waba-button" style="width: 250px; height: 30px; padding: 15px;text-align: center;" placeholder="Добавить кнопку">
                                </div>

                        </div>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self="update" class="modal fade" id="modalInage" tabindex="-1"  aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content" style="height: 700px">
                <div class="modal-header">
                    <h5 class="modal-title">Управление вложениями</h5>
                </div>
                <div class="modal-body">
                    @if(isset($WabaMessagePreview['url_type']))
                        <div class="message-header" style="padding: 0">
                            @if($WabaMessagePreview['url_type'] == 'image')
                                <img src={{$WabaMessagePreview['url_media']}} class="rounded-top-corners">
                            @endif
                        </div>
                        <div>
                            <form action="{{route('saveMedia.store',$id)}}" method="POST" enctype="multipart/form-data">
                                <div>
                                    <input class="form-control" type="hidden" name="replace" value="yes">
                                    <input wire:model.lazy="media" class="form-control" accept=".png, .jpg, .jpeg" type="file" name="media">
                                    <input class="form-control" type="hidden" value="{{$WabaMessagePreview['id'] ?? ''}} " name="message_id">
                                </div>
                                <button class="btn btn-success mt-3" style=" width: 100%;">
                                    Заменить
                                </button>
                            </form>

                            <form action="{{route('deleteMedia.store',$id)}}" method="POST" enctype="multipart/form-data">
                                <div>
                                    <input class="form-control" type="hidden" value="{{$WabaMessagePreview['id'] ?? ''}} " name="message_id">
                                    <input class="form-control" type="hidden" value="{{$WabaMessagePreview['url_media'] ?? ''}} " name="url_media">
                                </div>
                                <div class="col-12 p-0">
                                    <button class="btn btn-danger mt-3" style=" width: 100%;">
                                        Удалить
                                    </button>
                                </div>
                            </form>
                        </div>
                    @else
                        <form action="{{route('saveMedia.store',$id)}}" method="POST" enctype="multipart/form-data">
                            <div>
                                <input class="form-control" accept=".png, .jpg, .jpeg"  type="file" name="media">
                                <input class="form-control" type="hidden" value="{{$WabaMessagePreview['id'] ?? ''}} " name="message_id">
                            </div>
                            <button class="btn btn-danger mt-3" style=" width: 100%;">
                                Сохранить
                            </button>
                        </form>
                    @endif


                </div>

            </div>
        </div>
    </div>
</div>



@script
<script>
    $wire.watch('WabaMessagePreview.text', function () {
            setTimeout(() => {
                autoResize()
            }, 100)

        function autoResize() {
            let text  = document.getElementById('WabaMessagePreviewText')
            let newHeight = text.scrollHeight + 2;
            if (newHeight > 450) {
                newHeight = 450;
            }

            text.style.height = newHeight + 'px';

        }
    });

    $wire.on('preview', (event) => {
        function autoResize(textarea) {
            let newHeight = document.getElementById('WabaMessagePreviewText').scrollHeight + 2;
            if (newHeight > 450) {
                newHeight = 450;
            }
            textarea.style.height = newHeight + 'px';
        }

        document.querySelectorAll('.auto-resize').forEach(textarea => {
            textarea.addEventListener('input', () => autoResize(textarea));
            textarea.addEventListener('focus', () => autoResize(textarea));
            textarea.addEventListener('blur', () =>  autoResize(textarea));
        })
    });
</script>
@endscript


<script>
    function autoResized(textarea) {
        textarea.style.height = 'auto'; // Сброс высоты
        textarea.style.height = (textarea.scrollHeight) + 'px'; // Установка новой высоты
    }

    function resetHeight(textarea) {
        textarea.style.height = 'auto'; // Сброс высоты
        textarea.rows = 1; // Возврат высоты к одной строке
    }

    document.querySelectorAll('.custom-textarea').forEach(textarea => {
        textarea.addEventListener('input', () => autoResized(textarea));
        textarea.addEventListener('focus', () => autoResized(textarea));
        textarea.addEventListener('blur', () => resetHeight(textarea)); // Обработчик для установки высоты при потере фокуса
    });



</script>


