<div class="content-wrapper">
    <div class="row justify-content-center">
        <div class="col-md-11" style="margin-top: 30px">
            @if (session()->has('success') || session()->has('error'))
                <div id="allert" class="alert alert-{{ session()->has('success') ? 'success' : 'danger' }}">
                    {{ session()->get('success') ?? session()->get('error') }}
                </div>
            @endif
            <div class="row">
                <div class="col-7">
                    <form method="POST" action="{{ route('SendMessageWaba.store',$id)}}">
                        @csrf

                        <div class="card mt-4 col-12">
                            <div class="card-header d-flex justify-content-between">
                                <div>
                                    <h4>
                                        Отправить WABA рассылку
                                    </h4>
                                </div>
                                <!-- Кнопка с классом updateButton -->
                                <div class="ml-auto">
                                    <a href="{{ route('updateWabaTemplate.store', $id) }}" class="btn btn-sm btn-primary">Обновить шаблоны</a>
                                </div>

                                <!-- JavaScript код для блокировки кнопки -->
                            </div>

                            <div class="card-body">

                                <div class="row mb-3">
                                    <label for="phone" class="col-md-4 col-form-label text-md-end"> Вставьте номера</label>
                                    <div class="col-md-6">
                                        <textarea class="form-control" id="phone" style="margin-bottom: 18px" name="phone" rows="3"></textarea>
                                    </div>
                                </div>


                                <div class="row mb-3">
                                    <label for="delay_type" class="col-md-4 col-form-label text-md-end">Выберите шаблон</label>
                                    <div class="col-md-6">
                                        <select wire:change="wabaPreviews($event.target.value)" class="form-control " id="pipeline_step_id" name="name" style="width:100%;margin-left: 0">
                                            <option >Выберите шаблон</option>
                                            @foreach($wabaTemplates as $template)
                                                <option value="{{$template->name}}">{{$template->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if(isset($WabaMessagePreview['variable']))
                                    @foreach($WabaMessagePreview['variable'] as $index => $variable)
                                        <div class="row mb-3">
                                            <label for="delay_type" class="col-md-4 col-form-label text-md-end">
                                                Переменная {{ $variable['type'] == 'text' ? 'текста_' : ($variable['type'] == 'button' ? 'кнопки_' : '') }}{{ $variable['count_variable']}} </label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="variable_{{$variable['type']}}_{{$variable['count_variable']}}" value="{{$variable['value']}}" >
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                                <div class="row mt-3 mb-3 another">
                                    <label for="timeSend" class="col-md-4 col-form-label text-md-end"> Время отправки</label>
                                    <div  class="col-md-6">
                                        <input id="timeSend" type="datetime-local" class="form-control" name="timeSend">
                                    </div>
                                </div>

                                <div class="row mb-3">

                                    <label for="category" class="col-md-4 col-form-label text-md-end"> Исходящие номера</label>
                                    <div class="col-md-6">
                                        <select class="select2" name="waba_phone_ids[]" multiple data-placeholder="Исходящие номера" style="width: 100%;">
                                            @foreach($intances_id as $intance_id)
                                                <option value="{{$intance_id['waba_phone_id']}}"> {{$intance_id['phone']}} </option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>



                                <div class="row mb-0">
                                    <div class="col-md-8 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Добавить номера
                                        </button>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
                <div class="col-4">
                    <div class="p-4">
                        <div style="{{empty($WabaMessagePreview) ? ';display:none' : ''}}">
                            <div class="whatsapp-header-template">
                                Превью шаблона
                            </div>
                            <div class="whatsapp-background" style="height: 600px">
                                <div class="whatsapp-message">
                                    @if(isset($WabaMessagePreview['url_type']))
                                        <div class="message-header" style="padding: 0">
                                            @if($WabaMessagePreview['url_type'] == 'image')
                                                <img src={{$WabaMessagePreview['url_media']}} class="rounded-top-corners">
                                            @endif
                                        </div>
                                    @endif
                                        @if(isset($WabaMessagePreview['text']))
                                        <div class="message-body _6xe4">
                                        <textarea disabled id="WabaMessagePreviewText" wire:model.lazy="WabaMessagePreview.text" class="input-opacity auto-resize" style="height:auto;line-height: 1.5;background-color: rgba(0, 0, 0, 0);width: 100%" name="WabaMessagePreviewText"> </textarea>
                                        </div>
                                         @endif

                                    @if(isset($WabaMessagePreview['variable']))
{{--                                        @foreach($WabaMessagePreview['variable'] as $variable)--}}
{{--                                            <div class="message-footer">--}}
{{--                                                <button class="waba-button">{{$button['text']}}</button>--}}
{{--                                            </div>--}}
{{--                                        @endforeach--}}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>
</div>

    <script>
        document.addEventListener('livewire:initialized', () => {
            $(function () {
                //Initialize Select2 Elements
                $('.select2').select2()

                //Initialize Select2 Elements
                $('.select2bs4').select2({
                    theme: 'bootstrap4'
                })
            })
        })


    </script>

@script
<script>
    $wire.watch('WabaMessagePreview', function () {
        setTimeout(() => {
            autoResize()
        }, 100)

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })

        function autoResize() {
            let text  = document.getElementById('WabaMessagePreviewText')
            let newHeight = text.scrollHeight + 2;
            if (newHeight > 450) {
                newHeight = 450;
            }

            text.style.height = newHeight + 'px';

        }
    });
</script>
@endscript
