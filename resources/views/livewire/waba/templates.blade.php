<div>
    <div class="card">
        <div class="card-body">
            <table class="table">
                <thead>
                <tr>
                    <th style="width: 250px" scope="col">Название шаблона</th>
                    <th style="width: 100px" scope="col">waba_id</th>
                    <th style="width: 500px" scope="col">Текст</th>
                    <th style="width: 250px" scope="col">Смс замена</th>
                    <th scope="col">Действия</th>

                </tr>
                </thead>
                <tbody>
                @foreach($intanses as $index => $intans)
                    @foreach($intans['templates'] as $indexTemplates => $template)
                        <tr>
                            <td>
                                <div class="d-flex">
                                    {{$template['name']}}
                                </div>
                            </td>
                            <td>
                                <div class="d-flex">
                                    {{$intans['waba_phone_id']}}
                                </div>
                            </td>
                            <td>
                                <div class="d-flex text-sm text-truncates" style="width: 400px;height: 40px">
                                    {{$template['text']}}
                                </div>
                            </td>
                            <td>
                                <select wire:model.live="intanses.{{$index}}.templates.{{$indexTemplates}}.reserve_sms_id" style="width: 200px" name="reserve_sms_id" class="form-control input-opacity">
                                    <option></option>
                                    @foreach($webSms as $sms)
                                        <option value="{{$sms['id']}}">{{$sms['text']}}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <div>
                                    <img wire:click="updateWabaTemplateFb('{{$intans['waba_id']}}','{{$template['name']}}')" style="width: 30px" src="{{ asset('images/waba/refresh-outline.svg') }}" class="large-icon rotatable cursor-pointer" alt="update">
                                </div>
                            </td>



                        </tr>

                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
