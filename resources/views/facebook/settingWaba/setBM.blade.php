@extends('adminPanel.startAddProject')

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row mt-3" style="justify-content: center">

                <div class="d-flex justify-content-center max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-5">
                    <div class="col-xl-4 col-lg-5 col-md-7 col-sm-10 col-12 p-0 shadow sm:rounded-lg">
                        <div class="card p-0" style="background: #efefef;margin-bottom: 0;box-shadow: unset">
                            <div class="card-body" style="padding-top: 0">
                                <div>
                                    <div class="text-center mt-3 d-flex flex-column align-items-center">

                                        <h2 style="font-weight: 600">Настройка интеграции</h2>
                                        <div class="p-2 col-lg-10  col-md-11 col-12">
                                            <p style="font-weight: 200">шаг 1/2</p>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center mb-3">
                                        <div class="col-lg-10  col-md-11 col-12">
                                            <form action="{{route('setBM.store',$id)}}" method="GET">
                                                <x-form.input-nolabel id="owner_business_id" type="text" name="owner_business_id" :value="old('name')" placeholder="Введите ID бизнес менеджера" required/>
                                                <x-input-error :messages="$errors->get('name')" class="mt-1" />

                                                <x-form.button class="btn mt-4 w-100 shine-button" style="" type="submit"> Далее </x-form.button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




                <div class="card" style="padding: 0">
                    <div class="card-tools">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

<style>
    .shine-button:hover {
        background-color: #ffffff;
    }

    .shine-button {
        position: relative;
        border: none;
        overflow: hidden; /* Это важно для предотвращения переполнения эффекта за границы кнопки */
        transition: background-color 0.3s;
    }

    .shine-button:hover {
        background-color: #ffffff; /* Фон при наведении */
        color: #0a001f;
        transition: background-color 0.3s;
    }

    .shine-button:before {
        content: '';
        position: absolute;
        top: 50%;
        left: -100%; /* Начальная позиция блика за пределами левой границы кнопки */
        width: 200%;
        height: 200%;
        background: rgba(255, 255, 255, 0.3); /* Белый блик */
        transform: translateY(-50%) rotate(45deg);
        animation: shine 2s infinite; /* Анимация */
    }

    @keyframes shine {
        0% {
            left: -100%; /* Начало за пределами кнопки слева */
        }
        50% {
            left: 50%; /* Центр кнопки */
        }
        100% {
            left: 150%; /* Конец за пределами кнопки справа */
        }
    }
</style>