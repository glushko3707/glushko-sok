@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Настройки сервиса SipOut </div>

                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger d-flex justify-content-center" role="alert">
                                @foreach($errors->all() as $error)
                                    <h5>{{ $error }}</h5>
                                @endforeach
                            </div>
                        @endif


                        <form method="POST" action="{{ route('addAccountAdFacebook.store',$id)}}">
                            @csrf

                            <div class="row mb-3">
                                <label for="startDate" class="col-md-4 col-form-label text-md-end"> Дата отсчета </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="date" class="form-control" id="œ" name="startDate">
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="testPhone" class="col-md-4 col-form-label text-md-end"> Аккаунт ID </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="ad_account_id" name="ad_account_id">
                                    </div>
                                </div>
                            </div>



                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary" style="width: 150px">
                                    Сохранить
                                </button>
                            </div>



                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
