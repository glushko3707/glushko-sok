<!DOCTYPE html>
<html>
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])


    <meta charset="UTF-8">
    <title>Текстовый редактор</title>
    <style>
        /* Здесь вы можете добавить свои CSS-стили */
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        #container {
            width: 80%;
            margin: 20px auto;
            display: flex;
            flex-direction: column;
            align-items: center;
        }

        .editor {
            width: 100%;
            height: 300px;
            border: 1px solid black;
            font-family: Arial, sans-serif;
            font-size: 16px;
            padding: 10px;
            outline: none;
        }

        #output {
            width: 100%;
            height: 300px;
            border: 1px solid black;
            font-family: monospace;
            font-size: 14px;
            padding: 10px;
            white-space: pre-wrap;
        }

        #buttons {
            width: 100%;
            display: flex;
            justify-content: space-between;
            margin-top: 10px;
        }

        button {
            width: 100px;
            height: 40px;
            border: none;
            background-color: #4CAF50;
            color: white;
            font-weight: bold;
            cursor: pointer;
        }

        button:hover {
            background-color: #3E8E41;
        }

        button:active {
            background-color: #2E7D32;
        }
    </style>
</head>
<body>
@livewire('salebot.texteditor')
{{--<form action="{{route('SalebotTextInput.store')}}" method="POST">--}}
{{--</form>--}}
</body>
</html>
