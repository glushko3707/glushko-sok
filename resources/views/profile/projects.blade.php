<x-app-layout>


    <div class="py-12">
        @foreach($permissionsProject as $project)
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-5">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div>
                    @include('profile.projectsPartials.project')
                </div>
            </div>
        </div>
        @endforeach

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-5">
            <div class="p-4 sm:p-8 bg-white shadow sm:rounded-lg">
                <div class="max-w-xl">
                    <header class="mb-3">
                        <h5> Добавить проект </h5>
                        <form method="POST" action="{{route('createProject.store')}}" class="form-inline">
                            <div class="mb-3 t">
                                <label for="name" class="col-form-label md-3 font-medium mr-3 "> Напишите имя нового проекта </label>
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <input class="form-control" id="name" name="name" style="width: 300px" >
                                        <span class="input-group-append">
                                        <button type="submit" style="background-color: #2a7bf5" class="btn btn-primary">Создать проект</button>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </header>
                </div>
            </div>
        </div>



</x-app-layout>
