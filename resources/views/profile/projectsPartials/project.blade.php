<section>
    <header>
        <header class="mb-3">
            <div class="flex justify-between">
                <div class="">
                  <a class="text-xl" style="text-decoration: none;color: #0a0e14" href="{{route('sipoutStatistic.index',$project->id)}}"> <h5> {{ $project->name }} </h5>  </a>
                </div>

                <div class="">
                    <a class="text-sm"  onclick="return confirmDelete()" style="text-decoration: none;color: #ab0512" href="{{route('project.destroy',$project->id)}}"> Удалить проект  </a>
                </div>
            </div>
        </header>
    </header>

        <div class="flex items-center gap-4">
            <a href="{{route('sipoutStatistic.index',$project->id)}}">
                 <x-primary-button style="background: #2a7bf5; color: #ffffff">Перейти</x-primary-button>
            </a>
        </div>
</section>

<script>
    // Функция для подтверждения удаления
    function confirmDelete() {
        // Показать диалоговое окно с вопросом и кнопками OK и Cancel
        var result = confirm("Вы уверены, что хотите удалить проект?");

        // Если пользователь нажал OK, вернуть true
        if (result) {
            return true;
        }

        // Если пользователь нажал Cancel, вернуть false
        else {
            return false;
        }
    }
</script>
