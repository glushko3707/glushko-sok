<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lead Pro</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://yastatic.net/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://telegram.org/js/telegram-web-app.js"></script>

    @livewireStyles
    <style>
        #wheelOfFortune {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 1;
        }

        #wheel {
            display: block;
        }
        #spin {
            font-size: 10px;
            text-align: center;
            word-break: break-word;
            padding: 10px;
            user-select: none;
            cursor: pointer;
            display: flex;
            justify-content: center;
            align-items: center;
            position: absolute;
            top: 50%;
            left: 50%;
            width: 30%;
            height: 30%;
            margin: -15%;
            background: #fff;
            color: #fff;
            box-shadow: 0 0 0 8px currentColor, 0 0px 15px 5px rgba(0, 0, 0, 0.6);
            border-radius: 50%;
            transition: 0.8s;
        }

        #spin::after {
            content: "";
            position: absolute;
            top: -17px;
            border: 10px solid transparent;
            border-bottom-color: currentColor;
            border-top: none;
        }

        .container {
            position: relative;
            width: 350px;
            height: 350px;
        }

        .container img {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 10;
            pointer-events: none;
        }

        .pColorInfo {
            color: rgba(255,255,255,1);
            font-weight: 200;
            font-size: 12px;
        }

        .pColorName {
            color: rgba(255,255,255,1);
            font-weight: 600;
            font-size: 16px;
        }
        .dots {
            flex-grow: 1;
            border-bottom: 1px dotted #a2a2a2; /* цвет и стиль точек */
            margin: 0 10px; /* отступы по краям */
        }
        #result {
            margin-top: 20px;
            text-align: center;
            font-size: 20px;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div class="tg_bg">
    <div class="d-flex justify-content-center">
        <div class="col-12 d-flex flex-column justify-content-center" style="background: rgb(0,0,0,0.4); height: 80px">
          <div class="row d-flex justify-content-center" style="padding-top: -2px">
              <div id="profile-photo-div" class="col-4">
                  <img id="profile-photo" style="border-radius:50%;width: 65px">
              </div>
              <div class="col-6">
                  <div class="row p-0 mt-1" style="align-items: center;">
                      <div class="col-auto ml-2 p-0">
                          <p id="nameUser" class="mb-0 pColorName">Имя</p>
                      </div>
                      <div class="col-auto p-0">
                          <p id="lastNameUser" class="mb-0 ml-2 p-0 pColorName" style="color: rgba(255,255,255,1)">Фамилия</p>
                      </div>
                  </div>
                  <div class="row p-0 d-flex justify-content-between align-items-center" >
                      <div class="col-auto ml-2 p-0" style="margin-right: 5px">
                          <p class="mb-0 pColorInfo">Баллы</p>
                      </div>
                      <div class="col-auto m-0 dots" style="padding: 4px"></div>
                      <div class="col-auto p-0" >
                          <p class="mb-0 ml-2 p-0 pColorInfo">4</p>
                      </div>
                  </div>
                  <div class="row p-0 d-flex justify-content-between align-items-center" >
                      <div class="col-auto ml-2 p-0" style="margin-right: 5px">
                          <p class="mb-0 pColorInfo">Попыток</p>
                      </div>
                      <div class="col-auto m-0 dots" style="padding: 4px"></div>
                      <div class="col-auto p-0" >
                          <p class="mb-0 ml-2 p-0 pColorInfo">1</p>
                      </div>
                  </div>
              </div>
          </div>

        </div>
    </div>

        <div class="container" style="margin-top: 20px;">
            <img src="{{asset('images/telegram/FortunaRadius.png')}}" alt="Background Image">
            <div id="wheelOfFortune">
                <canvas id="wheel" width="350" height="350"></canvas>
                <div id="spin">SPIN</div>
            </div>
        </div>
        <div style="color:#ffffff" id="result">Результат будет здесь</div>
</div>
<script>
    const sectors = [
        {color:"#ff7600", colorText:"#ffffff", label:"Крути еще раз"},
        {color:"#000000", colorText:"#ffffff", label:"Мини-курс"},
        {color:"#ff7600", colorText:"#ffffff", label:"Консультация"},
        {color:"#000000", colorText:"#ffffff", label:"Пусто"},
        {color:"#ff7600", colorText:"#ffffff", label:"500 ЮМиксов"},
        {color:"#000000", colorText:"#ffffff", label:"10к на курс"},
    ];

    const rand = (m, M) => Math.random() * (M - m) + m;

    const tot = sectors.length;
    const elSpin = document.querySelector("#spin");
    const ctx = document.querySelector("#wheel").getContext('2d');
    const dia = ctx.canvas.width;
    const rad = dia / 2;
    const PI = Math.PI;
    const TAU = 2 * PI;
    const arc = TAU / tot;
    const friction = 0.981;
    const angVelMin = 0.002;
    let angVelMax = 0;
    let angVel = 0;
    let ang = 0;
    let isSpinning = false;
    let isAccelerating = false;
    let animFrame = null;

    const resultElement = document.getElementById('result');

    const getIndex = () => Math.floor(tot - ang / TAU * tot) % tot;

    const wrapText = (context, text, x, y, maxWidth, lineHeight) => {
        const words = text.split(' ');
        let line = '';
        const lines = [];

        for (let n = 0; n < words.length; n++) {
            let testLine = line + words[n] + ' ';
            let metrics = context.measureText(testLine);
            let testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) {
                lines.push(line);
                line = words[n] + ' ';
            } else {
                line = testLine;
            }
        }
        lines.push(line);

        const totalHeight = lines.length * lineHeight;
        let textY = y - totalHeight / 2;

        for (const line of lines) {
            context.fillText(line, x, textY);
            textY += lineHeight;
        }
    };
    const drawSector = (sector, i) => {
        const ang = arc * i;
        ctx.save();

        ctx.beginPath();
        ctx.fillStyle = sector.color;
        ctx.moveTo(rad, rad);
        ctx.arc(rad, rad, rad - 8, ang, ang + arc);
        ctx.lineTo(rad, rad);
        ctx.fill();

        const gradient = ctx.createRadialGradient(rad, rad, rad - 5, rad, rad, rad);
        gradient.addColorStop(0, 'rgb(238,238,238)');
        gradient.addColorStop(0.05, 'rgb(0,0,0)');
        gradient.addColorStop(1, 'rgb(208,208,208)');
        ctx.lineWidth = 5;
        ctx.strokeStyle = gradient;
        ctx.stroke();

        ctx.translate(rad, rad);
        ctx.rotate(ang + arc / 2);
        ctx.textAlign = "center";
        ctx.textBaseline = "middle";
        ctx.fillStyle = sector.colorText;
        ctx.font = "10px sans-serif";

        const maxWidth = rad * 0.45;
        const lineHeight = 14;

        wrapText(ctx, sector.label, rad / 2 + 30, 4, maxWidth, lineHeight);
        ctx.restore();
    };

    const rotate = () => {
        const sector = sectors[getIndex()];
        ctx.canvas.style.transform = `rotate(${ang - PI / 2}rad)`;
        elSpin.textContent = !angVel ? "Крутить еще" : sector.label;
        elSpin.style.background = sector.color;
    };

    const frame = () => {
        if (!isSpinning) return;

        if (angVel >= angVelMax) isAccelerating = false;

        if (isAccelerating) {
            angVel ||= angVelMin;
            angVel *= 1.06;
        } else {
            isAccelerating = false;
            angVel *= friction;

            if (angVel < angVelMin) {
                isSpinning = false;
                angVel = 0;
                const sector = sectors[getIndex()];
                resultElement.textContent = `Результат: ${sector.label}`;
                cancelAnimationFrame(animFrame);
            }
        }

        ang += angVel;
        ang %= TAU;
        rotate();
    };

    const engine = () => {
        frame();
        animFrame = requestAnimationFrame(engine);
    };

    elSpin.addEventListener("click", () => {
        if (isSpinning) return;
        isSpinning = true;
        isAccelerating = true;
        angVelMax = rand(0.25, 0.40);
        resultElement.textContent = "Вращение...";
        engine();
    });

    sectors.forEach(drawSector);

    rotate();
</script>

<script>
    window.addEventListener('load', function() {
        let App = window.Telegram.WebApp
        const user = App.initDataUnsafe.user;

        if (user){
            document.getElementById('nameUser').textContent = user.first_name;
        }
        if (user){
            document.getElementById('lastNameUser').textContent = user.last_name;
        }

        if (user && user.photo_url == 'undefined') {
            document.getElementById('profile-photo').src = user.photo_url;
        } else {
            document.getElementById('profile-photo-div').style.display = 'none';
        }
    });
</script>

</body>
</html>
