@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header d-flex justify-between">
                        <div class="col-6">Настройки интенсива {{\Illuminate\Support\Carbon::parse($data['date_intensive'])->format('d.m')}} </div>
                        <div class="col-6 d-flex justify-content-end">
                            <a class="text-sm" href="{{route('IntensiveListIndex',$id)}}"> К списку интенсивов </a>
                        </div>
                    </div>
                    <div class="card-body">


                            <div class="row mb-3">
                                <label for="date_intensive" class="col-md-4 col-form-label text-md-end"> Дата начала </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" type='date' id="date_intensive" name="date_intensive" value="{{$data ['date_intensive'] ?? ""}}">
                                    </div>
                                    @if ($errors->has('date_intensive'))
                                        <div class="error" style="color: red;">
                                            {{ $errors->first('date_intensive') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-3">

                                <label for="main_chat_id" class="col-md-4 col-form-label text-md-end"> Главный чат ID</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" value="{{$data['main_chat_id']}}" id="main_chat_id" name="main_chat_id">
                                    </div>
                                    @if ($errors->has('main_chat_id'))
                                        <div class="error" style="color: red;">
                                            {{ $errors->first('main_chat_id') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-3">

                                <label for="main_chat_id" class="col-md-4 col-form-label text-md-end"> Список чатов</label>
                                <div class="col-md-6 d-flex align-items-center">
                                    <div class="input-group d-flex align-items-center">
                                        <div class="d-flex borderGlushko align-items-center">
                                            @if(!empty($data['getIntensiveChats']))
                                                  <div class="d-flex justify-start">
                                                      @foreach($data['getIntensiveChats'] as $chats)
                                                          <a class="mr-2 clickable-element">{{$chats->chat_name}}</a>
                                                      @endforeach
                                                 @endif
                                             </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-1 clickable-element zoom-effect text-2xl d-flex justify-content-start align-items-center" style="color: #727272FF">
                                    <a href="{{route('IntensiveChatCreatesIndex',[ $data['intensive_id'],$id])}}"> + </a>
                                </div>
                            </div>



                            <div class="d-flex justify-content-center ">
                                <button class="btn btn-primary mt-3" style="width: 150px">
                                    Сохранить
                                </button>
                            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
