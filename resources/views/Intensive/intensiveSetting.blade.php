@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Настройки сервиса SipOut </div>
                    <div class="card-body">

                        <form method="POST" action="{{ route('sipoutSetting.store',$id)}}">
                            @csrf
                            <div class="row mb-3">
                                <label for="emailSmsAero" class="col-md-4 col-form-label text-md-end"> SipOut API-КЛЮЧ  </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="sipOutSetting"  value="{{$project['sipOutSetting'] ?? ""}}" name="sipOutSetting" >
                                    </div>
                                    @if($errors->any())
                                        <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>
                                    @endif
                                </div>
                            </div>


                            <div class="row mb-3">
                                <label for="dateStartIntensive" class="col-md-4 col-form-label text-md-end"> Дата начала </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" type='date' id="dateStartIntensive" name="dateStartIntensive" value="{{$project['testPhone'] ?? ""}}">
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="mainChat" class="col-md-4 col-form-label text-md-end"> Главный чат (ссылка)</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="mainChat" name="mainChat" value="{{$project['testPhone'] ?? ""}}">
                                    </div>
                                </div>
                            </div>




                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary" style="width: 150px">
                                    Сохранить
                                </button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
