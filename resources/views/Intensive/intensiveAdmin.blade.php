@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                @livewire('intensive.user-intensive-admin', [
                              'id' => $id,'intensive_id' => $intensive_id])
            </div>
        </div>
    </div>

@endsection


<style>
    /* Стиль для строк таблицы */
    table tr {
        height: 40px; /* Используйте нужное вам значение высоты */
    }

    /* Стиль для ячеек таблицы, если хотите использовать line-height */
    table td, table th {
        line-height: 40px; /* значение должно совпадать или быть в соотношении с высотой строки */
    }
</style>

<style>
    .popup-container {
        position: relative;
    }

    .popup-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        padding: 10px;
        border: 1px solid #ccc;
        z-index: 1;
        right: -600px; /* Сдвигаем попап влево на 600px */
        max-width: 600px; /* Максимальная ширина попапа */
        word-wrap: break-word; /* Перенос текста на новую строку при необходимости */
        white-space: normal; /* Разрешить автоматический перенос текста */
    }
     .custom-textarea {
         line-height: 1.5;
         background-color: rgba(0, 0, 0, 0);
         width: 250px;
         overflow: hidden; /* Скрытие полос прокрутки */
         resize: none; /* Отключаем возможность изменения размера вручную */
     }

</style>