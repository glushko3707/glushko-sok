@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Создание нового чата интенсива от {{\Illuminate\Support\Carbon::parse($data['date_intensive'])->format('d.m')}} </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('IntensiveChatCreatesStore',[$data ['intensive_id'],$id])}}">
                            @csrf
                            <div class="row mb-3">
                                <label for="chat_name" class="col-md-4 col-form-label text-md-end"> Имя чата </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control"  id="chat_name" name="chat_name" value="{{$data['chat_name'] ?? ""}} {{\Illuminate\Support\Carbon::parse($data['date_intensive'])->format('d.m')}}">
                                        <input class="form-control" type="hidden" name="chat_name_id" value="{{$data['chat_name_id'] ?? ""}}">
                                    </div>
                                    @if ($errors->has('date_intensive'))
                                        <div class="error" style="color: red;">
                                            {{ $errors->first('date_intensive') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-3">

                                <label for="chat_id" class="col-md-4 col-form-label text-md-end"> Чат ID</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="chat_id" name="chat_id">
                                    </div>
                                    @if ($errors->has('chat_id'))
                                        <div class="error" style="color: red;">
                                            {{ $errors->first('chat_id') }}
                                        </div>
                                    @endif
                                </div>
                            </div>




                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary" style="width: 150px">
                                    Сохранить
                                </button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
