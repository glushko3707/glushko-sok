@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Создание нового интенсива </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('IntensiveCreatesStore',$id)}}">
                            @csrf
                            <div class="row mb-3">
                                <label for="date_intensive" class="col-md-4 col-form-label text-md-end"> Дата начала </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" type='date' id="date_intensive" name="date_intensive" value="{{$data ['date_intensive'] ?? ""}}">
                                    </div>
                                    @if ($errors->has('date_intensive'))
                                        <div class="error" style="color: red;">
                                            {{ $errors->first('date_intensive') }}
                                        </div>
                                    @endif
                                </div>
                            </div>

                            <div class="row mb-3">

                                <label for="main_chat_id" class="col-md-4 col-form-label text-md-end"> Главный чат ID</label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="main_chat_id" name="main_chat_id">
                                    </div>
                                    @if ($errors->has('main_chat_id'))
                                        <div class="error" style="color: red;">
                                            {{ $errors->first('main_chat_id') }}
                                        </div>
                                    @endif
                                </div>
                            </div>




                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary" style="width: 150px">
                                    Сохранить
                                </button>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
