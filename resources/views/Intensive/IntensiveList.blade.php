@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            @livewire('intensive.intensive-list', [
                         'id' => $id,
                                        ])
            </div>
        </div>
    </div>

@endsection
