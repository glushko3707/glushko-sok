<form action="{{route('zvonobotKey.store',$id)}}" method="GET">
    <div class="row mb-3">
        <label for="zvonobot_key" class="col-md-4 col-form-label text-md-end"> Секретный ключ Zvonobot
            <span class="tooltip_2">
                                                     <span class="tooltip_link">Где взять ключ? <a href="https://lk.zvonobot.ru/panel/api" target="_blank" rel="noopener"> ПЕРЕЙТИ</a></span>
                                                     <ion-icon name="help-circle-outline"></ion-icon>
                                            </span>
        </label>
        <div class="col-md-6">
            <input class="form-control" id="zvonobot_key" style="margin-bottom: 18px" value="{{$notificationSetting['zvonobot_key'] ?? ""}}" name="zvonobot_key" >
        </div>
    </div>
    <div style="text-align: center">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
</form>
