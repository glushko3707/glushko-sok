<form action="{{route('bizonApiSetting.store',$id)}}" method="POST">

    <div class="row mb-3">
        <label for="bizon" class="col-md-4 col-form-label text-md-end"> Bizon API-КЛЮЧ
            <span class="tooltip_2">
                     <span class="tooltip_link">Где взять ключ? <a href="https://start.bizon365.ru/admin/users?ui=1" target="_blank" rel="noopener"> ПЕРЕЙТИ</a></span>
                     <ion-icon name="help-circle-outline"></ion-icon>
            </span>
        </label>
        <div class="col-md-6">
            <input class="form-control" id="bizon_key" style="margin-bottom: 18px" value="{{$notificationSetting['bizon_key'] ?? ""}}" name="bizon_key" >
        </div>
    </div>
    <div class="row mb-3">
        <label for="emailSmsAero" class="col-md-4 col-form-label text-md-end"> Адрес вебхука после вебинара Bizon </label>
        <div class="col-md-6">
            <div class="input-group">
                <p class="form-control copy-text" style="font-size: 14px">https://glushko-sok.ru/bizon/after/{{$id}}</p>
                <span class="input-group-append">
                <button type="button" class="btn btn-light copy-button">Копировать</button>
            </span>
            </div>
        </div>
    </div>


    <div style="text-align: center">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
</form>
