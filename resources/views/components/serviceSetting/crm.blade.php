<div>
    @livewire('Crm.Crm-setting',[
                           'id' => $id,
                                          ])

    <ul class="nav nav-pills mb-4 flex-wrap justify-content-center" id="whatsUpSubTab" role="tablist">
        <li class="nav-item mr-2">
            <a class="nav-link active" id="pipeline-tab" data-toggle="tab" href="#pipelineSetting" role="tab" aria-controls="waba" aria-selected="true">Настройка этапов</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="crmSetting-tab" data-toggle="tab" href="#crmSetting" role="tab" aria-controls="whatsMonster" aria-selected="false">Настройка подключения</a>
        </li>
    </ul>
    <div class="tab-content" id="whatsUpSubTabContent">
        <div class="tab-pane fade show active" id="pipelineSetting" role="tabpanel" aria-labelledby="pipeline-tab">
           <a href="{{route('pipelineSetting.index', $id)}}" class="btn btn-outline-primary"> Перейти к настройкам этапов </a>
        </div>
        <div class="tab-pane fade" id="crmSetting" role="tabpanel" aria-labelledby="crmSetting-tab">
            <div>
                <script
                        class="kommo_oauth"
                        charset="utf-8"
                        data-client-id="{{$notificationSetting->CrmSettings['clientIdAmo'] ?? ''}}"
                        data-title="Установить связь"
                        data-compact="false"
                        data-class-name="className"
                        data-theme="light"
                        data-locale="en"
                        data-state="state"
                        data-error-callback="functionName"
                        data-mode="popup"
                        src="https://www.kommo.com/auth/button.js"
                ></script>
            </div>
        </div>
    </div>




</div>
