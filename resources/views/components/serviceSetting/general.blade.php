<form action="{{route('autoWebActive.store',$id)}}" method="POST">
    @csrf
    <div class="row">
        <label for="autoWeb" class="col-md-4 col-form-label text-md-end"> Режим автовеб</label>
        <div class="col-md-6 d-flex align-items-center">
            <div class="form-check form-switch d-flex align-items-center">
                <input onclick="this.form.submit()" class="form-check-input ml-0 mt-0" {{ $notificationSetting['autoWeb'] == 1 ? 'checked' : '' }} style="width: 40px;height: 20px"  type="checkbox" id="autoWeb" name="autoWeb">
            </div>
        </div>
    </div>
</form>
