<div class="card-body table-responsive p-0" style="max-height: 450px;">
    <table class="table table-head-fixed table-sm table-bordered text-nowrap">
        <thead>
        <tr>
            <th style="max-width: 300px" rowspan="2">Название рекламных компаний</th>
            <th rowspan="2">Расход</th>
            <th colspan="2" style="text-align: center">Лиды</th>
            <th colspan="4" style="text-align: center">Вебинар</th>
            <th colspan="3" style="text-align: center">Оплаты трипваева</th>
            <th colspan="2" style="text-align: center">Основной курс</th>
        </tr>
        <tr>
            <th class="thCoolspan2">кол-во</th>
            <th class="thCoolspan2">цена</th>
            <th class="thCoolspan2">кол-во</th>
            <th class="thCoolspan2">цена</th>
            <th class="thCoolspan2">доходимость %</th>
            <th class="thCoolspan2">Горячих %</th>
            <th class="thCoolspan2">кол-во</th>
            <th class="thCoolspan2">цена</th>
            <th class="thCoolspan2">%</th>
            <th class="thCoolspan2">кол-во</th>
            <th class="thCoolspan2">цена</th>
        </tr>
        </thead>
        <tbody>
        @foreach($dayWebStatistice as $key => $accaunt)
            <tr class="clickable" data-toggle="{{$accaunt['account_name']}}">
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div class="d-flex">
                            <div class="mr-1">{{$accaunt['account_name']}}</div> <div class="text-sm"> ({{$key}})</div>
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div>
                            {{round($accaunt['spend'])}} ₽
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{$accaunt['lead']}}
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{round($accaunt['leadPrice']) ?? 0}} ₽
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{$accaunt['webinar']}}
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{round($accaunt['webinarPrice']) ?? 0}} ₽
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{round($accaunt['webinarPercent']) ?? 0}}
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{round($accaunt['percentHot']) ?? 0}}
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{$accaunt['payment']}}
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            @if($accaunt['payment'] > 0)
                                {{round($accaunt['spend']/$accaunt['payment'])}}
                            @else
                                0
                            @endif
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            @if($accaunt['webinar'] > 0)
                                {{round($accaunt['payment']/$accaunt['webinar']*100,2)}}
                            @else
                                0
                            @endif
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            {{$accaunt['paymentMain']}}
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-center">
                        <div>
                            @if($accaunt['paymentMain'] > 0)
                                {{round($accaunt['spend']/$accaunt['paymentMain'])}}
                            @else
                                0
                            @endif
                        </div>
                    </div>
                </th>
            </tr>
            @foreach($accaunt['campaign_collect'] as $campaignKey => $campaign)
                <tr class="clickable {{$accaunt['account_name']}}"  data-toggle="{{$campaign['campaign_id']}}" style="display: none;background: #e0f9ff">
                    <th style="font-weight: normal;padding-left: 50px;overflow: hidden;max-width: 200px">
                        <div class="d-flex justify-content-start">
                            <div class="d-flex">
                                <div class="mr-1">{{$campaign['campaign_name']}}</div>
                                <div class="text-sm">
                                    ({{$campaign['campaign_id']}})
                                </div>
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-start">
                            <div>
                                {{round($campaign['spend'])}} ₽
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{$campaign['lead']}}
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{round($campaign['leadPrice']) ?? 0}} ₽
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{$campaign['webinar']}}
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{round($campaign['webinarPrice']) ?? 0}} ₽
                            </div>
                        </div>
                    </th>

                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{round($campaign['webinarPercent']) ?? 0}}
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{round($campaign['percentHot']) ?? 0}}
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{$campaign['payment']}}
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                @if($campaign['payment'] > 0)
                                    {{round($campaign['spend']/$campaign['payment'])}}
                                @else
                                    0
                                @endif
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                @if($campaign['webinar'] > 0)
                                    {{round($campaign['payment']/$campaign['webinar']*100,2)}}
                                @else
                                    0
                                @endif
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                {{$campaign['paymentMain']}}
                            </div>
                        </div>
                    </th>
                    <th style="font-weight: normal">
                        <div class="d-flex justify-content-center">
                            <div>
                                @if($campaign['paymentMain'] > 0)
                                    {{round($campaign['spend']/$campaign['paymentMain'])}}
                                @else
                                    0
                                @endif
                            </div>
                        </div>
                    </th>
                </tr>
                @foreach($campaign['adset_collect'] as $adsetKey => $adset)
                    <tr class="clickable {{$campaign['campaign_id']}}" data-toggle="{{$adset['adset_id']}}" style="display: none;background: #edffeb">
                        <th style="font-weight: normal; padding-left: 70px">
                            <div class="d-flex justify-content-start">
                                <div class="d-flex">
                                    <div class="mr-1">{{$adset['adset_name']}}</div> <div class="text-sm">({{$adset['adset_id']}})</div>
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-start">
                                <div>
                                    {{round($adset['spend'])}} ₽
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{$adset['lead']}}
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{round($adset['leadPrice']) ?? 0}} ₽
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{$adset['webinar']}}
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{round($adset['webinarPrice']) ?? 0}} ₽
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{round($adset['webinarPercent']) ?? 0}}
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{round($adset['percentHot']) ?? 0}}
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{$adset['payment']}}
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    @if($adset['payment'] > 0)
                                        {{round($adset['spend']/$adset['payment'])}}
                                    @else
                                        0
                                    @endif
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    @if($adset['webinar'] > 0)
                                        {{round($adset['payment']/$adset['webinar']*100),2}}
                                    @else
                                        0
                                    @endif
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    {{$adset['paymentMain']}}
                                </div>
                            </div>
                        </th>
                        <th style="font-weight: normal">
                            <div class="d-flex justify-content-center">
                                <div>
                                    @if($adset['paymentMain'] > 0)
                                        {{round($adset['spend']/$adset['paymentMain'])}}
                                    @else
                                        0
                                    @endif
                                </div>
                            </div>
                        </th>
                    </tr>
                    @foreach($adset['ad_collect'] as $adKey => $ad)
                        <tr class="clickable {{$adset['adset_id']}}" style="display: none">
                            <th style="font-weight: normal; padding-left: 100px">
                                <div class="d-flex justify-content-start">
                                    <div class="d-flex">
                                        <div class="mr-1"> {{$ad['ad_id']}}</div> <div class="text-sm"> ({{$ad['ad_id']}})</div>
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-start">
                                    <div>
                                        {{$ad['spend']}} ₽
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{$ad['lead']}}
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{round($ad['leadPrice']) ?? 0}} ₽
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{$ad['webinar']}}
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{round($ad['webinarPrice']) ?? 0}} ₽
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{round($ad['webinarPercent']) ?? 0}}
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{round($ad['percentHot'],2) ?? 0}}
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{$ad['payment']}}
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        @if($ad['payment'] > 0)
                                            {{round($ad['spend']/$ad['payment'])}}
                                        @else
                                            0
                                        @endif
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        @if($ad['webinar'] > 0)
                                            {{round($ad['payment']/$ad['webinar'])}}
                                        @else
                                            0
                                        @endif
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        {{$ad['paymentMain']}}
                                    </div>
                                </div>
                            </th>
                            <th style="font-weight: normal">
                                <div class="d-flex justify-content-center">
                                    <div>
                                        @if($ad['paymentMain'] > 0)
                                            {{round($ad['spend']/$ad['paymentMain'])}}
                                        @else
                                            0
                                        @endif
                                    </div>
                                </div>
                            </th>
                        </tr>
                    @endforeach
                @endforeach
            @endforeach
        @endforeach
        </tbody>
        <tfoot style="background: #ffffff;font-weight: bold;position: sticky; bottom: 0;">
        <td>
            Всего
        </td>
        <td>
            {{round($statistica['spend']) ?? ''}} ₽
        </td>
        <td style="text-align: center">
            {{$statistica['lead'] ?? ''}}
        </td>
        <td style="text-align: center">
            {{$statistica['leadPrice'] ?? ''}} ₽
        </td>
        <td style="text-align: center">
            {{$statistica['webinar'] ?? ''}}
        </td>
        <td style="text-align: center">
            {{$statistica['webinarPrice'] ?? ''}} ₽
        </td>
        <td style="text-align: center;">
            {{$statistica['webinarReach'] ?? ''}}
        </td>
        <td style="text-align: center;">
            {{$statistica['percentHot'] ?? ''}}
        </td>
        <td style="text-align: center;">
            {{$statistica['payment'] ?? ''}}
        </td>
        <td style="text-align: center;">
            {{$statistica['paymentPrice'] ?? ''}} ₽
        </td>
        <td style="text-align: center">
            {{$statistica['paymentPercent'] ?? ''}}
        </td>
        <td style="text-align: center">
            {{$statistica['paymentMain'] ?? ''}}
        </td>
        <td style="text-align: center">
            {{$statistica['paymentMainPrice'] ?? ''}} ₽
        </td>
        </tfoot>
    </table>
</div>
