<div class="card-body table-responsive p-0" style="max-height: 450px;">
    <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">
        <thead>
        <tr>
            <th width="15%">Дата</th>
            <th width="8%">Расходы</th>
            <th width="6%">Лиды</th>
            <th width="6%">Цена лида</th>
        </tr>
        </thead>
        <tbody>
        @foreach($statDays as $key => $day)
            <tr >
                    <th style="font-weight: normal">
                        {{$key}}
                    </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div>
                            {{round($day['rashod'])}} ₽
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div>
                            {{round($day['newleads'])}}
                        </div>
                    </div>                                                        </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div>
                            {{round($day['leadPrice'])}} ₽
                        </div>
                    </div>
                </th>
            </tr>
        @endforeach
        </tbody>
        <tfoot style="background: #ffffff;font-weight: bold;position: sticky; bottom: 0px;">
        <td>
            Всего
        </td>
        <td class="rub">
            {{$allDays['rashod'] ?? ''}}
        </td>
        <td>
            {{$allDays['newleads'] ?? ''}}
        </td>
        <td class="rub">
            {{$allDays['leadPrice'] ?? ''}}
        </td>
        </tfoot>
    </table>
</div>
