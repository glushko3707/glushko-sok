<div class="card-body table-responsive p-0" style="max-height: 450px;">
    <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">
        <thead>
        <tr>
            <th width="15%">ID</th>
            <th width="8%">Расходы</th>
            <th width="6%">Лиды</th>
            <th width="6%">Цена лида</th>
            <th width="12%">Номер телефона</th>
            <th width="15%">Оффер</th>
            <th>Комментарий</th>
            <th width="3%"></th>
        </tr>
        </thead>
        <tbody>
        @foreach($stat as $key => $day)
            <tr>
                    <th onclick="copys(this)" style="font-weight: normal;cursor: pointer;color: rgb(139 92 246)">
                        <div class="d-flex">
                            <div id="textCopy"> {{ substr($key, 1) }} </div>  <div> <ion-icon class="ml-2" style="cursor: pointer;" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="getCall(this,{{$day['value']}})" name="cloud-upload-outline"></ion-icon></div>
                        </div>
                    </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div>
                            {{round($day['rashod'])}} ₽
                        </div>
                    </div>
                </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div>
                            {{round($day['newleads'])}}
                        </div>
                    </div>                                                        </th>
                <th style="font-weight: normal">
                    <div class="d-flex justify-content-start">
                        <div>
                            {{round($day['leadPrice'])}} ₽
                        </div>
                    </div>
                </th>
                    <th style="font-weight: normal; font-size: 14px">
                        <input id="phone{{$day['value']}}" style="width: 100%" onblur="updateRow(this,{{$day['value']}})"  type="text"  class="input-opacity" name="phone" value="{{$day['phone'] ?? ''}}">
                    </th>
                    <th style="font-weight: normal; font-size: 14px">
                        <input id="offers{{$day['value']}}" onblur="updateOffer(this,{{$day['value']}})" list="options" style="width: 100%"  type="text"  class="input-opacity" name="offers" value="{{$day['offer'] ?? ''}}">
                        <datalist id="options">
                            @foreach($offers as $offer)
                                <option value="{{$offer}}">
                            @endforeach
                        </datalist>
                    </th>

                    <th style="font-weight: normal; font-size: 14px">
                        <input id="timeProgram{{$day['value']}}" style="width: 100%" onblur="updateRow(this,{{$day['value']}})"  type="text"  class="input-opacity" name="comment" value="{{$day['comment'] ?? ''}}">
                    </th>

                    <th style="font-weight: normal">
                        <div>
                            <form action="{{route('callId.delete',$id)}}" method="POST">
                                @csrf @method('DELETE')
                                <input type="hidden" name="step_id" value="{{$day['value']}}">
                                <button type="submit">
                                    <i class="fas fa-times ml-2" style="color: red"></i>
                                </button>
                            </form>
                        </div>
                    </th>
            </tr>
        @endforeach
        </tbody>
        <tfoot style="background: #ffffff;font-weight: bold;position: sticky; bottom: 0;">
        <td>
            Всего
        </td>
        <td class="rub">
            {{$all['rashod'] ?? ''}}
        </td>
        <td>
            {{$all['newleads'] ?? ''}}
        </td>
        <td class="rub">
            {{$all['leadPrice'] ?? ''}}
        </td>
            <td>
            </td>
            <td>
            </td>
        </tfoot>
    </table>
</div>


<script>
    function copys(element) {
        // Находим элемент с id textCopy внутри нажатого <th>
        const textElement = element.querySelector('#textCopy');

        if (textElement) {
            // Получаем текст из элемента
            const text = textElement.innerText;

            // Создаем временный элемент
            const tempInput = document.createElement('input');
            tempInput.style.position = 'absolute';
            tempInput.style.left = '-9999px';
            tempInput.value = text;
            document.body.appendChild(tempInput);

            // Выделяем и копируем текст из временного элемента
            tempInput.select();
            document.execCommand('copy');

            // Удаляем временный элемент после копирования
            document.body.removeChild(tempInput);

            // Для подтверждения копирования
            console.log('Скопировано в буфер обмена: ', text);
        }
    }
</script>