@props(['value'])


{{--<label {{ $attributes->merge(['class' => 'block font-medium text-sm text-gray-700']) }}>--}}
{{--    {{ $value ?? $slot }}--}}
{{--</label>--}}


{{--@props(['required'=>false])--}}

{{--<label {{ $attributes->class([--}}
{{--    ($required ? "required" : ''),--}}
{{--]) }}>--}}
{{--    {{ $slot }}--}}
{{--</label>--}}




@props(['required'=>false])

<label {{ $attributes->class([
    ($required ? "required" : ''),
]) }}>
{{ $value ?? $slot }}
</label>
