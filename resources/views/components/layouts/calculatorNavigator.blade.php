<nav x-data="{ open: false }" class="bg-white border-b border-gray-100">
    <!-- Primary Navigation Menu -->
    <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex justify-between h-16">
            <div class="flex">

                <div class="hidden space-x-8 sm:-my-px sm:ml-10 sm:flex">
                    <x-nav-link :href="route('calculator.diagnostic')" :active="request()->routeIs('calculator.diagnostic')">
                        {{ __('Диагностика') }}
                    </x-nav-link>

                    <x-nav-link :href="route('calculator.webinar')" :active="request()->routeIs('calculator.webinar')">
                        {{ __('Вебинар') }}
                    </x-nav-link>

                    <x-nav-link :href="route('calculator.threeDay')" :active="request()->routeIs('calculator.threeDay')">
                        {{ __('Трехдневник') }}
                    </x-nav-link>
                </div>
            </div>
            <!-- Hamburger -->
        </div>
    </div>

    <!-- Responsive Navigation Menu -->
</nav>
