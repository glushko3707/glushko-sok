<input {{ $attributes->class([
    'form-control', 'mb-3'
])->merge([
    'type' => 'text',
]) }}>
