@props(['value'])

<label {!! $attributes->merge(['class' => '', 'style' => 'font-weight: 400']) !!}>
    {{ $value ?? $slot }}
</label>