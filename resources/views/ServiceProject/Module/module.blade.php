@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Модули</h4>
                    </div>

                    <div class="card-body">

                        <div class="row mb-3 d-flex align-items-center">
                            <label for="sipOut" class="col-md-4 col-form-label text-md-end"> Аналитика трафика </label>
                            <div class="col-md-6 d-flex">
                                <div>
                                    <input type="checkbox"  checked class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px" disabled >
                                </div>
                                <div>
                                    <div class="ml-3"> 499 руб. в месяц </div>
                                </div>
                            </div>
                        </div>

                        @foreach($modules as $module)
                            <form action="{{route('serviceModule.store',$id)}}" method="post">
                                @csrf @method('PATCH')
                                    <div class="row mb-3 d-flex align-items-center">
                                        <label class="col-md-4 col-form-label text-md-end"> {{$module->name}} </label>
                                        <div class="col-md-6 d-flex">
                                            <div>
                                                <input onchange="this.form.submit()" type="checkbox" {{ $module['active'] == true ? 'checked' : '' }} class="form-check-input ml-0 mt-0 position-relative" style="width: 25px;height: 25px" name="value" >
                                                <input type="hidden" value="{{$module->tag}}" name="tag" >
                                            </div>
                                            <div class="d-flex">
                                                <div class="ml-3"> {{$module->price}} руб. в месяц </div>
                                                @if($module->availible == true)
                                                <div class="ml-3"> (не требует доплат) </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                            </form>
                        @endforeach
                    </div>
                    <div class="row mb-3 d-flex align-items-center">
                        <div class="col-md-4 col-form-label text-md-end">
                            <h4>Общая сумма:</h4>
                        </div>
                        <div class="col-md-6 d-flex">
                            <h4>{{$sum}} рублей</h4>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>




    <script>
        var copyButton = document.getElementById('copyButton');
        var paragraph = document.getElementById('paragraph');


        copyButton.addEventListener('click', function() {

            // Используем библиотеку clipboard.js для копирования текста в буфер обмена
            var temp = document.createElement("textarea");
            document.body.appendChild(temp);
            temp.value = paragraph.innerText;
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);


            // Визуальное подтверждение копирования
            copyButton.innerText = 'Скопировано!';
            setTimeout(function(){
                copyButton.innerText = 'Скопировать';
            }, 2000);
        });
    </script>



@endsection
