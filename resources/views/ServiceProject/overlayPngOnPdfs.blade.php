@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div>
                    <h3 class="mb-5">Поставить подписи на документах</h3>
                    <form action="{{route('sipoutDocument.store',['id' => $id])}}" method="post" enctype="multipart/form-data">
                        @csrf

                        @if(!$signaturePath['signature'])
                            <div class="col-6">
                                <label style="font-weight: 400" for="signature"><b>Добавьте подпись</b> (png без фона 300x300 px):</label>
                                <input class="form-control" type="file" id="signature" name="signature" accept=".png" >
                            </div>
                        @endif

                        @if(!$signaturePath['signatureName'])
                            <div class="mt-3 col-6">
                                <label style="font-weight: 400" for="signatureName"><b>Добавьте расшифровку</b> (png без фона 100x100 px):</label>
                                <input class="form-control" type="file" id="signatureName" name="signatureName" accept=".png" >
                            </div>
                        @endif

                        @if(!empty($signaturePath['signatureName']) && !empty($signaturePath['signature']))
                        <div class="col-6">
                                <label for="pdfs">Выберите несколько PDF договоров:</label>
                                <input class="form-control" type="file" id="pdfs" name="pdfs[]" accept=".pdf" multiple >
                            </div>
                        @endif
                        <!-- Кнопка отправки формы -->
                        <div class="col-6">
                            <button style="width: 100%" class="btn btn-primary mt-3 mb-3" type="submit"> Загрузить </button>
                        </div>
                    </form>
                </div>

                @if(count($contactDownload) > 0)
                    @foreach($contactDownload as $step)
                        <div class="mb-2" >
                            <div class="row" id="step-{{$loop->index}}">
                                <div class="col"> {{$step}}</div>
                                <div class="col">
                                    <div class="d-flex justify-content-end">
                                        <div class="mr-2">
                                            <form id="form-{{$loop->index}}" action="{{ route('getPDFFile.store',$id) }}" method="POST">
                                                @csrf
                                                <input type="hidden" value="{{$step}}" class="form-control" name="fileName">
                                                <button type="button" class="btn btn-primary" onclick="handleDownload(event, 'step-{{$loop->index}}', 'form-{{$loop->index}}')">
                                                    Скачать базу
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <script>
                        function handleDownload(event, stepId, formId) {
                            event.preventDefault();
                            let stepElement = document.getElementById(stepId);
                            if (stepElement) {
                                stepElement.style.display = 'none'; // Скрываем элемент
                            }
                            document.getElementById(formId).submit(); // Отправляем форму
                        }
                    </script>
                @endif

            </div>
        </div>
    </div>



@endsection
