<x-noreg-layout>


    <div id="app">
        <diagnostic-component>

        </diagnostic-component>
    </div>

</x-noreg-layout>

<script>
    import DiagnosticComponent from "@/Components/Calculator/DiagnosticComponent";
    export default {
        components: {DiagnosticComponent}
    }
</script>