<x-noreg-layout>


    <div id="app">
        <webinar-component>
        </webinar-component>
    </div>

</x-noreg-layout>

<script>
    import WebinarComponent from "@/Components/Calculator/WebinarComponent";
    export default {
        components: {WebinarComponent}
    }
</script>