@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header text-center">
                        <h4>Оплата сервиса</h4>
                    </div>

                    <div class="card-body">


                    </div>


                    <div class="row mb-1 d-flex align-items-center">
                        <div class="col-md-6 col-form-label text-md-end">
                            <h5>Общая сумма:</h5>
                        </div>
                        <div class="col-md-6 d-flex">
                            <h5>{{$sum}} рублей</h5>
                        </div>

                    </div>
                    <div class="d-flex mb-3 justify-content-center">
                        <div>
                            <a href="{{$confirmationUrl}}" class="btn btn-primary"> Перейти к оплате</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@endsection
