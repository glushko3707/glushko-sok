@extends('test.noAuth')

@section('content')

    <div class="content">

        <div id="container">
            <div id="element1" class="element">Element 1</div>
            <div id="element2" class="element">Element 2</div>
            <div id="element3" class="element">Element 3</div>
        </div>
    </div>

    <style>
        #container {
            position: relative;
            width: 800px;
            height: 600px;
            border: 1px solid #ccc;
        }

        .element {
            width: 150px;
            height: 100px;
            border: 1px solid #000;
            position: absolute;
            cursor: move;
        }
    </style>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            // Инициализация jsPlumb
            var instance = jsPlumb.getInstance({
                Connector: "Flowchart",
                Endpoint: ["Dot", { radius: 5 }],
                EndpointStyle: { fill: "#456" },
                PaintStyle: { stroke: "#456", strokeWidth: 2 },
                Container: "container"
            });

            // Определение элементов
            var elements = ["element1", "element2", "element3"];

            // Делаем элементы draggable
            elements.forEach(function(elementId) {
                interact('#' + elementId).draggable({
                    onmove: function(event) {
                        var target = event.target;
                        var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx;
                        var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

                        target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';
                        target.setAttribute('data-x', x);
                        target.setAttribute('data-y', y);

                        instance.repaintEverything();
                    }
                });

                // Устанавливаем точки подключения
                instance.addEndpoint(elementId, {
                    anchors: ["RightMiddle"],
                    isSource: true,
                    maxConnections: -1
                });
                instance.addEndpoint(elementId, {
                    anchors: ["LeftMiddle"],
                    isTarget: true,
                    maxConnections: -1
                });
            });

            // Соединяем элементы
            instance.connect({ source: "element1", target: "element2" });
            instance.connect({ source: "element2", target: "element3" });
        });
    </script>
@endsection
