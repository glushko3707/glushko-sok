<x-app-layout>


    <div style="margin-right: auto;margin-left: auto;max-width: 70%;">
    <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header text-center">
                        <h4 class="p-3 text-2xl font-semibold">Оплата сервиса</h4>
                    </div>

                    <div class="row mt-3 p-3 d-flex text-center">
                        <h5>Тариф активен <b>до {{ \Illuminate\Support\Carbon::parse(auth()->user()->expire)->format('d-m-y') }}</b> чтобы продлить работу сервиса оплатите тариф</h5>
                    </div>

                    <div class="d-flex p-4 justify-content-center">
                        <div>
                            <a href="{{$confirmationUrl ?? ''}}" class="btn btn-primary"> Перейти к оплате</a>
                        </div>
                    </div>

                    <div class="row mb-5 d-flex text-center text-xl">
                            <h5>Общая сумма к оплате: {{$sum ?? 0}} рублей</h5>
                    </div>

                </div>

            </div>

        <div class="d-flex mt-4 justify-content-center">
            <form class="form-inline" action="{{route('promoCodeActive.store')}}">
                <div data-element="fields" data-stacked="false" class="flex items-center w-full max-w-md mb-3 seva-fields formkit-fields">
                    <div class="relative w-full mr-3 formkit-field">
                        <input id="promocode" class="formkit-input bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" name="promocode" aria-label="promocode" placeholder="Промокод" required="" type="text">
                    </div>
                    <button data-element="submit" class="formkit-submit">
                        <span style="padding-top: 0.6rem;padding-bottom: 0.6rem;background-color: #e5e5e5;" class="px-5 border-2 text-sm text-center text-black rounded-lg cursor-pointer hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Активировать</span>
                    </button>
                </div>
            </form>
        </div>
        @if ($errors->any())
            <div class="text-center">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </div>
        @endif

        </div>

    </div>

</x-app-layout>
