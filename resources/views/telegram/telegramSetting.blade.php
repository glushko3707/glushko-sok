@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Настройки Telegram </div>

                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger d-flex justify-content-center" role="alert">
                                @foreach($errors->all() as $error)
                                    <h5>{{ $error }}</h5>
                                @endforeach
                            </div>
                        @endif

                        @if(isset($telegramSetting['sheet_id']))

                            <div class="row mb-3">
                                <label for="sheet" class="col-md-4 col-form-label text-md-end"> Таблица с заявками </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="sheet" value="{{$telegramSetting['sheet_id'] ?? ""}}" disabled >
                                        <span class="input-group-append">
                                        <a href="{{"https://docs.google.com/spreadsheets/d/" . $telegramSetting['sheet_id'] .'/edit#gid=0' }}" class="btn btn-outline-light active" aria-current="page">Перейти</a>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        @endif


                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        var copyButton = document.getElementById('copyButton');
        var paragraph = document.getElementById('paragraph');


        copyButton.addEventListener('click', function() {

            // Используем библиотеку clipboard.js для копирования текста в буфер обмена
            var temp = document.createElement("textarea");
            document.body.appendChild(temp);
            temp.value = paragraph.innerText;
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);


            // Визуальное подтверждение копирования
            copyButton.innerText = 'Скопировано!';
            setTimeout(function(){
                copyButton.innerText = 'Скопировать';
            }, 2000);
        });
    </script>



@endsection
