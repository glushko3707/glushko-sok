@extends('adminPanel.startAddProject')

@section('content')
    <div>
    <script>
        // Получаем хеш-фрагмент из URL
        var hash = window.location.hash.substring(1);

        // Преобразуем хеш-фрагмент в объект
        var params = new URLSearchParams(hash);

        // Создаем URL для перенаправления
        var redirectUrl = "{{route('oauthRedirectHandle',$id)}}?" + params.toString();


        // Перенаправляем на новый URL
        window.location.href = redirectUrl;
    </script>
</div>

@endsection
