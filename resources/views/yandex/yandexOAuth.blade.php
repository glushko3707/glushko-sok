@extends('adminPanel.startAddProject')

@section('content')

    <script src="https://yastatic.net/s3/passport-sdk/autofill/v1/sdk-suggest-with-polyfills-latest.js"></script>

    <script>

        window.onload = function() {
            window.YaAuthSuggest.init({
                    client_id: "{!! $client_id !!}",
                    response_type: 'token',
                    redirect_uri: "{!! $redirect_uri !!}"
                },
                'https://examplesite.com', {
                    view: 'button',
                    parentId: 'container',
                    buttonView: 'main',
                    buttonTheme: 'light',
                    buttonSize: 'm',
                    buttonBorderRadius: 0
                }
            )
                .then(function(result) {
                    return result.handler()
                })
                .then(function(data) {
                    console.log('Сообщение с токеном: ', data);
                    document.body.innerHTML += `Сообщение с токеном: ${JSON.stringify(data)}`;
                })
                .catch(function(error) {
                    console.log('Что-то пошло не так: ', error);
                    document.body.innerHTML += `Что-то пошло не так: ${JSON.stringify(error)}`;
                });
        };
    </script>

@endsection
