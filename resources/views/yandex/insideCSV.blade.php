@extends('adminPanel.startAddProject')

@section('content')

   <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid ">
                <div class="d-flex justify-content-center">
                    <div class="col-9 col mt-5">
                   <div class="card">

                    <div class="card-body">
                        <div class="d-flex justify-content-center">
                            <div class="col-9">
                                <form action="{{route('getReportCSV.store',$id)}}" enctype="multipart/form-data" METHOD="POST">

                                    <div class="row mb-3">
                                            <label for="phoneWeb" class="col-md-4 col-form-label text-md-end"> Вставьте отчет</label>
                                        <div class="col-md-6">
                                            <input id="report" accept=".csv" multiple type="file" class="form-control" name="report" autofocus>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <label for="accaunt_name" class="col-md-4 col-form-label text-md-end"> Название аккаунта</label>
                                        <div class="col-md-6">
                                            <input id="accaunt_name" list="options"  type="text" class="form-control" name="accaunt_name" autofocus>
                                            <datalist id="options">
                                                @foreach($getAccounts as $accounts)
                                                    <option value="{{$accounts}}">
                                                @endforeach
                                            </datalist>
                                        </div>
                                    </div>
                                    <div class="d-flex justify-content-center">
                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </div>
                                </form>
                            </div>

                        </div>

                </div>
                </div>
                </div>
                </div>
            </div>
        </div>

@endsection
