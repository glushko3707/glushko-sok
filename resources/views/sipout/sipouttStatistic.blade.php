@extends('adminPanel.startAddProject')

@section('content')
    @push('title')
       Kupr | Статистика SipOut
    @endpush


    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid col-12 col-md-11">
                <div class="row">
                    <div class="col" style="text-align: center">
                        <h3> Статистика по лидам проекта {{$project['name']}}</h3>
                    </div><!-- /.col -->
                </div>
                <div class="row">
                    <div class="col-6">
                         <span class="input-group-append">
                                <button type="button" id="copyButton" class="btn btn-success">Webhook</button>
                                <button type="button" id="copyButtonStop" class="btn btn-danger ml-1">Stop List</button>
                         </span>
                    </div>
                    <div class="col-6" style="display: flex; justify-content: flex-end;">
                            <a href="{{route('sipoutStatisticUpdate.index',$id)}}" class="btn btnSok">Обновить</a>
                            <form method="post"  action="{{route('sipoutStatisticUploadReport.index',$id)}}" enctype="multipart/form-data">
                                @csrf
                                <label class="input-file">
                                    <input onchange="this.form.submit()"  type="file" accept=".csv" name="csv_file">
                                    <span>Загрузить отчет</span>
                                </label>
                            </form>
                    </div>
                </div>
                @livewire('sipout.statistica', [
               'id' => $id,
                              ])
            </div>
            <!-- /.content -->
        </div>
    </div>


    <div class="modal fade"  id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered h-auto modal-xl modal-dialog-scrollable" style="max-width: 800px">
            <div class="modal-content"style="background: #ffffff">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Добавить номера в обзвон &nbsp</h5> <h5 class="modal-title" id="id_call"> ID</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div style="display: flex;">
                        <div class="card-body table-responsive p-0" style="max-height: 300px;">
                            <table class="table table-head-fixed table-bordered text-nowrap">

                                <thead>
                                <tr>
                                    <th style="width: 250px">Категория</th>
                                    <th style="width: 200px">Осталось номеров</th>
                                    <th style="width: 150px">Номеров добавить</th>
                                    <th style="width: 150px">Действие</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categoryDatabase as $keys)
                                    @if ($keys['count'] - $keys['used_count'] != 0)
                                            <tr >
                                                <td style="max-height:92px; font-weight: normal">
                                                    <div class="d-flex justify-content-start">
                                                        <div>
                                                            {{$keys['category_name']}}
                                                        </div>
                                                    </div>
                                                </td>
                                                <td style="max-height:92px;font-weight: normal">
                                                    <div class="d-flex justify-content-start">
                                                        <div>
                                                            {{$keys['count'] - $keys['used_count']}}
                                                        </div>
                                                    </div>
                                                </td>
                                                <form action="{{route('addSipoutContact.store',$id)}}" METHOD="POST">
                                                    <td style="max-height:92px; font-weight: normal">
                                                        <div class="d-flex justify-content-start">
                                                            <div>
                                                                <input id="phone{{$keys['id']}}" style="width: 100%"  type="number"  class="input-opacity" name="count" value="1000">
                                                                <input style="width: 100%"  type="hidden"  name="category_id" value="{{$keys['id']}}">
                                                                <input style="width: 100%"  type="hidden"  class="call_id_hidden" name="call_id">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td style="max-height:92px; font-weight: normal;width: auto">
                                                        <div class="d-flex justify-content-center">
                                                            <div>
                                                                <button type="submit" class="btn btn-sm btn-success"> Добавить </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </form>

                                            </tr>

                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>




    <script>
        var copyButton = document.getElementById('copyButton');
        var copyButtonStop = document.getElementById('copyButtonStop');

        copyButtonStop.addEventListener('click', function() {

            // Используем библиотеку clipboard.js для копирования текста в буфер обмена
            var temp = document.createElement("textarea");
            document.body.appendChild(temp);
            temp.value = "https://glushko-sok.ru/api/newLeadsSipOut/stopList/{{$id}}?phone=%VAR:CID%&utm_medium=%VAR:DID%";
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);


            // Визуальное подтверждение копирования
            copyButtonStop.innerText = 'Скопировано!';
            setTimeout(function(){
                copyButtonStop.innerText = 'Stop List';
            }, 2000);
        });

        copyButton.addEventListener('click', function() {

            // Используем библиотеку clipboard.js для копирования текста в буфер обмена
            var temp = document.createElement("textarea");
            document.body.appendChild(temp);
            temp.value = "https://glushko-sok.ru/api/newLeadsSipOut/{{$id}}?phone=%VAR:CID%&utm_medium=%VAR:DID%";
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);


            // Визуальное подтверждение копирования
            copyButton.innerText = 'Скопировано!';
            setTimeout(function(){
                copyButton.innerText = 'Webhook';
            }, 2000);
        });
    </script>

    <script>
        function getCurrentDate() {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0');
            var yyyy = today.getFullYear();
            return yyyy + '-' + mm + '-' + dd;
        }

        function addParamsToUrl(url, params) {
            var newUrl = new URL(url);
            for (var key in params) {
                newUrl.searchParams.append(key, params[key]);
            }
            return newUrl.toString();
        }

        var link = document.getElementById("link");

        // Получить текущую дату
        var currentDate = getCurrentDate();

        // Добавить параметры date_from и date_to к url ссылки
        var newUrl = addParamsToUrl(link.href, {date_from: currentDate, date_to: currentDate, period:'phone'});

        // Обновить атрибут href ссылки
        link.href = newUrl;

    </script>

    <script>


        function getCall(callId,stat){
            console.log(callId.textContent)
            $('#id_call').text(callId.textContent)
            $('.call_id_hidden').val(stat)

        }



        function updateRow(row,messege_id,id){
            let text = document.getElementById(row.id).value

            $.ajax({
                url: "/api/sipoutCallIdUpdateComment/" + id ,
                type: 'POST',
                data: {
                    'text': text,
                    'type': row.name,
                    'messege_id': messege_id,
                    'id': id,
                },
                success:function(response){
                }
            });
        }

        function updateOffer(row,call_id){
            let offer_name = document.getElementById(row.id).value
            $.ajax({
                url: "{{ route('sipoutCallIdUpdateOffer.store', ['id' => $id]) }}",
                type: 'POST',
                data: {
                    'offer_name': offer_name,
                    'call_id': call_id,
                },
            });

        }

        function submit(){
            const formed = document.getElementById('formed');
            formed.submit()
        }

    </script>


    <style>
        .btnSok {
            position: relative;
            display: inline-block;
            margin-right: 10px;
            cursor: pointer;
            outline: none;
            font-size: 14px;
            vertical-align: middle;
            color: #ffffff;
            text-align: center;
            font-weight: bold;
            border-radius: 4px;
            background-color: #2c73fd;
            height: 40px;
            padding: 10px 20px;
            box-sizing: border-box;
            border: none;
        }
        .btnSok:hover{
            background-color: #0500a6;
            color: #ffffff;
        }
        .btnSok:active  {
            background-color: #2c73fd;
        }
    </style>
@endsection
