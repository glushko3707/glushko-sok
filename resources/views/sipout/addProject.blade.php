@extends('adminPanel.startAddProject')

@section('content')


    <div class="content-wrapper ">
        <main class="flex flex-grow-1 py-3">
            <section>
                <div class="container  col-6">

                    <div class="card mt-5">
                        <div class="card-header">
                            <div class="d-flex justify-content-between">
                                <div></div>
                                <div><h5>Добавить проект</h5></div>
                                <div>К проектам</div>
                            </div>

                        </div>

                        <div class="card-body">
                            <form method="POST" action="{{ route('createProject.store') }}">
                                @csrf

                                <div class="row mb-4">
                                    <label for="delay" class="col-md-4 col-form-label text-md-end">Имя проекта</label>
                                    <div class="col-md-3">
                                        <input id="name" type="text" class="form-control" name="name">
                                        @if($errors->any())
                                            <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>
                                        @endif
                                    </div>
                                </div>



                                <div class="row mb-0">
                                    <div class="col-md-3 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Отправить запрос
                                        </button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>


@endsection
