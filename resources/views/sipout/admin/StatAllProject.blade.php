@extends('adminPanel.startAddProject')

@section('content')


    <div class="content-wrapper" id="app">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <section class="content">
                <div class="container-fluid col-11 p-0">
                    <div class="container-fluid p-0">
                        <div class="d-flex p-1 justify-content-between">
                            <div>
                                <h3> Статистика по лидам </h3>
                            </div>
                            <form action="{{route('StatAllProject.index',$id,request('period'))}}" id="filterDate" method="GET">
                                <div class="col-12 d-flex p-0 justify-content-center mb-3">
                                    <div class="d-flex">
                                        <label  class="d-flex ml-3 mr-1" style="align-items:center;height: 100%">От</label>
                                        <input class="form-control background-input-transparent" type="date" value="{{request('date_from')}}" name="date_from">
                                    </div>
                                    <div class="d-flex">
                                        <label  class="d-flex ml-3 mr-1" style="align-items:center;height: 100%">До</label>
                                        <input class="form-control background-input-transparent" type="date" value="{{request('date_to')}}" name="date_to">
                                    </div>
                                    <div>
                                        <button type="submit" class="btn ml-3 btn-outline-primary ">Применить</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                            <div class="d-flex justify-content-sm-start">
                                    <div class="col-4">
                                        <!-- Виджет 1: Остаток бюджета -->
                                        <div class="maksCard small-box">
                                            <div class="inner">
                                                <p> Затраты всего </p>
                                                <h1> {{$all ['money'] ?? '0'}} ₽ </h1>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <!-- Виджет 1: Остаток бюджета -->
                                        <div class="maksCard small-box">
                                            <div class="inner">
                                                <p> Лиды </p>
                                                <h1> {{$all ['lead'] ?? '0'}} </h1>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <!-- Виджет 1: Остаток бюджета -->
                                        <div class="maksCard small-box">
                                            <div class="inner">
                                                <p> Стоимость лидов </p>
                                                <h1> {{$all['leadPrice'] ?? '0'}} ₽ </h1>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                    </div>

                    <div class="p-1">
                        <div class="card" style="margin-bottom: 0;box-shadow: unset">
                            <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">
                                    <thead>
                                    <tr>
                                        <th width="15%">Проект</th>
                                        <th>Владелец</th>
                                        <th>Расход</th>
                                        <th>Лиды</th>
                                        <th>Цена лида</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($ads_price as $id => $ads)

                                        <tr >
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-start">
                                                    <div>
                                                        <a href="{{route('sipoutStatistic.index',$id)}}" target="_blank">{{$id}}</a>
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-start">
                                                    <div>
                                                        неизвестно
                                                    </div>
                                                </div>                                                        </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-start">
                                                    <div>
                                                        {{round($ads['money'])}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-start">
                                                    <div>
                                                        {{$ads['lead']}}
                                                    </div>
                                                </div>
                                            </th>
                                            <th style="font-weight: normal">
                                                <div class="d-flex justify-content-start">
                                                    <div>
                                                        {{$ads['LeadPrice']}}
                                                    </div>
                                                </div>
                                            </th>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>

                </div>
            </section>

            <!-- /.content -->
        </div>
    </div>


@endsection
