@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="mb-3">
                <button id="clientLink" class="btn btn-outline-primary"> Ссылка для клиента </button>
            </div>
            @livewire('crm.lead-crm', [
                         'id' => $id,
                                        ])
            </div>
        </div>
    </div>

<script>
function updateLead(info){

 let id = info.getAttribute('data-id')
 let value = info.value
 let methods = info.name
  console.log(value)

    $.ajax({
        url: "/api/crm/lead/update/store/",
        type: 'POST',
        data: {
            'methods' : methods,
            'value': value,
            'id': id,
        },
    });

}
</script>

<script>

    var copyButton = document.getElementById('clientLink');
    copyButton.addEventListener('click', function() {

        // Используем библиотеку clipboard.js для копирования текста в буфер обмена
        var temp = document.createElement("textarea");
        document.body.appendChild(temp);
        var link = 'dasd213c32fwrdasd' + {{$id}} + 'ad3fsdf'
        temp.value = "https://glushko-sok.ru/sipout/leads/client/" + link ;
        temp.select();
        document.execCommand("copy");
        document.body.removeChild(temp);


        // Визуальное подтверждение копирования
        copyButton.innerText = 'Скопировано!';
        setTimeout(function(){
            copyButton.innerText = 'Ссылка для клиента';
        }, 2000);
    });

</script>

@endsection
