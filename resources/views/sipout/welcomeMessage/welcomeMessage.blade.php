@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <div class="row justify-content-center">
            <div class="col-md-10" style="margin-top: 30px">
                <div class="col-lg-3 col-6 p-0">
                    <!-- Виджет 1: Остаток бюджета -->
                    <div class="small-box">
                        <div class="inner">
                            <p> Sms Aero </p>
                            <h4> {{$stat ['sms'] ?? '0'}} ₽ </h4>
                        </div>
                        <div class="icon">
                            <i class="ion ion-speedometer"></i>
                        </div>
                    </div>
                </div>
                <div class="card mt-4">

                    <div class="card-header d-flex justify-content-between">
                        <div class="col-6">
                            <h4>
                            Приветственные SMS
                            </h4>
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            <form action="{{route('firstSmsActive.store',$id)}}" method="POST">
                                @method('PATCH')
                                @csrf
                                    <div class="ml-3">
                                        <div class="mb-2 form-check form-switch" style="justify-content: left;display: flex;">
                                            <input type="checkbox" name="smsWelcome" id="smsWelcome" onclick="this.form.submit()" {{ $welcome['activeFirstSms'] ? 'checked' : '' }} class="form-check-input">
                                        </div>
                                    </div>
                            </form>
                        </div>


                    </div>

                    <div class="card-body" style="{{ $welcome['activeFirstSms'] ? 'display: ' : 'display: none' }}">

                    <div class="d-flex justify-content-between mb-2">
                              <div>

                                <div class="ml-2">
                                    <h5>Введите текст SMS </h5>
                                </div>
                                <div>
                                    <textarea style="margin-bottom: 2px; height: 100px;border-color:rgb(203,203,203)" id="smsWelcomeStore" class="textarea-element" onblur="updateFirstSms(this, smsWelcomeStore ,{{$id}})" name="smsWelcomeStore">{{$welcome['sms']}}</textarea>
                                </div>
                                <div class="pl-2" style="font-size: 10px;font-style: italic;width: 250px;">
                                  *Сервис сокращает ссылки автоматически. <br/> Используйте полную ссылку.
                                </div>
                                  <div class="mt-2">
                                      <div class="d-flex">
                                          <p>Количество символов:</p>
                                          <p class='ml-1' id="countText"></p>
                                      </div>

                                  </div>
                             </div>
                        <div class="col-4">
                            <form action="{{route('smsAeroSetting.store',$id)}}" method="POST">
                                <div id="smsAero">
                                    <div class="form-group col-12">
                                        Email smsAero
                                        @if ($errors->has('smsAeroEmail'))
                                            <p style="color: red">{{ $errors->first('emailSmsAero') }}</p>
                                        @endif
                                        <input class="form-control" type="email" id="smsAeroEmail" value="{{$notificationSetting['smsAeroEmail'] ?? ""}}" name="smsAeroEmail" >
                                        <input class="form-control"  type="hidden" value="smsAero" name="service" >
                                    </div>
                                    <div class="form-group col-12">
                                        API-ключ smsAero   <span class="tooltip_2">
                                                     <span class="tooltip_link">Где взять ключ? <a href="https://smsaero.ru/cabinet/settings/apikey/" target="_blank" rel="noopener"> ПЕРЕЙТИ</a></span>
                                                     <ion-icon name="help-circle-outline"></ion-icon>
                                                 </span>
                                        @if ($errors->has('smsAero_key'))
                                            <p style="color: red">{{ $errors->first('smsAero_key') }}</p>
                                        @endif
                                        <input class="form-control" id="smsAero_key" value="{{$notificationSetting['smsAero_key'] ?? ""}}" name="smsAero_key" >
                                    </div>

                                    @if($smsAeroSenderName != null)
                                        <div class="form-group col-12">
                                            Имя отправителя
                                            <select class="form-control input-opacity" id="smsAeroSenderName" name="smsAeroSenderName" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                                                @foreach($smsAeroSenderName as $name)
                                                    <option {{ $getSmsSetting['smsAeroSenderName'] == $name ? 'selected' : '' }} value="{{$name}}">{{$name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                    <div class="form-group col-md-12">
                                        <button class="btn btn-success w-100" type="submit"> Сохранить</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                        </div>
                    </div>

                </div>

                {{--wa black--}}
                <div class="card mt-4">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-6">
                            <h4>
                                Приветственные WA сообшения
                            </h4>
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            <form method="POST" action="{{ route('firstWaActive.store',$id)}}">
                                @csrf
                                @method('PATCH')

                                <div class="d-flex justify-content-end">
                                    <div class="ml-3">
                                        <div class="mb-2 form-check form-switch">
                                            <input type="checkbox" name="waWelcome" id="waWelcome" onclick="this.form.submit()" {{ $welcome['activeFirstWa'] ? 'checked' : '' }} class="form-check-input">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    </div>
                     <div class="card-body" style="{{ $welcome['activeFirstWa'] ? 'display: ' : 'display: none' }}">
                            <div class="d-flex justify-content-between">
                                <div class="col-8">
                                    <div class="ml-2">
                                        <h5>Введите текст WA </h5>
                                    </div>
                                    <div>
                                        <textarea  onblur="updateFirstWa(this, waWelcomeStore ,{{$id}})" style="width: 100%;height: 300px" id="waWelcomeStore" class="textarea-element" name="waWelcomeStore">{{$welcome['wa']}}</textarea>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div style="align-items: flex-end">
                                        <div>
                                            <div class="text-end">
                                                <h5>Количество активных номеров:</h5>
                                            </div>
                                            <div class="text-end">
                                                {{$welcome['waCount'] ?? 0}}
                                            </div>
                                        </div>

                                        <div class="col-md-12 mt-5 p-0">
                                            <div class="mb-0">Whatsmonster Token
                                                 <span class="tooltip_2">
                                                     <span class="tooltip_link">Где взять токен? <a href="https://whatsmonster.ru/whatsapp_api" target="_blank" rel="noopener"> ПЕРЕЙТИ</a></span>
                                                     <ion-icon name="help-circle-outline"></ion-icon>
                                                 </span>
                                            </div>



                                            <form method="POST" action="{{ route('waTokenUpdate.store',$id)}}">
                                                @csrf

                                                <div class="input-group">
                                                    <input class="form-control" id="wapico_key" value="{{$notificationSetting['wapico_key'] ?? ""}}" name="wapico_key" >
                                                    <span class="input-group-append">
                                                       <button   class="btn btn-success" type="submit"> Сохранить </button>
                                                    </span>
                                                </div>
                                                @if ($errors->has('wapico_key'))
                                                    <p style="color: red">{{ $errors->first('wapico_key') }}</p>
                                                @endif
                                            </form>

                                        </div>
                                        <div class="col-md-12 mt-3 p-0">
                                            <a href="{{route('instanceSetting.index',$id)}}" style="{{$notificationSetting['wapico_key'] ? "" : 'display:none' }}" class="btn btn-primary w-100"> Перейти к настройкам </a>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        </div>

                </div>

                {{--wabi--}}
                @if(auth()->user()->name == 'Maks Glushko')
                <div class="card mt-4">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-6">
                            <h4>
                                Приветственные Официальный WA
                            </h4>
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                            <form method="POST" action="{{ route('firstWabaActive.store',$id)}}">
                                @csrf
                                @method('PATCH')

                                <div class="d-flex justify-content-end">
                                    <div class="ml-3">
                                        <div class="mb-2 form-check form-switch">
                                            <input type="checkbox" name="wabaWelcome" id="wabaWelcome" onclick="this.form.submit()" {{ $notificationSetting->welcomeWaba == 1 ? 'checked' : '' }} class="form-check-input">
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                    </div>
                    <div class="card-body" style="{{ $notificationSetting->welcomeWaba == 1 ? 'display: ' : 'display: none' }}">
                        <div class="row">
                            <div class="col-lg-8 col-md-12 d-flex align-items-center">
                                <div class="row mb-3">
                                    <label for="category" class="col-md-4 ml-2 col-form-label text-md-start"> Выберите шаблон </label>
                                    <select onchange="updateFirstWaba(this)" id="wabaWelcomeStore" style="width: 300px" name="wabaWelcomeStore" class="form-control input-opacity col-6">
                                        <option></option>
                                        @if(isset($notificationSetting->wabaTemplates))
                                            @foreach($notificationSetting->wabaTemplates->unique('name') as $teplates)
                                                <option {{$teplates['name'] == $notificationSetting->wabaSetting->welcome_template_name ? 'selected' : ''}} value="{{$teplates['name']}}">{{$teplates['name']}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                    <div class="col-1 ">
                                        <img style="color:#0a53be;width: 25px;margin: 5px;" src="{{ asset('images/look.png') }}" class="large-icon rotatable cursor-pointer" alt="update">
                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-4 col-md-12 mt-3 mt-lg-0">
                                <div class="col-md-12 p-0">
                                    <a href="{{route('settingWaba.index',$id)}}" style="{{$notificationSetting['wapico_key'] ? '' : 'display:none' }}" class="btn btn-primary w-100"> Перейти к настройкам </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>




    <script>

        function updateFirstSms(row,messege_id,id){
            let text = document.getElementById(row.id).value

            let adjustedLength = text.replace(/https?:\/\/\S+/g, '1234567890').length;

            if (adjustedLength.length > 70){
                alert('более 1ой смс! ' + adjustedLength.length + '/70 символов. Сократи текст')
                return 0;
            }

            $.ajax({
                url: "{{ route('firstSmsUpdateText.store', ['project_id' => $id]) }}",
                type: 'POST',
                data: {
                    'smsWelcomeStore': text,
                    'id': id,
                },
                success:function(response){
                }
            });
        }

        function updateFirstWa(row,messege_id,id){

            let text = document.getElementById(row.id).value
            if (text.length > 2000){
                alert('Превышан лимит! ' + text.length + '/2000 символов. Сократи текст')
                return 0;
            }
            $.ajax({
                url: "{{ route('firstWaUpdateText.store', ['project_id' => $id]) }}",
                type: 'POST',
                data: {
                    'waWelcomeStore': text,
                    'id': id,
                },
                success:function(response){

                }
            });
        }

        function updateFirstWaba(selectElement){
            let text = selectElement.value;

            $.ajax({
                url: "{{ route('firstWabaUpdateText.store', ['project_id' => $id]) }}",
                type: 'POST',
                data: {
                    'wabaWelcomeStore': text,
                },
                success: function(response) {
                    // Обработка успешного ответа
                    alert('Данные успешно обновлены!');
                },
                error: function(error) {
                    // Обработка ошибки
                    alert('Произошла ошибка при обновлении данных.');
                }
            });

        }


        let countSms = document.querySelector('#countText');
        let textarea = document.querySelector('#smsWelcomeStore');
        let text = textarea.value;
        countSms.textContent = countCharacters(text);

        textarea.addEventListener('input', function() {
            countSms.textContent = countCharacters(this.value);
        });

        function countCharacters(text) {
            const urlRegex = /https?:\/\/[^\s]+/g;
            let modifiedText = text.replace(urlRegex, match => 'x'.repeat(25));
            return modifiedText.length;
        }




    </script>

@endsection
