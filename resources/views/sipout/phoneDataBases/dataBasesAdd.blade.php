@extends('adminPanel.startAddProject')

@section('content')


    <div class="content-wrapper">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-9 col mt-5">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div> </div>
                            <div> <h5>Добавить базу </h5></div>
                        <div><a href="{{route('contactDatabase.index',$id)}}">К списку</a></div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="d-flex justify-content-start">
                            <ul class="nav nav-pills mb-4">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#detal" data-toggle="tab">Добавить новые</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#day" data-toggle="tab">Загрузить недозвоны</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#minus" data-toggle="tab">Удаление из базы</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#fileMinus" data-toggle="tab">Вычитание файлов</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane active" id="detal">
                                <form method="POST" enctype="multipart/form-data" action="{{ route('contactAddDatabase.store',$id)}}">
                                    @csrf
                                    <div id="body">
                                        <div class="row mb-3">
                                            <label for="phoneWeb" class="col-md-4 col-form-label text-md-end"> Вставьте файл с номерами</label>
                                            <div class="col-md-6">
                                                <input id="phoneWeb" accept=".txt" multiple type="file" class="form-control" name="phoneWeb[]" autofocus>
                                            </div>
                                        </div>

                                        <div class="row mb-3">

                                            <label for="category" class="col-md-4 col-form-label text-md-end"> Выберите ЦА или создайте новую</label>
                                            <div class="col-md-6">
                                                <select onchange="categoryChange(this)" class="form-control" name="category">
                                                    <option value="0">Выберите категорию</option>
                                                    <option value="other">Добавить новую</option>
                                                    @foreach($category as $valueCategory)
                                                        <option value="{{$valueCategory['category_name']}}"> {{$valueCategory['category_name']}} </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div style="display:none" class="row mt-3 mb-3 another">
                                                <label for="anotherСategory" class="col-md-4 col-form-label text-md-end"> Напишите новую категорию</label>
                                                <div  class="col-md-6">
                                                    <input id="anotherСategory" type="text" class="form-control" name="anotherСategory">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row mb-3">

                                            <label for="category" class="col-md-4 col-form-label text-md-end"> За исключением</label>
                                            <div class="col-md-6">
                                                <select class="select2" name="minus[]" multiple data-placeholder="База вычитания" style="width: 100%;">
                                                    @foreach($category as $valueCategory)
                                                        <option value="{{$valueCategory['category_name']}}"> {{$valueCategory['category_name']}} </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="row mt-3 mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Загрузить
                                            </button>
                                            @if($errors->any())
                                                <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>

                                            @endif


                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="day">
                                <form method="POST" enctype="multipart/form-data" action="{{ route('contactAddDatabase.store',$id)}}">
                                    @csrf
                                    <div id="body">

                                        <div class="row mb-3">
                                            <label for="type" class="col-md-4 col-form-label text-md-end"> Какие недозвоны загружать</label>

                                            <div class="col-md-6">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="wait" value="ring_timeout" checked>
                                                    <label class="form-check-label"  for="inlineCheckbox1">Остатки базы</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="ring_timeout" value="ring_timeout" checked>
                                                    <label class="form-check-label" for="inlineCheckbox2">Не дозвонились</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="filed_machine" value="filed_machine">
                                                    <label class="form-check-label" for="inlineCheckbox3"> Машины </label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="all" value="all">
                                                    <label class="form-check-label" for="inlineCheckbox3"> Все </label>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="row mb-3">
                                            <label for="contactOld" class="col-md-4 col-form-label text-md-end"> Вставьте файл из SipOut</label>
                                            <div class="col-md-6">
                                                <input id="contactOld" accept=".csv" type="file" class="form-control" multiple  name="contactOld[]" autofocus>
                                            </div>
                                        </div>

                                        <div class="row mb-3">

                                            <label for="category" class="col-md-4 col-form-label text-md-end"> Выберите ЦА или создайте новую</label>
                                            <div class="col-md-6">
                                                {{--                                    <input id="category" type="text" class="form-control" name="category" >--}}
                                                <select onchange="categoryChange(this)" class="form-control" name="category">
                                                    <option value="0">Выберите категорию</option>
                                                    <option value="other">Добавить новую</option>
                                                    @foreach($category as $valueCategory)
                                                        <option value="{{$valueCategory['category_name']}}"> {{$valueCategory['category_name']}} </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div style="display:none" class="row mt-3 mb-3 another">
                                                <label for="anotherСategory" class="col-md-4 col-form-label text-md-end"> Напишите новую категорию</label>
                                                <div  class="col-md-6">
                                                    <input id="anotherСategory" type="text" class="form-control" name="anotherСategory">
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="row mt-3 mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Загрузить
                                            </button>
                                            @if($errors->any())
                                                <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>

                                            @endif


                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="tab-pane" id="minus">
                                <form method="POST" enctype="multipart/form-data" action="{{ route('contactMinusDatabase.store',$id)}}">
                                    @csrf
                                    <div id="body">
                                        <div class="row mb-3">
                                            <label for="category" class="col-md-4 col-form-label text-md-end"> Основные базы</label>
                                            <div class="col-md-6">
                                                <select class="form-control" name="category"  data-placeholder="Основная база" style="width: 100%;">
                                                    @foreach($category as $valueCategory)
                                                        <option value="{{$valueCategory['category_name']}}"> {{$valueCategory['category_name']}} </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="row mb-3">

                                            <label for="category" class="col-md-4 col-form-label text-md-end"> Базы вычитания</label>
                                            <div class="col-md-6">
                                                <select class="select2" name="minus[]" multiple data-placeholder="База вычитания" style="width: 100%;">
                                                    @foreach($category as $valueCategory)
                                                        <option value="{{$valueCategory['category_name']}}"> {{$valueCategory['category_name']}} </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="row mt-3 mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Загрузить
                                            </button>
                                            @if($errors->any())
                                                <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>

                                            @endif


                                        </div>
                                    </div>
                                </form>

                            </div>
                            <div class="tab-pane" id="fileMinus">
                                <form method="POST" enctype="multipart/form-data" action="{{ route('fileMinus.store',$id)}}">
                                    @csrf
                                    <div id="body">
                                        <div class="row mb-3">
                                            <label for="phoneWeb" class="col-md-4 col-form-label text-md-end"> Вставьте файл с номерами</label>
                                            <div class="col-md-6">
                                                <input accept=".txt" multiple type="file" class="form-control" name="file[]" autofocus>
                                            </div>
                                        </div>

                                        <div class="row mb-3">
                                            <label for="phoneWeb" class="col-md-4 col-form-label text-md-end"> Вставьте файл вычитания</label>
                                            <div class="col-md-6">
                                                <input accept=".txt" multiple type="file" class="form-control" name="minus[]" autofocus>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row mt-3 mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Загрузить
                                            </button>
                                            @if($errors->any())
                                                <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>

                                            @endif


                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function categoryChange(catigorys){

            if (catigorys.value == 'other'){

                document.querySelectorAll('.another').forEach((elem) => {
                    elem.style.display = "";
                });
            } else {
                document.querySelectorAll('.another').forEach((elem) => {
                    elem.style.display = "none";
                    document.getElementById('anotherСategory').value = '';
                });
            }
        }
    </script>


    <script>
        var deductions = 1

        function deduction(catigorys){

            let div = document.createElement("div")
            div.classList.add('row', 'mb-3');

            let label = document.createElement("label")
            label.for = 'deductions_' + deductions;
            label.classList.add('col-md-4','col-form-label','text-md-end')
            label.textContent = 'База для исключения'
            div.appendChild(label)

            let diva = document.createElement("div")
            diva.classList.add('col-md-6');



            let select = document.createElement("select")
            select.classList.add('form-control','form-select-inline');
            select.id = 'deductions_' + deductions;
            select.name = 'deductions_' + deductions;
            select.style.marginLeft = '0';



            catigorys.forEach(element => {
                console.log(element)
                let option = document.createElement("option")
                option.value = element;
                option.textContent = element;
                select.appendChild(option)
            });

            diva.appendChild(select)
            div.appendChild(diva)


            let body  = $( "#body" )[0];
            body.appendChild(div)


        }

        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })
        })

    </script>




@endsection
