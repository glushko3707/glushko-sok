@extends('adminPanel.startAddProject')

@section('content')


    <div class="content-wrapper">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-9 col mt-5">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-center">
                            <div> <h5>Добавить номера в черный список </h5></div>
                        </div>
                    </div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('blackList.store',$id)}}">
                            @csrf

                            <div class="row mb-3">
                                <label for="phoneWebOne" class="col-md-4 col-form-label text-md-end"> Вставьте номера</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" id="phoneWebOne" style="margin-bottom: 18px" name="phoneWebOne" rows="3"></textarea>
                                </div>
                            </div>



                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Добавить номера
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        function categoryChange(catigorys){

            if (catigorys.value == 'other'){

                document.querySelectorAll('.another').forEach((elem) => {
                    elem.style.display = "";
                });
            } else {
                document.querySelectorAll('.another').forEach((elem) => {
                    elem.style.display = "none";
                    document.getElementById('anotherСategory').value = '';
                });
            }
        }
    </script>


    <script>
        var deductions = 1

        function deduction(catigorys){

            let div = document.createElement("div")
            div.classList.add('row', 'mb-3');

            let label = document.createElement("label")
            label.for = 'deductions_' + deductions;
            label.classList.add('col-md-4','col-form-label','text-md-end')
            label.textContent = 'База для исключения'
            div.appendChild(label)

            let diva = document.createElement("div")
            diva.classList.add('col-md-6');



            let select = document.createElement("select")
            select.classList.add('form-control','form-select-inline');
            select.id = 'deductions_' + deductions;
            select.name = 'deductions_' + deductions;
            select.style.marginLeft = '0';



            catigorys.forEach(element => {
                console.log(element)
                let option = document.createElement("option")
                option.value = element;
                option.textContent = element;
                select.appendChild(option)
            });

            diva.appendChild(select)
            div.appendChild(diva)


            let body  = $( "#body" )[0];
            body.appendChild(div)


        }
    </script>




@endsection
