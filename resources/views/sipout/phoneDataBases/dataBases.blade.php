@extends('adminPanel.startAddProject')
@section('content')
@push('title')
    Kupr | База номеров
@endpush

    <div class="content-wrapper">
        <main class="flex flex-grow-1 py-3">
            <section style="width: 100%">

                <div class="container col-9" style="margin-top: 50px">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                        <div><h5>Базы номеров</h5></div>
                        <div>
                            <a href="{{ route('contactAddDatabase.index',$id) }}" class="btn btn-sm btn-outline-primary" >Добавить базу</a> </div>
                       </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">База</th>
                                <th scope="col">Сброс</th>
                                <th scope="col">Объём</th>
                                <th scope="col">Использовано</th>
                                <th scope="col">Осталось</th>
                                <th scope="col">Сколько файлов</th>
                                <th scope="col">В каждом файле номеров</th>
                                <th scope="col">Получить</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($contacts) > 0)
                            @foreach($contacts as $step)
                                <tr>
                                    <th scope="row">{{$step['category_name']}}</th>
                                    <th>
                                        <div>
                                            <a href="{{route('updateCategory.index',['id' => $id,'category_id' => $step['id'] ])}}" onclick="return confirm('Это операция трудоёмкая. Она нужна, чтобы проверить ошибки в счётчике. Вы уверены, что хотите обновить категорию?')">
                                                <ion-icon name="cloud-upload-outline"></ion-icon>
                                            </a>
                                        </div>
                                    </th>
                                    <td>{{$step['count']}}</td>
                                    <td>{{$step['used_count']}}
                                    </td>
                                    <td>{{ ($step['count']  - $step['used_count'])}}</td>
                                    <form action="{{ route('contactDatabase.store',$id) }}" method="POST">
                                        @csrf
                                        <td>
                                            <input style="max-width: 100px;" id="countFile" type="number" class="form-control" value="1" min="1" max="5" name="countFile">
                                        </td>
                                        <td>
                                            <input style="max-width: 100px;" id="count" type="number" class="form-control" value="8000" name="count">
                                            <input type="hidden" value="{{$step['id']}}" class="form-control" name="category_id">
                                        </td>


                                        <td>
                                            <button type="submit" class="btn btn-outline-primary">
                                              Получить
                                            </button>
                                        </td>
                                        <td>
                                            <i onclick="deletes('{{ (string)$step['id']}}',{{$id}})" class="fas fa-times" style="color: red"></i>
                                        </td>
                                    </form>
                                    <td>
                                    <form action="{{ route('contactDatabaseCleanUsed.index',$id) }}" method="POST">
                                        @csrf

                                        <input type="hidden" value="{{$step['id']}}" class="form-control" name="categoryCleanUsed_id">

                                        <button type="submit" class="btn btn-sm btn-outline-danger">
                                            Обнулить
                                        </button>


                                    </form>
                                    </td>
                                </tr>
                            @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card mt-3">
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Название</th>
                                <th scope="col"> @if ($contacts_work != 0){{$contacts_work}} баз в загрузке @endif</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($contactDownload) > 0)
                                @foreach($contactDownload as $step)
                                    <tr id="step-{{$loop->index}}">
                                        <td id="{{$step}}"> {{$step}}</td>
                                        <td>
                                            <div class="d-flex justify-content-end">
                                                <div class="mr-2">
                                                    <form id="form-{{$loop->index}}" action="{{ route('getContactDatabase.store',$id) }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" value="{{$step}}" class="form-control" name="fileName">

                                                    <button type="button" class="btn btn-primary" onclick="handleDownload(event, 'step-{{$loop->index}}', 'form-{{$loop->index}}')">
                                                        Скачать базу
                                                    </button>

                                                </form>
                                                </div>
                                                <div>


                                                <form action="{{ route('getContactDatabase.delete',$id) }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" value="{{$step}}" class="form-control" name="fileName">

                                                    <button type="submit" class="btn btn-danger">
                                                        Удалить файл
                                                    </button>
                                                </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <script>
                                        function handleDownload(event, stepId, formId) {
                                            event.preventDefault();

                                            // Показать overlay
                                            document.getElementById('overlay').style.display = 'block';

                                            var stepElement = document.getElementById(stepId);
                                            if (stepElement) {
                                                stepElement.style.display = 'none';
                                            }
                                            setTimeout(function() {
                                                document.getElementById('overlay').style.display = 'none';
                                            }, 2000);
                                            document.getElementById(formId).submit();
                                        }

                                        // Убираем overlay после завершения обработки
                                    </script>

                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
            </section>
        </main>
    </div>


    <script>

        function deletes(row,id){
            confirm("Are you sure?");
            $.ajax({
                url: "/api/contactDatabase/delete/store/" + id,
                type: 'DELETE',
                data: {
                    'category_id': row,
                },
                success:function(response){
                    setTimeout(function() {
                        location.reload(); //
                    }, 1000); //
                }
            });
        }



        function categoryChange(catigorys){

            if (catigorys.value == 'other'){

                document.querySelectorAll('.another').forEach((elem) => {
                    elem.style.display = "";
                });
            } else {
                document.querySelectorAll('.another').forEach((elem) => {
                    elem.style.display = "none";
                    document.getElementById('anotherСategory').value = '';
                });
            }
        }
    </script>

    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
@endsection
