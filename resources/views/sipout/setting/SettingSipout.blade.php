@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Настройки сервиса SipOut </div>

                    <div class="card-body">
                        @if($errors->any())
                            <div class="alert alert-danger d-flex justify-content-center" role="alert">
                                @foreach($errors->all() as $error)
                                    <h5>{{ $error }}</h5>
                                @endforeach
                            </div>
                        @endif

                        @if(isset($project['sheet']))

                            <div class="row mb-3">
                                <label for="sheet" class="col-md-4 col-form-label text-md-end"> Таблица с лидами </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="sheet" value="{{$project['sheet'] ?? ""}}" disabled >
                                        <span class="input-group-append">
                                        <a href="{{"https://docs.google.com/spreadsheets/d/" . $project['sheet'] .'/edit#gid=0' }}" class="btn btn-outline-light active" aria-current="page">Перейти</a>
                                        </span>
                                    </div>
                                </div>
                            </div>

                        @endif


                        <div class="row mb-3">
                            <label for="emailSmsAero" class="col-md-4 col-form-label text-md-end"> Адрес вебхука SipOut </label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <p id="paragraph" class="form-control" style="font-size: 8px"> https://glushko-sok.ru/api/newLeadsSipOut/{{$id}}?phone=%VAR:CID%&utm_medium=%VAR:DID% <p>
                                    <span class="input-group-append">
                                      <button type="button" id="copyButton" class="btn btn-light">Копировать</button>
                                    </span>
                                </div>
                            </div>
                        </div>


                        <form method="POST" action="{{ route('sipoutSetting.store',$id)}}">
                            @csrf



                            <div class="row mb-3">
                                <label for="emailSmsAero" class="col-md-4 col-form-label text-md-end"> SipOut API-КЛЮЧ  </label>

                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="sipOutSetting"  value="{{$project['sipOutSetting'] ?? ""}}" name="sipOutSetting" >
                                        @if(empty($project['sipOutSetting']))

                                        <span class="ml-1 tooltip_2">
                                                     <span class="tooltip_link"> Где взять API-КЛЮЧ? <a href="https://lk.sipout.net/userapi_doc/" target="_blank" rel="noopener"> ПЕРЕЙТИ</a></span>
                                                     <ion-icon name="help-circle-outline"></ion-icon>
                                        </span>
                                        @endif
                                    </div>
                                    @if($errors->any())
                                        <h5 style="color: red;font-size:16px">{{$errors->first()}}</h5>
                                    @endif
                                </div>
                            </div>


                            <div class="row mb-3">
                                <label for="testPhone" class="col-md-4 col-form-label text-md-end"> Ваш номер телефона </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="testPhone" name="testPhone" value="{{$project['testPhone'] ?? ""}}">
                                    </div>
                                </div>
                            </div>



                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary" style="width: 150px">
                                    Сохранить
                                </button>
                            </div>



                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        var copyButton = document.getElementById('copyButton');
        var paragraph = document.getElementById('paragraph');


        copyButton.addEventListener('click', function() {

            // Используем библиотеку clipboard.js для копирования текста в буфер обмена
            var temp = document.createElement("textarea");
            document.body.appendChild(temp);
            temp.value = paragraph.innerText;
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);


            // Визуальное подтверждение копирования
            copyButton.innerText = 'Скопировано!';
            setTimeout(function(){
                copyButton.innerText = 'Скопировать';
            }, 2000);
        });
    </script>



@endsection
