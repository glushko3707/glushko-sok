@extends('adminPanel.startAddProject')

@section('content')


    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Настройки трафика </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('storeSettingFacebook.store',$id)}}">
                            @csrf
                            <div class="row mb-3">
                                <div class="row">
                                    <label for="testPhone" class="col-md-4 col-form-label text-md-end"> Pixel id </label>
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <input class="form-control" id="pixel_id" name="pixel_id" value="{{$setting['pixel_id'] ?? ""}}">
                                        </div>
                                    </div>
                                    {{--                                Добавляем вывод ошибки в поле pixel_id, если она есть--}}
                                    <div class="col-md-6 offset-md-4">
                                        @if ($errors->has('pixel_id'))
                                            <div class="error" style="color: red;">
                                                {{ $errors->first('pixel_id') }}
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row mt-3">
                                <label for="testPhone" class="col-md-4 col-form-label text-md-end"> Token </label>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input class="form-control" id="pixel_id" name="pixel_id" value="{{$setting['token'] ?? ""}}">
                                    </div>
                                </div>
                                {{--                                Добавляем вывод ошибки в поле pixel_id, если она есть--}}
                                <div class="col-md-6 offset-md-4">
                                    @if ($errors->has('token'))
                                        <div class="error" style="color: red;">
                                            {{ $errors->first('token') }}
                                        </div>
                                    @endif
                                </div>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center">
                                <button class="btn btn-primary" style="width: 150px">
                                    Сохранить
                                </button>
                            </div>
                            <div>
                                webhook tilda:https://glushko-sok.ru/api/tilda/new_leads/{{$id}}
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
