@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                @livewire('Traffic.yandex',[
                             'id' => $id,
                                            ])
            </div>
        </div>
    </div>
    <script>

        function hideNested(rows) {
            rows.each(function () {
                var nestedId = $(this).data('toggle');
                var nestedRows = $('.' + nestedId);
                nestedRows.hide();
                hideNested(nestedRows);
            });
        }

        document.addEventListener('livewire:initialized', () => {
            setTimeout(function() {
                $('.clickable').off('click').on('click', function () {
                    console.log(this);
                    var id = $(this).data('toggle');
                    console.log(id);

                    var rows = $('.' + id);
                    rows.toggle();

                    // Скрыть все дочерние элементы при скрытии родителя
                    if (!rows.is(':visible')) {
                        hideNested(rows);
                    }
                });

            }, 500); // Задержка 3 секунды (4000 миллисекунд)
        })
    </script>


    <style>
        .clickable {
            cursor: pointer;
            transition: box-shadow 0.3s ease;
        }
        .clickable:hover {
            box-shadow: inset 0 0 8px rgba(0, 0, 0, 0.2); /* Внутренняя тень */
            border: 1px solid rgba(0, 0, 0, 0.2); /* Легкая обводка */
            z-index: 1; /* Чтобы тень и обводка были видны поверх других элементов *    </style>
@endsection
