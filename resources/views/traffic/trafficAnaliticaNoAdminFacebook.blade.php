<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Lead Pro</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>


    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>

    <!-- Select2 -->
    <link rel="stylesheet" href={{asset("plugins/select2/css/select2.min.css")}}>
    <link rel="stylesheet" href={{asset("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css")}}>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href= {{asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback')}}>
    <!-- Font Awesome -->
    <link rel="stylesheet" href={{asset('plugins/fontawesome-free/css/all.min.css')}}>
    <!-- Ionicons -->
    <link rel="stylesheet" href={{asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}>
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href={{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}>
    <!-- iCheck -->
    <link rel="stylesheet" href={{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}>
    <!-- JQVMap -->
    <link rel="stylesheet" href={{asset("plugins/jqvmap/jqvmap.min.css")}}>
    <!-- Theme style -->
    <link rel="stylesheet" href={{asset("dist/css/adminlte.min.css")}}>
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href={{asset("plugins/overlayScrollbars/css/OverlayScrollbars.min.css")}}>
    <!-- Daterange picker -->
    <link rel="stylesheet" href={{asset("plugins/daterangepicker/daterangepicker.css")}}>
    <!-- summernote -->
    <link rel="stylesheet" href={{asset("plugins/summernote/summernote-bs4.min.css")}}>

    <link rel="stylesheet" href={{asset("plugins/select2/css/select2.min.css")}}>
    <link rel="stylesheet" href={{asset("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css")}}>

    <script src="https://yastatic.net/jquery/3.3.1/jquery.min.js"></script>

    @livewireStyles
</head>
<body class="hold-transition sidebar-mini layout-fixed">

<div>

    <div class="mobile-no-padding" style="padding: 70px">
        <div class="container-fluid">
            @livewire('Traffic.traffic-analytica-no-admin',[
                            'id' => $id,
                                           ])
        </div>
    </div>
</div>

<script src={{asset("plugins/bootstrap/js/bootstrap.bundle.min.js")}}></script>


<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<!-- jQuery -->
<script src={{asset("plugins/jquery/jquery.min.js")}}></script>
<!-- jQuery UI 1.11.4 -->
<script src={{asset("plugins/jquery-ui/jquery-ui.min.js")}}></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src={{asset("plugins/bootstrap/js/bootstrap.bundle.min.js")}}></script>
<!-- ChartJS -->
<script src={{asset("plugins/chart.js/Chart.min.js")}}></script>
<!-- Sparkline -->
<script src={{asset("plugins/sparklines/sparkline.js")}}></script>
<!-- JQVMap -->
<script src={{asset("plugins/jqvmap/jquery.vmap.min.js")}}></script>
<script src={{asset("plugins/jqvmap/maps/jquery.vmap.usa.js")}}></script>
<!-- jQuery Knob Chart -->
<script src={{asset("plugins/jquery-knob/jquery.knob.min.js")}}></script>
<!-- daterangepicker -->
<script src={{asset("plugins/moment/moment.min.js")}}></script>
<script src={{asset("plugins/daterangepicker/daterangepicker.js")}}></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src={{asset("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js")}}></script>
<!-- Summernote -->
<script src={{asset("plugins/summernote/summernote-bs4.min.js")}}></script>
<!-- overlayScrollbars -->
<script src={{asset("plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js")}}></script>
<!-- AdminLTE App -->
<script src={{asset("dist/js/adminlte.js")}}></script>
<!-- Select2 -->
<script src={{asset("plugins/select2/js/select2.full.min.js")}}></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>



@livewireScripts

</body>
</html>




<style>
    .background-leson1 {
        background: #d8ffff !important;
    }

    .background-homework {
        background: #eeffef !important;
    }

    .background-meet {
        background: #ffffff !important;
    }

    h1.webinar_title {
        flex-grow: 1;
        font-family: "Manrope", "Arial", sans-serif;
        font-weight: 600;
        line-height: 25px;
        color: #FFFFFF;
        text-align: left;
        font-size: 18px
    }

    /* Стиль для строк таблицы */
    table tr {
        height: 40px; /* Используйте нужное вам значение высоты */
    }

    /* Стиль для ячеек таблицы, если хотите использовать line-height */
    table td, table th {
        line-height: 40px; /* значение должно совпадать или быть в соотношении с высотой строки */
    }
</style>


    <style>

        .clickable {
            cursor: pointer;
            transition: box-shadow 0.3s ease;
        }
        .clickable:hover {
            box-shadow: inset 0 0 8px rgba(0, 0, 0, 0.2); /* Внутренняя тень */
            border: 1px solid rgba(0, 0, 0, 0.2); /* Легкая обводка */
            z-index: 1; /* Чтобы тень и обводка были видны поверх других элементов *    </style>

