<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Оформление рассрочки</title>
    <script src="https://forma.tinkoff.ru/static/onlineScript.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="shortcut icon" href="{{ asset('dist/img/telegram/faviconJuli.png') }}">


    <!-- Select2 -->
    <link rel="stylesheet" href={{asset("plugins/select2/css/select2.min.css")}}>
    <link rel="stylesheet" href={{asset("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css")}}>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href= {{asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback')}}>
    <!-- Font Awesome -->
    <link rel="stylesheet" href={{asset('plugins/fontawesome-free/css/all.min.css')}}>
    <!-- Ionicons -->
    <link rel="stylesheet" href={{asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}>
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href={{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}>
    <!-- iCheck -->
    <link rel="stylesheet" href={{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}>
    <!-- JQVMap -->
    <link rel="stylesheet" href={{asset("plugins/jqvmap/jqvmap.min.css")}}>
    <!-- Theme style -->
    <link rel="stylesheet" href={{asset("dist/css/adminlte.min.css")}}>
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href={{asset("plugins/overlayScrollbars/css/OverlayScrollbars.min.css")}}>
    <!-- Daterange picker -->
    <link rel="stylesheet" href={{asset("plugins/daterangepicker/daterangepicker.css")}}>
    <!-- summernote -->
    <link rel="stylesheet" href={{asset("plugins/summernote/summernote-bs4.min.css")}}>

    <script src="https://yastatic.net/jquery/3.3.1/jquery.min.js"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">

<div class="container">
{{--    <div class="d-flex justify-content-center align-items-center" style="min-height: 100vh;">--}}
        <div class="d-flex justify-content-center align-items-center" style="min-height: 100vh;">
        <div class="col-md-10" style="padding: 25px;">
            <div class="mb-5" style="font-size:24px ;display: flex; justify-content: center; align-items: center;  text-align: center;">
              Оформление рассрочки на обучение 100к на Telegram
            </div>
            <div class="row mb-3">
                <label for="main_chat_id" class="col-md-4 col-form-label text-md-end"> Срок рассрочки</label>
                <div class="col-md-5">
                    <div class="input-group">
                        <select class="form-control input-opacity" id="installment_period" name="installment_period" style="min-width: 150px; background-color: rgba(0, 0, 0, 0)">
                            <option selected  value="6_mes">6 месяцев</option>
                            <option  value="10_mes">10 месяцев</option>
                        </select>
                    </div>
                </div>
            </div>
            <div>
                <button type="button" class="TINKOFF_BTN_YELLOW text-bold" id="tinkoff-button">
                    Cумма рассрочки {{$sum}}
                </button>
                <div class="text-sm mt-1 text-center">
                    Переплата: 0 рублей
                </div>
            </div>
        </div>
    </div>
{{--    </div>--}}
</div>

<script>
    document.getElementById('tinkoff-button').addEventListener('click', function() {
        const period = document.getElementById('installment_period').value;

        let promoCode;
        switch(period) {
            case '6_mes':
                promoCode = 'installment_0_0_6_7'; // Замените X на фактическое значение для 6 месяцев
                break;
            case '10_mes':
                promoCode = 'installment_0_0_10_13,42'; // Замените Y на фактическое значение для 10 месяцев
                break;
            default:
                promoCode = 'installment_0_0_6_7'; // Значение promoCode по умолчанию
        }

        tinkoff.create({
            shopId: 'd1585364-c166-47f8-b83e-ff1a7c7bbbb2',
            showcaseId: '2395f54e-718d-4d50-9af2-1fa632246de7',
            demoFlow: 'sms',
            items: [
                {
                    name: 'nastavnichestvo',
                    price: {{$sum}},
                    quantity: 1
                }
            ],
            promoCode: promoCode,
            sum: {{$sum}}
        });
    });
</script>

{{--<div style="position: fixed; top: 0; z-index: 10; left: 0; width: 100%; height: 100%; background-color:  #ffffff; opacity: 70%;filter: blur(20px) ">--}}
{{--</div>--}}
<div class="wrapper">






</div>
<script src={{asset("plugins/bootstrap/js/bootstrap.bundle.min.js")}}></script>

<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<!-- jQuery -->
<script src={{asset("plugins/jquery/jquery.min.js")}}></script>
<!-- jQuery UI 1.11.4 -->
<script src={{asset("plugins/jquery-ui/jquery-ui.min.js")}}></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src={{asset("plugins/bootstrap/js/bootstrap.bundle.min.js")}}></script>
<!-- ChartJS -->
<script src={{asset("plugins/chart.js/Chart.min.js")}}></script>
<!-- Sparkline -->
<script src={{asset("plugins/sparklines/sparkline.js")}}></script>
<!-- JQVMap -->
<script src={{asset("plugins/jqvmap/jquery.vmap.min.js")}}></script>
<script src={{asset("plugins/jqvmap/maps/jquery.vmap.usa.js")}}></script>
<!-- jQuery Knob Chart -->
<script src={{asset("plugins/jquery-knob/jquery.knob.min.js")}}></script>
<!-- daterangepicker -->
<script src={{asset("plugins/moment/moment.min.js")}}></script>
<script src={{asset("plugins/daterangepicker/daterangepicker.js")}}></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src={{asset("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js")}}></script>
<!-- Summernote -->
<script src={{asset("plugins/summernote/summernote-bs4.min.js")}}></script>
<!-- overlayScrollbars -->
<script src={{asset("plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js")}}></script>
<!-- AdminLTE App -->
<script src={{asset("dist/js/adminlte.js")}}></script>
<!-- Select2 -->
<script src={{asset("plugins/select2/js/select2.full.min.js")}}></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>


<style>
    /*Эффект для кнопки*/
    .TINKOFF_BTN_YELLOW{
        justify-content: center;
        background-image: linear-gradient(45deg , #FA9D2E 60%, #FFCB15);
        box-shadow: 0 16px 32px 0 rgba(255, 141, 0, 0.35);
        background-position: 100% 0 !important;
        background-size: 100% 100%;
        transition: all 0.5s ease-in-out !important;
        margin-left: auto;
        margin-right: auto;
        width: 250px;
        display:flex;
        flex-direction: column;
        border-radius:50px;
        justify-content: center;
        align-items: center;
    }
    .TINKOFF_BTN_YELLOW:hover {
        box-shadow: 0 0 0 0 rgba(0,40,120,0);
        background-position: 0 0 !important;
    }
    .price{
        font-size: 20px;
        font-weight: bold;
    }
</style>

</body>
</html>
