@extends('payment.payment')

@section('content')

    <style>.payform-tinkoff-row{display:block;margin:1%;width:160px;}</style>

    <main class="flex flex-grow-1 py-3">
        <section>
            <div class="container col-12  col-sm-10 col-lg-12">
                <div class="row mt-3 m-1" style="justify-content: center">
                    <div class="d-flex justify-content-center max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-5">
                        <div class="col-xl-4 col-lg-5 col-md-7 col-sm-10 col-12 p-0 shadow sm:rounded-lg">
                            <div class="card p-0" style="background: #efefef;margin-bottom: 0;box-shadow: unset">
                                <div class="card-body" style="padding-top: 0">
                                    <img src={{asset("dist/img/headFormKupr.jpg")}} class="w-100">
                                    <div>
                                        <div class="text-center mt-3 d-flex flex-column align-items-center">

                                            <h2 style="font-weight: 600">Шаг 1</h2>
                                            <div class="p-3 col-lg-10  col-md-11 col-12">
                                                <p style="font-weight: 300;font-size: 20px">{{$description}}</p>
                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-center mb-2">
                                            <div class="col-lg-10  col-md-11 col-12">
                                                <form action="{{route('connectStore',['id' => $id])}}" method="POST" accept-charset="utf-8">
                                                    @csrf
                                                    <x-form.input-nolabel id="cps_phone" class="mt-2" type="text" name="cps_phone"  placeholder="Введите ваш номер телефона" required/>
                                                    <x-input-error :messages="$errors->get('phone')" class="mt-1" />

                                                    <div class="ym-hidden-inputs">
                                                        <input disabled="" type="hidden" name="text" value="{{$description}}"><input disabled="" type="hidden" name="price" value="{{$sum}}"><input disabled="" type="hidden" name="quantity" value="1"><input disabled="" type="hidden" name="paymentSubjectType" value="commodity"><input disabled="" type="hidden" name="paymentMethodType" value="full_payment"><input disabled="" type="hidden" name="tax" value="1">
                                                        <input name="shopSuccessURL" type="hidden" value="https://sok-agency.com/success_490">
                                                        <input name="paymentType" type="hidden" value="AC">
                                                        <input name="sum" type="hidden" value="{{$sum}}">
                                                        <input name="product_id" type="hidden" value="{{$product_id}}">
                                                      </div>

                                                    <x-form.button class="btn mt-4 mb-2 w-100 shine-button" style="" type="submit"> Продолжить </x-form.button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="padding: 0">
                        <div class="card-tools">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>


    <script src="https://snipp.ru/cdn/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://snipp.ru/cdn/maskedinput/jquery.maskedinput.min.js"></script>
    <script>
        $.mask.definitions['h'] = "[0|1|3|4|5|6|7|9]"
        $(".mask-phone").mask("+7 (h99) 999-99-99");
    </script>

@endsection
