@extends('noAuth')

@section('content')
        <section class="content">
            <div class="container-fluid">
                <div class="row mt-3" style="justify-content: center">

                    <div class="d-flex justify-content-center max-w-7xl mx-auto sm:px-6 lg:px-8 space-y-6 mt-5">
                        <div class="col-xl-4 col-lg-5 col-md-7 col-sm-10 col-12 p-0 shadow sm:rounded-lg">
                              <div class="card p-0" style="background: #efefef;margin-bottom: 0;box-shadow: unset">
                                   <div class="card-body" style="padding-top: 0">
                                        <img src={{asset("dist/img/project/498/headForm.jpg")}} class="w-100">
                                        <div>
                                       <div class="text-center mt-3 d-flex flex-column align-items-center">

                                           <h2 style="font-weight: 600">Заполните форму ниже</h2>
                                              <div class="p-3 col-lg-10  col-md-11 col-12">
                                                  <p style="font-weight: 200">Чтобы забронировать место на аудит бизнеса от Екатерины Уколовой</p>
                                              </div>
                                       </div>
                                      <div class="d-flex justify-center mb-3">
                                          <div class="col-lg-10  col-md-11 col-12">
                                              <form action="{{route('formStore',$id)}}" method="POST">
                                                  <input type="hidden" name="utm_source" value="{{ $utm_source }}">
                                                  <input type="hidden" name="utm_medium" value="{{ $utm_medium }}">
                                                  <input type="hidden" name="utm_campaign" value="{{ $utm_campaign }}">
                                                  <input type="hidden" name="utm_term" value="{{ $utm_term }}">
                                                  <input type="hidden" name="utm_content" value="{{ $utm_content }}">
                                                  <x-form.input-nolabel id="name" type="text" name="name" :value="old('name')" placeholder="Введите ваше имя" required/>
                                                  <x-input-error :messages="$errors->get('name')" class="mt-1" />

                                                  <x-form.input-nolabel id="phone" class="mt-4" type="text" name="phone"  :value="old('phone', $phone ?? '')" placeholder="Введите ваш номер телефона" required/>
                                                  <x-input-error :messages="$errors->get('phone')" class="mt-1" />

                                                  <x-form.label-input class="mb-2 mt-4" name="nisha"  :value="'Ваша сфера деятельности? / Ваша ниша?'" />
                                                  <x-form.input-nolabel id="nisha" type="text" name="nisha" :value="old('nisha', $nisha ?? '')" placeholder="Введите вашу нишу" required/>
                                                  <x-input-error :messages="$errors->get('nisha')" class="mt-1" />

                                                  <x-form.label-input  class="mb-2 mt-4" :value="'Ваш оборот ( $/мес )'" />
                                                  <x-form.select-nolabel id="oborot" type="text" name="oborot" placeholder="Укажите ваш оборот" required>
                                                      <option {{ $oborot == '' ? 'selected' : '' }} value="" disabled selected>Выберите ваш оборот</option>
                                                      <option {{ $oborot == 'до 5 000 $ /мес' ? 'selected' : '' }} value="до 5 000 $ /мес">до 5 000 $ /мес</option>
                                                      <option {{ $oborot == 'от 5 000 до 10 000 $/мес' ? 'selected' : '' }}  value="от 5 000 до 10 000 $/мес">от 5 000 до 10 000 $/мес</option>
                                                      <option {{ $oborot == 'от 10 000 до 30 000 $/мес' ? 'selected' : '' }}  value="от 10 000 до 30 000 $/мес">от 10 000 до 30 000 $/мес</option>
                                                      <option {{ $oborot== 'от 30 000 $/мес до 50 000 $/мес' ? 'selected' : '' }}  value="от 30 000 $/мес до 50 000 $/мес">от 30 000 до 50 000 $/мес</option>
                                                      <option {{ $oborot == 'свыше 50 000 $/мес' ? 'selected' : '' }}  value="свыше 50 000 $/мес">свыше 50 000 $/мес</option>
                                                  </x-form.select-nolabel>
                                                  <x-input-error :messages="$errors->get('oborot')" class="mt-1" />

                                                  <x-form.button class="btn mt-4 w-100 shine-button" style="" type="submit"> Отправить </x-form.button>
                                              </form>
                                          </div>
                                      </div>
                                  </div>
                                   </div>
                                </div>
                          </div>
                    </div>




                    <div class="card" style="padding: 0">
                        <div class="card-tools">
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

<style>
    .shine-button:hover {
        background-color: #ffffff;
    }

    .shine-button {
        position: relative;
        border: none;
        overflow: hidden; /* Это важно для предотвращения переполнения эффекта за границы кнопки */
        transition: background-color 0.3s;
    }

    .shine-button:hover {
        background-color: #ffffff; /* Фон при наведении */
        color: #0a001f;
        transition: background-color 0.3s;
    }

    .shine-button:before {
        content: '';
        position: absolute;
        top: 50%;
        left: -100%; /* Начальная позиция блика за пределами левой границы кнопки */
        width: 200%;
        height: 200%;
        background: rgba(255, 255, 255, 0.3); /* Белый блик */
        transform: translateY(-50%) rotate(45deg);
        animation: shine 2s infinite; /* Анимация */
    }

    @keyframes shine {
        0% {
            left: -100%; /* Начало за пределами кнопки слева */
        }
        50% {
            left: 50%; /* Центр кнопки */
        }
        100% {
            left: 150%; /* Конец за пределами кнопки справа */
        }
    }
</style>