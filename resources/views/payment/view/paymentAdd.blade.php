@extends('adminPanel.startAddProject')
@section('content')

    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> Добавить оплаты </div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('paymentAddControllerStore',$id)}}">
                            @csrf

                            <div class="row mb-3">
                                <label for="phoneWebOne" class="col-md-4 col-form-label text-md-end"> Вставьте номера</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" id="phoneWebOne" style="margin-bottom: 18px" name="phoneWebOne" rows="3"></textarea>
                                </div>

                                <label for="product" class="col-md-4 col-form-label text-md-end"> Название продукта </label>
                                <div class="col-md-6 mb-4">
                                    <input id="product" list="options" type="text"  class="form-control" name="product">
                                    <datalist id="options">
                                        @foreach($products as $key => $product)
                                            <option value="{{$key}}">
                                        @endforeach
                                    </datalist>
                                </div>
                                <label for="sum" class="col-md-4 col-form-label text-md-end"> Сумма </label>
                                <div class="col-md-6 mb-4">
                                    <input id="sum" type="number"  class="form-control" name="sum">
                                </div>


                                <label for="phoneWeb" class="col-md-4 col-form-label text-md-end"> Вставьте файл с номерами</label>
                                <div class="col-md-6">
                                    <input id="phoneWeb" type="file" class="form-control" name="phoneWeb" autofocus>
                                </div>
                            </div>



                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Загрузить
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        // Предполагаем, что у вас есть объект products, который содержит ключи продуктов и их суммы
        var products = {
            @foreach($products as $product => $sum)
            "{{ $product }}": {{ $sum }},
            @endforeach
        };

        document.addEventListener('DOMContentLoaded', (event) => {
            const productInput = document.getElementById('product');
            const sumInput = document.getElementById('sum');

            productInput.addEventListener('input', function() {
                const selectedProductKey = this.value;
                if (products.hasOwnProperty(selectedProductKey)) {
                    sumInput.value = products[selectedProductKey];
                } else {
                    sumInput.value = '';
                }
            });
        });
    </script>
@endsection
