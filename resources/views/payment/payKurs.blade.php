@extends('payment.payment')

@section('content')

    <style>.payform-tinkoff-row{display:block;margin:1%;width:160px;}</style>

    <main class="flex flex-grow-1 py-3">
        <section>
            <div class="container  col-sm-10 col-lg-6">

        <div style="padding:8px; margin-top: 60px">
            <link rel="stylesheet" href="https://yookassa.ru/integration/simplepay/css/yookassa_construct_form.css">
            <form class="yoomoney-payment-form" action="{{route('connectStoreKurs',['id' => $id])}}" method="post" accept-charset="utf-8" >
                @csrf
                <div class="ym-products">
                    <div class="ym-block-title ym-products-title">Страница оплаты</div>

                    <div class="ym-product">
                        <div class="ym-product-line">
                            <span class="ym-product-description" style="max-width: 350px">Предоплата 100к на Телеграм</span>
                            <span class="ym-product-price" data-price="{{$sum}}" data-id="306" data-count="1">{{$sum}} ₽</span>
                        </div>

                        <input disabled="" type="hidden" name="text" value="Предоплата 100к на Телеграм"><input disabled="" type="hidden" name="price" value="{{$sum}}"><input disabled="" type="hidden" name="quantity" value="1"><input disabled="" type="hidden" name="paymentSubjectType" value="commodity"><input disabled="" type="hidden" name="paymentMethodType" value="full_payment"><input disabled="" type="hidden" name="tax" value="1"></div></div>
                <div class="mt-3 ym-customer-info">
                    <div class="ym-block-title">Напишите свой номер</div>

                    <input name="cps_phone" class="form-control mask-phone" placeholder="Телефон" type="text" value="">

                </div>

                <div class="ym-hidden-inputs">
                    <input name="shopSuccessURL" type="hidden" value="https://sok-agency.com/success_490">
                    <input name="paymentType" type="hidden" value="AC">
                    <input name="sum" type="hidden" value="{{$sum}}">

                </div>


                <div class="ym-payment-btn-block ym-before-line ym-align-space-between d-flex justify-content-center">
                    <button style="height: 40px;width: 200px" class="btn btn-secondary">
                        Оплатить {{$sum}} ₽
                    </button>

                </div>
                <input name="shopId" type="hidden" value="820111"></form>
        </div>

            </div>
        </section>
    </main>


    <script src="https://snipp.ru/cdn/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://snipp.ru/cdn/maskedinput/jquery.maskedinput.min.js"></script>
    <script>
        $.mask.definitions['h'] = "[0|1|3|4|5|6|7|9]"
        $(".mask-phone").mask("+7 (h99) 999-99-99");
    </script>

@endsection
