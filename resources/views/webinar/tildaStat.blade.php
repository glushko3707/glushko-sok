@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper" id="app">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row">

                    <div class="col"></div>
                    <div class="col" style="text-align: center">
                        <h3> Статистика по вебинарам </h3>
                    </div><!-- /.col -->
                    <div class="col" style="    display: flex; justify-content: flex-end;">
                        <div class="col-lg-5 col-6 mr-5">
                            <!-- Виджет 1: Остаток бюджета -->
                            <a  href="{{ route('tildaLeadStatSave.index',$id) }}" class="btn period btn-outline-primary">Скачать файл</a>

                        </div>
                    </div>



                </div>

                <div>
                    <section class="content">
                        <div class="container-fluid">

                            <div class="row mt-3" style="justify-content: center">


                                <div class="card" style="width: 90%;padding: 0px">


                                    <div class="card-tools">
                                        <div class="row">
                                            <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                                                <div class="form-inline"  style="padding-left: 14px">

                                                    <div class="btn-group" onchange="submit()" id="period" name="period" style="border-radius: 15px">
                                                        <div class="btn-group">
                                                             <a href="{{route('tildaLeadStat.index',$id)}}?period=days" class="btn period btn-outline-primary active" aria-current="page">По дням</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <form action="{{route('tildaLeadStat.index',$id)}}" id="filterDate" method="GET">
                                                        <div class="form-inline float-right mr-3"  style="padding: 14px">
                                                            <div class="mr-3">
                                                            <select onchange="this.form.submit()" class="form-control input-opacity" name="warm" style="background-color: rgba(0, 0, 0, 0)">
                                                                <option {{(request('warm') == 'all') ? 'selected="selected"' : '' }} value="all">Все статусы</option>

                                                                <option {{(request('warm') == 'open') ? 'selected="selected"' : '' }} value="open">Открыта оплата</option>
                                                                <option {{(request('warm') == 'succeeded') ? 'selected="selected"' : '' }} value="succeeded">Оплачено</option>
                                                                <option {{(request('warm') == 'cold') ? 'selected="selected"' : '' }} value="cold">Не запустился</option>
                                                                <option {{(request('warm') == 'hot') ? 'selected="selected"' : '' }} value="hot">Горячий</option>
                                                                <option {{(request('warm') == 'normal') ? 'selected="selected"' : '' }} value="normal">Холодный</option>

                                                            </select>
                                                            </div>
                                                            <label  class="mr-3">От</label>
                                                            <input class="form-control mr-3" type="date" value="{{request('date_from')}}" name="date_from">
                                                            <label  class="mr-3">До</label>
                                                            <input class="form-control mr-3" type="date" value="{{request('date_to')}}" name="date_to">
                                                            <button type="submit" class="btn ml-3 btn-outline-primary ">Фильтр</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <!-- /.card-header -->
                                    <div class="card" style="margin-bottom: 0;box-shadow: unset">
                                        <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                            <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                                                <thead>

                                                    <tr>
                                                        <th>Дата</th>
                                                        <th>Количество</th>
                                                        <th>Яндекс</th>
                                                        <th>TG</th>
                                                        <th>Zvonobot</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($webinarUser as $key => $day)
                                                    <tr >

                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$key}}
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['count']}}
                                                                </div>

                                                            </div>                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['yandex']}}
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['tg']}}
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['zvonobot']}}
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['hot']}}
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['convertionWarm']}}
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['payment']}}
                                                                </div>
                                                            </div>
                                                        </th>
                                                        <th style="font-weight: normal">
                                                            <div class="d-flex justify-content-start">
                                                                <div>
                                                                    {{$day['convertionPay']}}
                                                                </div>
                                                            </div>
                                                        </th>





                                                    </tr>
                                                @endforeach

                                                </tbody>
{{--                                                <tfoot style="background: #ffffff;font-weight: bold;position: sticky; bottom: 0px;">--}}
{{--                                                <td>--}}
{{--                                                    Всего--}}
{{--                                                </td>--}}
{{--                                                <td class="rub">--}}
{{--                                                    {{$all['rashod'] ?? ''}}--}}
{{--                                                </td>--}}
{{--                                                <td>--}}
{{--                                                    {{$all['newleads'] ?? ''}}--}}
{{--                                                </td>--}}
{{--                                                <td class="rub">--}}
{{--                                                    {{$all['leadPrice'] ?? ''}}--}}
{{--                                                </td>--}}

{{--                                                @if (request('period') == 'phone')--}}
{{--                                                    <td>--}}
{{--                                                    </td>--}}
{{--                                                    <td>--}}
{{--                                                    </td>--}}
{{--                                                @endif--}}

{{--                                                </tfoot>--}}
                                            </table>
                                        </div>

                                    </div>

                                </div>
                            </div>





                        </div>
                    </section>
                </div>



            </div>
            <!-- /.content -->
        </div>
    </div>


    <script>
        // Получаем параметр utm_source из адресной строки
        let urlsl = new URL(window.location.href);
        let paramses = new URLSearchParams(urlsl.search);
        let from = paramses.get("date_from");
        let warm = paramses.get("warm");

        let to = paramses.get("date_to");

        // Выбираем все ссылки на странице
        let links = document.getElementsByClassName("period");

        // Перебираем все ссылки в цикле
        for (let i = 0; i < links.length; i++) {
            // Создаем объект URL из href ссылки
            let url = new URL(links[i].href);

            if (from !== null){
                url.searchParams.append("date_from", from);
            }
            if (warm !== null){
                url.searchParams.append("warm", warm);
            }
            if (to !== null){
                url.searchParams.append("date_to", to);
            }
            // Добавляем параметр utm_source к объекту URL

            // Обновляем href ссылки с новым URL
            links[i].href = url.toString();
        }


    </script>

    <script>
        function getCurrentDate() {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0');
            var yyyy = today.getFullYear();
            return yyyy + '-' + mm + '-' + dd;
        }

        function addParamsToUrl(url, params) {
            var newUrl = new URL(url);
            for (var key in params) {
                newUrl.searchParams.append(key, params[key]);
            }
            return newUrl.toString();
        }

        var link = document.getElementById("link");

        // Получить текущую дату
        var currentDate = getCurrentDate();

        // Добавить параметры date_from и date_to к url ссылки
        var newUrl = addParamsToUrl(link.href, {date_from: currentDate, date_to: currentDate, period:'webStat'});

        // Обновить атрибут href ссылки
        link.href = newUrl;

    </script>

@endsection
