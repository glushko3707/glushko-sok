@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper" id="intance_all">
        <div class="row justify-content-center">
            <div class="col-md-8" style="margin-top: 30px">
                <div class="card mt-4">

                    <div class="card-header d-flex justify-content-between">
                        <div class="col-12 text-center">
                            <h4>
                                Подключение нового номера
                            </h4>
                        </div>
                    </div>

                    <div class="card-body">
                        <form action="{{route('addInstanceDatabase.store',$id)}}" method="post">
                        @csrf
                        <div class="d-flex justify-center">
                            <div style="width: 250px">
                                <label class="col-form-label ml-2 text-md-start"> Номер телефона </label>
                                <div class="col-md-12">
                                    <input id="phone" class="form-control" type="text" style="margin-bottom: 18px" value="{{$whatsUp['instance_phone'] ?? ""}}" name="instance_phone">
                                </div>
                            </div>

                            <div id="getInstance">
                                <label  class="col-form-label ml-2 text-md-start"> Получить Intance </label>
                                <div class="col-md-12">
                                    <button type="button" id="getInstanceButton" class="btn btn-outline-primary" data-project="{{$id}}"  onclick="getIntance()">
                                        Получить Intance
                                    </button>
                                </div>
                            </div>
                            <div class="col-2" id="Instance" style="display: none">
                                <div class="col-md-12">
                                    <input class="form-control" type="hidden" id="instance_id" style="margin-bottom: 18px" value="{{$whatsUp['instance_id'] ?? ""}}" name="instance_id" >
                                </div>
                            </div>
                            <div id="Save" style="display: none">
                                <label  class="col-form-label ml-2 text-md-start"> Сохранить </label>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary"> Сохранить </button>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

                <div style="background: transparent">
                    <div class="mb-2 text-center" id="beforeQR" style="display:none">
                        Подождите 3-10 секунд, пока загрузится QR
                    </div>
                    <div class="text-center" id="afterQR" style="display:none">
                        Отсканируйте QR в приложении Whats Up.<br> Затем проверьте, что появился новый Instance <a href="https://whatsmonster.ru/whatsapp_profile" target="_blank"> ЗДЕСЬ</a>

                        <div class="d-flex mb-2 justify-center">
                            <img id="qr" width="300" height="300">
                        </div>
                        <div>
                            После проверки сохраните результат
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <script>
        const qr = document.getElementById('qr');
        const instance_id = document.getElementById('instance_id');
        const id = {{$id}};


        function updateQR() {

            $.ajax({
                url: "/api/wa/instance/getQr/" + id + "/" + instance_id.value,
                type: 'POST',
                data: {
                    'instance_id': instance_id.value,
                    'id': id,
                },
                success: function (response) {
                    qr.src = response.qr
                    $('#getInstance').css('display','none')
                    $('#beforeQR').css('display','none')
                    $('#afterQR').css('display','')
                    $('#Save').css('display','')
                }
            });

        }

        function getIntance(){
            $('#beforeQR').css('display','')
            $('#getInstanceButton').attr('disabled',true)

            $.ajax({
                url: "{{ route('addInstance.store', ['id' => $id]) }}",
                type: 'POST',
                success: function (response) {
                    instance_id.value = response
                    // $('#Instance').css('display','')
                    // $('#QR').css('display','')
                    updateQR()
                },
            });
        }
    </script>



@endsection
