@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper">
        <div class="row justify-content-center">
            <div class="col-md-10" style="margin-top: 30px">
                @if (session()->has('success') || session()->has('error'))
                    <div id="allert" class="alert alert-{{ session()->has('success') ? 'success' : 'danger' }}">
                        {{ session()->get('success') ?? session()->get('error') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('SendWaMessage.store',$id)}}">

                <div class="card mt-4">
                    <div class="card-header d-flex justify-content-between">
                        <div class="col-6">
                            <h4>
                                Приветственные WA сообшения
                            </h4>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div class="col-8">
                                <div class="ml-2">
                                    <h5>Введите текст WA </h5>
                                </div>
                                <div>
                                    <textarea style="width: 100%;height: 300px" name="text">Введи текст</textarea>
                                </div>
                            </div>
                            <div class="col-4">
                                <div style="align-items: flex-end">
                                    <div class="col-md-12 mt-5 p-0">
                                        <div class="mb-0">Номер телефона
                                        </div>
                                            @csrf
                                            <div class="input-group">
                                                <input class="form-control" type="number" id="wapico_key"  name="phone" >
                                                <span class="input-group-append">
                                                       <button   class="btn btn-success" type="submit"> Отправить </button>
                                                    </span>
                                            </div>
                                            @if ($errors->has('phone'))
                                                <p style="color: red">{{ $errors->first('phone') }}</p>
                                            @endif

                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                </div>
                </form>

            </div>
        </div>
    </div>


    <script>
        // Получаем элемент по id
        var alert = document.getElementById("allert");

        // Устанавливаем таймер на 3 секунды
        setTimeout(function() {
                alert.style.display = "none";
        }, 3000);
    </script>
@endsection
