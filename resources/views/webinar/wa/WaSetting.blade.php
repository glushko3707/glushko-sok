@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper" id="">
        <div class="row justify-content-center">
            <div class="col-md-10" style="margin-top: 30px">
                <div class="card mt-4">

                    <div class="card-header d-flex justify-content-between">
                        <div class="col-6 text-start">
                            <h4>
                                Настройки WA
                            </h4>
                        </div>
                        <div class="col-6">
                            <div class="d-flex justify-end">
                                <div class="form-inline">
                                    <label class="col-form-label mr-2" style="font-weight: 500"> Номер телефона для проверки</label>
                                    <input style="width: 150px"  class="form-control" id="test_phone" name="test_phone">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        @foreach($wa as $key => $whatsUp)

                        <div class="d-flex justify-center">
                                <div>
                                    <label class="col-form-label ml-2 text-md-start"> Номер телефона </label>
                                    <div class="col-md-12">
                                        <input type="hidden" value="{{$whatsUp['id'] ?? ""}}" name="id" >
                                        <input disabled class="form-control" id="instance_phone_{{$key}}" style="margin-bottom: 18px" value="{{$whatsUp['instance_phone'] ?? ""}}" name="instance_phone" >
                                    </div>
                                </div>
                                <div>
                                    <label class="col-form-label ml-2 text-md-start"> Intance ID </label>
                                    <div class="col-md-12">
                                        <input disabled class="form-control"  style="margin-bottom: 18px" value="{{$whatsUp['instance_id'] ?? ""}}" name="instance_id" >
                                    </div>
                                </div>
                                <div>
                                     <label class="col-form-label ml-2 text-md-start"> ACTIVE </label>
                                    <div class="d-flex justify-center col-md-12">
                                        <input disabled type="checkbox" {{ $whatsUp['active'] == 1 ? 'checked' : '' }} class="form-check-input ml-0 mt-0" style="width: 37px;height: 37px; margin-bottom: 18px" name="active" >
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-12">
                                        <a href="{{route('instanceCheck.store',['id' => $id,'instance_id' => $whatsUp['instance_id'] ])}}"  style="margin-top: 37px" class="btn btn-outline-primary"> Перезагрузка </a>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-12">
                                        <button onclick="testPhone('{{$whatsUp['instance_id']}}')" id="button_test" type="button" style="margin-top: 37px" class="btn btn-outline-primary"> Проверить </button>
                                    </div>
                                </div>

                                <div>
                                    <div class="col-md-12">
                                        <a href="{{route('instance.delete',['id' => $id,'instance_id' => $whatsUp['instance_id'] ])}}" style="margin-top: 37px"  class="btn btn-danger"> Удалить </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="d-flex mt-3 justify-content-center">
                    <div class="m-2">
                        <a href="{{route('addInstance.index',$id)}}" class="btn btn-primary" style="align-items: center;margin: auto; width: 226px;">
                            Добавить номер
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function testPhone(instance_id){
            // Проверяем, есть ли значение у input
            if ($('#test_phone').val() == '') {
                // Если нет, делаем фокус на нем
                $('#test_phone').focus();
                // Задаем время в миллисекундах (3 секунды = 3000 миллисекунд)
                // Создаем функцию, которая показывает alert с сообщением
                function showAlert() {
                    alert('Введите номер телефона!');
                }
                // Используем функцию setTimeout для вызова функции showAlert через заданное время
                showAlert()
            } else {
                $.ajax({
                    url: "{{ route('instanceTestMessage.store', ['id' => $id]) }}",
                    type: 'POST',
                    data: {
                        'instance_id': instance_id,
                        'phone': $('#test_phone').val(),
                    },
                    success:function(response){
                    }
                });
            }



        }
    </script>

@endsection
