@extends('webinar.adminPanel.adminPanel')

@section('contentCrm')
    <div class="content-wrapper" id="app">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

                <div>
                    <section class="content">
                        <div class="container-fluid">

                        <div class="row" style="justify-content: center">


                            <div class="row" style="display: flex; justify-content: center;background-color: #f4f6f9  ; width: 90%;padding: 0px;">

                                <div class="col-6 mt-2" style="max-width: 49%;margin-right: 10px;display: flex;flex-direction: column;padding: 15px;box-shadow: 0px 0px 5px 1px #d1d1d1 inset; margin-bottom: 10px">
                                    <div class="mb-2 mt-2 ml-1" style="display: flex;justify-content: start">
                                        <h5> {{$lastStats['date'] ?? 'Сегодня'}} </h5>
                                    </div>
                                    <div style="display: flex;">
                                        <div class="col-4">
                                            <!-- Виджет 1: Остаток бюджета -->
                                            <div class="maksCard small-box">
                                                <div class="inner">
                                                    <p> Затраты </p>
                                                    <h1> {{$lastStats['sum'] ?? '0'}} ₽ </h1>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <!-- Виджет 1: Остаток бюджета -->
                                            <div class="maksCard small-box">
                                                <div class="inner">
                                                    <p> Лиды </p>
                                                    <h1> {{$lastStats['lead'] ?? '0'}} </h1>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <!-- Виджет 1: Остаток бюджета -->
                                            <div class="maksCard small-box">
                                                <div class="inner">
                                                    <p> Стоимость лидов </p>
                                                    <h1> {{$lastStats['leadPrice'] ?? '0'}} ₽ </h1>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-6 mt-2" style="max-width: 49%;margin-left: 10px;display: flex;flex-direction: column;padding: 15px;box-shadow: 0px 0px 5px 1px #d1d1d1 inset; margin-bottom: 10px">
                                    <div class="mb-2 mt-2 ml-1" style="display: flex;justify-content: start">
                                        <h5> {{$lastStats['lastTimes'] ?? ''}} </h5>
                                    </div>
                                    <div style="display: flex;">
                                        <div class="col-4">
                                            <!-- Виджет 1: Остаток бюджета -->
                                            <div class="maksCard small-box">
                                                <div class="inner">
                                                    <p> Затраты </p>
                                                    <h1> {{$lastStats['lastTimeMoney'] ?? '0'}} </h1>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <!-- Виджет 1: Остаток бюджета -->
                                            <div class="maksCard small-box">
                                                <div class="inner">
                                                    <p> Лиды </p>
                                                    <h1> {{$lastStats['lastTimeLeads'] ?? '0'}} </h1>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-speedometer"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <!-- Виджет 1: Остаток бюджета -->
                                            <div class="maksCard small-box">
                                                <div class="inner">
                                                    <p> Стоимость лидов </p>
                                                    <h1> {{$lastStats['lastTimeLeadsPrice'] ?? '0'}} </h1>
                                                </div>
                                                <div class="icon">
                                                    <i class="ion ion-speedometer"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="card" style="width: 90%;padding: 0px">


                        <div class="card-tools">
                            <div class="row">
                                <div class="col align-self-center" style="display: flex;justify-content: space-between;">
                                        <div class="form-inline"  style="padding-left: 14px">

                                            <div class="btn-group" onchange="submit()" id="period" name="period" style="border-radius: 15px">
                                                <div class="btn-group">
                                                    @if (request('period') == 'days' || request('period') == null)
                                                    <a href="{{route('sipOutStat',$id)}}?period=days" class="btn period btn-outline-primary active" aria-current="page">По дням</a>
                                                    @else
                                                    <a href="{{route('sipOutStat',$id)}}?period=days" class="btn period btn-outline-primary" aria-current="page">По дням</a>
                                                    @endif

                                                    <div class="btn-group">
                                                        @if (request('period') == 'phone' )
                                                            <a href="{{route('sipOutStat',$id)}}?period=phone" class="btn period btn-outline-primary active" aria-current="page">По номерам</a>
                                                        @else
                                                            <a href="{{route('sipOutStat',$id)}}?period=phone" class="btn period btn-outline-primary" aria-current="page">По номерам</a>
                                                        @endif
                                                   </div>

                                            </div>
                                            </div>
                                            @if (request('period') == 'days' || request('period') == null)
                                                <a class="ml-3">c учетом стоимости номеров</a>
                                            @elseif(request('date_from') != now()->format('Y-m-d'))
                                                <a class="ml-3" id="link" href="{{route('sipOutStat',$id)}}">Посмотреть за cегодня</a>
                                            @elseif(request('date_from') == now()->format('Y-m-d'))
                                                <a class="ml-3" id="linkAll" href="{{route('sipOutStat',$id)}}?period=phone">Посмотреть за все время</a>
                                            @endif

                                        </div>
                                        <div>
                                            <form action="{{route('sipOutStat',$id,request('period'))}}" id="filterDate" method="GET">
                                            <div class="form-inline float-right mr-3"  style="padding: 14px">
                                                <label  class="mr-3">От</label>
                                                <input class="form-control mr-3" type="date" value="{{request('date_from')}}" name="date_from">
                                                <label  class="mr-3">До</label>
                                                <input class="form-control mr-3" type="date" value="{{request('date_to')}}" name="date_to">
                                                <button type="submit" class="btn ml-3 btn-outline-primary ">Фильтр</button>
                                                </div>
                                            </form>
                                        </div>
                                </div>

                            </div>
                        </div>


                        <!-- /.card-header -->
                        <div class="card" style="margin-bottom: 0;box-shadow: unset">
                            <div class="card-body table-responsive p-0" style="max-height: 450px;">
                                <table class="table table-head-fixed table-striped table-sm table-bordered text-nowrap">

                                    <thead>
                                    <tr>
                                        @if (request('period') == 'phone' )
                                            <th width="15%">Номер</th>
                                        @else
                                            <th width="15%">Дата и время</th>
                                        @endif
                                        <th width="15%">Расходы</th>
                                        <th>Лиды</th>
                                        <th>Цена лида</th>
                                        @if (request('period') == 'phone')
                                                <th>Номер телефона</th>
                                                <th width="30%">Комментарий</th>
                                                @if(auth()->user()->role == 'superAdmin')

                                                <th>Вероятность СПАМа</th>
                                                @endif

                                        @endif
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($stat as $key => $day)
                                    <tr >
                                        <th style="font-weight: normal">
                                            {{$key}}
                                        </th>
                                        <th style="font-weight: normal">
                                            {{$day['rashod']}} ₽
                                        </th>
                                        <th style="font-weight: normal">
                                            {{$day['newleads']}}
                                        </th>
                                        <th style="font-weight: normal">
                                            {{round($day['leadPrice'])}} ₽
                                        </th>
                                        @if (request('period') == 'phone')
                                            <th style="font-weight: normal; font-size: 14px">
                                                <input id="phone{{$day['value']}}" style="width: 100%" onblur="updateRow(this,{{$day['value']}})"  type="text"  class="input-opacity" name="phone" value="{{$day['phone'] ?? ''}}">
                                            </th>
                                            <th style="font-weight: normal; font-size: 14px">
                                            <input id="timeProgram{{$day['value']}}" style="width: 100%" onblur="updateRow(this,{{$day['value']}})"  type="text"  class="input-opacity" name="comment" value="{{$day['comment'] ?? ''}}">
                                            </th>
                                            @if(auth()->user()->role == 'superAdmin')

                                            <th style="font-weight: normal">
                                                {{round($day['spamCef'] ?? '')}} %
                                            </th>
                                            @endif

                                        @endif



                                    </tr>
                                    @endforeach
                                    </tbody>
                                    <tfoot style="background: #ffffff;font-weight: bold;position: sticky; bottom: 0px;">
                                    <td>
                                        Всего
                                    </td>
                                    <td>
                                        {{$all['rashod'] ?? ''}} ₽
                                    </td>
                                    <td>
                                        {{$all['newleads'] ?? ''}}
                                    </td>
                                    <td>
                                        {{$all['leadPrice'] ?? ''}} ₽
                                    </td>

                                    @if (request('period') == 'phone')
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                    @endif

                                    </tfoot>
                                </table>
                            </div>

                        </div>

                    </div>
                </div>





                        </div>
                    </section>
                </div>



            </div>
            <!-- /.content -->
        </div>
    </div>

    <script>
        // Получаем параметр utm_source из адресной строки
        let url = new URL(window.location.href);
        let params = new URLSearchParams(url.search);
        let period = params.get("period");

        // Создаем скрытое поле с именем utm_source и значением параметра
        let hiddenField = document.createElement("input");
        hiddenField.type = "hidden";
        hiddenField.name = "period";
        hiddenField.value = period;

        // Добавляем скрытое поле в форму
        const form = document.getElementById('filterDate');

        form.appendChild(hiddenField);
    </script>

    <script>
        // Получаем параметр utm_source из адресной строки
        let urlsl = new URL(window.location.href);
        let paramses = new URLSearchParams(urlsl.search);
        let from = paramses.get("date_from");

        let to = paramses.get("date_to");

        // Выбираем все ссылки на странице
        let links = document.getElementsByClassName("period");

        // Перебираем все ссылки в цикле
        for (let i = 0; i < links.length; i++) {
            // Создаем объект URL из href ссылки
            let url = new URL(links[i].href);

            if (from !== null){
                url.searchParams.append("date_from", from);
            }

            if (to !== null){
                url.searchParams.append("date_to", to);
            }
            // Добавляем параметр utm_source к объекту URL

            // Обновляем href ссылки с новым URL
            links[i].href = url.toString();
        }


    </script>

    <script>
        function getCurrentDate() {
            var today = new Date();
            var dd = String(today.getDate()).padStart(2, '0');
            var mm = String(today.getMonth() + 1).padStart(2, '0');
            var yyyy = today.getFullYear();
            return yyyy + '-' + mm + '-' + dd;
        }

        function addParamsToUrl(url, params) {
            var newUrl = new URL(url);
            for (var key in params) {
                newUrl.searchParams.append(key, params[key]);
            }
            return newUrl.toString();
        }

        var link = document.getElementById("link");

        // Получить текущую дату
        var currentDate = getCurrentDate();

        // Добавить параметры date_from и date_to к url ссылки
        var newUrl = addParamsToUrl(link.href, {date_from: currentDate, date_to: currentDate, period:'phone'});

        // Обновить атрибут href ссылки
        link.href = newUrl;

    </script>

    <script>

        function updateRow(row,messege_id,id){
            let text = document.getElementById(row.id).value

            $.ajax({
                url: "/api/sipoutCallIdUpdateComment/" + id ,
                type: 'POST',
                data: {
                    'text': text,
                    'type': row.name,
                    'messege_id': messege_id,
                    'id': id,
                },
                success:function(response){

                }
            });
        }

        function submit(){
            const form = document.getElementById('formed');
            form.submit()
        }
    </script>

@endsection
