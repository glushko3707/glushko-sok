@extends('webinar.adminPanel.adminPanel')

@section('contentCrm')





    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header">  </div>

                    <div class="card-body">
                        <h5 class="mb-5 mt-3" style="text-align: center">Укажите сумму пополнения, которое было примерно в указанную дату и время</h5>
                        <form method="POST" action="{{ route('sipoutAddMoney.store',$id)}}">
                            @csrf

                            @foreach($moneysLast as $key)
                                <div class="row mb-3">
                                    <label for="secretKey" class="col-md-4 col-form-label text-md-end">{{\Carbon\Carbon::parse($key->created_at)->format('d.m.Y в H:i')}}</label>
                                    <div class="col-md-6">
                                        <input class="form-control" id="secretKeyZvonobot" style="margin-bottom: 18px" name="rec_{{$key->id}}" >
                                    </div>
                                </div>
                            @endforeach


                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Загрузить
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
