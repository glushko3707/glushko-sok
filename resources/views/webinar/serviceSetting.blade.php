@extends('adminPanel.startAddProject')

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10">
                <div class="row">
                    <div class="col-lg-3 col-md-6 mb-4">
                        <!-- Виджет 1: Остаток бюджета - Sms Aero -->
                        <div class="small-box bg-light shadow-sm p-3 rounded">
                            <div class="inner text-center">
                                <p class="mb-1">Sms Aero</p>
                                <h4 class="mb-0">{{ $stat['sms'] ?? '0' }} ₽</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 mb-4">
                        <!-- Виджет 2: Остаток бюджета - Zvonobot -->
                        <div class="small-box bg-light shadow-sm p-3 rounded">
                            <div class="inner text-center">
                                <p class="mb-1">Zvonobot</p>
                                <h4 class="mb-0">{{ $stat['zvonobot'] ?? '0' }} ₽</h4>
                            </div>
                        </div>
                    </div>
                    <!-- Добавьте дополнительные виджеты здесь при необходимости -->
                </div>

                <div class="card">
                    <div class="card-header text-center">
                        <h4>Настройки сервисов доведения до вебинара</h4>
                    </div>
                    <div class="card-body pb-5">
                        @if($errors->any())
                            <div class="alert alert-danger d-flex flex-column align-items-center" role="alert">
                                @foreach($errors->all() as $error)
                                    <span>{{ $error }}</span>
                                @endforeach
                            </div>
                        @endif

                        <!-- Навигационные вкладки -->
                        <ul class="nav nav-pills mb-4 flex-wrap justify-content-center" id="settingsTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="general-tab" data-toggle="tab" href="#general" role="tab" aria-controls="general" aria-selected="true">Общие</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="zvonobot-tab" data-toggle="tab" href="#zvonobot" role="tab" aria-controls="zvonobot" aria-selected="false">Звонобот</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="sms-tab" data-toggle="tab" href="#sms" role="tab" aria-controls="sms" aria-selected="false">Sms</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="whatsUp-tab" data-toggle="tab" href="#whatsUp" role="tab" aria-controls="whatsUp" aria-selected="false">Whats Up</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="bizon-tab" data-toggle="tab" href="#bizon" role="tab" aria-controls="bizon" aria-selected="false">Bizon 365</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="crm-tab" data-toggle="tab" href="#crm" role="tab" aria-controls="crm" aria-selected="false">Crm</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="permission-tab" data-toggle="tab" href="#permission" role="tab" aria-controls="permission" aria-selected="false">Доступы</a>
                            </li>
                        </ul>

                        <!-- Содержимое вкладок -->
                        <div class="tab-content" id="settingsTabContent">
                            <div class="tab-pane fade show active" id="general" role="tabpanel" aria-labelledby="general-tab">
                                @include('components.serviceSetting.general')
                            </div>
                            <div class="tab-pane fade" id="zvonobot" role="tabpanel" aria-labelledby="zvonobot-tab">
                                @include('components.serviceSetting.zvonobot')
                            </div>
                            <div class="tab-pane fade" id="sms" role="tabpanel" aria-labelledby="sms-tab">
                                <form action="{{ route('smsAeroSetting.store', $id) }}" method="POST" class="needs-validation" novalidate>
                                    @csrf
                                    <div id="smsAeroDisplay" class="row mb-3">
                                        <label for="smsAero_key" class="col-md-4 col-form-label text-md-end">
                                            SmsAero API-КЛЮЧ
                                            <span class="tooltip_2">
                                                <span class="tooltip_link">
                                                    Где взять ключ?
                                                    <a href="https://smsaero.ru/cabinet/settings/apikey/" target="_blank" rel="noopener">ПЕРЕЙТИ</a>
                                                </span>
                                                <ion-icon name="help-circle-outline"></ion-icon>
                                            </span>
                                        </label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control mb-3" id="smsAero_key" name="smsAero_key" value="{{ $notificationSetting['smsAero_key'] ?? '' }}" required>
                                            <input type="hidden" name="service" value="smsAero">
                                        </div>

                                        <label for="smsAeroEmail" class="col-md-4 col-form-label text-md-end">E-mail smsAero</label>
                                        <div class="col-md-6">
                                            <input type="email" class="form-control mb-3" id="smsAeroEmail" name="smsAeroEmail" value="{{ $notificationSetting['smsAeroEmail'] ?? '' }}" required>
                                        </div>

                                        @if($smsAeroSenderName)
                                            <label for="smsAeroSenderName" class="col-md-4 col-form-label text-md-end mt-4">Имя отправителя</label>
                                            <div class="col-md-6 mt-4">
                                                <select class="form-control" id="smsAeroSenderName" name="smsAeroSenderName" required>
                                                    @foreach($smsAeroSenderName as $name)
                                                        <option value="{{ $name }}" {{ ($getSmsSetting['smsAeroSenderName'] ?? '') == $name ? 'selected' : '' }}>
                                                            {{ $name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        @else
                                            <div class="col-12 text-center mt-4">
                                                Зарегистрируй Send Name <a href="https://smsaero.ru/cabinet/signs/" target="_blank" rel="noopener">ПЕРЕЙТИ</a>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">Сохранить</button>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade" id="bizon" role="tabpanel" aria-labelledby="bizon-tab">
                                @include('components.serviceSetting.bizon')
                            </div>
                            <div class="tab-pane fade" id="whatsUp" role="tabpanel" aria-labelledby="whatsUp-tab">
                                <ul class="nav nav-pills mb-4 flex-wrap justify-content-center" id="whatsUpSubTab" role="tablist">
                                    <li class="nav-item mr-2">
                                        <a class="nav-link active" id="waba-tab" data-toggle="tab" href="#waba" role="tab" aria-controls="waba" aria-selected="true">Waba</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="whatsMonster-tab" data-toggle="tab" href="#whatsMonster" role="tab" aria-controls="whatsMonster" aria-selected="false">whatsMonster</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="whatsUpSubTabContent">
                                    <div class="tab-pane fade show active" id="waba" role="tabpanel" aria-labelledby="waba-tab">
                                        <form action="{{ route('wabaToken.store', $id) }}" method="POST" class="needs-validation" novalidate>
                                            @csrf
                                            <div class="row mb-3">
                                                <label for="wabaToken" class="col-md-4 col-form-label text-md-end">WABA KEY</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="wabaToken" name="wabaToken" value="{{ $ServiceSetting->waba_token ?? '' }}" required>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="whatsMonster" role="tabpanel" aria-labelledby="whatsMonster-tab">
{{--                                        <form action="{{ route('whatsMonster.store', $id) }}" method="POST" class="needs-validation" novalidate>--}}
                                            @csrf
                                            <div class="row mb-3">
                                                <label for="wapico_key" class="col-md-4 col-form-label text-md-end">WHATSMONSTER.RU</label>
                                                <div class="col-md-6">
                                                    <input type="text" class="form-control" id="wapico_key" name="wapico_key" value="{{ $notificationSetting['wapico_key'] ?? '' }}" required>
                                                </div>
                                            </div>
                                            <div class="row mb-3">
                                                <label for="whatsUpSettings" class="col-md-4 col-form-label text-md-end">Настройки Whats Up</label>
                                                <div class="col-md-6">
                                                    <a href="{{ route('instanceSetting.index', $id) }}" class="btn btn-info">Перейти</a>
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <button type="submit" class="btn btn-primary">Сохранить</button>
                                            </div>
{{--                                        </form>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="crm" role="tabpanel" aria-labelledby="crm-tab">
                                @include('components.serviceSetting.crm')
                            </div>
                            <div class="tab-pane fade" id="permission" role="tabpanel" aria-labelledby="permission-tab">
                                @include('components.serviceSetting.permission')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Скрипты -->
        <script>
            // Функция для подтверждения удаления
            function confirmDelete() {
                return confirm("Вы уверены, что хотите удалить проект?");
            }

            // Обработчик кнопок копирования
            document.querySelectorAll('.copy-button').forEach(function(button) {
                button.addEventListener('click', function() {
                    var copyText = this.closest('.input-group').querySelector('.copy-text').textContent.trim();

                    // Создаём временный textarea для копирования
                    var temp = document.createElement("textarea");
                    temp.style.position = "absolute";
                    temp.style.left = "-9999px";
                    document.body.appendChild(temp);
                    temp.value = copyText;
                    temp.select();
                    document.execCommand("copy");
                    document.body.removeChild(temp);

                    // Визуальное подтверждение копирования
                    var originalText = this.innerText;
                    this.innerText = 'Скопировано!';
                    this.disabled = true;
                    setTimeout(() => {
                        this.innerText = originalText;
                        this.disabled = false;
                    }, 2000);
                });
            });

            // Обработчик радиокнопок для переключения сервисов
            document.addEventListener("DOMContentLoaded", function() {
                const radios = document.getElementsByName("service");
                const smsAero = document.getElementById("smsAeroDisplay");
                const P1smsDisplay = document.getElementById("P1smsDisplay");

                radios.forEach(radio => {
                    radio.addEventListener("change", function() {
                        if (this.value === "p1Sms") {
                            smsAero.style.display = "none";
                            P1smsDisplay.style.display = "";
                        } else {
                            smsAero.style.display = "";
                            P1smsDisplay.style.display = "none";
                        }
                    });
                });
            });
        </script>
    </div>
@endsection
