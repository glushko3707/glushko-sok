@extends('webinar.adminPanel.adminPanel')

@section('contentCrm')





    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"> вставить лиды в Амо </div>

                    <div class="card-body">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('changeStatusAmoStore',$id)}}">
                            @csrf

                            <div class="row mb-3">
                                <label for="phoneWebOne" class="col-md-4 col-form-label text-md-end"> Вставьте номера</label>
                                <div class="col-md-6">
                                    <textarea class="form-control" id="phoneWebOne" style="margin-bottom: 18px" name="phoneWebOne" rows="3"></textarea>
                                </div>

                                <label for="phoneWeb" class="col-md-4 col-form-label text-md-end"> Вставьте файл с номерами</label>
                                <div class="col-md-6">
                                    <input id="phoneWeb" type="file" class="form-control" name="phoneWeb" autofocus>
                                </div>


                            </div>



                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Загрузить
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>







@endsection
