@extends('adminPanel.startAddProject')

@section('content')





    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">

                <div class="card">
                    <div style="text-align: center" class="card-header"> <h4>Настройки ссылок</h4> </div>

                    <div class="card-body">


                            @foreach($linkChangeModel as $key => $link)
                            <form method="POST" action="{{ route('linkChangeSetting.store',$id)}}">
                                @csrf

                                <div class="row">
                                    <div class="col-3">
                                        <label for="utm_sourceNew" class="col-form-label ml-2 text-md-start"> Source </label>
                                        <div class="col-md-12">
                                            <input class="form-control" id="utm_sourceNew" style="margin-bottom: 18px" value="{{$link['utm_source'] ?? ""}}" name="utm_source" >
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="linkNameNew" class="col-form-label ml-2 text-md-start"> linkName </label>
                                        <div class="col-md-12">
                                            <input class="form-control" id="linkNameNew{{$link['id']}}" style="margin-bottom: 18px" value="{{$link['linkName'] ?? ""}}" name="linkName" >
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="linkNew" class="col-form-label ml-2 text-md-start"> link </label>
                                        <div class="col-md-12">
                                            <input type="text" class="form-control" id="linkNew{{$link['id']}}" style="margin-bottom: 18px" value="{{$link['link'] ?? ""}}" name="link" >
                                        </div>
                                    </div>
                                    <input type="hidden" class="form-control" style="margin-bottom: 18px" value="{{$link['id']}}" name="id" >

                                    <div class="col-2 d-flex align-items-center">
                                        <button type="submit" class="mt-3 btn btn-primary"> Сохранить </button>
                                    </div>

                                </div>
                            </form>

                        @endforeach
                        <div class="mt-4">
                            Новый
                        </div>

                            <form method="POST" action="{{ route('linkChangeSetting.store',$id)}}">
                                @csrf

                            <div class="row mt-4">
                                <div class="col-3">
                                    <label for="utm_sourceNew" class="col-form-label ml-2 text-md-start"> Source </label>
                                    <div class="col-md-12">
                                        <input class="form-control" id="utm_sourceNew" style="margin-bottom: 18px" value="" name="utm_source" >
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label for="linkNameNew" class="col-form-label ml-2 text-md-start"> linkName </label>
                                    <div class="col-md-12">
                                        <input class="form-control" id="linkNameNew" style="margin-bottom: 18px" value="" name="linkName" >
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label for="linkNew" class="col-form-label ml-2 text-md-start"> link </label>
                                    <div class="col-md-12">
                                        <input type="text" class="form-control" id="linkNew" style="margin-bottom: 18px" value="" name="link" >
                                    </div>
                                </div>
                                <input type="hidden" class="form-control" style="margin-bottom: 18px" value="new" name="id" >

                                <div class="col-2 d-flex align-items-center">
                                <button type="submit" class="mt-3 btn btn-primary"> Сохранить </button>
                                </div>

                            </div>
                        </form>
                    </div>

                </div>

            </div>

        </div>
    </div>


@endsection
