@extends('adminPanel.startAddProject')

@section('content')
123
    <div class="content-wrapper ">
        <main class="flex flex-grow-1 py-3">
            <section>
                <div class="container" >

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">ID</th>
                            <th scope="col">Время</th>
                            <th scope="col">Тип контента</th>
                            <th scope="col">Контент ID</th>
                            <th scope="col">Приоритет</th>
                            <th scope="col">Активый?</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($webNotificationPipleline as $step)
                        <tr>
                            <th scope="row">{{$step['id']}}</th>
                            <td>{{$step['timeSend']}}</td>
                            <td>{{$step['content_type']}}</td>
                            <td>{{$step['content_id']}}</td>
                            <td>{{$step['priority']}}</td>
                            <td>{{$step['active']}}</td>

                        </tr>
                        @endforeach
                        </tbody>
                    </table>


                </div>
            </section>
        </main>
    </div>

@endsection
