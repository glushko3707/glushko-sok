@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper ">
        <main class="flex flex-grow-1 py-3">
            <section style="width: 100%">
                <div class="container">

                    <div class="row" style="justify-content: start;display: flex; " >
                        <div class="col-lg-3 col-6">
                            <!-- Виджет 1: Остаток бюджета -->
                            <div class="small-box">
                                <div class="inner">
                                    <p style="margin-bottom: 0.7rem"> Дата вебинара </p>
                                    <form id="changeDateWeb"  action="{{route('changeDateWeb',$id)}}" method="post">
                                        @csrf
                                        <div class="input-group">
                                            <input style="background-color: rgba(0, 0, 0, 0);border-color: #c9c9c9;border-top-left-radius: 6px; border-bottom-left-radius: 6px;" type="date" name="changeDateWeb" id="changeDateWeb" value="{{$notificationSetting['dateWeb'] ?? ''}}">
                                            <span class="input-group-append">
                                                   <button style="border-color: #c9c9c9" onClick="document.getElementById('changeDateWeb').submit();" class="btn btn-outline-secondary" type="submit"> Сохранить </button>
                                                </span>
                                             </div>
                                    </form>
                                </div>


                            </div>

                        </div>
                        <div class="col-lg-3 col-6">
                            <!-- Виджет 1: Остаток бюджета -->
                            <div class="small-box">
                                <div class="inner">
                                    <p> Sms Aero </p>
                                    <h4> {{$stat ['sms'] ?? '0'}} ₽ </h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-6">
                            <!-- Виджет 1: Остаток бюджета -->
                            <div class="small-box">
                                <div class="inner">
                                    <p> Zvonobot </p>
                                    <h4> {{$stat ['zvonobot'] ?? '0'}} ₽ </h4>
                                </div>
                            </div>
                        </div>
                    </div>


                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Время отправки</th>
                            <th scope="col">Тип контента</th>
                            <th scope="col">Контент</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($webNotificationPipleline as $step)
                        <tr>
                            <td>
                                <div class="d-flex">
                                    <div>
                                <input id="timeSend{{$step['id']}}" style="width: 100px" onblur="updateRow(this,{{$step['id']}},{{$id}})"  type="time"  class="input-opacity" name="timeSend" value="{{$step['timeSend'] ?? ''}}">
                               @if($step['nextDay'] == true)
                                    </div>
                                <div style="font-size: 12px">След. день</div>
                               @endif
                                </div>
                            </td>

                            @if($step['content_type'] == 'CALL')
                            <td>Звонок</td>
                            @else
                            <td>{{$step['content_type']}}</td>
                            @endif

                            <td>
                            @if($step['content_type'] == 'WABA')
                                <select style="width: 300px" onchange="updateRow(this,{{$step['id']}},{{$id}})" class="form-control input-opacity" name="text">
                                    @foreach($notificationSetting->wabaTemplates as $wabaTemplates)
                                        <option {{ ($step['content'] == $wabaTemplates['name']) ? 'selected' : '' }} name="name">
                                            {{ $wabaTemplates['name'] }}
                                        </option>
                                    @endforeach
                                </select>

                            @elseif($step['content_type'] != 'SMS')
                                    <input style="width: 300px" class="form-control input-opacity" id="post{{$step['id']}}" onblur="updateRow(this,{{$step['id']}},{{$id}})" value="{{$step['content']}}" name="text" >
                            @else
                                    <textarea style="width: 300px"  class="form-control input-opacity custom-textarea" id="post{{$step['id']}}" onblur="updateRow(this,{{$step['id']}},{{$id}})" rows="1" name="text" >{{$step['content']}}</textarea>
                            @endif
                            </td>

                            <td>
                                <form action="{{route('createNotification.delete',$id)}}" method="POST">
                                   @csrf @method('DELETE')
                                    <input type="hidden" name="step_id" value="{{$step['id']}}">
                                    <button type="submit">
                                        <i class="fas fa-times" style="color: red"></i>
                                    </button>
                                </form>
                            </td>


                        </tr>
                        @endforeach

                        @foreach($webNotificationPiplelineNoTime as $step)
                            <tr>
                                <td>
                                    <div class="d-flex">
                                        <div>
                                            <input id="timeSend{{$step['id']}}" style="width: 100px" onblur="updateRow(this,{{$step['id']}},{{$id}})"   class="input-opacity" name="timeSend" value="{{$step['type'] ?? ''}}">
                                            @if($step['nextDay'] == true)
                                        </div>
                                        <div style="font-size: 12px">След. день</div>
                                        @endif
                                    </div>
                                </td>

                                @if($step['content_type'] == 'CALL')
                                    <td>Звонок</td>
                                @else
                                    <td>{{$step['content_type']}}</td>
                                @endif

                                <td>
                                    @if($step['content_type'] == 'WABA')
                                        <select style="width: 300px" onchange="updateRow(this,{{$step['id']}},{{$id}})" class="form-control input-opacity" name="text">
                                            @foreach($notificationSetting->wabaTemplates as $wabaTemplates)
                                                <option {{ ($step['content_id'] == $wabaTemplates['name']) ? 'selected' : '' }} name="name">
                                                    {{ $wabaTemplates['name'] }}
                                                </option>
                                            @endforeach
                                        </select>

                                    @elseif($step['content_type'] != 'SMS')
                                        <input style="width: 300px" class="form-control input-opacity" id="post{{$step['id']}}" onblur="updateRow(this,{{$step['id']}},{{$id}})" value="{{$step['content']}}" name="text" >
                                    @else
                                        <textarea style="width: 300px"  class="form-control input-opacity custom-textarea" id="post{{$step['id']}}" onblur="updateRow(this,{{$step['id']}},{{$id}})" rows="1" name="text" >{{$step['content']}}</textarea>
                                    @endif
                                </td>

                                <td>
                                    <form action="{{route('createNotification.delete',$id)}}" method="POST">
                                        @csrf @method('DELETE')
                                        <input type="hidden" name="step_id" value="{{$step['id']}}">
                                        <button type="submit">
                                            <i class="fas fa-times" style="color: red"></i>
                                        </button>
                                    </form>
                                </td>


                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                    <div>
                        <a href="{{route('createNotification.index',$id)}}" class="btn btn-primary" style="display: flex; justify-content: center; align-items: center;margin: auto; width: 300px;">
                            Добавить новое уведомление
                        </a>
                    </div>


                </div>
            </section>
        </main>
    </div>

    <script>

        function deletes(row,messege_id,id){
            confirm("Are you sure?");
            $.ajax({
                url: "/api/webinar/createNotification/delete/" + id ,
                type: 'DELETE',
                data: {
                    'delete' : 'yes',
                    'messege_id': messege_id,
                    'id': id,
                },
                success:function(response){

                }
            });
        }



        function updateRow(row,messege_id,id){
            let text = row.value;

            $.ajax({
                url: "{{route('notificationStore',$id)}}",
                type: 'POST',
                data: {
                    'text': text,
                    'type': row.name,
                    'messege_id': messege_id,
                    'id': id,
                },
                success:function(response){

                }
            });
        }


        function updateFirstSms(row,messege_id,id){
            let text = document.getElementById(row.id).value
            if (text.length > 70){
                alert('более 1ой смс! ' + text.length + '/70 символов. Сократи текст')
                return 0;
            }
            $.ajax({
                url: "/api/webinar/notification/store/" + id ,
                type: 'POST',
                data: {
                    'smsWelcomeStore': text,
                    'id': id,
                },
                success:function(response){
                }
            });
        }

        function updateFirstWa(row,messege_id,id){

            let text = document.getElementById(row.id).value
            if (text.length > 2000){
                alert('Превышан лимит! ' + text.length + '/2000 символов. Сократи текст')
                return 0;
            }
            $.ajax({
                url: "/api/webinar/notification/store/wa/" + id ,
                type: 'POST',
                data: {
                    'waWelcomeStore': text,
                    'id': id,
                },
                success:function(response){

                }
            });
        }



    </script>

    <script>
        function autoResized(textarea) {
            textarea.style.height = 'auto'; // Сброс высоты
            textarea.style.height = (textarea.scrollHeight) + 'px'; // Установка новой высоты
        }

        function resetHeight(textarea) {
            textarea.style.height = 'auto'; // Сброс высоты
            textarea.rows = 1; // Возврат высоты к одной строке
        }

        document.querySelectorAll('.custom-textarea').forEach(textarea => {
            textarea.addEventListener('input', () => autoResized(textarea));
            textarea.addEventListener('focus', () => autoResized(textarea));
            textarea.addEventListener('blur', () => resetHeight(textarea)); // Обработчик для установки высоты при потере фокуса
        });

    </script>

@endsection
