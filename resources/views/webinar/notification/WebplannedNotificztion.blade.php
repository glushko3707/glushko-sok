@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper ">
        <main class="flex flex-grow-1 py-3">
            <section style="width: 100%">

                @livewire('notification.planned', [
                                       'id' => $id,
                                                      ])
            </section>
        </main>
    </div>

@endsection
