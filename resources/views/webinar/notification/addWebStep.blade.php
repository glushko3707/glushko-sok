@extends('adminPanel.startAddProject')

@section('content')

    <div class="content-wrapper ">
            <section>
                <div class="container">
                    <div class="row" style="height: 50px"></div>
                    <div class="card">
                        <div class="card-header"> Добавить событие уведомления </div>

                        <div class="card-body">
                            <form method="POST" id="pipeline_step" enctype="multipart/form-data" action="{{ route('createNotification.store',$id) }}">
                                @csrf

                                <div class="row mb-3">
                                    <label for="delay_type" class="col-md-4 col-form-label text-md-end">Тип уведомления</label>
                                    <div class="col-md-3">
                                        <select class="form-control" onchange="notificationType(this.value)" id="notification_type" name="notification_type" style="width:100%;margin-left: 0">
                                            <option value="SMS">SMS</option>
                                            <option value="CALL">Звонок</option>
                                            <option value="WA">WA_СЕРВЫЙ</option>
                                            <option value="WABA">WABA</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row mb-6 mb-3 timeSend">
                                    <label for="timeSend" class="col-md-4 col-form-label text-md-end">Время отправки</label>
                                    <div class="col-md-7  p-0" style="display: flex;">
                                        <div class="col-md-3">
                                            <input id="timeSend" type="time" class="form-control" name="timeSend">
                                        </div>
                                    </div>
                                </div>

                                <div class="row mb-3 text">
                                    <label for="textarea-element" class="col-md-4 col-form-label text-md-end">Текст</label>
                                    <div class="col-md-3">
                                        <textarea class="textarea-element" style="border-color: rgba(40, 40, 40, 0.2)" rows="5" name="text_post"></textarea>
                                    </div>
                                </div>

                                <div class="row mb-3 waba"  style="display: none;">
                                    <label for="textarea-element" class="col-md-4 col-form-label text-md-end">Waba</label>
                                    <div class="col-md-3">
                                        <select style="width: 300px" onchange="updateRow(this,{{$id}})" class="form-control input-opacity" name="waba">
                                            @foreach($notificationSetting->wabaTemplates as $wabaTemplates)
                                                <option>
                                                    {{ $wabaTemplates['name'] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                                {{--Тип контента--}}
                                <div class="row mb-3 callId" style="display: none;" >
                                    <label for="campaign_id" class="col-md-4 col-form-label text-md-end">Call ID Campaign</label>
                                    <div class="col-md-3" style="display: flex;">
                                        <input id="campaign_id" type="text" class="form-control" name="campaign_id">
                                    </div>
                                </div>

                                @if(auth()->user()->name == 'Maks Glushko')
                                <div class="row mb-3">
                                    <label for="type" class="col-md-4 col-form-label text-md-end">Кому уходят уведомления </label>
                                    <div class="col-md-3">
                                        <select class="form-control" id="type" name="type" style="width:100%;margin-left: 0">
                                            <option value="all">Всем</option>
                                            <option value="onWeb">Кроме тех, кто зашел на вебинар</option>
                                            <option value="new_noToday">Новым лидам (кроме тех, кто пришел сегодня)</option>
                                        </select>
                                    </div>
                                </div>
                                @endif

                                <div class="row mb-0">
                                    <div class="col-md-3 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Отправить запрос
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
              </div>
            </section>
    </div>


    <script>


        function notificationType(typed){
            if (typed === 'WA'){
                document.querySelectorAll('.callId').forEach((elem) => {
                    elem.style.display = "none";
                });

                document.querySelectorAll('.text').forEach((elem) => {
                    elem.style.display = "";
                });

                document.querySelectorAll('.requstTime').forEach((time) => {
                    time.style.display = "none";
                });

                document.querySelectorAll('.waba').forEach((time) => {
                    time.style.display = "none";
                });

            }
            else if (typed === 'WABA'){
                document.querySelectorAll('.callId').forEach((elem) => {
                    elem.style.display = "none";
                });

                document.querySelectorAll('.waba').forEach((elem) => {
                    elem.style.display = "";
                });
                document.querySelectorAll('.text').forEach((elem) => {
                    elem.style.display = "none";
                });
                document.querySelectorAll('.requstTime').forEach((time) => {
                    time.style.display = "none";
                });
            }

            else if (typed === 'CALL'){
                document.querySelectorAll('.callId').forEach((elem) => {
                    elem.style.display = "";
                });
                document.querySelectorAll('.waba').forEach((time) => {
                    time.style.display = "none";
                });
                document.querySelectorAll('.text').forEach((elem) => {
                    elem.style.display = "none";
                });

                document.querySelectorAll('.requstTime').forEach((time) => {
                    time.style.display = "";
                });
            }

            else if (typed === 'SMS'){
                document.querySelectorAll('.callId').forEach((elem) => {
                    elem.style.display = "none";
                });
                document.querySelectorAll('.waba').forEach((time) => {
                    time.style.display = "none";
                });
                document.querySelectorAll('.text').forEach((elem) => {
                    elem.style.display = "";
                });
                document.querySelectorAll('.requstTime').forEach((time) => {
                    time.style.display = "";
                });
            }

        }



    </script>



@endsection
