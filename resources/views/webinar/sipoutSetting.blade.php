@extends('webinar.adminPanel.adminPanel')

@section('contentCrm')





    <div class="container">
        <div class="row justify-content-center" style="margin-top: 50px">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header"> Укажите номер, которой принедлежит обзвону </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('sipoutCallIdEmpty.store',$id)}}">
                            @csrf

                            @foreach($callIdEmpty as $key => $value)
                            <div class="row mb-3">
                                <label for="secretKey" class="col-md-4 col-form-label text-md-end"> {{$key}}</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="secretKeyZvonobot" style="margin-bottom: 18px" value="{{$value ?? ''}}" name="{{$key}}" >
                                </div>
                            </div>
                            @endforeach


                            <div class="row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Загрузить
                                    </button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <script>
        var copyButton = document.getElementById('copyButton');
        var paragraph = document.getElementById('paragraph');


        copyButton.addEventListener('click', function() {

            // Используем библиотеку clipboard.js для копирования текста в буфер обмена
            var temp = document.createElement("textarea");
            document.body.appendChild(temp);
            temp.value = paragraph.innerText;
            temp.select();
            document.execCommand("copy");
            document.body.removeChild(temp);


            // Визуальное подтверждение копирования
            copyButton.innerText = 'Скопировано!';
            setTimeout(function(){
                copyButton.innerText = 'Скопировать';
            }, 2000);
        });
    </script>



@endsection
