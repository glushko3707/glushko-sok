<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Select2 -->
    <link rel="stylesheet" href={{asset("plugins/select2/css/select2.min.css")}}>
    <link rel="stylesheet" href={{asset("plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css")}}>
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href= {{asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback')}}>
    <!-- Font Awesome -->
    <link rel="stylesheet" href={{asset('plugins/fontawesome-free/css/all.min.css')}}>
    <!-- Ionicons -->
    <link rel="stylesheet" href={{asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}>
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href={{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}>
    <!-- iCheck -->
    <link rel="stylesheet" href={{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}>
    <!-- JQVMap -->
    <link rel="stylesheet" href={{asset("plugins/jqvmap/jqvmap.min.css")}}>
    <!-- Theme style -->
    <link rel="stylesheet" href={{asset("dist/css/adminlte.min.css")}}>
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href={{asset("plugins/overlayScrollbars/css/OverlayScrollbars.min.css")}}>
    <!-- Daterange picker -->
    <link rel="stylesheet" href={{asset("plugins/daterangepicker/daterangepicker.css")}}>
    <!-- summernote -->
    <link rel="stylesheet" href={{asset("plugins/summernote/summernote-bs4.min.css")}}>

    <script src="https://yastatic.net/jquery/3.3.1/jquery.min.js"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">

{{--<div style="position: fixed; top: 0; z-index: 10; left: 0; width: 100%; height: 100%; background-color:  #ffffff; opacity: 70%;filter: blur(20px) ">--}}
{{--</div>--}}
<div class="wrapper">

    <nav class="main-header navbar navbar-expand navbar-light" style="display: flex;justify-content: space-between;">
        <div>
            <ul class="navbar-nav">

                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li>
                    <form action="">
                        <div class="d-flex justify-content-between">
                            <div>
                                <label for="project" class="col-md-4 col-form-label text-md-end">Проект</label>
                            </div>
                            <div>
                                <select style="width: 200px" class="form-select" id="project" name="project" onchange="changeProject(this.value)"></select>
                            </div>
                        </div>
                    </form>
                </li>
            </ul>
        </div>
        <div>
            <ul class="navbar-nav">

                <li class="float-right" style="margin-right: 90px">
                    <div class="dropdown">
                        <button class="btn btn-sm text-gray border dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false" style="border-radius: 5px;padding: 6px 25px;">
                            {{ auth()->user()->name }}
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="{{route('profile.edit')}}">Профиль</a>
                            <form action="{{route('logout')}}" method="POST">
                                @csrf
                                <button class="dropdown-item" onclick="this.form.submit()">Выйти</button>
                            </form>
                        </div>
                    </div>
                </li>
            </ul>

        </div>

    </nav>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a class="brand-link">
            <img src={{asset("dist/img/sok.png")}} alt="Sok_Tunnels" class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">Sok Analytics</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src={{asset("dist/img/user2.png")}} class="img-circle" alt="User Image">
                </div>
                <div class="info">
                    <a   class="d-block">{{auth()->user()->name}}</a>
                </div>
            </div>


            <!-- SidebarSearch Form -->
            <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-sidebar">
                            <i class="fas fa-search fa-fw"></i>
                        </button>
                    </div>
                </div>
            </div>




            <nav class="mt-2" >
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item menu">
                        <a href="#" class="nav-link">
                            <p>
                                SipOut
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('sipoutStatistic.index',$id  ?? '') }}" class="nav-link">
                                    <p>
                                        Статистика трафика
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('contactDatabase.index',$id  ?? '') }}" class="nav-link">
                                    <p>
                                        База номеров
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('sipoutSetting.index',$id  ?? '') }}" class="nav-link">
                                    <p>
                                        Настройки
                                    </p>
                                </a>
                            </li>

                        </ul>
                    </li>
                    <li class="nav-item menu">
                        <a href="#" class="nav-link">
                            <p>
                                Уведомления о вебинаре
                                <i class="right fas fa-angle-left"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('plannedNotificztion',$id ?? '') }}" class="nav-link">
                                    <p>
                                        Запланированные уведомления
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('addNotificationWeb.index',$id  ?? '') }}" class="nav-link">
                                    <p>
                                        Добавить уведомления на сегодня
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('pipelineNotificztion',$id  ?? '') }}" class="nav-link">
                                    <p>
                                        Воронка уведомлений
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('serviceSetting.index',$id  ?? '') }}" class="nav-link">
                                    <p>
                                        Настройки сервисов доведения
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item" style="padding: 5px">
                                <a  href="{{ route('linkSetting.index',$id  ?? '') }}" class="nav-link">
                                    <p>
                                        Настройки ссылок
                                    </p>
                                </a>
                            </li>

                        </ul>
                    </li>





                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
    </aside>


    @yield('content')


</div>


<script src={{asset("plugins/bootstrap/js/bootstrap.bundle.min.js")}}></script>

<script>

    function onload(id_select) {
        $.ajax({
            type: 'GET',
            url: '{{route('getPermissionProject.index')}}',
            success: function (data) {
                let project = data
                let select = document.getElementById('project');

                for(var i = 0; i < project.length; i++){
                    var option = document.createElement("option");
                    option.value = project[i].id;
                    option.text = project[i].id + ". " + project[i].name;
                    select.appendChild(option);
                }
                for(var j = 1; j < select.options.length; j++){

                    if (select.options[j].value == id_select) {
                        select.options[j].selected = true;
                    }
                }
            }
        });
    }

    onload({{$id  ?? 'нет проектов'}});
</script>
<script>
    function changeProject(value){
        location.href = window.location.href.slice(0, -(window.location.href.split('/').pop().length + 1)) + "/" + value
    }

    $('.dropdown-toggle').dropdown();


</script>

<script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
<script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script>
<!-- jQuery -->
<script src={{asset("plugins/jquery/jquery.min.js")}}></script>
<!-- jQuery UI 1.11.4 -->
<script src={{asset("plugins/jquery-ui/jquery-ui.min.js")}}></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src={{asset("plugins/bootstrap/js/bootstrap.bundle.min.js")}}></script>
<!-- ChartJS -->
<script src={{asset("plugins/chart.js/Chart.min.js")}}></script>
<!-- Sparkline -->
<script src={{asset("plugins/sparklines/sparkline.js")}}></script>
<!-- JQVMap -->
<script src={{asset("plugins/jqvmap/jquery.vmap.min.js")}}></script>
<script src={{asset("plugins/jqvmap/maps/jquery.vmap.usa.js")}}></script>
<!-- jQuery Knob Chart -->
<script src={{asset("plugins/jquery-knob/jquery.knob.min.js")}}></script>
<!-- daterangepicker -->
<script src={{asset("plugins/moment/moment.min.js")}}></script>
<script src={{asset("plugins/daterangepicker/daterangepicker.js")}}></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src={{asset("plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js")}}></script>
<!-- Summernote -->
<script src={{asset("plugins/summernote/summernote-bs4.min.js")}}></script>
<!-- overlayScrollbars -->
<script src={{asset("plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js")}}></script>
<!-- AdminLTE App -->
<script src={{asset("dist/js/adminlte.js")}}></script>
<!-- Select2 -->
<script src={{asset("plugins/select2/js/select2.full.min.js")}}></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</body>
</html>
