const elementForAdd = document.getElementById('content')
const itemLetters = document.createElement('div');

// Создаем кнопку с текстом "номера"
const button = document.createElement('button');
button.classList.add(
    'mb-2',
    'button','is-info','is-outlined','has-text-weight-semibold',
);
button.setAttribute('id', 'glushkoFilter');

button.textContent = 'Фильтр номеров';

function hideElements(){
    // Находим все элементы с классом number_info_content
    var numberInfoElements = document.querySelectorAll('.number_info_content');
        console.log('hideElements запущена')
// Проходим по каждому элементу
    numberInfoElements.forEach(function(numberInfoElement) {

        // Получаем текстовое содержание элемента
        var textContent = numberInfoElement.textContent || numberInfoElement.innerText;
        console.log(textContent)

        // Проверяем, содержит ли текст слова "Подозрение" или "Жалобы"
        if (!textContent.includes('Нет информации по этому номеру')) {
            // Находим ближайшего родителя с классом list-item и did_container
            var listItem = numberInfoElement.closest('.list-item.did_container');

            if (listItem) {
                // Скрываем родительский элемент
                listItem.style.display = 'none';
            }
        }
    });
        alert('Готово')
}

// Добавляем кнопку в div
itemLetters.appendChild(button);

// Вставляем новый div в начало элемента elementForAdd
elementForAdd.insertBefore(itemLetters, elementForAdd.firstChild);

function simulateHoverOnElements() {
    // Находим все элементы с классом has-text-info на странице
    const elements = document.querySelectorAll('.has-text-info');

    elements.forEach((element, index) => {
        // Эмулируем событие наведения мыши
            const event = new Event('mouseenter', {
                bubbles: true,
                cancelable: true
            });
            element.dispatchEvent(event);
            console.log(`Наведено на элемент ${index + 1}`);
    });
    setTimeout(hideElements, 12000);

}

button.addEventListener('click', simulateHoverOnElements);

