import './bootstrap';

import { createApp } from "vue/dist/vue.esm-bundler";

import DiagnosticComponent from "@/Components/Calculator/DiagnosticComponent.vue";
import WebinarComponent from "@/Components/Calculator/WebinarComponent.vue";
import ThreeDayComponent from "@/Components/Calculator/ThreeDayComponent.vue";

const app = createApp({
    components:{
        WebinarComponent,ThreeDayComponent,DiagnosticComponent,
    }
});


app.mount('#app')