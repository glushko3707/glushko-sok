<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use libphonenumber\NumberParseException;
use libphonenumber\PhoneNumberUtil;

class PhoneNumber implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $phoneUtil = PhoneNumberUtil::getInstance();

        try {
            $phoneNumber = $phoneUtil->parse($value, 'ZZ'); // 'ZZ' означает, что номер может быть из любой страны
            if (!$phoneUtil->isValidNumber($phoneNumber)) {
                $fail('Введите корректный номер телефона.');
            }
        } catch (NumberParseException $e) {
            $fail('Введите корректный номер телефона.');
        }
    }
}
