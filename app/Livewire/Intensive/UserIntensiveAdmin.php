<?php

namespace App\Livewire\Intensive;

use App\Http\Controllers\Intensive\ListController;
use App\Models\Intensive\IntensiveUser;
use Livewire\Component;

class UserIntensiveAdmin extends Component
{

    public $id;
    public $intensive_id;
    public $intensiveUser;
    public $faculty_names;
    public $stat;
    public $intensiveUsers_not_chat_active;
    public $search;
    public $status;

//    #[Validate('required|min:5')]
//    public $name = '';

    public function mount($id, $intensive_id)
    {
        $this->id = $id;
        $this->intensive_id = $intensive_id;
    }

    public function save(){
        $filename = 'intensive.txt';
        $handle = fopen($filename, 'w');

        foreach ($this->intensiveUser as $row) {
            fwrite($handle, $row['phone'] . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }


    public function updated($propertyName, $value)
    {
        if ($propertyName == 'search'){
            return 200;
        }

        if ($propertyName == 'status'){
            return 200;
        }

        list($array, $index, $field) = explode('.', $propertyName);

        // Преобразование значения чекбокса в 1 или 0 для базы данных
        if ($field == 'chat_main_active') {
            $value = $value ? 1 : 0;
        }


        if ($field == 'meet_dateTime' && $value == ""){
            $value = null;
        }

        $userId = $this->intensiveUser[$index]['id']; // Получение ID пользователя
        $user = IntensiveUser::find($userId); // Поиск пользователя по идентификатору
        if ($user) {
            $user->$field = $value; // Присвоение обновлённого значения поля
            $user->save(); // Сохранение изменений
        }
        return 200;
    }


    public function render()
    {
        $intensive_id = $this->intensive_id;
        $id = $this->id;

        $IntensiveUsers = IntensiveUser::query()
            ->where('project_id',$id)
            ->where('intensive_list_id',$this->intensive_id)
            ->with(['getAnketa','getLeadsForWebs']);

        $query = $this->search;
        $status = $this->status;

        if (!empty($status)) {$IntensiveUsers = $IntensiveUsers->where(function($q) use ($status) {
            if ($status == 'noChat'){
                $q->where('chat_main_active','!=',1);
            }
            if ($status == 'noMeet'){
                $q->where('meet_dateTime','!=',1);
            }
            if ($status == 'meetToday'){
                $q->whereDate('meet_dateTime','=',today());
            }
        });}

        if (!empty($this->search)) {$IntensiveUsers = $IntensiveUsers->where(function($q) use ($query) {
            $q->where('name', 'LIKE', '%' . $query . '%')
                ->orWhere('phone', 'LIKE', '%' . $query . '%')
                ->orWhere('faculty_name', 'LIKE', '%' . $query . '%')
                ->orWhere('telegram_username', 'LIKE', '%' . $query . '%');
        });}


        $this->intensiveUsers_not_chat_active = IntensiveUser::query()
            ->where('project_id',$id)
            ->where('intensive_list_id',$this->intensive_id)
            ->where('chat_main_active','!=',1)
            ->with('getLinkMainChat')
            ->get()
            ->groupBy('phone')
            ->map(function ($stats) {
                $name_link = 'intensive_' . $stats->first()->phone;
                $link = $stats->first()->getLinkMainChat->where('link_name',$name_link)->first()->link ?? null;
                return [
                    'link' => $link,
                ];
            })->toArray();

        $IntensiveUser =  $IntensiveUsers->get();

        $IntensiveUser = $IntensiveUser->map(function ($user) {
        if ($user->getAnketa) {
            // Добавляем JSON значение в отношение getAnketa
            $leadText = [];
            $json = get_object_vars(json_decode($user->getAnketa->answers));
            foreach ($json as $question => $answer) {
                if(is_array($answer)){
                    $answer = implode(', ', $answer);
                }
                $leadText[] = "$question: $answer";
            }
            $user->source = $user->getLeadsForWebs->utm_source ?? null;
            $user->anketa = $leadText;
        }
        return $user;
    });
        $this->stat['count'] = $IntensiveUser->count();

        $this->intensiveUser = $IntensiveUser->toArray();

        $listController = new ListController();
        $getIntensiveChats = $listController->getListChat($intensive_id);
        $IntensiveChats = $getIntensiveChats->pluck('chat_name');
        $this->faculty_names = $IntensiveChats->unique();
        return view('livewire.intensive.user-intensive-admin');
    }
}
