<?php

namespace App\Livewire\Intensive;

use Livewire\Component;

class IntensiveList extends Component
{

    public $id;
    public $IntensiveList;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function render()
    {
        $IntensiveList = \App\Models\Intensive\IntensiveList::query()->where('project_id',$this->id)->with('getUsers')->get()->reverse();
        $this->IntensiveList = $IntensiveList;

        return view('livewire.intensive.intensive-list');
    }
}
