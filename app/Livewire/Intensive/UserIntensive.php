<?php

namespace App\Livewire\Intensive;

use App\Http\Controllers\Intensive\ListController;
use App\Models\Intensive\IntensiveChat;
use App\Models\Intensive\IntensiveUser;
use Livewire\Component;

class UserIntensive extends Component
{
    public $id;
    public $intensive_id;
    public $intensiveUser;
    public $faculty_names;
    public $onlyMyFaculties = false;

    public $stat;
    public $intensiveUsers_not_chat_active;
    public $search;

//    #[Validate('required|min:5')]
//    public $name = '';

    public function mount($id, $intensive_id)
    {
        $this->id = $id;
        $this->intensive_id = $intensive_id;
    }


    public function updated($propertyName, $value)
    {
        if ($propertyName == 'search' || $propertyName == 'onlyMyFaculties'){
            return 200;
        }

        list($array, $index, $field) = explode('.', $propertyName);

        // Преобразование значения чекбокса в 1 или 0 для базы данных
        if ($field == 'chat_main_active') {
            $value = $value ? 1 : 0;
        }

        $userId = $this->intensiveUser[$index]['id']; // Получение ID пользователя
        $user = IntensiveUser::find($userId); // Поиск пользователя по идентификатору

        if ($user) {
            $user->$field = $value; // Присвоение обновлённого значения поля
            $user->save(); // Сохранение изменений
        }
        return 200;
    }

    public function render()
    {
        $intensive_id = $this->intensive_id;
        $id = $this->id;

        $IntensiveUsers = IntensiveUser::query()
            ->where('project_id',$id)
            ->where('intensive_list_id',$this->intensive_id);

        // получаем название факультета пользователя
        $telegram_id = auth()->user()->telegram_id ?? null;
        if(!empty($telegram_id)){
            $faculty_name = IntensiveUser::query()
                ->where('project_id',$id)
                ->where('telegram_id',$telegram_id)
                ->where('intensive_list_id',$this->intensive_id)
                ->get()
                ->last()
                ->faculty_name;

            if ($this->onlyMyFaculties){
                $IntensiveUsers = $IntensiveUsers->where('faculty_name', $faculty_name);
            }
        }

        $query = $this->search;
        if (!empty($this->search)) {$IntensiveUsers = $IntensiveUsers->where(function($q) use ($query) {
            $q->where('name', 'LIKE', '%' . $query . '%')
                ->orWhere('phone', 'LIKE', '%' . $query . '%')
                ->orWhere('telegram_username', 'LIKE', '%' . $query . '%');
        });}

//        if ($this->onlyMyFaculties) {
//            $IntensiveUsers = $IntensiveUsers->where('chat_main_active', 1);
//        }


        $this->intensiveUsers_not_chat_active = IntensiveUser::query()
            ->where('project_id',$id)
            ->where('intensive_list_id',$this->intensive_id)
            ->where('chat_main_active','!=',1)
            ->with('getLinkMainChat')
            ->get()
            ->groupBy('phone')
            ->toArray();

        $IntensiveUser =  $IntensiveUsers->get();

        $this->stat['count'] = $IntensiveUser->count();

        $this->intensiveUser = $IntensiveUser->toArray();

        $listController = new ListController();
        $getIntensiveChats = $listController->getListChat($intensive_id);
        $IntensiveChats = $getIntensiveChats->pluck('chat_name');
        $this->faculty_names = $IntensiveChats->unique();

        return view('livewire.intensive.user-intensive');
    }
}
