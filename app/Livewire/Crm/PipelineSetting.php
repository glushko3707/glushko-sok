<?php

namespace App\Livewire\Crm;

use App\Http\Controllers\Crm\Amo\AmoController;
use App\Http\Controllers\Crm\Commo\CommoController;
use App\Models\Crm\CrmSettingModel;
use Livewire\Component;

class PipelineSetting extends Component
{
    public  $accountCrmStatus = [];
    public  $id;
    public $new_lead_status;
    public $phone_id_status;


    public function mount($id)
    {
        $this->id = $id;
    }

    public  function updated($propertyName, $value)
    {
        if ($propertyName == 'new_lead_status'){
            $CrmSettingModel = CrmSettingModel::query()->where('project_id', $this->id)->first();
            $settings = json_decode($CrmSettingModel->statuses, true);
            $settings['new_lead_status'] = $value;

            $CrmController = new CommoController();
            $customFields = $CrmController->customFields($this->id);
            $phone_id_status = $CrmController->getPhoneFromContactField($customFields['contacts']);
            $settings['phone_id_status'] = $phone_id_status;

            CrmSettingModel::query()->updateOrCreate(['project_id' => $this->id],[
                'statuses' => json_encode($settings)
            ]);
        }
    }

    public function render()
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $this->id)->first();
        if (!empty($CrmSettingModel)){
            $statuses = json_decode($CrmSettingModel->statuses, true);
            $this->new_lead_status = $statuses['new_lead_status'] ?? null;

            $CrmController = new CommoController();
            $amoPipelines = $CrmController->getAmoStatus($this->id);
            $this->accountCrmStatus = $amoPipelines;
        }


        return view('livewire.crm.pipeline-setting');
    }
}
