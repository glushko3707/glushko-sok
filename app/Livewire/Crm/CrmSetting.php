<?php

namespace App\Livewire\Crm;

use App\Models\Crm\CrmSettingModel;
use Kommo\KommoAPI;
use Livewire\Component;

class CrmSetting extends Component
{
    public $id;
    public $crm_type;
    public $step = 1;
    public $clientSecret;
    public $clientIdAmo;



    public function nextStep()
    {
        $step = $this->step;
        $this->step = $step + 1;
    }

    public function mount($id)
    {
        $this->id = $id;
    }


    public function updated($propertyName, $value)
    {
        if ($propertyName == 'crm_type'){
            CrmSettingModel::query()->updateOrCreate(['project_id' => $this->id],[
                'crm_type' => $value
            ]);
       };

        if ($this->crm_type == 'Commo'){
            $CrmSettingModel = CrmSettingModel::query()->where('project_id', $this->id)->first();
            $setting = $CrmSettingModel->settings;
            $settings = json_decode($setting, true);

            if ($propertyName == 'clientSecret'){
                $settings['clientSecret'] = $value;
            };

            if ($propertyName == 'clientIdAmo'){
                $settings['clientIdAmo'] = $value;
            };

            CrmSettingModel::query()->updateOrCreate(['project_id' => $this->id],[
                'settings' => json_encode($settings)
            ]);

        };

        return 200;
    }


    public function render()
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $this->id)->first();
        if (!empty($CrmSettingModel->settings)){
            $setting = $CrmSettingModel->settings;
            $settings = json_decode($setting, true);
            $this->crm_type = $CrmSettingModel->crm_type ?? null;

            $this->clientSecret = $settings['clientSecret'] ?? null;
            $this->clientIdAmo = $settings['clientIdAmo'] ?? null;
        }
        return view('livewire.crm.crm-setting');
    }
}
