<?php

namespace App\Livewire\Service;

use App\Models\Permissoin;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class Permission extends Component
{
    public $id;
    public $email;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function addPermisson()
    {
        $email = $this->email;
        $user = User::query()->where('email',$email)->get()->first();
        if (empty($user)){
            $user = User::create([
                'name' => $email,
                'email' => $email,
                'password' => Hash::make('qwerty12345'),
            ]);
        }
        Permissoin::query()->firstOrCreate(['user_id' => $user->id, 'project_id' => $this->id]);
    }

    public function deleteUser($user_id)
    {
        Permissoin::query()->where(['user_id' => $user_id, 'project_id' => $this->id])->delete();
    }

    public function render()
    {
        $permissionUser = Permissoin::query()->where('project_id', $this->id)->with('user')->get();
        return view('livewire.service.permission',[
            'permissionUser' => $permissionUser
            ]);
    }
}
