<?php

namespace App\Livewire\Webinar;

use App\Models\GlushkoTelegramConnect;
use App\Models\Payment;
use App\Models\Telegram\UserChannel;
use App\Models\Webinar\LeadForWeb;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Livewire\Component;

class Reaching extends Component
{
    public $date_to = '';
    public $date_from = '2024-04-01';
    public $id;
    public $dayWebStatistice = [];
    public $period;
    public $warm;
    public $statistica;
    public $utm_source;
    public $utm_medium;
    public $utm_source_all = [];

    public function mount($id)
    {
        $this->id = $id;
        $this->date_to = today()->format('Y-m-d');
    }

    public function render()
    {



        $id = $this->id;
        $LeadForWeb = LeadForWeb::query()->where('project_id',$id)->with(['getTelegramUsers','getTildaLeads','getPayments','getWebinarUser']);
        if (!empty($this->date_to)) {$LeadForWeb->whereDate('dateWeb', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$LeadForWeb->whereDate('dateWeb', '>=', $this->date_from);}
        if (isset($this->utm_source) && $this->utm_source != 'all') {$LeadForWeb->where('utm_source', '=', $this->utm_source);} else {
            $this->utm_source_all = $LeadForWeb->pluck('utm_source')->unique();
        }

        if (!empty($this->utm_medium)) {
            $LeadForWeb->whereHas('getTildaLeads', function ($query) {
                $query->where('utm_campaign', '=', $this->utm_medium)
                    ->orWhere('utm_medium', '=', $this->utm_medium)
                    ->orWhere('utm_content', '=', $this->utm_medium);
            });
        }

//        $LeadForWeb = $this->getStat($LeadForWeb);

        $LeadForWeb = $LeadForWeb->get()->reverse();

//        $LeadForWebGroupBy = $LeadForWeb
//            ->groupBy(function($item) {
//                return \Carbon\Carbon::parse($item->dateWeb)->toDateString();
//            });
//        dd($LeadForWebGroupBy);

        $telegramUsers = $LeadForWeb->Map(function ($item) {
            return $item->getTelegramUsers;
        })->filter(function ($item) {
            return !is_null($item); // Удаляет null значения
        });

        $telegramUsersDay = $telegramUsers->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('Y-m-d');
        });
        $telegramUsersDay = $telegramUsersDay->map->count();
        $GlushkoTelegramConnect = GlushkoTelegramConnect::query()->where('project_id','=',$id)->get()->first();

        if (!empty($GlushkoTelegramConnect)) {
            $user_channels = UserChannel::query()->where('project_id', '=', $GlushkoTelegramConnect->telegram_project);
            if ($id == 114) {$user_channels->where(function($query) {$query->where('link_name', 'WABA_ЗАКРЫТЫЙ')->orWhere('link_name', 'SMS ЗАКРЫТЫЙ');});}
            if (!empty($this->date_to)) {$user_channels->whereDate('created_at', '<=', $this->date_to);}
            if (!empty($this->date_from)) {$user_channels->whereDate('created_at', '>=', $this->date_from);}
            $user_channels = $user_channels->get()->groupBy(function($date) {
                return Carbon::parse($date->created_at)->format('Y-m-d');}
            );
            $groupUser_channels = $user_channels->map->count();

            $groupUser_channels = collect($groupUser_channels);
            $telegramUsersDay = collect($telegramUsersDay);

            $groupUser_channels = $groupUser_channels->merge($telegramUsersDay)->map(function ($item, $key) use ($groupUser_channels, $telegramUsersDay) {
                $groupValue = $groupUser_channels->get($key, 0);
                $telegramValue = $telegramUsersDay->get($key, 0);

                return $groupValue + $telegramValue;
            });

        }

        $LeadForWebGroupBy = $LeadForWeb->groupBy('dateWeb');
        $groupCountsLeadForWeb = $LeadForWebGroupBy->map->count();

        $webinarUsers = \App\Models\Webinar\WebinarUser::query()->where('project_id',$id);
        if (!empty($this->date_to)) {$webinarUsers->whereDate('date', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$webinarUsers->whereDate('date', '>=', $this->date_from);}
        if (isset($this->utm_source) && $this->utm_source != 'all') {$webinarUsers->where('utm_source', '=', $this->utm_source);}
        if (isset($this->utm_source) && $this->utm_source != 'all') {$webinarUsers->where('utm_source', '=', $this->utm_source);}


        $webinarUsers = $webinarUsers->get()->reverse();
        $webinarUsersGroupBy = $webinarUsers->groupBy('date');
        $groupCountsWebinarUsers = $webinarUsersGroupBy->map->count();

        $Payments = Payment::query()->where('project_id', $id)->with('webinarUsers');
        if (isset($this->utm_source) && $this->utm_source != 'all') {
        $Payments->whereHas('webinarUsers', function ($query) {if (isset($this->utm_source) && $this->utm_source !== 'all') {$query->where('utm_source', '=', $this->utm_source);}})
        ;}

        if (!empty($this->date_to)) {$Payments->whereDate('date', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$Payments->whereDate('date', '>=', $this->date_from);}

        if ($id == 114){
            $product = 'Оплата интенсив по телеграм с Юлией Купр';
            $Payments = $Payments->where('product', $product)->whereIn('status',['succeeded','по карте']);
        } else {
            $Payments = $Payments->whereIn('status',['succeeded','по карте']);
        }

        $webinarUsers = $Payments->get()->reverse();
        $PaymentsUsersGroupBy = $webinarUsers->groupBy('date');
        $groupCountsPayments = $PaymentsUsersGroupBy->map->count();

        $dayWebStatistice = [];

        foreach ($groupCountsLeadForWeb as $day => $dayLeadForWeb){
            $dayWebStatistice[$day]['userReg'] = $dayLeadForWeb;
            if (isset($groupCountsWebinarUsers[$day])){
                $dayWebStatistice[$day]['visitWeb'] = $groupCountsWebinarUsers[$day];
                $dayWebStatistice[$day]['visitConversion'] = round($dayWebStatistice[$day]['visitWeb']/$dayLeadForWeb * 100);
            } else {
                $dayWebStatistice[$day]['visitWeb'] = 0;
                $dayWebStatistice[$day]['visitConversion'] = 0;
            }

            if (isset($groupUser_channels[$day])){
                $dayWebStatistice[$day]['channelUser'] = $groupUser_channels[$day];
                $dayWebStatistice[$day]['channelUserConversion'] = round($dayWebStatistice[$day]['channelUser']/$dayLeadForWeb * 100);
            } else {
                $dayWebStatistice[$day]['channelUser'] = 0;
                $dayWebStatistice[$day]['channelUserConversion'] = 0;
            }


            if (isset($webinarUsersGroupBy[$day])){
                $dayWebStatistice[$day]['visitWeb'] = $groupCountsWebinarUsers[$day];
                $dayWebStatistice[$day]['visitConversion'] = round($dayWebStatistice[$day]['visitWeb']/$dayLeadForWeb * 100);
            } else {
                $dayWebStatistice[$day]['visitWeb'] = 0;
                $dayWebStatistice[$day]['visitConversion'] = 0;
            }

            if (isset($groupCountsPayments[$day])){
                $dayWebStatistice[$day]['paymentsWeb'] = $groupCountsPayments[$day];
                if ($dayWebStatistice[$day]['visitWeb'] > 0){
                    $dayWebStatistice[$day]['paymentConversion'] = round($dayWebStatistice[$day]['paymentsWeb']/$dayWebStatistice[$day]['visitWeb'] * 100);
                } else {
                    $dayWebStatistice[$day]['paymentConversion'] = 'Данные недоступны';
                }
            } else {
                $dayWebStatistice[$day]['paymentsWeb'] = 0;
                $dayWebStatistice[$day]['paymentConversion'] = 0;
            }
        }


        $this->dayWebStatistice = $dayWebStatistice;

        // общие суммы
        $CollectDayWebStatistice = collect($dayWebStatistice);
        $this->statistica['userReg'] = $CollectDayWebStatistice->sum('userReg');
        $this->statistica['visitWeb'] = $CollectDayWebStatistice->sum('visitWeb');
        $this->statistica['paymentsWeb'] = $CollectDayWebStatistice->sum('paymentsWeb');
        $this->statistica['channelUser'] = $CollectDayWebStatistice->sum('channelUser');

        if ($this->statistica['userReg'] > 0){
            $this->statistica['visitConversion'] = round($this->statistica['visitWeb']/$this->statistica['userReg']*100);
            $this->statistica['channelUserConversion'] = round($this->statistica['channelUser']/$this->statistica['userReg']*100);
        } else {
            $this->statistica['visitConversion'] = 0;
            $this->statistica['channelUserConversion'] = 0;
        }

        if ($this->statistica['visitWeb'] > 0){
            $this->statistica['paymentConversion'] = round($this->statistica['paymentsWeb']/$this->statistica['visitWeb']*100);
        } else {
            $this->statistica['paymentConversion'] = 0;
        }

        return view('livewire.webinar.reaching');
    }

    public function getStat($LeadForWeb)
    {
        $GlushkoTelegramConnect = GlushkoTelegramConnect::query()->where('project_id','=',$this->id)->get()->first();

        $LeadForWebss = $LeadForWeb
            ->with([
                'getPayments' => function ($query) {
                    $query->whereDate('date', '>=', $this->date_from)
                        ->whereDate('date', '<=', $this->date_to)
                        ->where('project_id', '=',$this->id)
                        ->whereIn('status', ['succeeded','по карте']);
                }, 'getWebinarUser' => function ($query) {
                    $query->whereDate('date', '>=', $this->date_from)
                        ->whereDate('date', '<=', $this->date_to)
                        ->where('project_id', '=',$this->id);
                }, 'getTildaLeads' => function ($query) {
                    $query->where('project_id', '=',$this->id);
                }
                ,'getTelegramUsers' => function ($query) use($GlushkoTelegramConnect) {
                    $query->whereDate('created_at', '>=', $this->date_from)
                        ->whereDate('created_at', '<=', $this->date_to)
                        ->where('project_id', '=',$GlushkoTelegramConnect->telegram_project);
                }

            ])
            ->get()
            ->map(function ($stats) {
                return [
                    'payment' => $stats->getPayments->id ?? null,
                    'webinar' => $stats->getWebinarUser->id ?? null,
                    'phone' => $stats->phone,
                    'date' => $stats->dateWeb,
                    'utm_source' => $stats->getTildaLeads->utm_source ?? $stats->utm_source ?? null,
                    'utm_medium' => $stats->getTildaLeads->utm_medium ??  null,
                    'utm_content' => $stats->getTildaLeads->utm_content ?? null,
                    'utm_campaign' => $stats->getTildaLeads->utm_campaign ?? null,
                    'telegram_id' => $stats->getTelegramUsers->id ?? null,
                ];
            });

        $LeadForWebss = collect($LeadForWebss);

        $LeadForWeb = $LeadForWebss->reverse();
        $LeadForWebGroupBy = $LeadForWeb
            ->groupBy('date');
        return $LeadForWebGroupBy;
    }


}
