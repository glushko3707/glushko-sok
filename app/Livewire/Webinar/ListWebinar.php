<?php

namespace App\Livewire\Webinar;

use App\Bizon365\Bizon365API;
use App\Models\Notification\NotificationSetting;
use Livewire\Component;

class ListWebinar extends Component
{

    public $id;
    public $webinars;
    public $date_to = '';
    public $date_from = '2024-01-01';

    public function mount($id)
    {
        $this->id = $id;
    }

    public function render()
    {
        $BizonSetting = NotificationSetting::query()->where('project_id','=',$this->id)->get()->first();
        $bizon365 = new Bizon365API($BizonSetting->bizon_key);
        $webinars = $bizon365->getWebinarList($skip = 0, $limit = 100);
        $this->webinars = $webinars;

        return view('livewire.webinar.list-webinar');
    }
}
