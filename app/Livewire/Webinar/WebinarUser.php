<?php

namespace App\Livewire\Webinar;

use App\Models\Notification\NewLeadWeb;
use App\Models\Payment;
use App\Models\Webinar\LeadForWeb;
use Carbon\Carbon;
use Livewire\Component;

class WebinarUser extends Component
{
    public $date_to = '';
    public $date_from = '';
    public $id;
    public $webinarUser;
    public $period;
    public $warm;
    public $statistica;
    public $utm_source;
    public $utm_source_all = [];

    public function mount($id)
    {
        $this->id = $id;
    }

    public function save(){
        $filename = 'webinar_user.txt';
        $handle = fopen($filename, 'w');
        foreach ($this->webinarUser as $row) {
            fwrite($handle, $row->phone . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);

    }

    public function render()
    {
        $id = $this->id;
          $webinarUsers = \App\Models\Webinar\WebinarUser::query()->where('project_id',$id);
            if (!empty($this->date_to)) {$webinarUsers->whereDate('date', '<=', $this->date_to);}
            if (!empty($this->date_from)) {$webinarUsers->whereDate('date', '>=', $this->date_from);}
            if (isset($this->warm) && $this->warm != 'all') {$webinarUsers->where('warm', '=', $this->warm);}
            if (isset($this->utm_source) && $this->utm_source != 'all') {$webinarUsers->where('utm_source', '=', $this->utm_source);} else {
                $this->utm_source_all = $webinarUsers->pluck('utm_source')->unique();
            }
            $webinarUser = $webinarUsers->get()->reverse();


        $this->webinarUser = $webinarUser;
        $this->statistica['count'] = $webinarUser->count();

        $Payments = Payment::query()->where('project_id',$id);
         if (!empty($this->date_to)) {$Payments->whereDate('date', '<=', $this->date_to);}
         if (!empty($this->date_from)) {$Payments->whereDate('date', '>=', $this->date_from);}

       if ($id == 114){
           $prodect = 'Оплата интенсив по телеграм с Юлией Купр';
           $Payments->where(['product' => $prodect,'status' => 'succeeded']);
           $this->statistica['payment'] = $Payments->count();
       } else {
           $this->statistica['payment'] = $webinarUser->where('warm','=','succeeded')->count();
       }


        if ($this->statistica['count'] > 0){
            $this->statistica['PercentPayment'] = round($this->statistica['payment']/$this->statistica['count']*100);
        } else {
            $this->statistica['PercentPayment'] = 0;
        }

        $this->statistica['hot'] = $webinarUser->where('warm','=','hot')->count() + $this->statistica['payment'];

        if ($this->statistica['count'] > 0){
            $this->statistica['hotPercent'] = round($this->statistica['hot']/$this->statistica['count']*100);
        } else {
            $this->statistica['hotPercent'] = 0;
        }




        return view('livewire.webinar.webinar-user',[
            'webinarUser' => $webinarUser
        ]);
    }
}
