<?php

namespace App\Livewire\Salebot;

use Livewire\Component;

class Texteditor extends Component
{
    public $text;
    public $exit;

    function lines($text){
        $lines = explode("\n", $text);

        $exit = '';
        foreach ($lines as $line){
            if ($exit == ''){
                $exit = '"' . $line . '"';
                continue;
            }
            if ($line == ''){
                $exit = $exit  . '+"\n"';
            }
            else {
                $exit = $exit . '+"\n"+'  . '"' . $line. '"';
            }
        }

        return $exit;
    }

    public function render()
    {
        $text = str_replace('&quot;', '"', $this->text);

        $parts = explode("===", $text);
        $final = [];
        $index = 0;

        if ($parts[0] == ''){
            return view('livewire.salebot.texteditor');
        }

        foreach ($parts as $part){
            $text = str_replace('===', '\n', $part);
            if ($index != 0){
                $final[$index] = substr($final[$index-1], 0, -1) . substr($this->lines($text), 1);
                $final[$index] = ltrim($final[$index]);

            } else {
                $final[$index] = $this->lines($text);
            }
            $index++;
        }

        $this->exit = $final;
        return view('livewire.salebot.texteditor');
    }


}
