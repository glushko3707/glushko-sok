<?php

namespace App\Livewire\Sipout;

use App\Models\Offer;
use App\Models\Project;
use App\Models\Sipout\LeadSipout;
use App\Models\Sipout\SipoutCallId;
use Carbon\Carbon;
use Livewire\Component;

class Statistica extends Component
{
    public $id;
    public $date_to = '';
    public $date_from = '';
    public $offer = '';
    public $offers ;
    public $all = []; public $stat = [];    public $allDays = []; public $statDays = [];
    public $period = 'phone';

    public $lastStats;


    public function mount($id)
    {
        $this->id = $id;
        if ($id == 114){
            $this->date_from = now()->subDays(7)->format('Y-m-d');
        }

    }

    public function getSipoutPhone(){

        if ($this->date_from == ''){
            return back();
        }

        $dayFrom = Carbon::parse($this->date_from)->format('d.m.Y');
        $dayTo = Carbon::parse($this->date_to)->format('d.m.Y');
        $id = $this->id;

        $project = Project::query()->find($id);
        $sipProject = $project->getSipProject;
        $api_key = $sipProject->api_key;

        $url = 'https://lk.sipout.net/userapi/?key='.$api_key.'&method=call_stat&action=get_list&ds='.$dayFrom.'&de=' . $dayTo;
        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->get($url);


        if (!isset($response->json()['data']['list'])){
            return 'ошибка возврата статистики звонков';
        }

        $call = collect($response->json()['data']['list'])->unique()->pluck('caller');
        $filename = "звонкиПроекта_$id" . "_$dayFrom" . "_$dayTo" .".txt";
        $handle = fopen($filename, 'w');
        foreach ($call as $row) {
            fwrite($handle, $row . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }



    public function render()
    {
        $id = $this->id;
        $leads = LeadSipout::query()->where('project_id','=',$id)->whereNotNull('utm_medium');
        $sipoutCallId = SipoutCallId::query()->where('project_id','=',$id);


        if (!empty($this->date_from)) {
                $leads->whereDate('created_at', '>=', $this->date_from);
                $sipoutCallId->whereDate('created_at', '>=', $this->date_from);
            }

            $leads = $leads->get();
            $sipoutCallId = $sipoutCallId->get();

            if ($sipoutCallId->isNotEmpty()) {
                $this->lastStats = $this->getLastStat($sipoutCallId,$leads);
            } else {$this->lastStats = [];}

            $getStat = $this->getStatPhone($id,$this->date_from,$this->date_to,$leads,$sipoutCallId);
            $this->all = $getStat['all'];
            $this->stat = collect($getStat['number']);

            $getStatDays = $this->getStatDays($id);
            $this->allDays = $getStatDays['all'];
            $this->statDays = $getStatDays['stat'];

              if ($this->offer != '' && $this->offer != 'all') {
                  $this->stat = $this->stat->where('offer', '=', $this->offer);
              if (isset($this->stat)){
                  $number = collect($this->stat);

             $all['rashod'] = round($number->sum('rashod')) ;
             $all[ 'newleads'] = $number->sum('newleads') ;
             if ($all['newleads'] != 0) {
                 $all ['leadPrice'] =  round($all ['rashod'] / $all ['newleads']);
             } else {$all ['leadPrice'] = 0;}
             $this->all = $all;
         } else {
             $this->all = [];
         }
        ;}

        $this->offers = Offer::query()->where('project_id',$id)->pluck('offer_name')->unique();
        return view('livewire.sipout.statistica');
    }
    public function getStatPhone($id,$dateFrom,$dateTo,$leads,$callId){

        $Offer = Offer::query()->where('project_id',$id)->get(['offer_name','call_id']);

        if ($dateTo) {$leads->where('updated_at', '<=', $dateTo);}
        if ($dateFrom) {$leads->where('updated_at', '>=', $dateFrom);}

        $leads = $leads->groupBy('utm_medium');

        $callId = $callId->groupBy('callId');

        $callId = $callId->reverse();
        foreach ($callId as $value => $key) {
            $number [$key->first()->callId]['id'] = $value;
            $number [$key->first()->callId]['date'] = $key->first()->date;
            $number [$key->first()->callId]['phone'] = $key->first()->phone;
            $number [$key->first()->callId]['callId'] = $key->first()->callId;
            $number [$key->first()->callId]['rashod'] = round($key->sum('money'));
            $number [$key->first()->callId]['timePhone'] = $key->sum('timePhone');
            $number [$key->first()->callId]['comment'] = $key->first()->comment;
            $number [$key->first()->callId]['value'] = $key->first()->id;
            $number [$key->first()->callId]['callId_for'] = substr($key->first()->callId,1);

            $offers = $Offer->where('call_id',$key->first()->callId);
            if (count($offers) > 0) {$number[$key->first()->callId]['offer'] = $offers->first()->offer_name;}

            if (isset($leads[$key->first()->callId])) {$number [$key->first()->callId]['newleads'] = $leads[$key->first()->callId]->count();} else{$number [$key->first()->callId]['newleads'] = 0;}
            if (isset($leads[$key->first()->callId])) {$number [$key->first()->callId]['leadPrice'] =  round($number [$key->first()->callId]['rashod']/$leads[$key->first()->callId]->count(),2);} else{$number [$key->first()->callId]['leadPrice'] = 0;}
        }

        if (isset($number)){
            $number = collect($number);
            $all['rashod'] = round($number->sum('rashod')) ;
            $all[ 'newleads'] = $number->sum('newleads') ;
            if ($all['newleads'] != 0) {
                $all ['leadPrice'] =  round($all ['rashod'] / $all ['newleads']);
            } else {$all ['leadPrice'] = 0;}

        } else {
            $all = [];
        }
        if (!isset($number)){$number = [];}
        if (!empty($number)){
            $number = $number->where('rashod','>',3);
        }

        return [
            'number' => $number,
            'all' => $all,
        ];
    }

    function getStatDays($id){
        $money = SipoutCallId::query()
            ->where('project_id','=',$id)
            ->orderByDesc('created_at')->get()->groupBy(function($events) {
                return \Illuminate\Support\Carbon::parse($events->created_at)->format('d-m-y'); // А это то-же поле по нему мы и будем группировать
            });

        $leads = LeadSipout::query()
            ->where('project_id','=',$id)
            ->orderByDesc('created_at')->get()->groupBy(function($events) {
                return \Illuminate\Support\Carbon::parse($events->created_at)->format('d-m-y'); // А это то-же поле по нему мы и будем группировать
            });

        if (isset($money)){
            foreach ($money as $day => $value){
                $stat[$day] = ['rashod' => round($value->sum('money')),];
                if (isset($leads[$day])) {
                    $stat[$day]['newleads'] =  $leads[$day]->count();
                    $stat[$day]['leadPrice'] =  round($stat[$day]['rashod']/$stat[$day]['newleads']);
                } else{
                    $stat[$day]['newleads'] = 0;
                    $stat[$day]['leadPrice'] = 0;
                }
            }
        }
        if (isset($stat)){
            $stat = collect($stat);
            $all['rashod'] = round($stat->sum('rashod'));
            $all['newleads'] = $stat->sum('newleads');
            if (isset($all['newleads']) && $all['newleads'] > 0 ){
                $all['leadPrice'] =  round($all['rashod']/$all['newleads']);
            } else{
                $all['leadPrice'] = 0;
            }
        }

        if (!isset($stat)){$stat = [];}
        if (!isset($all)){$all = [];}

        return[
            'stat' => $stat,
            'all' => $all
        ];
    }

    public function getLastStat($sipoutCallId,$leads){

        $date = Carbon::parse($sipoutCallId->last()->date);
        $sipoutCallId = $sipoutCallId->filter(function ($item) use ($date) {
            return Carbon::parse($item['date'])->isSameDay($date);
        });
        $leads = $leads->filter(function ($item) use ($date) {
            return Carbon::parse($item['updated_at'])->isSameDay($date);
        });
        if ($leads->count() > 0){
            $todayLeadPrice = $sipoutCallId->sum('money') / $leads->count();
        } else {
            $todayLeadPrice = 0;
        }
        if ($date->isToday()) {
            $date = 'Сегодня';
        } elseif ($date->isYesterday()) {
            $date = 'Вчера ('.$date->format('d.m.Y').')';
        } else{
            $date = $date->format('d.m.Y');
        }

        return [
            'todaySpend' => round($sipoutCallId->sum('money')),
            'todayLeads' => $leads->count(),
            'todayLeadPrice' => round($todayLeadPrice),
            'date' => $date,
        ];
    }

}
