<?php

namespace App\Livewire\Payment;

use Illuminate\Support\Carbon;
use Livewire\Component;

class Payment extends Component
{

    public $date_to = '';
    public $date_from = '';
    public $id;
    public $products = [];
    public $webinarUser;
    public $period;
    public $payment;
    public $status;
    public $product; public $paymentRequestCount;
    public $paymentStat;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function save(){

        $filename = 'payment.txt';
        $handle = fopen($filename, 'w');
        foreach ($this->payment as $row) {
           if (isset($row['phone'])){
               fwrite($handle, $row['phone'] . PHP_EOL);
           }
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }


    public function render()
    {
        $id = $this->id;
        $paymentRequest = \App\Models\Payment::query()->where('project_id',$id)->with(['getLeadForWeb','getTildaLead','getUkolovaLead']);

        if (!empty($this->date_to)) {$paymentRequest->whereDate('created_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$paymentRequest->whereDate('created_at', '>=', $this->date_from);}

        if (isset($this->status) && $this->status != 'all') {
            if ($this->status == 'allPayments'){
                $paymentRequest->whereIn('status',['succeeded','по карте']);
            } else {
                $paymentRequest->where('status', '=', $this->status);
            }
            
            ;}

        if (isset($this->product) && $this->product != 'all') {$paymentRequest->where('product', '=', $this->product);}

        $payment = $paymentRequest->get()->reverse();

        $payment = $payment->map(function ($item) {
            $utm_source = $item->getLeadForWeb->where('project_id',$item->project_id)->whereNotNull('utm_source')->last()->utm_source ?? null;
            $utm_medium = $item->getLeadForWeb->where('project_id',$item->project_id)->whereNotNull('utm_medium')->last()->utm_medium ?? null;
            return [
                'date' => $item->created_at->format('d.m.Y'),
                'dateTime' =>  Carbon::parse($item->created_at)->locale('ru')->day . ' ' . Carbon::parse($item->created_at)->locale('ru')->getTranslatedMonthName('Do MMMM') . ' ' . Carbon::parse($item->created_at)->format('H:i'),
                'status' => $item->status,
                'sum' => $item->sum,
                'product' => $item->product,
                'id' => $item->id,
                'phone' => $item->phone,
                'utm_source' => $utm_source ?? $item->getTildaLead->where('project_id',$item->project_id)->whereNotNull('utm_source')->last()->utm_source ?? $item->getUkolovaLead->where('project_id',$item->project_id)->whereNotNull('utm_source')->last()->utm_source ?? null,
                'utm_medium' => $utm_medium ?? $item->getTildaLead->where('project_id',$item->project_id)->whereNotNull('utm_medium')->last()->utm_medium ?? $item->getUkolovaLead->where('project_id',$item->project_id)->whereNotNull('utm_medium')->last()->utm_medium ?? null
            ];
        });
        $this->products = $payment->pluck('product')->unique();
        $this->payment = $payment;
        $paymentDay = $payment->groupBy('date');

        foreach ($paymentDay as $day => $value){
            $payments = $value->whereIn('status',['по карте','succeeded']);
            $paymentCard = $value->where('status','=','по карте');
            $paymentKassa = $value->where('status','=','succeeded');
            $paymentsOrder = $value->whereIn('status',['по карте','succeeded','open','canceled']);

            $paymentStat[$day] = [
                'paymentCount' => count($payments),
                'paymentCard' => count($paymentCard),
                'paymentKassa' => count($paymentKassa),
                'paymentOrder' => count($paymentsOrder),
                'sum' => ($payments->sum('sum')),
            ];
        }

        $this->paymentRequestCount = $paymentRequest->count();
        $this->paymentStat = $paymentStat ?? [];
        return view('livewire.payment.payment');
    }
}
