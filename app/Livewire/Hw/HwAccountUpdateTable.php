<?php

namespace App\Livewire\Hw;

use App\Models\HWChannalForUpdate;
use Livewire\Component;

class HwAccountUpdateTable extends Component
{
    public $channels;
    public $newChannel = [
        'channel' => '',
        'utm_source' => '',
        'utm_medium' => '',
        'sheetName' => '',
        'currency' => '',
        'active' => 1,
        'account_id' => '',
    ];
    public function mount()
    {
        $this->loadChannels();
    }

    public function loadChannels()
    {
        $this->channels = HWChannalForUpdate::all();
    }
    public function addChannel()
    {
        $this->validate([
            'newChannel.channel' => 'required',
            'newChannel.utm_source' => 'required',
            'newChannel.utm_medium' => 'required',
            'newChannel.sheetName' => 'required',
            'newChannel.currency' => 'required',
            'newChannel.active' => 'boolean',
            'newChannel.account_id' => 'required',
        ]);

        HWChannalForUpdate::query()->updateOrCreate(
            ['account_id' => $this->newChannel['account_id']],
            $this->newChannel
        );

        $this->newChannel = [
            'channel' => '',
            'utm_source' => '',
            'utm_medium' => '',
            'sheetName' => '',
            'currency' => '',
            'active' => 1,
            'account_id' => '',
        ];

        $this->loadChannels();
    }

    public function deleteChannel($id)
    {
        HWChannalForUpdate::findOrFail($id)->delete();
        $this->loadChannels();
    }

    public function render()
    {
        return view('livewire.hw.hw-account-update-table');
    }
}
