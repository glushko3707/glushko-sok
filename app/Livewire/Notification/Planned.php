<?php

namespace App\Livewire\Notification;

use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Project;
use Livewire\Component;

class Planned extends Component
{

    public $id;
    public $stat = [];
    public $webNotificationPlaned;
    public $date_to = '';
    public $date_from = '';
    public  $searchUtm = '';
    public $search = '';
    public $searchPhone = '';
    public $inputText;
    public $contentType;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function deleteNotification($notificationStepId = null)
    {
        if ($notificationStepId != null){
            $WebNotificationSchedule = WebNotificationSchedule::query()->find($notificationStepId);
            if (!empty($WebNotificationSchedule)) {
                $WebNotificationSchedule->delete();
            }
        } else {
            WebNotificationSchedule::query()->whereIn('id',$this->webNotificationPlaned->pluck('id'))->delete();
            };
    }



    public function render()
    {
        if (!empty($this->inputText)){
            $this->searchPhone = array_filter(array_map('trim', explode("\n", $this->inputText)));
        }

        $project_id = $this->id;
        $webNotificationPlaneds = WebNotificationSchedule::query()->where('project_id',$project_id)->with(['getPipelineStep','getLeads']);

        if (!empty($this->date_to)) {$webNotificationPlaneds->whereDate('updated_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$webNotificationPlaneds->whereDate('updated_at', '>=', $this->date_from);}

        $search = $this->search;

        if (!empty($search)) {
            $webNotificationPlaneds = $webNotificationPlaneds->whereHas(
                'getPipelineStep', function ($query) use ($search) {
                $query
                    ->where('content_type', '=', $search)
                ;
            }
            );
        }

        if (!empty($this->searchPhone)) {
            $webNotificationPlaneds = $webNotificationPlaneds
                ->whereIn('phone', $this->searchPhone);
        };



        $webNotificationPlaned = $webNotificationPlaneds->orderBy('timeSend')
            ->get();

        $searchUtm =  $this->searchUtm;
        $webNotificationPlaned = $webNotificationPlaned->map(function ($webNotificationPlaned) use ($searchUtm) {
            $lastLead = $webNotificationPlaned->getLeads->last();
            // Если есть utm_source и он не совпадает с искомым, пропускаем запись
            if ($searchUtm != '' && (!isset($lastLead->utm_source) || $lastLead->utm_source != $searchUtm)) {
                return null;
            }
            return [
                'timeSend' => $webNotificationPlaned->timeSend,
                'id' => $webNotificationPlaned->id,
                'type' => $webNotificationPlaned->getPipelineStep->content_type,
                'step_id' => $webNotificationPlaned->step_id,
                'phone' => $webNotificationPlaned->phone,
                'utm_source' => $webNotificationPlaned->getLeads->last()->utm_source ?? '',
            ];
        })->filter();
        $this->stat['countPeapleToday'] = $webNotificationPlaned->unique('phone')->count();
        $this->stat['countPeapleCanToday'] = NewLeadWeb::query()->where('project_id',$project_id)->where('addWeb','=',0)->count();

        $this->contentType = $webNotificationPlaned->pluck('type')->unique()->toArray();


        $this->webNotificationPlaned = $webNotificationPlaned;

        return view('livewire.notification.planned',[
            'webNotificationPlaned' => $webNotificationPlaned,
        ]);
    }


    public function downloadPlannedPhones()
    {
        $filteredPlannedPhones = $this->webNotificationPlaned;
        $filteredPlannedPhonesNumbers = $filteredPlannedPhones->pluck('phone')->toArray();
        $fileName = 'planned_phones.csv';
        $headers = [
            'Content-type' => 'text/csv',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0',
        ];
        $callback = function () use ($filteredPlannedPhonesNumbers) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['phone']);
            foreach ($filteredPlannedPhonesNumbers as $number) {
                fputcsv($file, [$number]);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);
    }


}
