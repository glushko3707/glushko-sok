<?php

namespace App\Livewire\Waba;

use App\Http\Controllers\Waba\TemplatesController;
use App\Models\Notification\WebSms;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\WabaTemplates;
use Illuminate\Support\Str;
use Livewire\Component;

class Templates extends Component
{
    public $id;
    public $intanses;
    public $webSms;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function updateWabaTemplateFb($waba_id,$templateName)
    {
        $templatesController = new TemplatesController();
        $templatesController->updateWabaTemplatesStoreOne($waba_id,$templateName,$this->id);

//        $names = collect($availableTemplates)->flatMap(function ($items) {
//            return collect($items)->pluck('name');
//        });
//        WabaTemplates::query()->where('project_id',$id)->whereNotIn('name',$names)->delete();
//        $WabaMessageController->setTemplateWaba($availableTemplates,$id);
//        dd($templatesInfo);
    }

    public function updated($propertyName, $propertyValue)
    {
        // Проверка, что обновилось именно нужное свойство
        if (preg_match('/^intanses\.(\d+)\.templates\.(\d+)\.reserve_sms_id$/', $propertyName, $matches)) {
            // Извлечение индексов
            $index = $matches[1];
            $indexTemplates = $matches[2];

            // Обновление модели WabaTemplates
            $this->updateWabaTemplateColmn($index, $indexTemplates, $propertyValue);
        }
    }

    public function updateWabaTemplateColmn($index, $indexTemplates, $reserveSmsId)
    {
        // Проверка существования нужного элемента в массиве
        if (isset($this->intanses[$index]['templates'][$indexTemplates])) {
            $template = $this->intanses[$index]['templates'][$indexTemplates];

            $reserveSmsId = $reserveSmsId == '' ? null : $reserveSmsId;

            // Обновление записи в базе данных
            WabaTemplates::query()->where('id', $template['id'])->update([
                'reserve_sms_id' => $reserveSmsId
            ]);

            // Дополнительная логика, если нужно
        }
    }

    public function render()
    {
        $intanses = WabaIntance::query()->where('project_id',$this->id)->with(['getTemplates','getTemplates.getVariable'])->get();
        $this->intanses = $intanses->map(function ($wabaIntance) {
            $wabaIntance->templates =  $wabaIntance->getTemplates->map(function ($templates){
                $templates->variable = $templates->getVariable->toArray();
                return $templates;
              })->toArray();
            return $wabaIntance;
        })->toArray();

        $this->webSms = WebSms::query()->where('project_id',$this->id)->get()->unique();

        return view('livewire.waba.templates');
    }
}
