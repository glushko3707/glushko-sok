<?php

namespace App\Livewire\Waba;

use App\Models\Intensive\IntensiveUser;
use App\Models\Waba\WabaButton;
use App\Models\Waba\WabaMessage;
use App\Models\Waba\wabaSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Livewire\Attributes\On;
use Livewire\Component;
use Livewire\WithFileUploads;
use function PHPUnit\Framework\callback;


class ResponseSetting extends Component
{
    use WithFileUploads;


    public $id;
    public $newButton;
    public $WabaMessage;
    public $media;
    public $WabaMessageNew = [];
    public $WabaMessagePreview = ['WabaMessagePreview' => ''];
    public $messageId;

    public function updated($propertyName, $value)
    {
        switch ($propertyName) {
            case 'WabaMessageNew.callback':
            case 'WabaMessageNew.text':
                break;
            case 'newButton':
                $this->addButton($value);
                break;
            case 'WabaMessagePreview.text':
                $this->updateWabaMessageText($this->WabaMessagePreview['id'], $value);
                break;
            default:
                $this->updateDynamicProperty($propertyName, $value);
                break;
        }
    }


    public function deleteButton($button_id)
    {
        WabaButton::query()->find($button_id)->delete();

        $wabaMessage = \App\Models\Waba\WabaMessage::find($this->WabaMessagePreview['id']);
        $wabaMessage->button = $wabaMessage->getButtons;
        $this->WabaMessagePreview = $wabaMessage->toArray();
    }

    public function addButton($value)
    {
        $wabaMessage = \App\Models\Waba\WabaMessage::find($this->WabaMessagePreview['id']);
        WabaButton::query()->updateOrCreate([
            'project_id' => $this->id,
            'waba_message_id' => $wabaMessage->id,
            'text' => $value,
        ],[
            'text' => $value,
            'callback' => rand(1,10000),
        ]);
        $wabaMessage = \App\Models\Waba\WabaMessage::find($this->WabaMessagePreview['id']);
        $wabaMessage->button = $wabaMessage->getButtons;
        $this->WabaMessagePreview = $wabaMessage->toArray();
        $this->newButton = null;
    }

    protected function updateDynamicProperty($propertyName, $value)
    {
        list($array, $index, $field) = explode('.', $propertyName);

        if (isset($this->WabaMessage[$index])) {
            $userId = $this->WabaMessage[$index]['id'];
            $user = \App\Models\Waba\WabaMessage::find($userId);

            if ($user) {
                $user->$field = $value;
                $user->save();
            }
        }

        return 200;
    }


    protected function updateWabaMessageText($id, $value)
    {
        $wabaMessage = \App\Models\Waba\WabaMessage::find($id);

        if ($wabaMessage) {
            $wabaMessage->text = $value;
            $wabaMessage->save();
        }

        return 200;
    }


    public function mount($id)
    {
        $this->id = $id;
    }

    public function save()
    {
        \App\Models\Waba\WabaMessage::query()->updateOrCreate(
            [
                'project_id' => $this->id,
                'callback' => $this->WabaMessageNew['callback']
            ], [
                'text' => $this->WabaMessageNew['text']
            ]
        );
        $this->WabaMessageNew = ['callback' => '','text' => ''];
    }

    public function delete($wabaMessage_id)
    {
        \App\Models\Waba\WabaMessage::query()->find($wabaMessage_id)->delete();
    }

    public function preview($WabaMessage_id)
    {
        $WabaMessagePreview = \App\Models\Waba\WabaMessage::query()->find($WabaMessage_id);
        $WabaMessagePreview->button = $WabaMessagePreview->getButtons;
        $this->WabaMessagePreview = $WabaMessagePreview->toArray();

        $this->messageId = $WabaMessage_id;

        // Вызов JavaScript события
        $this->dispatch('preview', messageId: '123');

    }

    public function render()
    {
        $WabaMessage = \App\Models\Waba\WabaMessage::query()->where('project_id', $this->id)->with('getButtons')->get();

        $WabaMessage = $WabaMessage->map(function ($message) {
            $message->buttons = $message->getButtons;
            return $message;
        });

        $this->WabaMessage = $WabaMessage->toArray();
        return view('livewire.waba.response-setting');
    }
}
