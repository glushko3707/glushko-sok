<?php

namespace App\Livewire\Waba;

use App\Http\Controllers\Waba\WabaMessageController;
use App\Models\Waba\WabaHistory;
use App\Traits\Notification;
use Livewire\Component;

class WabaMessage extends Component
{
    use Notification;

    public $phones;
    public $phonePointer = '';
    public $statistic = [];
    public $dialog = [];
    public $id;
    public $search = '';
    public $filterStatus;
    public $filterTime = 'updated_at';

    public $text = '';
    public $phone = '';


    public $filterTypeMessage;  public $date_to = ''; public $date_from = '';

    public function mount($id)
    {
        $this->id = $id;
    }

    public function dialogs($phone){
        $this->phonePointer = $phone;
        $WabaHistory = WabaHistory::query()->where('phone',$phone)->where('project_id',$this->id)->where('status','=','in')->update(['status'=>'in_reed']);
        $this->dialog = WabaHistory::query()->where('project_id',$this->id)->where('phone',$phone)->orderBy('updated_at')->get();
    }

    public function save(){
        $filename = 'phoneWabaHistory.txt';
        $handle = fopen($filename, 'w');
        foreach ($this->phones as $row) {
            fwrite($handle, $row . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);

    }

    public function sendMessageStore(){

        $text = $this->text;
        $id = $this->id;
        $phone = $this->phonePointer;

        if ($phone != '' && $text != ''){
            $WabaMessageController = new WabaMessageController();
            $WabaMessageController->sendMessageWabaOne($phone,$text,$id);
        }

        usleep(10000);
        $this->dialog = WabaHistory::query()->where('project_id',$this->id)->where('phone',$phone)->orderBy('updated_at')->get();
        $this->text = '';
    }

    public function render()
    {
        $wabaHistory = WabaHistory::query()->where('project_id',$this->id)->orderByDesc($this->filterTime);

        if (!empty($this->filterStatus) && $this->filterStatus != 'all') {
            if ($this->filterStatus == 'fail'){
                $wabaHistory->whereIn('status', ['failed','sent']);
            }
            if ($this->filterStatus == 'deliver'){
                $wabaHistory->whereIn('status', ['delivered','read']);
            }
            if ($this->filterStatus == 'new'){
                $wabaHistory->whereIn('status', ['in']);
            }
        }
        if (!empty($this->date_to)) {$wabaHistory->whereDate('created_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$wabaHistory->whereDate('created_at', '>=', $this->date_from);}

        if ($this->search!= ''){
            $searchTerm = '%' . $this->search . '%';
            $wabaHistory->where('message', 'like', $searchTerm);
        }


        $this->phones = $wabaHistory->get(['phone','status'])->unique('phone');
        $this->statistic['count'] = count($this->phones);
        return view('livewire.waba.waba-message');
    }
}
