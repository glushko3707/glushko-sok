<?php

namespace App\Livewire\Waba;

use App\Models\Waba\WabaIntance;
use App\Models\Waba\WabaTemplates;
use Livewire\Component;

class SendMessage extends Component
{
    public $id;
    public $wabaTemplates;
    public $intances_id;
    public $WabaMessagePreview ;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function wabaPreviews($template_name)
    {
        $wabaTemplates = WabaTemplates::query()->where('project_id',$this->id)->where('name',$template_name)->get()->first();
        $getVariable = $wabaTemplates->getVariable;
        $wabaTemplates->variable = $getVariable->toArray();
        $this->WabaMessagePreview = $wabaTemplates->toArray();
    }

    public function render()
    {
        $id = $this->id;
        $wabaIntance = WabaIntance::query()->where('project_id',$id)->get();
        $this->intances_id = $wabaIntance;
        $wabaTemplates = WabaTemplates::query()->where('project_id',$id)->get();
        $this->wabaTemplates = $wabaTemplates->unique('name');

        return view('livewire.waba.send-message');
    }
}
