<?php

namespace App\Livewire\Traffic;

use App\Http\Controllers\Notification\TrafficController;
use App\Http\Controllers\Yandex\Direct\DirectController;
use App\Models\Notification\TildaLead;
use App\Models\Payment;
use Carbon\Carbon;
use Livewire\Component;

class Yandex extends Component
{
    public $date_to = '';
    public $date_from = '2024-01-01';
    public $search = '';
    public $id;
    public $statistica;
    public $statisticaType = 'ads';
    public $dayWebStatistice;
    public $isLoading = false;
    public $add_account_name;
    public function mount($id)
    {
        $this->id = $id;
    }

    public function add_account(){
        $directController = new DirectController();
        $report = $directController->getReport($this->add_account_name, today()->subDays(30)->format('Y-m-d'), today()->format('Y-m-d'),$this->id);
        $directController->setDatetoDB($report, $this->id, $this->add_account_name);
//        $directController->addYandexDirect($this->id);
    }
    public function updateStatistica(){
        $directController = new DirectController();
        $directController->updateYandexDirect($this->id);
    }

    public function analyticStatistica($ads, $id)
    {

        $facebook_account_id = $ads->groupBy('account_name')->map(function ($stats) {
            $lead = $stats->unique('campaign_id')->sum('lead')  ?: 0;
            $webinar = $stats->unique('campaign_id')->sum('webinar')  ?: 0;
            $payment = $stats->unique('campaign_id')->sum('payment')  ?: 0;
            return [
                'account_name' => $stats->first()['account_name'],
                'lead' => $lead,
                'leadPrice' => $lead > 0 ? ($stats->sum('spend') / $lead) : 0,
                'webinar' => $webinar,
                'webinarPrice' => $webinar > 0 ? ($stats->sum('spend') / $webinar) : 0,
                'webinarPercent' => $lead > 0 ? ($webinar / $lead) * 100 : 0,
                'webinarWarm' => $stats->sum('webinarWarm'),
                'percentHot' => $stats->sum('webinar') > 0 ? ($stats->sum('webinarWarm') / $stats->sum('webinar')) * 100 : 0,
                'paymentMain' => $stats->sum('paymentMain'),
                'payment' => $payment,
                'clicks' => $stats->sum('clicks'),
                'impressions' => $stats->sum('impressions'),
                'spend' => $stats->sum('spend'),
                'campaign_collect' => $stats->groupBy('campaign_id')->map(function ($statsCampaign) {
                    $lead = $statsCampaign->unique('campaign_id')->sum('lead')  ?: 0;
                    $webinar = $statsCampaign->unique('campaign_id')->sum('webinar')  ?: 0;
                    $payment = $statsCampaign->unique('campaign_id')->sum('payment')  ?: 0;
                    return [
                        'campaign_id' => $statsCampaign->first()['campaign_id'],
                        'campaign_name' => $statsCampaign->first()['campaign_name'],
                        'lead' => $lead,
                        'leadPrice' => $lead > 0 ? ($statsCampaign->sum('spend') / $lead) : 0,
                        'webinar' => $webinar,
                        'webinarPrice' => $webinar > 0 ? ($statsCampaign->sum('spend') / $webinar) : 0,
                        'webinarPercent' => $lead > 0 ? ($webinar / $lead) * 100 : 0,
                        'webinarWarm' => $statsCampaign->sum('webinarWarm'),
                        'percentHot' => $statsCampaign->sum('webinar') > 0 ? ($statsCampaign->sum('webinarWarm') / $statsCampaign->sum('webinar')) * 100 : 0,
                        'paymentMain' => $statsCampaign->sum('paymentMain'),
                        'payment' => $payment,
                        'clicks' => $statsCampaign->sum('clicks'),
                        'impressions' => $statsCampaign->sum('impressions'),
                        'spend' => $statsCampaign->sum('spend'),
                        'adset_collect' => $statsCampaign->groupBy('adset_id')->map(function ($statsAdset) {
                                return [
                                    'adset_id' => $statsAdset->first()['adset_id'],
                                    'adset_name' => $statsAdset->first()['adset_name'],
                                    'lead' => $statsAdset->sum('lead'),
                                    'leadPrice' => $statsAdset->sum('lead') > 0 ? ($statsAdset->sum('spend') / $statsAdset->sum('lead')) : 0,
                                    'webinar' => $statsAdset->sum('webinar'),
                                    'webinarPrice' => $statsAdset->sum('webinar') > 0 ? ($statsAdset->sum('spend') / $statsAdset->sum('webinar')) : 0,
                                    'webinarPercent' => $statsAdset->sum('lead') > 0 ? ($statsAdset->sum('webinar') / $statsAdset->sum('lead')) * 100 : 0,
                                    'webinarWarm' => $statsAdset->sum('webinarWarm'),
                                    'percentHot' => $statsAdset->sum('webinar') > 0 ? ($statsAdset->sum('webinarWarm') / $statsAdset->sum('webinar')) * 100 : 0,
                                    'paymentMain' => $statsAdset->sum('paymentMain'),
                                    'payment' => $statsAdset->sum('payment'),
                                    'clicks' => $statsAdset->sum('clicks'),
                                    'impressions' => $statsAdset->sum('impressions'),
                                    'spend' => $statsAdset->sum('spend'),
                                    'ad_collect' => $statsAdset->groupBy('ad_id')->map(function ($statsAd) {
                                        return [
                                            'ad_id' => $statsAd->first()['ad_id'],
                                            'lead' => $statsAd->sum('lead'),
                                            'leadPrice' => $statsAd->sum('lead') > 0 ? ($statsAd->sum('spend') / $statsAd->sum('lead')) : 0,
                                            'webinar' => $statsAd->sum('webinar'),
                                            'webinarPrice' => $statsAd->sum('webinar') > 0 ? ($statsAd->sum('spend') / $statsAd->sum('webinar')) : 0,
                                            'webinarPercent' => $statsAd->sum('lead') > 0 ? ($statsAd->sum('webinar') / $statsAd->sum('lead')) * 100 : 0,
                                            'webinarWarm' => $statsAd->sum('webinarWarm'),
                                            'percentHot' => $statsAd->sum('webinar') > 0 ? ($statsAd->sum('webinarWarm') / $statsAd->sum('webinar')) * 100 : 0,
                                            'paymentMain' => $statsAd->sum('paymentMain'),
                                            'payment' => $statsAd->sum('payment'),
                                            'clicks' => $statsAd->sum('clicks'),
                                            'impressions' => $statsAd->sum('impressions'),
                                            'spend' => $statsAd->sum('spend'),
                                        ];
                                    }),

                                ];
                        })
                    ];
                })
            ];
        });


//        // Группировка и агрегация данных
//        $facebook_ad_id = $ads->groupBy('ad_id')->map(function ($stats) {
//            $firstStat = $stats->first();
//            $adId = $firstStat['ad_id'];
//            $lead = $firstStat['lead'];
//            $payment = $firstStat['payment'];
//            $paymentMain = $firstStat['paymentMain'];
//            $webinar = $firstStat['webinar'];
//            $webinarWarm = $firstStat['webinarWarm'];
//            $spend = $stats->sum('spend');
//            $clicks = $stats->sum('clicks');
//            $impressions = $stats->sum('impressions');
//
//            return [
//                'leadPrice' => $lead > 0 ? ($spend / $lead) : 0,
//                'lead' => $lead,
//                'webinar' => $webinar,
//                'webinarPrice' => $webinar > 0 ? ($spend / $webinar) : 0,
//                'webinarPercent' => $lead > 0 ? ($webinar / $lead) * 100 : 0,
//                'webinarWarm' => $webinarWarm,
//                'percentHot' => $webinar > 0 ? ($webinarWarm / $webinar) * 100 : 0,
//                'clicks' => $clicks,
//                'impressions' => $impressions,
//                'spend' => $spend,
//                'account_name' => $firstStat['account_name'],
//                'campaignLeads' => $firstStat['campaignLeads'],
//                'campaign_id' => $firstStat['campaign_id'],
//                'campaign_name' => $firstStat['campaign_name'],
//                'adset_id' => $firstStat['adset_id'],
//                'adset_name' => $firstStat['adset_name'],
//                'ad_id' => $adId,
//                'payment' => $payment,
//                'paymentMain' => $paymentMain,
//            ];
//        });
//
//        // Группировка по adset_id
//        $facebook_adset_id = $facebook_ad_id->groupBy('adset_id')->map(function ($stats) {
//            return [
//                'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') / $stats->sum('lead')) : 0,
//                'lead' => $stats->sum('lead'),
//                'webinar' => $stats->sum('webinar'),
//                'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') / $stats->sum('webinar')) : 0,
//                'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') / $stats->sum('lead')) * 100 : 0,
//                'webinarWarm' => $stats->sum('webinarWarm'),
//                'percentHot' => $stats->sum('webinar') > 0 ? ($stats->sum('webinarWarm') / $stats->sum('webinar')) * 100 : 0,
//                'paymentMain' => $stats->sum('paymentMain'),
//                'payment' => $stats->sum('payment'),
//                'impressions' => $stats->sum('impressions'),
//                'spend' => $stats->sum('spend'),
//                'account_name' => $stats->first()['account_name'],
//                'campaignLeads' => $stats->first()['campaignLeads'],
//                'campaign_id' => $stats->first()['campaign_id'],
//                'campaign_name' => $stats->first()['campaign_name'],
//                'adset_id' => $stats->first()['adset_id'],
//                'adset_name' => $stats->first()['adset_name'],
//                'ad_collect' => $stats,
//            ];
//        });
//
//        // Группировка по campaign_id
//        $facebook_campaign_id = $facebook_adset_id->groupBy('campaign_id')->map(function ($stats) {
//            return [
//                'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') / $stats->sum('lead')) : 0,
//                'lead' => $stats->sum('lead') ?: $stats->sum('campaignLeads') ?: 0,
//                'webinar' => $stats->sum('webinar'),
//                'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') / $stats->sum('webinar')) : 0,
//                'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') / $stats->sum('lead')) * 100 : 0,
//                'webinarWarm' => $stats->sum('webinarWarm'),
//                'percentHot' => $stats->sum('webinar') > 0 ? ($stats->sum('webinarWarm') / $stats->sum('webinar')) * 100 : 0,
//                'payment' => $stats->sum('payment'),
//                'paymentMain' => $stats->sum('paymentMain'),
//                'clicks' => $stats->sum('clicks'),
//                'impressions' => $stats->sum('impressions'),
//                'spend' => $stats->sum('spend'),
//                'clickAll' => $stats->sum('clickAll'),
//                'clickLink' => $stats->sum('clickLink'),
//                'account_name' => $stats->first()['account_name'],
//                'campaign_id' => $stats->first()['campaign_id'],
//                'campaign_name' => $stats->first()['campaign_name'],
//                'adset_collect' => $stats,
//            ];
//        });
//
//        // Группировка по account_name
//        $facebook_account_id = $facebook_campaign_id->groupBy('account_name')->map(function ($stats) {
//            return [
//                'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') / $stats->sum('lead')) : 0,
//                'lead' => $stats->sum('lead'),
//                'webinar' => $stats->sum('webinar'),
//                'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') / $stats->sum('webinar')) : 0,
//                'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') / $stats->sum('lead')) * 100 : 0,
//                'webinarWarm' => $stats->sum('webinarWarm'),
//                'percentHot' => $stats->sum('webinar') > 0 ? ($stats->sum('webinarWarm') / $stats->sum('webinar')) * 100 : 0,
//                'paymentMain' => $stats->sum('paymentMain'),
//                'payment' => $stats->sum('payment'),
//                'clicks' => $stats->sum('clicks'),
//                'impressions' => $stats->sum('impressions'),
//                'spend' => $stats->sum('spend'),
//                'clickAll' => $stats->sum('clickAll'),
//                'clickLink' => $stats->sum('clickLink'),
//                'account_name' => $stats->first()['account_name'],
//                'campaign_collect' => $stats,
//            ];
//        });

        return $facebook_account_id;
    }

//    function getLead($ads,$id)
//    {
//        // Инициализация запроса
//        $leadsQuery = TildaLead::query()
//            ->with(['getWebinarUser' => function ($query) use ($id) {
//                $query
//                    ->where('project_id', $id);
//            }]);
//
//        // Применение фильтров по дате, если они заданы
//        if (!empty($this->date_to)) {
//            $leadsQuery->whereDate('created_at', '<=', $this->date_to);
//        }
//        if (!empty($this->date_from)) {
//            $leadsQuery->whereDate('created_at', '>=', $this->date_from);
//        }
//
//        // Фильтрация по utm_content и utm_campaign
//        $utmContent = $ads->pluck('ad_id')->toArray();
//        $utmCampaign = $ads->pluck('campaign_id')->toArray();
//
//        $leads = $leadsQuery
//            ->where(function($query) use ($utmContent, $utmCampaign) {
//                $query->whereIn('utm_content', $utmContent)
//                    ->orWhereIn('utm_campaign', $utmCampaign);
//            })
//            ->get();
//        $leads = $leads->map(function ($lead){
//            if ($lead->getWebinarUser != null){
//                $lead->webinarUser = $lead->getWebinarUser;
//            } else {
//                $lead->webinarUser = null;
//            }
//
//            return $lead;
//        });
//        return $leads;
//    }


    public function loadStatistics()
    {
        if ($this->date_to == '') {
            $this->date_to = now()->format('Y-m-d');
        }
        if ($this->date_from == '2024-01-01') {
            $this->date_from = now()->subDays(2)->format('Y-m-d');
        }

        $TrafficController = new TrafficController();

        $ads = $TrafficController->getYandex($this->id, $this->date_from, $this->date_to, $this->search);
        $facebook_account_id = $this->analyticStatistica($ads, $this->id);
        // Суммируем необходимые значения
        $spendTotal = round($facebook_account_id->sum('spend'));
        $leadTotal = $facebook_account_id->sum('lead');
        $webinarTotal = $facebook_account_id->sum('webinar');
        $webinarWarm = $facebook_account_id->sum('webinarWarm');
        $paymentTotal = $facebook_account_id->sum('payment');
        $paymentMainTotal = $facebook_account_id->sum('paymentMain');

        // Рассчитываем статистические показатели
        $statistica = [
            'spend' => $spendTotal,
            'lead' => $leadTotal,
            'webinar' => $webinarTotal,
            'leadPrice' => $leadTotal > 0 ? round($spendTotal / $leadTotal) : 0,
            'webinarPrice' => $webinarTotal > 0 ? round($spendTotal / $webinarTotal) : 0,
            'webinarReach' => $leadTotal > 0 ? round(($webinarTotal / $leadTotal) * 100) : 0,
            'percentHot' => $webinarTotal > 0 ? round(($webinarWarm / $webinarTotal) * 100) : 0,
            'payment' => $paymentTotal,
            'paymentPrice' => $paymentTotal > 0 ? round($spendTotal / $paymentTotal) : 0,
            'paymentPercent' => $webinarTotal > 0 ? round(($paymentTotal / $webinarTotal) * 100) : 0,
            'paymentMain' => $paymentMainTotal,
            'paymentMainPrice' => $paymentMainTotal > 0 ? round($spendTotal / $paymentMainTotal) : 0,
        ];

        // Присваиваем рассчитанную статистику свойствам класса
        $this->statistica = $statistica;
        $this->dayWebStatistice = $facebook_account_id;
    }

    public function render()
    {
        if ($this->isLoading) {
            return 200;
        }
        $this->isLoading = true;
        $this->loadStatistics();
        $this->isLoading = false;
        return view('livewire.traffic.yandex');
    }
}
