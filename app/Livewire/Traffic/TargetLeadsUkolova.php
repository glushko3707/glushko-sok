<?php

namespace App\Livewire\Traffic;

use App\Models\Lead\Ukolova;
use App\Models\Notification\NewLeadWeb;
use Carbon\Carbon;
use Livewire\Component;

class TargetLeadsUkolova extends Component
{

    public $id;
    public $leads;
    public $searchPhone = '';
    public $dateTask = '';
    public $date_to = '';
    public $date_from = '';
    public $status = '';
    public $stat = [];
    public $NewLeadWeb = [];

    public function mount($id)
    {
        $this->id = $id;
    }


    public function render()
    {
        $id =  $this->id;

        $NewLeadWeb = Ukolova::query()->where('project_id',$id)->orderByDesc('created_at');

        if (!empty($this->date_to)) {$NewLeadWeb->whereDate('created_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$NewLeadWeb->whereDate('created_at', '>=', $this->date_from);}

        if (!empty($this->dateTask) && $this->dateTask != 'Все') {
            if ($this->dateTask == 'Будущие') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->whereDate('dateTask', '>', now());
                });
            }
            if ($this->dateTask == 'Просроченные') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->whereDate('dateTask', '<', now());
                });
            }
            if ($this->dateTask == 'Сегодня') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->whereDate('dateTask', '=', now());
                });
            }
        }

        if (!empty($this->status) && $this->status != 'all') {
            if ($this->status == 'new') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    $query->where('status', '=', 'Новый лид');
                });
            }
            if ($this->status == 'noConnect') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    $query->where('status', '=', 'Недозвон');
                });
            }

            if ($this->status == 'bad') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    $query->where('status', '=', 'Негатив');
                });
            }

            if ($this->status == 'meet') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->where('status', '=', 'Встреча');
                });
            }

            if ($this->status == 'possibleSale') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->where('status', '=', 'Заинтересован');
                });
            }

            if ($this->status == 'sales') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->where('status', '=', 'Продажа');
                });
            }


            if ($this->status == 'no_success') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->where('status', '=', 'Не успешно');
                });
            }


            if ($this->status == 'noTarget') {
                $NewLeadWeb->whereHas('getCrm', function ($query) {
                    // Задать желаемый диапазон дат
                    $query->where('status', '=', 'Не целевой');
                });
            }

        }
        if ($this->searchPhone != ''){
            $searchTerm = '%' . $this->searchPhone . '%';
            $NewLeadWeb->where('phone', 'like', $searchTerm);
        }

        $stat['count'] = $NewLeadWeb->count();
        $this->NewLeadWeb = $NewLeadWeb->get();

        $leadses = $NewLeadWeb->paginate('30');

        $leads = [];


        foreach ($leadses->items() as $key => $value){
            $leads [$value->id]['id']= $value['id'] ?? '';
            $leads [$value->id]['phone']= $value['phone'] ?? '';
            $leads [$value->id]['utm_source'] = $value['utm_source'] ?? '';
            $leads [$value->id]['utm_campaign'] = $value['utm_campaign'] ?? '';
            $date[$value->id] =  Carbon::parse($value['created_at'])->locale('ru');
            $leads [$value->id]['created_at'] = $date[$value->id]->day  . ' ' . $date[$value->id]->getTranslatedMonthName('Do MMMM') . ' ' . Carbon::parse($value['created_at'])->format('H:i');
            $leads [$value->id]['updated_at'] = Carbon::parse($value['updated_at'])->format('Y-m-d') ?? '';


            $getCrm = $value->getCrm;
            if (empty($getCrm)) {
                $getCrm =  \App\Models\LeadCrm\LeadCrm::query()->updateOrCreate([
                    'project_id' => $id,
                    'new_lead_web_id' => $value['id'],
                ],[
                    'status' => 'Новый лид',
                    'dateTask' => $value['created_at'],
                ]);
            }
            $leads [$value->id]['comment'] = $getCrm['comment'] ?? '';
            $leads [$value->id]['task'] = $getCrm['task'] ?? '';
            $leads [$value->id]['dateTask'] = $getCrm['dateTask'] ?? '';
            $leads [$value->id]['status'] = $getCrm['status'] ?? '';
            $leads [$value->id]['target'] = $getCrm['target'] ?? '';
            $leads [$value->id]['manager'] = $getCrm['manager'] ?? '';
            $leads [$value->id]['tryCall'] = $getCrm['tryCall'] ?? '';
        }

        $this->leads = $leads;
        $this->stat = $stat;

        return view('livewire.traffic.target-leads-ukolova',[
            'leadses' => $leadses,
        ]);
    }
}
