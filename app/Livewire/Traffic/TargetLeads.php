<?php

namespace App\Livewire\Traffic;

use App\Models\Lead\Ukolova;
use App\Models\Notification\TildaLead;
use Carbon\Carbon;
use Livewire\Component;
use Livewire\WithPagination;
class TargetLeads extends Component
{

    public $id;
//    public $leads;
    public $search = '';
    public $dateTask = '';
    public $date_to = '';
    public $date_from = '';
    public $status = '';
    public $stat = [];

    public function mount($id)
    {
        $this->id = $id;
    }

    public function save(){
        $filename = 'file.txt';
        $handle = fopen($filename, 'w');
        foreach ($this->leads as $row) {
            fwrite($handle, $row->phone . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);

    }

    public function render()
    {
        $id =  $this->id;
        $NewLeadWeb = TildaLead::query()->where('project_id',$id)->orderByDesc('created_at');

//        if ($id == 498){
//            $NewLeadWeb = Ukolova::query()->where('project_id',$id)->orderByDesc('created_at');
//        }

        if (!empty($this->date_to)) {$NewLeadWeb->whereDate('created_at', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$NewLeadWeb->whereDate('created_at', '>=', $this->date_from);}


        if (!empty($this->search)) {$NewLeadWeb = $NewLeadWeb->where(function($q)  {
            $query = $this->search;
            $q->where('phone', 'LIKE', '%' . $query . '%')
                ->orWhere('utm_source', 'LIKE', '%' . $query . '%')
                ->orWhere('utm_medium', 'LIKE', '%' . $query . '%')
                ->orWhere('utm_campaign', 'LIKE', '%' . $query . '%')
                ->orWhere('utm_content', 'LIKE', '%' . $query . '%')
                ->orWhere('utm_term', 'LIKE', '%' . $query . '%')
//                ->orWhere('value_1', 'LIKE', '%' . $query . '%')
            ;});}

        $stat['count'] = $NewLeadWeb->count();

        // Делаем клон запроса для подсчета лидов за сегодня
        $NewLeadWebToday = clone $NewLeadWeb;
        $stat['countToday'] = $NewLeadWebToday->whereDate('created_at', today())->count();

        $this->stat = $stat;
        return view('livewire.traffic.target-leads' , [
            'leads' => $NewLeadWeb->paginate(50)
            ]);
    }
}
