<?php

namespace App\Livewire\Traffic;

use App\Models\Facebook\FacebookStat;
use App\Models\Payment;
use Livewire\Component;

class TrafficAnalyticaNoAdmin extends Component
{
    public $date_to = '';
    public $date_from = '2024-04-01';
    public $id;
    public $dayWebStatistice = [];
    public $period;
    public $warm;
    public $statistica;
    public $utm_source;
    public $search;
    public $utm_source_all = [];

    public function mount($id)
    {
        $this->id = $id;
    }

    public function render()
    { $id = $this->id;
        if ($this->date_to == '') {$this->date_to = now()->format('Y-m-d');}
        $facebook_ad_ids = FacebookStat::where('project_id', $id);

        if (!empty($this->date_to)) {$facebook_ad_ids->whereDate('date', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$facebook_ad_ids->whereDate('date', '>=', $this->date_from);}


        $query = $this->search;
        if (!empty($this->search)) {$facebook_ad_ids->where(function($q) use ($query) {
            $q->where('ad_name', 'LIKE', '%' . $query . '%')
                ->orWhere('adset_name', 'LIKE', '%' . $query . '%')
                ->orWhere('campaign_name', 'LIKE', '%' . $query . '%');
        });}

        $facebook_ad_id =  $facebook_ad_ids
            ->with(['getLeads'  => function ($query) {
                $query->whereDate('created_at', '>=', $this->date_from)
                    ->whereDate('created_at', '<=', $this->date_to);
            },'getWebinarUser'  => function ($query) {
                $query->whereDate('date', '>=', $this->date_from)
                    ->whereDate('date', '<=', $this->date_to);
            }])
            ->get()
            ->map(function ($stats) {
                return [
                    'lead' => $stats->getLeads->count(),
                    'webinar' => $stats->getWebinarUser->count(),
                    'clicks' => $stats->clicks,
                    'impressions' => $stats->impressions,
                    'spend' => $stats->spend,
                    'clickAll' => $stats->clickAll,
                    'clickLink' => $stats->clickLink,
                    'account_id' => $stats->account_id,
                    'account_name' => $stats->account_name,
                    'campaign_id' => $stats->campaign_id,
                    'campaign_name' => $stats->campaign_name,
                    'adset_id' => $stats->adset_id,
                    'adset_name' => $stats->adset_name,
                    'ad_name' => $stats->ad_name,
                    'ad_id' => $stats->ad_id,
                    'payment' => 0,
                    'paymentMain' => 0,
                ];
            });

        $facebook_ad_id = $facebook_ad_id->groupBy('ad_id')
            ->map(function ($stats) {
                return [
                    'leadPrice' => $stats->first()['lead'] > 0 ? ($stats->sum('spend') /$stats->first()['lead']) : 0,
                    'lead' => $stats->first()['lead'],
                    'webinar' => $stats->first()['webinar'],
                    'webinarPrice' => $stats->first()['webinar'] > 0 ? ($stats->sum('spend') /$stats->first()['webinar']) : 0,
                    'webinarPercent' => $stats->first()['lead'] > 0 ? ($stats->first()['webinar']/$stats->first()['lead']) : 0,
                    'clicks' => $stats->sum('clicks'),
                    'impressions' => $stats->sum('impressions'),
                    'spend' => $stats->sum('spend'),
                    'clickAll' => $stats->sum('clickAll'),
                    'clickLink' => $stats->sum('clickLink'),
                    'account_id' => $stats->first()['account_id'],
                    'account_name' => $stats->first()['account_name'],
                    'campaign_id' => $stats->first()['campaign_id'],
                    'campaign_name' => $stats->first()['campaign_name'],
                    'adset_id' => $stats->first()['adset_id'],
                    'adset_name' => $stats->first()['adset_name'],
                    'ad_name' => $stats->first()['ad_name'],
                    'ad_id' => $stats->first()['ad_id'],
                    'payment' => 0,
                    'paymentMain' => 0,
                ];
            });


        $payments = Payment::query()->where('project_id',$id)->with('getUkolovaLead');
        if (!empty($this->date_to)) {$payments->whereDate('date', '<=', $this->date_to);}
        if (!empty($this->date_from)) {$payments->whereDate('date', '>=', $this->date_from);}
        $payments = $payments->get();

        foreach ($payments as $payment){
            $ad_id = $payment->getUkolovaLead->last()->utm_content ?? null;
            $product = $payment->product;

            $facebook_ad_id = $facebook_ad_id->map(function ($item, $k) use ($ad_id, $product) {
                // Предполагаем, что $ad_id - это ключ для обновления в коллекции
                if ($k == $ad_id) {
                    if ($product == 'Аудит') {
                        $item['payment']++;
                    } elseif ($product == 'Основной продукт') {
                        $item['paymentMain']++;
                    }
                }
                return $item;
            });
        }

        $facebook_adset_id = $facebook_ad_id
            ->groupBy('adset_id')
            ->map(function ($stats) {
                // Подсчет агрегированных значений для каждой группы
                return [
                    'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') /$stats->sum('lead')) : 0,
                    'lead' => $stats->sum('lead'),
                    'webinar' => $stats->sum('webinar'),
                    'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') /$stats->sum('webinar')) : 0,
                    'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') /$stats->sum('lead'))*100 : 0,
                    'paymentMain' => $stats->sum('paymentMain') ?? 0,
                    'payment' => $stats->sum('payment') ?? 0,
                    'impressions' => $stats->sum('impressions'),
                    'spend' => $stats->sum('spend'),
                    'clickAll' => $stats->sum('clickAll'),
                    'clickLink' => $stats->sum('clickLink'),
                    'account_id' => $stats->first()['account_id'],
                    'account_name' => $stats->first()['account_name'],
                    'campaign_id' => $stats->first()['campaign_id'],
                    'campaign_name' => $stats->first()['campaign_name'],
                    'adset_id' => $stats->first()['adset_id'],
                    'adset_name' => $stats->first()['adset_name'],
                    'facebook_ad_collect' => $stats,
                ];
            });

        $facebook_campaign_id = $facebook_adset_id
            ->groupBy('campaign_id')
            ->map(function ($stats) {
                return [
                    'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') /$stats->sum('lead')) : 0,
                    'lead' => $stats->sum('lead'),
                    'webinar' => $stats->sum('webinar'),
                    'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') /$stats->sum('webinar')) : 0,
                    'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') /$stats->sum('lead'))*100 : 0,
                    'paymentMain' => $stats->sum('paymentMain') ?? 0,
                    'payment' => $stats->sum('payment') ?? 0,
                    'clicks' => $stats->sum('clicks'),
                    'impressions' => $stats->sum('impressions'),
                    'spend' => $stats->sum('spend'),
                    'clickAll' => $stats->sum('clickAll'),
                    'clickLink' => $stats->sum('clickLink'),
                    'account_id' => $stats->first()['account_id'],
                    'account_name' => $stats->first()['account_name'],
                    'campaign_id' => $stats->first()['campaign_id'],
                    'campaign_name' => $stats->first()['campaign_name'],
                    'facebook_adset_collect' => $stats,
                ];
            });

        $facebook_account_id = $facebook_campaign_id
            ->groupBy('account_id')
            ->map(function ($stats) {
                // Подсчет агрегированных значений для каждой группы
                return [
                    'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') /$stats->sum('lead')) : 0,
                    'lead' => $stats->sum('lead'),
                    'webinar' => $stats->sum('webinar'),
                    'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') /$stats->sum('webinar')) : 0,
                    'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') /$stats->sum('lead'))*100 : 0,
                    'paymentMain' => $stats->sum('paymentMain') ?? 0,
                    'payment' => $stats->sum('payment') ?? 0,
                    'clicks' => $stats->sum('clicks'),
                    'impressions' => $stats->sum('impressions'),
                    'spend' => $stats->sum('spend'),
                    'clickAll' => $stats->sum('clickAll'),
                    'clickLink' => $stats->sum('clickLink'),
                    'account_id' => $stats->first()['account_id'],
                    'account_name' => $stats->first()['account_name'],
                    'facebook_campaign_collect' => $stats,
                ];
            });

        $statistica = [];

        $statistica['spend'] = $facebook_account_id->sum('spend');
        $statistica['lead'] = $facebook_account_id->sum('lead');
        $statistica['webinar'] = $facebook_account_id->sum('webinar');

        $statistica['leadPrice'] = $statistica['lead'] > 0 ? $statistica['spend'] / $statistica['lead'] : 0;
        $statistica['webinarPrice'] = $statistica['webinar'] > 0 ? $statistica['spend'] / $statistica['webinar'] : 0;
        $statistica['webinarReach'] = $statistica['lead'] > 0 ? round($statistica['webinar'] / $statistica['lead'] * 100) : 0;

        $this->statistica = $statistica;
        $this->dayWebStatistice = $facebook_account_id;

        return view('livewire.traffic.traffic-analytica-no-admin');
    }
}
