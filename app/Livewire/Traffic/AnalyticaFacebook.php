<?php

namespace App\Livewire\Traffic;

use App\Http\Controllers\Facebook\FbAdminController;
use App\Http\Controllers\Notification\TrafficController;
use App\Jobs\Facebook\AdsUpdate;
use App\Models\Facebook\FacebookStat;
use App\Models\Lead\Ukolova;
use App\Models\Payment;
use Carbon\Carbon;
use Livewire\Component;

class AnalyticaFacebook extends Component
{
    public $date_to = '';
    public $date_from = '2024-01-01';
    public $date_preview;
    public $id;
    public $add_account_id = '';
    public $dayWebStatistice = [];
    public $period;
    public $warm;
    public $statistica;
    public $utm_source;
    public $search = null;
    public $utm_source_all = [];
    public $add_accounts_id = [];
    public $bm_id;
    public $add_accounts_mode = false;

    public function mount($id)
    {
        $this->id = $id;
    }

    public function updateStatistica()
    {
        $FacebookStat = FacebookStat::query()->where('project_id', $this->id)->pluck('account_id')->unique();

        foreach ($FacebookStat as $account_id) {

            if ($account_id == '708424221102216'){
                continue;
            }
//            $TrafficController = new TrafficController();
//            $TrafficController->targetUpdateStat($this->id, $account_id);
            AdsUpdate::dispatch($this->id, $account_id);
        }

    }

    protected $rules = [
        'bm_id' => 'required|numeric',
    ];

    public function add_account_mode ()
    {
        $this->add_accounts_mode = true;
    }

    public function add_account()
    {
        $this->validate();
        $FbAdminController = new FbAdminController();
        $add_accounts_id = $FbAdminController->getAdAccaunts($this->id,$this->bm_id);
        $this->add_accounts_id = $add_accounts_id;
    }

    public function analyticStatistica($ads, $id)
    {
        // Группировка и агрегация данных
        $facebook_ad_id = $ads->groupBy('ad_id')->map(function ($stats) {
            $firstStat = $stats->first();
            $adId = $firstStat['ad_id'];
            $lead = $firstStat['lead'];
            $payment = $firstStat['payment'];
            $webinar = $firstStat['webinar'];
            $webinarWarm = $firstStat['webinarWarm'];
            $spend = $stats->sum('spend');
            $clicks = $stats->sum('clickLink');
            $impressions = $stats->sum('impressions');
            // Предварительная фильтрация платежей
            return [
                'leadPrice' => $lead > 0 ? ($spend / $lead) : 0,
                'lead' => $lead,
                'webinar' => $webinar,
                'webinarPrice' => $webinar > 0 ? ($spend / $webinar) : 0,
                'webinarPercent' => $lead > 0 ? ($webinar / $lead) * 100 : 0,
                'webinarWarm' => $webinarWarm,
                'percentHot' => $webinar > 0 ? ($webinarWarm / $webinar) * 100 : 0,
                'clicks' => $clicks,
                'impressions' => $impressions,
                'spend' => $spend,
                'account_id' => $firstStat['account_id'],
                'account_name' => $firstStat['account_name'],
                'campaign_id' => $firstStat['campaign_id'],
                'campaign_name' => $firstStat['campaign_name'],
                'adset_id' => $firstStat['adset_id'],
                'adset_name' => $firstStat['adset_name'],
                'ad_id' => $adId,
                'ad_name' => $firstStat['ad_name'],
                'payment' => $payment,
            ];
        });
        // Группировка по adset_id
        $facebook_adset_id = $facebook_ad_id->groupBy('adset_id')->map(function ($stats) {
            return [
                'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') / $stats->sum('lead')) : 0,
                'lead' => $stats->sum('lead'),
                'webinar' => $stats->sum('webinar'),
                'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') / $stats->sum('webinar')) : 0,
                'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') / $stats->sum('lead')) * 100 : 0,
                'webinarWarm' => $stats->sum('webinarWarm'),
                'percentHot' => $stats->sum('webinar') > 0 ? ($stats->sum('webinarWarm') / $stats->sum('webinar')) * 100 : 0,
                'payment' => $stats->sum('payment'),
                'impressions' => $stats->sum('impressions'),
                'spend' => $stats->sum('spend'),
                'clicks' => $stats->sum('clicks'),
                'account_id' => $stats->first()['account_id'],
                'account_name' => $stats->first()['account_name'],
                'campaign_id' => $stats->first()['campaign_id'],
                'campaign_name' => $stats->first()['campaign_name'],
                'adset_id' => $stats->first()['adset_id'],
                'adset_name' => $stats->first()['adset_name'],
                'facebook_ad_collect' => $stats,
            ];
        });

        // Группировка по campaign_id
        $facebook_campaign_id = $facebook_adset_id->groupBy('campaign_id')->map(function ($stats) {
            return [
                'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') / $stats->sum('lead')) : 0,
                'lead' => $stats->sum('lead'),
                'webinar' => $stats->sum('webinar'),
                'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') / $stats->sum('webinar')) : 0,
                'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') / $stats->sum('lead')) * 100 : 0,
                'webinarWarm' => $stats->sum('webinarWarm'),
                'percentHot' => $stats->sum('webinar') > 0 ? ($stats->sum('webinarWarm') / $stats->sum('webinar')) * 100 : 0,
                'payment' => $stats->sum('payment'),
                'clicks' => $stats->sum('clicks'),
                'impressions' => $stats->sum('impressions'),
                'spend' => $stats->sum('spend'),
                'clickLink' => $stats->sum('clickLink'),
                'account_id' => $stats->first()['account_id'],
                'account_name' => $stats->first()['account_name'],
                'campaign_id' => $stats->first()['campaign_id'],
                'campaign_name' => $stats->first()['campaign_name'],
                'facebook_adset_collect' => $stats,
            ];
        });

        // Группировка по account_name
        $facebook_account_id = $facebook_campaign_id->groupBy('account_name')->map(function ($stats) {
            return [
                'leadPrice' => $stats->sum('lead') > 0 ? ($stats->sum('spend') / $stats->sum('lead')) : 0,
                'lead' => $stats->sum('lead'),
                'webinar' => $stats->sum('webinar'),
                'webinarPrice' => $stats->sum('webinar') > 0 ? ($stats->sum('spend') / $stats->sum('webinar')) : 0,
                'webinarPercent' => $stats->sum('lead') > 0 ? ($stats->sum('webinar') / $stats->sum('lead')) * 100 : 0,
                'webinarWarm' => $stats->sum('webinarWarm'),
                'percentHot' => $stats->sum('webinar') > 0 ? ($stats->sum('webinarWarm') / $stats->sum('webinar')) * 100 : 0,
                'payment' => $stats->sum('payment'),
                'clicks' => $stats->sum('clicks'),
                'impressions' => $stats->sum('impressions'),
                'spend' => $stats->sum('spend'),
                'account_id' => $stats->first()['account_id'],
                'account_name' => $stats->first()['account_name'],
                'facebook_campaign_collect' => $stats,
            ];
        });

        return $facebook_account_id;
    }


    public function render()
    {


        $id = $this->id;
        if ($this->date_to == '') {$this->date_to = now()->format('Y-m-d');}
        if ($this->date_from == '2024-01-01') {
            $this->date_from = now()->subDays(7)->format('Y-m-d')
            ;}

        switch ($this->date_preview) {
            case 'today':
                $this->date_to = today()->format('Y-m-d');
                $this->date_from = today()->format('Y-m-d');
                break;
            case 'yesterday':
                $this->date_to = today()->subDay()->format('Y-m-d');
                $this->date_from = today()->subDay()->format('Y-m-d');
                break;
            case '2dayAgo':
                $this->date_from = today()->subdays(2)->format('Y-m-d');
                $this->date_to = today()->subdays(2)->format('Y-m-d');
                break;
            case 'las7Days':
                $this->date_from = today()->subDays(7)->format('Y-m-d');
                $this->date_to = today()->format('Y-m-d');
                break;
            case 'month':
                $this->date_from = today()->startOfMonth()->format('Y-m-d');
                $this->date_to = today()->format('Y-m-d');
                break;
            case 'last30Days':
                $this->date_from = today()->subDays(30)->format('Y-m-d');
                $this->date_to = today()->format('Y-m-d');
                break;
        }

        $TrafficController = new TrafficController();

        $ads = $TrafficController->getFacebook($this->id,$this->date_from,$this->date_to,$this->search);
        $facebook_account_id = $this->analyticStatistica($ads, $this->id);

        $statistica = [];

        $statistica['spend'] = $facebook_account_id->sum('spend');
        $statistica['lead'] = $facebook_account_id->sum('lead');
//        $statistica['leadToday'] = $ads->sum('leadToday');
        $statistica['webinar'] = $facebook_account_id->sum('webinar');

        $statistica['leadPrice'] = $statistica['lead'] > 0 ?  round($statistica['spend'] / $statistica['lead'],2) : 0;
        $statistica['webinarPrice'] = $statistica['webinar'] > 0 ? round($statistica['spend'] / $statistica['webinar'],2) : 0;
        $statistica['webinarReach'] = $statistica['lead'] > 0 ? round($statistica['webinar'] / $statistica['lead'] * 100) : 0;

        $statistica['payment'] = $facebook_account_id->sum('payment');
        $statistica['paymentPrice'] = $statistica['payment'] > 0 ?  round($statistica['spend'] / $statistica['payment'],2) : 0;
        $statistica['paymentPercent'] = $statistica['webinar'] > 0 ?  round($statistica['payment'] / $statistica['webinar']*100,2) : 0;

        $statistica['paymentMain'] = $facebook_account_id->sum('paymentMain');
        $statistica['paymentMainPrice'] = $statistica['paymentMain'] > 0 ?  round($statistica['spend'] / $statistica['paymentMain'],) : 0;


        $this->statistica = $statistica;
        $this->dayWebStatistice = $facebook_account_id;

        return view('livewire.traffic.analytica-facebook');
    }

}
