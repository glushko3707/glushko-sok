<?php

namespace App\Events\Payment;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PaymentIntensiveKupr
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public array $data;
    public int $project_id;

    /**
     * Create a new event instance.
     *
     * @param array $data
     * @param int $project_id
     * @return void
     */

    public function __construct(array $data,int $project_id)
    {
        $this->data = $data;
        $this->project_id = $project_id;
    }

}
