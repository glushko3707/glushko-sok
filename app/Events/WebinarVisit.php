<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class WebinarVisit
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public array $userWeb;
    public int $id;

    /**
     * Create a new event instance.
     *
     * @param array $userWeb
     * @param int $id
     * @return void
     */
    public function __construct(array $userWeb, int $id)
    {
        $this->userWeb = $userWeb;
        $this->id = $id;
    }

}
