<?php

namespace App\Http\Controllers\Lead;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\NotificationController;
use App\Http\Controllers\Notification\WelcomeMessageController;
use App\Models\BlackList;
use App\Models\LeadCrm\LeadCrm;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\WebSms;
use App\Models\Webinar\LeadForWeb;
use Carbon\Carbon;
use Throwable;

class NewLeadController extends Controller
{
    public function newLeeds($phone, $project, $data)
    {
        $data['date'] = today()->format('Y-m-d');

        $data['utm_medium'] = $data ['utm_medium'] ?? '';

        if (empty($data ['utm_medium']) && !empty($data ['utm_campaign'])){
            $data ['utm_medium'] = $data ['utm_campaign'];
        }

        $NewLeadWeb = NewLeadWeb::query()->updateOrCreate([
            'phone' => $phone,
            'project_id' => $project->id,
            'date' => $data['date'],
        ], $data);

        LeadCrm::query()->updateOrCreate([
            'new_lead_web_id' => $NewLeadWeb->id,
            'project_id' => $project->id,
        ], [
            'new_lead_web_id' => $NewLeadWeb->id,
            'dateTask' => today(),
        ]);


        try {
            $this->newLeadNotification($project,$phone,$data,$NewLeadWeb);
        } catch (Throwable $e){
            report($e);
        }
    }

    public function newLeadNotification($projectModel,$phone,$data,$newLeadWeb = null)
    {
        $notificationSetting = $projectModel->getNotificationSetting;
        if ($notificationSetting != null) {
            $welcomeMessageController = new WelcomeMessageController();


            if ($notificationSetting->welcomeWa == '1') {
                $blackList = BlackList::query()->where('phone', $phone)->exists();
                if (!$blackList) {
                    $welcomeMessageController->sendWelcomeWa($phone, $projectModel);
                }
            }


            if ($notificationSetting->welcomeWaba == '1') {
                $blackList = BlackList::query()->where('phone', $phone)->exists();
                if (!$blackList) {
                    $welcomeMessageController->sendWelcomeWaba($phone, $projectModel);
                }
            }


            if ($notificationSetting->welcomeSms == '1') {
                $sms = WebSms::query()->where('project_id', $projectModel->id)->whereNull('timeSend')->get()->first();
                $blackList = BlackList::query()->where('phone', $phone)->exists();
                if (!$blackList) {
                    $welcomeMessageController->sendWelcomeSms($sms, $phone, $projectModel);
                }
            }


            // добавляем инфо про вебинар
            if (isset($notificationSetting->dateWeb)) {
                $dateWeb = Carbon::parse($notificationSetting->dateWeb);

                LeadForWeb::query()->updateOrCreate([
                    'phone' => $phone,
                    'project_id' => $projectModel->id,
                    'dateWeb' => $dateWeb,
                ], [
                    'utm_source' => $data ['utm_source'],
                    'utm_medium' => $data ['utm_medium'],
                    'user_name' => $data ['name'] ?? '',
                ]);
            }

            // добавляем уведомления если вебинар сегодня
            if (isset($notificationSetting->dateWeb) && Carbon::parse($notificationSetting->dateWeb)->isToday()) {
                $newLeadWeb = $newLeadWeb ?? null;
                $NotificationController = new NotificationController();
                $NotificationController->addNotification($phone, $projectModel->id, $data['utm_source'],$newLeadWeb);
            }
        }

    }

}
