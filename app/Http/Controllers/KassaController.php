<?php

namespace App\Http\Controllers;

use App\Events\Payment\PaymentIntensiveKupr;
use App\Http\Controllers\Intensive\IntensiveUserController;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Models\Notification\NewLeadWeb;
use App\Models\Payment;
use App\Models\PaymentService;
use App\Models\User;
use App\Traits\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Throwable;
use YooKassa\Client;

class KassaController extends Controller
{
    use Notification;
    public function index(Request $request,$id)
    {

        $object = $request->object;
        $status = $object['status'] ?? '';
        $orderId = $object['id'] ?? '';
        $phone = $object['merchant_customer_id'] ?? '';
        $description = $object['description'];
        $sum = (int)$object['amount']['value'];

        $payment = Payment::query()->where([ 'project_id' => $id,'sum' => $sum, 'phone' => $phone, 'product' => $description])->whereIn('status', ['succeeded', 'по карте'])->whereDate('date', today())->get()->last();

        if (!empty($payment)) {
            return 200;
        }

        Payment::query()->updateOrCreate([
            'project_id' => $id,
            'phone' => $phone,
            'sum' => $sum,
            'product' => $description,
            'date' => now()->format('Y-m-d'),
        ],[
            'sum' => $sum,
            'orderId' => $orderId,
            'status' => $status,

        ]);

        return 200;
    }
    public function tildaRequest(Request $request,$id){
        $phone = $request->phone ?? '';
        $product_id = $request->product_id ?? 3;

        $telegram_info = $this->getTelegram_info($request,$phone,$product_id);
        // делаем редирект
        return $this->kassaRedirect($phone,$id,$telegram_info,$product_id);
    }

    public function kassaRedirect($phone,$id,$telegram_info,$product_id){

        $KassaProduct =  Payment\KassaProduct::query()->find($product_id);
        $description = $KassaProduct->description;

        if (isset($phone)){
            $merchant_customer_id = preg_replace('/\D/', '', $phone); // Удаляем все символы, кроме цифр, с помощью регулярного выражения
        } else {
            $merchant_customer_id = '';
        }

        $metadata ['purpose'] = $KassaProduct->trigger;
        $metadata ['project_id'] = $id;

        $metadata ['phone'] = $phone ?? '';
        $return_url = $KassaProduct->return_url;
        $price = $KassaProduct->price;
        $url = $this->createKassa($price,$description,$merchant_customer_id,$return_url ,$metadata);
        $params = $url['params'];

        $this->paymentRequest($merchant_customer_id,$description,$price,$params['orderId'],$metadata ['project_id'],$telegram_info);
        $confirmationUrl = $url['confirmationUrl'];
        return redirect($confirmationUrl);
    }
    public function getTelegram_info($request,$phone,$product_id){
        $KassaProduct =  Payment\KassaProduct::query()->find($product_id);
        if (isset($request->ssId)){
            $smartsender = [
                'ssId' => $request->ssId ?? '',
                'trigger' => $KassaProduct->trigger,
            ];
            $metadata['smartsender'] = json_encode($smartsender);
            $telegram_info = [
                'telegram_platform' => 'smartsender',
                'platform_id' => $request->ssId ?? '',
            ];
        }

        if (isset($request->platform_id)){
            $salebot = [
                'platform_id' => $request->platform_id ?? '',
                'trigger' => $KassaProduct->trigger,
            ];
            $metadata['salebot'] = json_encode($salebot);
            $telegram_info = [
                'telegram_platform' => 'salebot',
                'platform_id' => $request->platform_id ?? '',
            ];
        }

        $telegram_info = $telegram_info ?? null;

        return $telegram_info;
    }

    public function connect(Request $request,$sum,$id)
    {
        $product_id = $request->product_id ?? null;

        // переадресация на оплату, если это предоплата
        if (($sum == 1500 || $sum == 2500) && $id == 114 && $product_id == null) {
            $product_id = 5;
            $KassaProduct =  Payment\KassaProduct::query()->find($product_id);
            $description = $KassaProduct->description;
            return view('payment.pay',compact('sum','id','description','product_id'));
        }

        // переадресация на оплату, если есть номер телефона
        if (isset($request->phone) &&  !empty($request->phone) && $id == 114){
            $CheckRussiaPhoneController = new CheckRussiaPhoneController();
            $phone = $CheckRussiaPhoneController->checkPhone($request->phone);

            if ($phone){
                $product_id = $request->product_id ?? 3;

                $telegram_info = $this->getTelegram_info($request,$phone,$product_id);
                // делаем редирект
                return $this->kassaRedirect($phone,$id,$telegram_info,$product_id);
            }
        }

        if ($id == 114) {
            $product_id = $product_id ?? $request->product_id ?? 3;
            $KassaProduct =  Payment\KassaProduct::query()->find($product_id);
            $description = $KassaProduct->description;
            return view('payment.pay',compact('sum','id','description','product_id'));
        }

        return view('payment.pay',compact('sum','id'));
    }
    public function connectKurs(Request $request,$sum,$id)
    {
        return view('payment.payKurs',compact('sum','id'));
    }
    public function connectStore(Request $request,$id)
    {
        $validated = $request->validate([
            'cps_phone' => 'required',
        ], [
            'cps_phone' => 'Обязательно для заполнения',
        ]);

        $product_id = $request->product_id ?? 3;
        $KassaProduct =  Payment\KassaProduct::query()->find($product_id);
        $description = $KassaProduct->description;

        $return_url = $KassaProduct->return_url ?? 'https://sok-agency.com/success_490';
        $sum = (int)$request->sum;
        $merchant_customer_id = $phone = preg_replace('/\D/', '', $request->cps_phone); // Удаляем все символы, кроме цифр, с помощью регулярного выражения
        $metadata = ['purpose' => 'intensive','project_id' => $id,'sum' => $sum,'product_id' => $product_id];

        $url = $this->createKassa($sum,$description,$merchant_customer_id,$return_url ,$metadata);

        $params = $url['params'];
        $confirmationUrl = $url['confirmationUrl'];

        $product = $KassaProduct->description;
        $payment = Payment::query()->where([ 'project_id' => $id, 'phone' => $phone,'product' => $product,'sum' => $sum ])->whereDate('date', today())->get()->first();
        if (empty($payment)){

            $newLead = NewLeadWeb::query()->where('phone', $phone)->where('project_id', $id)->get()->last();

            $date = [
                'date' => now()->format('Y-m-d'),
                'status' => 'open',
                'project_id' => $id,
                'phone' => $phone,
                'product' => $product,
                'sum' => $sum,
                'orderId' => $params['orderId'],
            ];

            if (!empty($newLead)){
                $date ['lead_id'] = $newLead->id;
                $date ['lead_date'] = Carbon::parse($newLead->created_at)->format('Y-m-d');
            }

            Payment::query()->create($date);
        }

        return redirect($confirmationUrl);
    }

    public function connectStoreKurs(Request $request,$id)
    {
        $validated = $request->validate([
            'cps_phone' => 'required',
        ], [
            'cps_phone' => 'Обязательно для заполнения',
        ]);

        $return_url = 'https://sok-agency.com/success_2500';

        $sum = (int)$request->sum;
        $merchant_customer_id = $phone = preg_replace('/\D/', '', $request->cps_phone); // Удаляем все символы, кроме цифр, с помощью регулярного выражения
        $description = 'Предоплата 100к на телеграм с Юлией Купр';
        $metadata = ['purpose' => 'kurs','project_id' => $id,'sum' => $sum];
        $url = $this->createKassa($sum,$description,$merchant_customer_id,$return_url ,$metadata);

        $params = $url['params'];
        $confirmationUrl = $url['confirmationUrl'];

        $product = 'Предоплата 100к на телеграм с Юлией Купр';

        $payment = Payment::query()->where([ 'project_id' => $id, 'phone' => $phone,'product' => $product,'sum' => $sum ])->whereDate('date', today())->get()->first();

        if (empty($payment)){

            Payment::query()->insert([
                'date' => now()->format('Y-m-d'),
                'updated_at' => now(),
                'created_at' => now(),
                'project_id' => $id,
                'phone' => $phone,
                'product' => $product,
                'sum' => $sum,
                'orderId' => $params['orderId'],
                'status' => 'open',
            ]);
        }

        return redirect($confirmationUrl);
    }
    public function notifications(Request $request){
        try {
              if (isset($request->object['metadata']['purpose'])) {
            $purpose =  $request->object['metadata']['purpose'];

                  // определяем какой сервис оплачивается
                  switch ($purpose) {
                case 'LeadPro':
                    if ($request->event == 'payment.succeeded')  {
                        $this->paymentService($request->object);
                    }
                    break;
                case 'chat_99':
                    if ($request->event == 'payment.succeeded')  {
                        $this->paymentChat99($request->object);
                    }
                    break;
                default:
                    if ($request->event == 'payment.succeeded')  {
                        $this->intensive_299($request->object);
                    }
                    break;
            }

//            if (isset($request->object['metadata']['salebot'])){
//                $status = $this->salebotActionPayment();
//            }

        }
        } catch (Throwable $e) {
            report($e);
            return 201;

        }
            return 200;
    }

    function createKassa($sum,$description,$merchant_customer_id,$return_url ,$metadata = ['text' => 'unknown']) {

        $client = new Client();
        $client->setAuth('820111', 'live_xueyiUBPI-ES-DMpCGP8LsRQyRHausGnPxjDoynPO3Q');
        $idempotenceKey = uniqid('', true);
        $response = $client->createPayment(
            array(
                'amount' => array(
                    'value' => $sum,
                    'currency' => 'RUB',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => $return_url,
                ),
                'description' => $description,
                'merchant_customer_id' => $merchant_customer_id,
                'metadata' => $metadata,
            ),
            $idempotenceKey
        );

        //get confirmation url
        $confirmationUrl = $response->getConfirmation()->getConfirmationUrl();
        $url_components = parse_url( $confirmationUrl, $component = -1 );
        parse_str($url_components['query'], $params);

        return [
            'confirmationUrl' => $confirmationUrl,
            'params' => $params,
        ];
    }
    function paymentIntensive($object){

        $status = $object['status'] ?? '';
        $orderId = $object['id'] ?? '';
        $phone = $object['merchant_customer_id'] ?? '';
        $description = $object['description'];
        $sum = (int)$object['amount']['value'];

        $project_id = $object['metadata']['project_id'];


        $payment = Payment::query()->where([ 'project_id' => $project_id,'sum' => $sum, 'phone' => $phone, 'product' => $description])->whereIn('status', ['succeeded', 'по карте'])->whereDate('date', today())->get()->last();
        if (!empty($payment)) {
            return 200;
        }
        Payment::query()->updateOrCreate([
            'project_id' => $project_id,
            'phone' => $phone,
            'sum' => $sum,
            'product' => $description,
            'date' => now()->format('Y-m-d'),
        ],[
            'sum' => $sum,
            'orderId' => $orderId,
            'status' => $status,
        ]);
        return 200;
    }
    function intensive_299($object){
        $status = $object['status'] ?? '';
        $orderId = $object['id'] ?? '';
        $phone = $object['merchant_customer_id'] ?? '';
        $description = $object['description'] ?? '';
        $sum = (int)$object['amount']['value'] ?? '';
        $product_id = $object['metadata']['product_id'] ?? null;

        $project_id = $object['metadata']['project_id'] ?? 114;

        $payment = Payment::query()->where([ 'project_id' => $project_id,'sum' => $sum, 'phone' => $phone, 'product' => $description])->whereIn('status', ['succeeded', 'по карте'])->whereDate('date', today())->get()->last();
        if (!empty($payment)) {
            return 200;
        }

        if (Carbon::now()->format('H') < 16 && $description == 'Оплата интенсив по телеграм с Юлией Купр') {
            $date = today()->subDay()->format('Y-m-d');
        } else {
            $date = today()->format('Y-m-d');
        }

        $newLead = NewLeadWeb::query()->where('phone', $phone)->where('project_id', $project_id)->get()->last();

        $data = [
            'date' => $date,
            'phone' => $phone,
            'sum' => $sum,
            'product' => $description,
            'status' => $status,
        ];

        if (!empty($newLead)){
            $data ['lead_id'] = $newLead->id;
            $data ['lead_date'] = Carbon::parse($newLead->created_at)->format('Y-m-d');
        }

        Payment::query()->updateOrCreate([
            'project_id' => $project_id,
            'orderId' => $orderId,
        ],$data);

        Payment::query()
            ->where('project_id',$project_id)
            ->where('status','!=', $status)
            ->where('phone','=', $phone)
            ->where('product','=', $description)
            ->whereDate('date', today()
            )->delete();

        // добавляем пользователя в интенсив
        try {
            if ($description == 'Оплата интенсив по телеграм с Юлией Купр' || $description == 'Недельный интенсив с Юлией Купр'){
                $product_id = 3;
            }
            if (!empty($product_id) && ($product_id == 3 || $product_id == 4)){

                $IntensiveUserController = new IntensiveUserController();
                $addUserToNextIntensive = $IntensiveUserController->addUserToNextIntensive($phone,$project_id);
                $NextIntensive = $addUserToNextIntensive ['NextIntensive'];
                $IntensiveUser = $addUserToNextIntensive ['IntensiveUser'];

                $LinkInvitePhoneController = new LinkInvitePhoneController();
                $link = $LinkInvitePhoneController->createInviteLinkIntensivePhone($project_id,$NextIntensive->main_chat_id,$phone);
                $data['linkInviteIntensive'] = $link;

                event(new PaymentIntensiveKupr($data,$project_id));

                $IntensiveUser->payment_method = 'kassa';
                $IntensiveUser->save();
//                $this->sendMessagesPayment($phone,$project_id);
            }
        } catch (Throwable $e) {
            report($e);
        }

        return 200;
    }



    function paymentChat99($object){
        $status = $object['status'] ?? '';
        $orderId = $object['id'] ?? '';
        $phone = $object['metadata']['phone'] ?? '';
        $description = $object['description'];
        $sum = (int)$object['amount']['value'];

        $project_id = $object['metadata']['project_id'];

        Payment::query()->updateOrCreate([
            'project_id' => $project_id,
            'phone' => $phone,
            'sum' => $sum,
            'product' => $description,
            'date' => now()->format('Y-m-d'),
        ],[
            'sum' => $sum,
            'orderId' => $orderId,
            'status' => $status,
        ]);

        if (isset($object['metadata']['smartsender'])){
            $smartsender = json_decode($object['metadata']['smartsender']);
            $ssId = $smartsender->ssId ?? null;
            $ssTrigger = $smartsender->trigger ?? null;
            $telegram_project_id = 5;
            if ($ssId && $ssTrigger){
                $date = [
                    'ssId' => $ssId,
                    'trigger' => $ssTrigger,
                ];
                try{
                    $response = Http::withHeaders([
                        "content-type" => "application/json",
                        'accept' => 'application/json',
                    ])->post('https://telegram.glushko-sok.ru/api/smartsender/fire/' . $telegram_project_id,$date);
                } catch (Throwable $e){
                    report($e);
                }

                $dates = [
                    'ssId' => $ssId,
                    'value' => '+' . $phone,
                    'variable_name' => 'phone',
                ];
                try{
                    $response = Http::withHeaders([
                        "content-type" => "application/json",
                        'accept' => 'application/json',
                    ])->post('https://telegram.glushko-sok.ru/api/smartsender/setVaruable/' . $telegram_project_id,$dates);
                } catch (Throwable $e){
                    report($e);
                }


            }
        }

        return 200;
    }
    function paymentService($object){
        $date['tariff'] = $object['metadata']['purpose'] ?? '';
        $date['user_id'] = $object['merchant_customer_id'] ?? '';
        $date['sum'] = $object['amount']['value'] ?? '';
        $date['id_payment'] = $object['id'];
        $date['expire'] = now()->addMonths(1);
        PaymentService::query()->updateOrCreate([
            'user_id' => $date['user_id'],
            'id_payment' => $date['id_payment'],
        ],$date);
        User::query()->find($date['user_id'])->update(['expire' =>  $date['expire']]);
    }
    function paymentRequest($phone,$product,$sum,$orderId,$project_id,$telegram_info = null){

        if ($telegram_info) {
            $telegram_platform = $telegram_info['telegram_platform'];
            $platform_id =  $telegram_info['platform_id'];
        }

        $payment = Payment::query()->where([ 'project_id' => $project_id, 'phone' => $phone,'product' => $product,'sum' => $sum ])->whereDate('date', today())->get()->first();

        if (empty($payment)){
            Payment::query()->create([
                'date' => now()->format('Y-m-d'),
                'project_id' => $project_id,
                'phone' => $phone,
                'product' => $product,
                'sum' => $sum,
                'orderId' => $orderId,
                'status' => 'open',
                'telegram_platform' => $telegram_platform ?? '',
                'platform_id' => $platform_id ?? '',
            ]);
        }
    }

    public function tbankNotifications()
    {
        return 200;
    }
}
