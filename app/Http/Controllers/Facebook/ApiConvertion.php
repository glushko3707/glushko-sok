<?php

namespace App\Http\Controllers\Facebook;

use App\Http\Controllers\Controller;
use App\Models\Facebook\FacebookSettingApi;
use App\Models\Facebook\FacebookStat;
use Facebook\Facebook;
use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\AdsInsightsFields;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\UserData;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\EventRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class ApiConvertion extends Controller
{
    public function setEventPixel($id,$data){

        $FacebookSettingApi = FacebookSettingApi::query()->where('project_id',$id)->get()->first();
        if(empty($FacebookSettingApi)){return null;}

        $access_token = $FacebookSettingApi->token;
        $pixel_id = $FacebookSettingApi->pixel_id;

//        $access_token = 'Ваш Facebook Access Token';
//        $pixel_id = 'Ваш Facebook Pixel ID';

        Api::init(null, null, $access_token);

        $user_data = (new UserData());
        if (!empty($data['_fbc'])){$user_data->setFbc($data['_fbc']);}
        if (!empty($data['_fbp'])){$user_data->setFbp($data['_fbp']);}
        if (!empty($data['phone'])){
            $data ['phone'] = str_replace([' ', '(', ')'], '', $data ['phone']);
            $user_data->setPhone($data['phone']);
        }

        $event = (new Event())
            ->setEventTime(time())
            ->setUserData($user_data);;

        if (!empty($data['phone'])){$event->setEventName($data['event']);}

        $request = (new EventRequest($pixel_id))
            ->setEvents([$event]);

        $response = $request->execute();
        Log::debug($response);
    }

    public function sendEvent($id,$dates)
    {
        $FacebookSettingApi = FacebookSettingApi::query()->where('project_id',$id)->get()->first();
        if(empty($FacebookSettingApi)){return null;}
        $access_token = $FacebookSettingApi->token;
        $pixel_id = $FacebookSettingApi->pixel_id;

        $data = [
            'data' => [
                [
                    'event_name' => $dates['event'] ?? '',
                    'event_time' => time(),
                    'user_data' => [
                        'phone' => hash('sha256', $dates ['phone']), // Хешированный номер телефона
                        'fbp' => $dates['_fbp'] ?? '',
                        'fbc' => $dates['_fbc'] ?? '',
                    ]
                ]
            ],
            'access_token' => $access_token,
        ];



        $url = "https://graph.facebook.com/v19.0/$pixel_id/events";

        $response = Http::post($url, $data);

        return $response->json();
    }


    public function getStatFacebook($id,$day,$ad_account_id){
        $FacebookSettingApi = FacebookSettingApi::query()->where('project_id',$id)->get()->first();
        $access_token = $FacebookSettingApi->token;
        $default_graph_version = $FacebookSettingApi->default_graph_version;
        $since = $day;
        $until = now()->format('Y-m-d');
        // URL для запроса статистики кабинета
        $url = "https://graph.facebook.com/$default_graph_version/$ad_account_id/insights";

        $response = Http::get($url, [
            'fields' => 'impressions,spend,account_name,account_id,campaign_id,campaign_name,adset_id,adset_name,ad_id, ad_name, inline_link_clicks,clicks',
            'level' => 'ad',
            'time_range' => json_encode([
                'since' => $since,
                'until' => $until,
            ]),
            'time_increment' => 1,
            'access_token' => $access_token,
            'limit' => 100

        ]);

        $all_data = [];

        while (true) {
            if ($response->successful()) {
                $data = $response->json();
                $all_data = array_merge($all_data, $data['data']);

                if (isset($data['paging']['next'])) {
                    $response = Http::get($data['paging']['next']);
                } else {
                    break;
                }
            } else {
                // Обработка ошибки
                Log::error('Facebook API Error:', $response->json());
                break;
            }
        }

        return $all_data;
    }

    public function setAdStat($id,$stat){
        $recordsToUpdate = [];
        $adIds = [];
        $dates = [];

        foreach ($stat as $statMap) {
            $recordsToUpdate[] = [
                'project_id' => (int)$id,
                'date' => $statMap['date_start'],
                'ad_id' => $statMap['ad_id'],
                'account_id' => $statMap['account_id'],
                'account_name' => $statMap['account_name'],
                'campaign_name' => $statMap['campaign_name'],
                'campaign_id' => $statMap['campaign_id'],
                'adset_id' => $statMap['adset_id'],
                'adset_name' => $statMap['adset_name'],
                'ad_name' => $statMap['ad_name'],
                'spend' => (float) $statMap['spend'] ?? 0,
                'impressions' => $statMap['impressions'] ?? 0,
                'clickAll' => $statMap['clicks'] ?? 0,
                'clickLink' => $statMap['inline_link_clicks'] ?? 0,
            ];
            $adIds[] = $statMap['ad_id'];
            $dates[] = $statMap['date_start'];
        }

        // Удаляем старые записи
        $FacebookStat = FacebookStat::where('project_id', $id)
            ->whereIn('ad_id', $adIds)
            ->whereIn('date', $dates)
            ->delete();
        // Вставляем новые записи
        FacebookStat::upsert($recordsToUpdate, ['project_id', 'ad_id', 'date'], [
            'account_id',
            'account_name',
            'campaign_name',
            'campaign_id',
            'adset_id',
            'adset_name',
            'ad_name',
            'spend',
            'impressions',
            'clickAll',
            'clickLink'
        ]);
    }

}
