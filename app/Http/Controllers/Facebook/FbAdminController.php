<?php

namespace App\Http\Controllers\Facebook;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\TrafficController;
use App\Models\Facebook\FacebookSettingApi;
use App\Models\Facebook\FacebookStat;
use Carbon\Carbon;
use Facebook\Facebook;
use FacebookAds\Api;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class FbAdminController extends Controller
{
    /**
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function setEventPixel($id, $data){

        $FacebookSettingApi = FacebookSettingApi::query()->where('project_id',$id)->get()->first();
        $access_token = $FacebookSettingApi->token;
        $pixel_id = $FacebookSettingApi->pixel_id;


        $fb = new Facebook([
            'app_id' => '{app-id}',
            'app_secret' => '{app-secret}',
            'default_graph_version' => 'v19.0',
            'default_access_token' => $access_token, // Access token системного пользователя
        ]);

        try {
            $response = $fb->post('/{business-id}/users', [
                'email' => 'user@example.com',
                'role' => 'ADMIN',
            ]);
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {
            // Обработка ошибок от Graph API
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
            // Обработка ошибок SDK
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $graphNode = $response->getGraphNode();

// Вывод ID добавленного пользователя
        echo 'User ID: ' . $graphNode['id'];

    }

    public function addAccountAdFacebookIndex($id)
    {
        return view('facebook.fbAddAccount',compact('id'));
    }

    public function deleteAccountAdFacebook($id)
    {
        return view('facebook.fbDeleteAccount',compact('id'));

    }

    public function addAccountAdFacebookStore(Request $request,$id)
    {
        $ad_account_id = $request->ad_account_id;
        $startDate = $request->startDate;
        $trafficController = new TrafficController();
        $trafficController->targetUpdateStat($id,$ad_account_id,$startDate);

        return redirect()->route('trafficAnalyticaTarget.index', ['id' => $id]);
    }

    public function deleteAccountAdFacebookStore(Request $request,$id)
    {
        $ad_account_id = $request->ad_account_id;
        $startDate = $request->start_date;
        FacebookStat::query()->where('project_id',$id)->where('account_id',$ad_account_id)->whereDate('created_at','>=',Carbon::parse($startDate))->delete();

        return redirect()->route('trafficAnalyticaTarget.index', ['id' => $id]);
    }


    public function addAdminToBm($project_id,$businessId,$access_token) {
        $adminUserEmail = 'glushko3707@mail.ru';
        $FacebookSettingApi = FacebookSettingApi::query()->where('project_id', $project_id)->get()->first();

        $response = Http::post("https://graph.facebook.com/v19.0/{$businessId}/business_users", [
            'access_token' => $access_token,
            'email' => $adminUserEmail,
            'role' => 'ADMIN'
        ]);

        if ($response->successful()) {
            $responseData = $response->json();
            echo "Администратор успешно добавлен. ID: " . $responseData['id'];
        } elseif ($response->failed()) {
            $errorData = $response->json();
            echo "Ошибка Facebook API: " . $errorData['error']['message'];
        }
    }

    public function getAdAccaunts($id,$bm_id)
    {
        $FacebookSettingApi = FacebookSettingApi::query()->where('project_id',$id)->get()->first();
        $access_token = $FacebookSettingApi->token;
        $default_graph_version = $FacebookSettingApi->default_graph_version;
        // URL для запроса статистики кабинета
        $url = "https://graph.facebook.com/$default_graph_version/$bm_id/client_ad_accounts";


        try {
            $response = Http::withToken($access_token)
                ->get($url);

            if ($response->successful()) {
                return $response->json()['data'];
            } else {
                return response()->json([
                    'error' => 'Error: ' . $response->status() . ' - ' . $response->body()
                ], $response->status());
            }
        } catch (\Exception $e) {
            return response()->json(['error' => 'Request failed: ' . $e->getMessage()], 500);
        }
    }

}
