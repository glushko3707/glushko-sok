<?php

namespace App\Http\Controllers\Facebook;

use App\Http\Controllers\Controller;
use App\Models\Facebook\FacebookSettingApi;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index($id)
    {
        $setting = FacebookSettingApi::query()->where('project_id', $id)->first();
        return view('traffic.trafficSetting', ['id' => $id,'setting' => $setting]);
    }

    public function storeSettingFacebook(Request $request, $id) {
        $request->validate([
            'pixel_id' => 'required',
        ],[
            'pixel_id' => 'Обязательно для заполнения',
        ]);

        $FacebookSettingApi =  FacebookSettingApi::query()->where('project_id', $id)->first();
        // если нет записи создаём и вносим pixel_id
        if (!$FacebookSettingApi) {
            $FacebookSettingApi = new FacebookSettingApi();
            $FacebookSettingApi->project_id = $id;
        }
        $FacebookSettingApi->pixel_id = $request->pixel_id;
        $FacebookSettingApi->update();

        return back();
    }
}
