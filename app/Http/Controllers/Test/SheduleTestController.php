<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Ozon\OzonController;
use App\Jobs\Facebook\AdsUpdate;
use App\Models\Facebook\FacebookStat;
use Illuminate\Http\Request;

class SheduleTestController extends Controller
{
    public function testScheduledCode()
    {
        (new OzonController)->updateStatisticaHw();
        $FacebookStat = FacebookStat::query()->where('project_id', 599)->pluck('account_id')->unique();
        foreach ($FacebookStat as $account_id) {
            AdsUpdate::dispatch(599, $account_id);
        }
    }
}
