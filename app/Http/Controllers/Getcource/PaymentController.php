<?php

namespace App\Http\Controllers\Getcource;

use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Throwable;

class PaymentController extends Controller
{

    public function NewPaymentGetcource(){
        return 200;
    }

    public function NewPaymentTilda(Request $request,$id){

        try{
        // Получаем данные из запроса
        $phone = $request->Phone ?? null;
        $paymentInfo = $request->payment ?? null;

// Декодируем JSON и проверяем наличие нужного поля
        if (!$paymentInfo) {
            return 200;
        }


        $decoded = json_decode($paymentInfo);
        $product = null;
        $sum = 0;

        if (isset($decoded->products[0]) && (str_contains($decoded->products[0], 'Аудит') || str_contains($decoded->products[0], 'АУДИТ'))) {
            $product = "Аудит";
            $sum = 1500;
        }

// Очищаем номер телефона от всех символов, кроме цифр
        $onlyDigitsPhone = preg_replace('/\D+/', '', $phone);

// Получаем текущую дату в нужном формате
        $date = now()->format('Y-m-d');

// Формируем массив данных
        $dataToInsert = [
            'phone' => $onlyDigitsPhone,
            'project_id' => $id,
            'orderId' => 'na',
            'status' => 'succeeded',
            'sum' => $sum,
            'date' => $date,
            'product' => $product,
            'created_at' => now(),
            'updated_at' => now(),
        ];

        Payment::query()->insert($dataToInsert);

        }catch (Throwable $e){

        }
        return 200;
    }


}
