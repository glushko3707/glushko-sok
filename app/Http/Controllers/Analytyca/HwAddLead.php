<?php

namespace App\Http\Controllers\Analytyca;

use App\Http\Controllers\Controller;
use App\Models\Analytica\AnalyticaNewLead;
use App\Models\Analytica\AnalyticaSuccessDeal;
use App\Models\Analytica\AnalyticaTarget_1;
use App\Models\Analytica\AnalyticaTarget_2;
use App\Models\Analytica\AnalyticaUnfulfilled;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Throwable;

class HwAddLead extends Controller
{
    public function analyticsNewLead(Request $request)
    {
        $id = 599;

        try {
            $data = [];
            $data ['lead_id'] = $request->id ?? null;
            $data ['name'] = $request->name ?? null;
            $data ['date_creation'] = $request->date_creation ?? null;
            if ($data ['date_creation']) {
                $data ['date_creation'] = Carbon::parse($data ['date_creation'])  ;
            }
            $data ['utm_source'] = $request->utm_source ?? null;
            $data ['utm_medium'] = $request->utm_medium ?? null;
            $data ['utm_campaign'] = $request->utm_campaign ?? null;
            $data ['utm_content'] = $request->utm_content ?? null;
            $data ['utm_term'] = $request->utm_term ?? null;
            $data ['reason_refusal'] = $request->reason_refusal ?? null;
            $data ['date_first_lesson'] = $request->date_first_lesson ?? null;
            if ($request->date_first_lesson) {
                $data ['date_first_lesson'] = Carbon::parse($data ['date_first_lesson'])  ;
            }
            $data ['language'] = $request->language ?? null;
            $data ['responsible'] = $request->responsible ?? null;
            $data ['address'] = $request->address ?? null;
            $data ['amount'] = $request->amount ?? null;
            $data ['More_information'] = $request->More_information ?? null;
            $data ['passed_vu'] = $request->passed_vu ?? null;
            $data ['Directions'] = $request->Directions ?? null;
            $data ['country'] = $request->Country ?? null;

            AnalyticaNewLead::query()
                ->updateOrCreate(['project_id' => $id, 'lead_id' => $data ['lead_id']], $data);
        } catch (Throwable $e) {
            report($e);
            return 201;
        }

        return 200;
    }
    public function target_1()
    {
        $id = 599;

        try {
            $data = [];
            $data ['lead_id'] = $request->id ?? null;
            $data ['name'] = $request->name ?? null;
            $data ['date_creation'] = $request->date_creation ?? null;
            if ($data ['date_creation']) {
                $data ['date_creation'] = Carbon::parse($data ['date_creation'])  ;
            }
            $data ['utm_source'] = $request->utm_source ?? null;
            $data ['utm_medium'] = $request->utm_medium ?? null;
            $data ['utm_campaign'] = $request->utm_campaign ?? null;
            $data ['utm_content'] = $request->utm_content ?? null;
            $data ['utm_term'] = $request->utm_term ?? null;
            $data ['reason_refusal'] = $request->reason_refusal ?? null;
            $data ['date_first_lesson'] = $request->date_first_lesson ?? null;
            if ($request->date_first_lesson) {
                $data ['date_first_lesson'] = Carbon::parse($data ['date_first_lesson'])  ;
            }
            $data ['language'] = $request->language ?? null;
            $data ['responsible'] = $request->responsible ?? null;
            $data ['address'] = $request->address ?? null;
            $data ['amount'] = $request->amount ?? null;
            $data ['More_information'] = $request->More_information ?? null;
            $data ['passed_vu'] = $request->passed_vu ?? null;
            $data ['Directions'] = $request->Directions ?? null;
            $data ['country'] = $request->Country ?? null;

            AnalyticaTarget_1::query()
                ->updateOrCreate(['project_id' => $id, 'lead_id' => $data ['lead_id']], $data);
        } catch (Throwable $e) {
            report($e);
            return 201;
        }

        return 200;
    }
    public function target_2()
    {
        $id = 599;

        try {
            $data = [];
            $data ['lead_id'] = $request->id ?? null;
            $data ['name'] = $request->name ?? null;
            $data ['date_creation'] = $request->date_creation ?? null;
            if ($data ['date_creation']) {
                $data ['date_creation'] = Carbon::parse($data ['date_creation'])  ;
            }
            $data ['utm_source'] = $request->utm_source ?? null;
            $data ['utm_medium'] = $request->utm_medium ?? null;
            $data ['utm_campaign'] = $request->utm_campaign ?? null;
            $data ['utm_content'] = $request->utm_content ?? null;
            $data ['utm_term'] = $request->utm_term ?? null;
            $data ['reason_refusal'] = $request->reason_refusal ?? null;
            $data ['date_first_lesson'] = $request->date_first_lesson ?? null;
            if ($request->date_first_lesson) {
                $data ['date_first_lesson'] = Carbon::parse($data ['date_first_lesson'])  ;
            }
            $data ['language'] = $request->language ?? null;
            $data ['responsible'] = $request->responsible ?? null;
            $data ['address'] = $request->address ?? null;
            $data ['amount'] = $request->amount ?? null;
            $data ['More_information'] = $request->More_information ?? null;
            $data ['passed_vu'] = $request->passed_vu ?? null;
            $data ['Directions'] = $request->Directions ?? null;
            $data ['country'] = $request->Country ?? null;

            AnalyticaTarget_2::query()
                ->updateOrCreate(['project_id' => $id, 'lead_id' => $data ['lead_id']], $data);
        } catch (Throwable $e) {
            report($e);
            return 201;
        }
        return 200;
    }
    public function successDeal()
    {
        $id = 599;

        try {
            $data = [];
            $data ['lead_id'] = $request->id ?? null;
            $data ['name'] = $request->name ?? null;
            $data ['date_creation'] = $request->date_creation ?? null;
            if ($data ['date_creation']) {
                $data ['date_creation'] = Carbon::parse($data ['date_creation'])  ;
            }
            $data ['utm_source'] = $request->utm_source ?? null;
            $data ['utm_medium'] = $request->utm_medium ?? null;
            $data ['utm_campaign'] = $request->utm_campaign ?? null;
            $data ['utm_content'] = $request->utm_content ?? null;
            $data ['utm_term'] = $request->utm_term ?? null;
            $data ['reason_refusal'] = $request->reason_refusal ?? null;
            $data ['date_first_lesson'] = $request->date_first_lesson ?? null;
            if ($request->date_first_lesson) {
                $data ['date_first_lesson'] = Carbon::parse($data ['date_first_lesson'])  ;
            }
            $data ['language'] = $request->language ?? null;
            $data ['responsible'] = $request->responsible ?? null;
            $data ['address'] = $request->address ?? null;
            $data ['amount'] = $request->amount ?? null;
            $data ['More_information'] = $request->More_information ?? null;
            $data ['passed_vu'] = $request->passed_vu ?? null;
            $data ['Directions'] = $request->Directions ?? null;
            $data ['country'] = $request->Country ?? null;

            AnalyticaSuccessDeal::query()
                ->updateOrCreate(['project_id' => $id, 'lead_id' => $data ['lead_id']], $data);
        } catch (Throwable $e) {
            report($e);
            return 201;
        }

        return 200;
    }
    public function unfulfilled(Request $request)
    {
        $id = 599;

        try {
            $data = [];
            $data ['lead_id'] = $request->id ?? null;
            $data ['name'] = $request->name ?? null;
            $data ['date_creation'] = $request->date_creation ?? null;
            if ($data ['date_creation']) {
                $data ['date_creation'] = Carbon::parse($data ['date_creation'])  ;
            }
            $data ['utm_source'] = $request->utm_source ?? null;
            $data ['utm_medium'] = $request->utm_medium ?? null;
            $data ['utm_campaign'] = $request->utm_campaign ?? null;
            $data ['utm_content'] = $request->utm_content ?? null;
            $data ['utm_term'] = $request->utm_term ?? null;
            $data ['reason_refusal'] = $request->reason_refusal ?? null;
            $data ['date_first_lesson'] = $request->date_first_lesson ?? null;
            if ($request->date_first_lesson) {
                $data ['date_first_lesson'] = Carbon::parse($data ['date_first_lesson'])  ;
            }
            $data ['language'] = $request->language ?? null;
            $data ['responsible'] = $request->responsible ?? null;
            $data ['address'] = $request->address ?? null;
            $data ['amount'] = $request->amount ?? null;
            $data ['More_information'] = $request->More_information ?? null;
            $data ['passed_vu'] = $request->passed_vu ?? null;
            $data ['Directions'] = $request->Directions ?? null;
            $data ['country'] = $request->Country ?? null;
            AnalyticaUnfulfilled::query()
                ->updateOrCreate(['project_id' => $id, 'lead_id' => $data ['lead_id']], $data);

        } catch (Throwable $e) {
            report($e);
            return 201;
        }

        return 200;
    }
}
