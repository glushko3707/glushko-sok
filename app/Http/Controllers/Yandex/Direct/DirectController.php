<?php

namespace App\Http\Controllers\Yandex\Direct;

use App\Http\Controllers\Controller;
use App\Models\Yandex\YandexDirectStatistica;
use App\Models\Yandex\YandexSetting;
use Biplane\YandexDirect\Api\V5\Reports\DateRangeTypeEnum;
use Biplane\YandexDirect\Api\V5\Reports\FieldEnum;
use Biplane\YandexDirect\Api\V5\Reports\ReportDefinition;
use Biplane\YandexDirect\Api\V5\Reports\ReportRequestBuilder;
use Biplane\YandexDirect\Api\V5\Reports\ReportTypeEnum;
use Biplane\YandexDirect\Api\V5\Reports\SelectionCriteria;
use Biplane\YandexDirect\ConfigBuilder;
use Biplane\YandexDirect\ReportServiceFactory;
use Carbon\Carbon;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

use League\Csv\Reader;

class DirectController extends Controller
{

    private $token;
    private $login;



    public function getReportCSVIndex($id){
        $getAccounts = $this->getAccount($id);
        return view('yandex.insideCSV',compact('id','getAccounts'));
    }

    public function getReportCSVStore(Request $request,$id){

        $AccauntName = $request->input('accaunt_name');

        $collect = new \Illuminate\Support\Collection();
        if ($request->hasFile('report')) {
            // Получаем файл из запроса
            $file = $request->file('report');
                // Создаем объект Reader из файла
                $csv = Reader::createFromPath($file->getPathname());
                // Устанавливаем разделитель столбцов
                $csv->setDelimiter(';');
                // Получаем заголовки из первой строки
            // Пропускаем первые две строки
            $csv->setHeaderOffset(4);

            // Получаем заголовки из третьей строки (после пропуска первых двух)
            $headers = $csv->getHeader();

            $newHeaders = ['Date', 'CampaignName', 'CampaignId', 'AdGroupName', 'AdGroupId', 'AdId', 'Impressions', 'Clicks', 'Cost']; // Укажите свои заголовки
            // Получаем записи, начиная с третьей строки
            // Получаем все строки из CSV
            $allRecords = collect($csv->getRecords());

//            $secondRow = $allRecords->skip(0)->first();
//            preg_match('/\((.*?)\)/', $secondRow['Дата'], $matches);
//            $AccauntName = $matches[1] ?? null;

            // Пропускаем первые две строки
            $records = $allRecords->skip(3);

            $recordsWithNewHeaders = $records->map(function ($record) use ($newHeaders) {
                // Проверяем, совпадает ли количество элементов
                if (count($newHeaders) !== count($record)) {
                    throw new \Exception('Количество элементов в заголовках и записях не совпадает.');
                }
                return array_combine($newHeaders, array_values($record));
            });

            $recordsWithNewHeaders = $recordsWithNewHeaders->map(function ($record) use ($id,$AccauntName) {
                $record['AdId'] = preg_replace('/[^0-9]/', '', $record['AdId']);
                $record['project_id'] = $id;
                $record['Cost'] = str_replace(',', '.', $record['Cost']);
                $record['AccountName'] = $AccauntName;
                $record['Date'] = Carbon::parse($record['Date'])->format('Y-m-d');

                return $record;
            });


            $adIds = $recordsWithNewHeaders->pluck('AdId')->unique();
            $dates = $recordsWithNewHeaders->pluck('Date')->unique();
            $recordsWithNewHeaders = $recordsWithNewHeaders->toArray();

            YandexDirectStatistica::where('project_id', $id)
                ->whereIn('AdId', $adIds)
                ->whereIn('date', $dates)
                ->delete();

            // Вставляем новые записи
            YandexDirectStatistica::query()->upsert($recordsWithNewHeaders, ['project_id','CampaignId', 'AdId', 'Date'], [
                'CampaignName',
                'CampaignId',
                'AdGroupName',
                'AdGroupId',
                'Impressions',
                'Clicks',
                'Cost',
            ]);


        }
        return redirect()->route('getReportCSV.index',compact('id'));
    }

    public function updateYandexDirect($id)
    {
        $accounts = YandexDirectStatistica::query()
            ->select('AccountName', DB::raw('MAX(date) as last_date'))
            ->where('project_id', $id)
            ->groupBy('AccountName')
            ->get();

        foreach ($accounts as $account) {
            if ($id == 114 && $account->AccountName == 'sok-agency'){
                continue;
            }
           $report = $this->getReport($account->AccountName, $account->last_date, today()->format('Y-m-d'),$id);
           $this->setDatetoDB($report, $id, $account->AccountName);
        }
    }

    public function setDatetoDB($report, $id, $AccountName,){
        $combinedStat = collect($report)->filter(function ($statMap) {
            return $statMap['AdId'] == '--';
        })
//            ->reduce(function ($carry, $item) {
//            if (!$carry) {
//                return $item;
//            }
//
//            $carry['Impressions'] += $item['Impressions'];
//            $carry['Clicks'] += $item['Clicks'];
//            $carry['Cost'] += $item['Cost'];
//
//            return $carry;
//        }, null)
        ;


// Поля, которые нужно суммировать
        $fieldsToSum = ['Impressions', 'Clicks', 'Cost'];

// Массив для хранения агрегированных данных
        $aggregatedData = [];
//dd($combinedStat->where('CampaignId',"114425338"));
        foreach ($combinedStat as $entry) {
            // Создаем уникальный ключ на основе Date и CampaignId
            $key = $entry['Date'] . '_' . $entry['CampaignId'];

            if (!isset($aggregatedData[$key])) {
                // Инициализируем запись, если ее еще нет в агрегированном массиве
                $aggregatedData[$key] = [
                    'Date' => $entry['Date'],
                    'CampaignName' => $entry['CampaignName'],
                    'CampaignId' => $entry['CampaignId'],
                    'AdGroupName' => $entry['AdGroupName'],
                    'AdGroupId' => $entry['AdGroupId'],
                    'AdId' => $entry['AdId'],
                    'Impressions' => 0,
                    'Clicks' => 0,
                    'Cost' => 0.00,
                ];
            }

            // Суммируем нужные поля
            foreach ($fieldsToSum as $field) {
                if ($field === 'Cost') {
                    $aggregatedData[$key][$field] += (float)$entry[$field];
                } else {
                    $aggregatedData[$key][$field] += (int)$entry[$field];
                }
            }
        }

        $recordsToUpdate = collect($report)->filter(function ($statMap) {
            return $statMap['AdId'] != '--';
        })->map(function ($statMap) use ($AccountName, $id) {
            return [
                'Date' => $statMap['Date'],
                'project_id' => $id,
                'AccountName' => $AccountName,
                'CampaignName' => $statMap['CampaignName'],
                'CampaignId' => $statMap['CampaignId'],
                'AdGroupName' => $statMap['AdGroupName'],
                'AdGroupId' => $statMap['AdGroupId'],
                'AdId' => $statMap['AdId'],
                'Impressions' => $statMap['Impressions'],
                'Clicks' => $statMap['Clicks'],
                'Cost' => $statMap['Cost'],
            ];
        });

        if ($aggregatedData) {
            foreach ($aggregatedData as $aggregatedsData) {
                $recordsToUpdate->push([
                    'Date' => $aggregatedsData['Date'],
                    'project_id' => $id,
                    'AccountName' => $AccountName,
                    'CampaignName' => $aggregatedsData['CampaignName'],
                    'CampaignId' => $aggregatedsData['CampaignId'],
                    'AdGroupName' => $aggregatedsData['AdGroupName'],
                    'AdGroupId' => $aggregatedsData['AdGroupId'],
                    'AdId' => $aggregatedsData['AdId'],
                    'Impressions' => $aggregatedsData['Impressions'],
                    'Clicks' => $aggregatedsData['Clicks'],
                    'Cost' => $aggregatedsData['Cost'],
                ]);;
            }
        }

        $adIds = $recordsToUpdate->pluck('AdId')->unique();
        $dates = $recordsToUpdate->pluck('Date')->unique();
        $recordsToUpdate = $recordsToUpdate->toArray();

        YandexDirectStatistica::where('project_id', $id)
            ->whereIn('AdId', $adIds)
            ->whereIn('date', $dates)
            ->delete();

        // Вставляем новые записи
        YandexDirectStatistica::query()->upsert($recordsToUpdate, ['project_id','AdId', 'Date'], [
            'CampaignName',
            'CampaignId',
            'AdGroupName',
            'AdGroupId',
            'Impressions',
            'Clicks',
            'Cost',
        ]);
    }
    public function getAccount($id)
    {
        return YandexDirectStatistica::query()
            ->where('project_id', $id)
            ->distinct()
            ->pluck('AccountName');
    }

    public function getReport($login,$startDate,$endDate,$id){
//        $this->token  = 'y0_AgAAAABwW6Q-AAsBIAAAAAEIo382AABzfBQ7Ae9M-LMp1leNtFpB7mugSw';
        $this->login = $login;
        $this->token = YandexSetting::query()->where('project_id', $id)->first()->access_token;
//        $campaigns = $this->getCampaignIds($this->token,$this->login);
//        dd($campaigns);
        $serviceFactory = new ReportServiceFactory();

        $config = ConfigBuilder::create()
            ->setAccessToken($this->token)
            ->setClientLogin($this->login)
            ->setLocale('ru')
            ->getConfig();

        $service = $serviceFactory->createService($config);
        $SelectionCriteria = SelectionCriteria::create()
        ->setDateFrom($startDate)
        ->setDateTo($endDate);
        $reportDefinition = ReportDefinition::create()
            ->setSelectionCriteria($SelectionCriteria)
            ->setReportName(now()->timestamp)
            ->setReportType(ReportTypeEnum::AD_PERFORMANCE_REPORT)
            ->setDateRangeType(DateRangeTypeEnum::CUSTOM_DATE)
            ->setFieldNames([
                FieldEnum::DATE,
                FieldEnum::CAMPAIGN_NAME,
                FieldEnum::CAMPAIGN_ID,
                FieldEnum::AD_GROUP_NAME,
                FieldEnum::AD_GROUP_ID,
                FieldEnum::AD_ID,
                FieldEnum::IMPRESSIONS,
                FieldEnum::CLICKS,
                FieldEnum::COST,
            ])
            ->setIncludeVAT(true);

        $request = ReportRequestBuilder::create()
            ->setReportDefinition($reportDefinition)
            ->returnMoneyInMicros(false)
            ->skipReportHeader(true)
            ->getReportRequest();

        $result = $service->getReady($request);
        $data = $result->getAsString();
        $lines = explode("\n", $data);

// Первая строка - заголовок
        $header = array_shift($lines);
        $headerColumns = explode("\t", $header);

        $result = [];

        foreach ($lines as $line) {
            if (trim($line) === '' || stripos($line, 'Total rows:') !== false || stripos($line, 'НАЗВАНИЕ_ОТЧЕТА') !== false) {
                continue; // Пропускаем пустые строки и заголовок отчета
            }

            $columns = explode("\t", $line);
            if (count($columns) === count($headerColumns)) {
                $result[] = array_combine($headerColumns, $columns);
            }
        }
        return $result;

    }
    public function getReportAds() {

//        $startDate = date('Y-m-d', strtotime('-7 days'));
//        $endDate = date('Y-m-d');
        $this->token = $access_token['access_token'] = 'y0_AgAAAABwW6Q-AAsBIAAAAAEIo382AABzfBQ7Ae9M-LMp1leNtFpB7mugSw';
        $this->login = 'glushko-sok';

        $body = [
            'params' => [
                    "SelectionCriteria" => (object) [],
                    "FieldNames" => ["Date", "CampaignName", "AdId", "Impressions", "Clicks", "Cost"],
                    "ReportName" => "НАЗВАНИЕ_ОТЧЕТА",
                    "ReportType" => "AD_PERFORMANCE_REPORT",
                    "DateRangeType" => "LAST_MONTH",
                    "Format" => "TSV",
                    "IncludeVAT" => "NO",
                    "IncludeDiscount" => "NO"
            ],

        ];

        $response = Http::withHeaders([
            'Authorization' => "Bearer {$this->token}",
            'Accept-Language' => 'ru',
            "processingMode: auto",
            'Client-Login' => $this->login, // Ваш логин в Яндекс.Директ
        ])
            ->post('https://api-sandbox.direct.yandex.ru/json/v5/reports',$body);

        $data = $response->body();

        $lines = explode("\n", $data);

        // Первая строка - заголовок
        $header = array_shift($lines);
        $headerColumns = explode("\t", array_shift($lines));

        $result = [];

        foreach ($lines as $line) {
            if (trim($line) === '' || stripos($line, 'Total rows:') !== false || stripos($line, 'НАЗВАНИЕ_ОТЧЕТА') !== false) {
                continue; // Пропускаем пустые строки и заголовок отчета
            }

            $columns = explode("\t", $line);
            if (count($columns) === count($headerColumns)) {
                $result[] = array_combine($headerColumns, $columns);
            }
        }

            return $result;
    }
    public function getCampaignIds($token,$login)
    {

        $body = [
            'method' => 'get',
            'params' => [
                'SelectionCriteria' => (object) array(),
                'FieldNames' => ['Id', 'Name'],
                "TextCampaignFieldNames" => ["CounterIds", "RelevantKeywords", "Settings","BiddingStrategy"],
            ]
        ];

        try {

            $response = Http::withHeaders([
                'Authorization' => "Bearer {$this->token}",
                'Accept-Language' => 'ru',
                'Content-Type' => 'application/json',  // Тип данных и кодировка запроса
                'Client-Login' => $login, // Ваш логин в Яндекс.Директ
            ])
                ->post('https://api.direct.yandex.com/json/v5/campaigns',$body);
            $data = $response->json();
            return $data['result']['Campaigns'] ?? null;
        } catch (RequestException $e) {
            echo 'Ошибка при запросе: ' . $e->getMessage() . "\n";
            if ($e->hasResponse()) {
                $response = $e->getResponse();
                $responseBody = $response->getBody()->getContents();
                echo 'Ответ сервера: ' . $responseBody . "\n";
            }
            return null;
        }
    }


    public function getAdCosts($campaignId, $startDate, $endDate)
    {
        $body = [
            "params" => [
                "SelectionCriteria" => [
                    "DateFrom" => $startDate,
                    "DateTo" => $endDate,
                    "Filter" => [
                        [
                            "Field" => "CampaignId",
                            "Operator" => "EQUALS",
                            "Values" => [$campaignId]
                        ]
                    ]
                ],
                "FieldNames" => ["Date", "Cost"],
                "ReportName" => "AdCostsReport",
                "ReportType" => "CAMPAIGN_PERFORMANCE_REPORT",
                "DateRangeType" => "CUSTOM_DATE",
                "Format" => "TSV",
                "IncludeVAT" => "NO",
                "IncludeDiscount" => "NO"
            ]
        ];

        $response = $this->client->post('reports', [
            'json' => $body,
        ]);

        return $response->getBody()->getContents();
    }


    public function oauth($id)
    {
        $client_id = 'dd33603f67204d9786391a4025624afb';
        $redirect_uri = 'https://glushko-sok.ru/api/yandex/oauth/redirect/114';

        return view('yandex.yandexOAuth',compact('client_id','redirect_uri','id'));
    }

    public function oauthRedirect(Request $request, $id)
    {
        return view('yandex.redirectYandex',compact('id'));
    }

    public function oauthRedirectHandle(Request $request, $id)
    {

        $access_token['access_token'] = $request->access_token ?? null;
        $access_token['expires_in'] = $request->expires_in ?? null;

        YandexSetting::query()->where('project_id',$id)->updateOrCreate([
            'project_id' => $id,
        ],[
            'access_token' => $access_token['access_token'],
            'expires_in' => $access_token['expires_in'],
        ]);

        return redirect()->route('trafficAnalyticaYandex.index',$id);
    }

    public function segment(Request $request, $id)
    {
        $url = "https://api-audience.yandex.ru/v1/management/segments";

        $access_token['access_token'] = 'y0_AgAAAABwW6Q-AAsBIAAAAAD1H8vSbsmhbX70ST-GZpEgOHG7TtlZpqM';

        $response = Http::withHeaders([
            "Host" => "api-audience.yandex.ru",
            "Authorization" => "OAuth " . $access_token['access_token'],
            "content-type" => "application/json",
        ])->get($url);

    }

    public function segmentAdd(Request $request, $id)
    {

        // Получаем данные из базы данных или массива
        $data = [
            ["phone" => "79259126938",],
        ];

        // Создаем временный файл
        $tempFile = tempnam(sys_get_temp_dir(), 'csv');

        // Открываем файл для записи
        $handle = fopen($tempFile, 'w');

        // Добавляем заголовки для CSV-файла
        fputcsv($handle, ["phone",]);

        // Добавляем данные для CSV-файла
        foreach ($data as $row) {
            fputcsv($handle, $row);
        }

        // Закрываем файл
        fclose($handle);

        // Отправляем запрос с файлом
        $response = Http::asMultipart()->post('http://api-audience.yandex.ru/v1/management/segments/upload_csv_file', [
            'file' => fopen($tempFile, 'r'),
        ]);

        // Удаляем временный файл
        unlink($tempFile);

        // Возвращаем ответ
        return $response->body();

    }


}
