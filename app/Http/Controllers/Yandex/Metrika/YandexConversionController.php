<?php

namespace App\Http\Controllers\Yandex\Metrika;

use App\Http\Controllers\Controller;
use App\Models\Notification\TildaLead;
use App\Models\Yandex\YandexSetting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Throwable;

class YandexConversionController extends Controller
{
    public function addConversionToGoal($id, $phone, $goals)
    {

        // Поиск последнего лида с указанными условиями
        $lead = TildaLead::query()
            ->where('project_id', $id)
            ->where('phone', $phone)
            ->where(function ($query) {
                $query->whereNotNull('_yclid')
                    ->orWhereNotNull('_ym_uid');
            })
            ->latest()
            ->first();

        // Проверка, найден ли лид
        if (!$lead) {
            return response()->json(['message' => 'Lead not found'], 404);
        }

        // Получение настроек Яндекс.Метрики для проекта
        $YandexSetting = YandexSetting::query()->where('project_id', $id)->first();
        if (!$YandexSetting) {
            return response()->json(['message' => 'Yandex settings not found'], 404);
        }

        $token = $YandexSetting->access_token;
        $domain = $this->getDomain($lead->url);
        // Список идентификаторов счетчиков для доменов
        $counterIds = [
            'sok-agency.com' => ['id' => 94753820, 'goals' => [
                'visitWeb' => 337229767,
                'paymentIntensive' => 337229786,
            ]],
            'kupr.pro' => ['id' => 97561863, 'goals' => [
                'visitWeb' => 337217745,
                'paymentIntensive' => 337217747,
            ]],
        ];

        // Проверка наличия домена в массиве идентификаторов счетчиков
        if (!array_key_exists($domain, $counterIds)) {
            return response()->json(['message' => 'goals not found in counter IDs'], 400);
        }

        // Проверка наличия 'id' для заданного домена
        if (!isset($counterIds[$domain]['id'])) {
            return null;
        }

        $counterId = $counterIds[$domain]['id'];

// Предполагается, что $goals уже определен и содержит необходимое значение
        $goal = $goals;

// Инициализация массива $conversions как двумерного массива с заголовками
        $conversions = [['Target', 'DateTime']];

// Формирование строки данных
        $row = [
            (string) $goal,
            (string) now()->timestamp,
        ];

// Добавление 'yclid' в заголовки и строку, если он существует
        if (!empty($lead->_yclid)) {
            // Добавляем 'yclid' в начало заголовков
            array_unshift($conversions[0], 'yclid');
            // Добавляем значение 'yclid' в начало строки данных
            array_unshift($row, (string) $lead->_yclid);
        }

// Добавление 'ClientID' в заголовки и строку, если он существует
        if (!empty($lead->_ym_uid)) {
            // Добавляем 'ClientID' в конец заголовков
            $conversions[0][] = 'ClientID';
            // Добавляем значение 'ClientID' в конец строки данных
            $row[] = (string) $lead->_ym_uid;
        }

// Добавляем строку данных в массив $conversions
        $conversions[] = $row;

// Создание CSV контента
        $csvContent = implode("\n", array_map(function($conversion) {
            return implode(',', $conversion);
        }, $conversions));

//// Например, запись в файл
//        $csvFile = 'conversions.csv';
//        $fileExists = file_exists($csvFile);
//
//        try {
//            $handle = fopen($csvFile, 'a');
//            if (!$handle) {
//                throw new Exception("Не удалось открыть файл для записи: $csvFile");
//            }
//
//            // Если файл только что создан, записываем заголовки
//            if (!$fileExists) {
//                if (fputcsv($handle, $conversions[0]) === false) {
//                    throw new Exception("Не удалось записать заголовки в CSV файл.");
//                }
//            }
//
//            // Записываем строку данных
//            if (fputcsv($handle, $row) === false) {
//                throw new Exception("Не удалось записать строку данных в CSV файл.");
//            }
//
//            fclose($handle);
//            // Логирование успешной записи
//            error_log("Данные успешно записаны в $csvFile: " . implode(',', $row));
//        } catch (Exception $e) {
//            // Логирование ошибки
//            error_log("Ошибка при записи данных: " . $e->getMessage());
//            // Дополнительные действия, например, уведомление администратора
//        }
        // Загрузка конверсий в Яндекс. Метрику
        try {
            $result = $this->uploadOfflineConversionsToMetrika($token, $counterId, $csvContent);
            return response()->json($result);
        } catch (Throwable $caught) {
            report($caught);
            return response()->json(['message' => 'Error uploading conversions'], 500);
        }
    }

    /**
     * Получает цели для указанного счетчика Яндекс.Метрики.
     *
     * Этот метод отправляет GET-запрос к API Яндекс. Метрики для получения списка целей,
     * установленных для указанного счетчика. Метод использует токен аутентификации для
     * авторизации запроса.
     *
     * @param string $token     Токен аутентификации API Яндекс.Метрики.
     * @param int $counterId Идентификатор счетчика Яндекс.Метрики.
     * @return array            Массив данных с целями, установленными для указанного счетчика.
     */

    function getGoals(string $token, int $counterId): array
    {
        $url = "https://api-metrika.yandex.net/management/v1/counter/{$counterId}/goals";
        $response = Http::withToken($token)->get($url);
        return $response->json();
    }

    /**
     * Загружает офлайн-конверсии в Яндекс.Метрику.
     *
     * Этот метод отправляет POST-запрос к API Яндекс.Метрики для загрузки офлайн-конверсий
     * в указанный счетчик. Метод использует токен аутентификации для авторизации запроса
     * и отправляет файл с данными конверсий в формате CSV.
     *
     * @param string $token     Токен аутентификации API Яндекс.Метрики.
     * @param int $counterId Идентификатор счетчика Яндекс.Метрики.
     * @param string $csvContent Содержимое файла CSV с данными конверсий.
     * @return array            Массив данных с результатами загрузки конверсий.
     */

    function uploadOfflineConversionsToMetrika(string $token, int $counterId, string $csvContent)
    {
        $url = "https://api-metrika.yandex.net/management/v1/counter/{$counterId}/offline_conversions/upload";
        $response = Http::withToken($token)
            ->attach('file', $csvContent, 'conversions.csv')
            ->post($url);
        return $response->json();
    }

    /**
     * Извлекает домен из указанного URL.
     *
     * Этот метод проверяет, содержит ли URL протокол (http или https), и если нет,
     * добавляет протокол http. Затем метод парсит URL и извлекает домен. Если домен
     * не может быть извлечен, используется значение по умолчанию 'sok-agency.com'.
     *
     * @param string $url URL для извлечения домена.
     * @return string     Домен, извлеченный из URL, или значение по умолчанию 'sok-agency.com'.
     */

    function getDomain($url): string {

        // Проверка, содержит ли URL протокол
        if (!preg_match('/^https?:\/\//', $url)) {
            $url = 'http://' . $url;
        }

        // Парсинг URL
        $parsedUrl = parse_url($url);

        // Извлечение домена
        return $parsedUrl['host'] ?? 'sok-agency.com';
    }

}
