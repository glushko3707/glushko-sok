<?php

namespace App\Http\Controllers\Sipout;

use App\Models\Offer;
use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\SipoutCallId;
use App\Models\Sipout\SipOutSetting;

use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use League\Csv\Reader;
use Throwable;

class SipoutStatisticController extends BaseController
{
    public function getCallStatisticIndex(Request $request,$id): ?array{
        // Получаем идентификатор вызова из запроса
        $callI= SipoutCallId::query()->find($request->call);
        $callId = $callI->callId;

        // Получаем данные о вызове из сессии, если они есть
        $days = 0;
        $dayFrom = now()->subDays($days)->format('d.m.Y');
        $dayTo = now()->format('d.m.Y');
        $project = Project::query()->find($id);
        $sipProject = $project->getSipProject;
        $api_key = $sipProject->api_key ?? null;

        $call = Session::get('call');

        if ($call == null) {

        if (!empty($sipProject->api_key)){
            return redirect()->route('sipoutSetting.index',$id)->withErrors('SipOut Api Key не заполнен');
        }
        $url = 'https://lk.sipout.net/userapi/?key='.$api_key.'&method=call_stat&action=get_list&ds='.$dayFrom.'&de=' . $dayTo;

        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->get($url);

        if (!isset($response->json()['data']['list'])){
            return 'ошибка возврата статистики звонков';
        }

        $call = collect($response->json()['data']['list']);
        Session::put('call',$call);
        }
// Фильтруем вызовы по идентификатору
        $value = collect($call)->where('called', $callId);

// Получаем разницу во времени из настроек проекта
        $timeDifferent = SipOutSetting::query()->where('project_id', $id)->first()->timeDifferent;

// Вычисляем дедлайн по дате последнего вызова
        $timeDedline = Carbon::parse($value->last()['date'])->subMinutes($timeDifferent);

// Отбираем только те вызовы, которые были сделаны после дедлайна
        $callCalledses = $value->filter(function ($item) use ($timeDedline) {
            return Carbon::parse($item['date'])->gt($timeDedline);
        });


     // Группируем вызовы по длительности и сортируем по убыванию
        $check = $callCalledses->groupBy('duration')->sortKeysDesc();

        $max = $callCalledses->max('duration');
       // Подсчитываем количество вызовов для каждой длительности


        for ($i = 1; $i <= $max; $i++) {
            if (isset($check[$i])){
                $maksim['valu'][$i] = count($check[$i]);
                $maksim['sec'][$i] = $i;
            } else {
                $maksim['sec'] [$i]= $i;
                $maksim['valu'][$i] = 0;
            }
        }
        return $maksim;
    }
    public function callIdDelete(Request $request){
        SipoutCallId::query()->find($request->step_id)->delete();
        return back();
    }
    public function sipoutStatisticIndex(Request $request,$id){

        $sipoutCallIdLast = SipoutCallId::query()->where('project_id','=',$id)->orderByDesc('created_at')->first();

        // ваша функция, которая использует кэш
        // проверка нужно ли обновление и обновление
        try {
            if (!empty($sipoutCallIdLast)){
                $needUpdate = $this->service->getNeedUpdate($sipoutCallIdLast);
            }
            else {
                $needUpdate = true;
            }
            if ($needUpdate){
                $this->sipoutStatisticUpdateStoreAuto($id);
            }
        } catch (Throwable $e) {
            report($e);
        }

        $project = Project::query()->find($id);

        $categoryDatabase = CategoryDatabase::query()
            ->where([
                ['project_id', '=', $id],
            ])
            ->get();


        return view('sipout.sipouttStatistic',compact(
            'id','project','categoryDatabase'
        ));

    }
    public function sipoutStatisticUpdateStoreAuto($id){
        $timeDifferent = 30;
        $days = 0;
        $dayFrom = now()->subDays($days)->format('d.m.Y');
        $dayTo = now()->format('d.m.Y');
        $project = Project::query()->find($id);

        $sipProject = $project->getSipProject ?? null;
        $api_key = $sipProject->api_key ?? null;
        if(empty($project->getSipProject)){
            return redirect(route('sipoutSetting.index',$id))->withErrors('Проект не найден');
        }
        $sipProject = $project->getSipProject ?? null;
        $api_key = $sipProject->api_key ?? null;

        if (!isset($sipProject->api_key)){
            return redirect(route('sipoutSetting.index',$id))->withErrors('SipOut Api Key не заполнен');
        }

        $priceSec = 0.016;
        $url = 'https://lk.sipout.net/userapi/?key='.$api_key.'&method=call_stat&action=get_list&ds='.$dayFrom.'&de=' . $dayTo;
        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->get($url);


        if (!isset($response->json()['data']['list'])){
            return 'ошибка возврата статистики звонков';
        }

        $call = collect($response->json()['data']['list']);


        if (count($call) > 0){
            $this->service->updateSipoutCall($project,$priceSec,$call,$timeDifferent);
        }

        return true;
    }
    public function getSipoutPhone($id){

        $dayFrom = Carbon::parse('24-02-29')->format('d.m.Y');
        $dayTo = Carbon::parse('24-02-29')->format('d.m.Y');
        $project = Project::query()->find($id);
        $sipProject = $project->getSipProject;
        $api_key = $sipProject->api_key;

        $url = 'https://lk.sipout.net/userapi/?key='.$api_key.'&method=call_stat&action=get_list&ds='.$dayFrom.'&de=' . $dayTo;
        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->get($url);


        if (!isset($response->json()['data']['list'])){
            return 'ошибка возврата статистики звонков';
        }

        $call = collect($response->json()['data']['list'])->unique()->pluck('caller');
        $filename = "звонкиПроекта_$id" . "_$dayFrom" . "_$dayTo" .".txt";
        $handle = fopen($filename, 'w');
        foreach ($call as $row) {
            fwrite($handle, $row . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }
    public function sipoutStatisticUpdateStore($id){
        $timeDifferent = 30;
        $days = 0;
        $dayFrom = now()->subDays($days)->format('d.m.Y');
        $dayTo = now()->format('d.m.Y');
        $project = Project::query()->find($id);
        $sipProject = $project->getSipProject;
        $api_key = $sipProject->api_key;

        if (!isset($sipProject->api_key)){
            return redirect(route('sipoutSetting.index',$id))->withErrors('SipOut Api Key не заполнен');
        }

        $priceSec = 0.016;
        $url = 'https://lk.sipout.net/userapi/?key='.$api_key.'&method=call_stat&action=get_list&ds='.$dayFrom.'&de=' . $dayTo;

        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->get($url);

        if (!isset($response->json()['data']['list'])){
            return 'ошибка возврата статистики звонков';
        }

        $call = collect($response->json()['data']['list']);


        if (count($call) > 0){
            $this->service->updateSipoutCall($project,$priceSec,$call,$timeDifferent);
        }

        return back();
    }
    public function sipoutStatisticUploadReportStore(Request $request,$id){
        $file = $request->file('csv_file');
        $csv = Reader::createFromPath($file, 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);
        $collection = Collection::make($csv->getRecords());
        $timeDifferent = 30;
        $this->service->uploadSipoutCall($collection,$id,$timeDifferent);

        return back();
    }
    public function sipoutCallIdUpdateComment(Request $request,$id){
        if ($request->type == 'comment'){
            SipoutCallId::query()->find($request->messege_id)->update(['comment' => $request->text]);
        }

        if ($request->type == 'phone'){
            SipoutCallId::query()->find($request->messege_id)->update(['phone' => $request->text]);
        }

        return 200;
    }
    public function sipoutCallIdUpdateOffer(Request $request,$id){
        $SipoutCallId = SipoutCallId::query()->find($request->call_id);


        Offer::query()->updateOrCreate([
            'project_id' => $id,
            'call_id' => $SipoutCallId->callId,
        ],[
            'offer_name' => $request->offer_name ?? '',
        ]);
        return 200;
}
}
