<?php

namespace App\Http\Controllers\Sipout;

use App\Events\NewLeadSipout;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Lead\NewLeadController;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Jobs\Google\LeadToGoogleSheet;
use App\Models\BlackList;
use App\Models\Project;
use App\Models\Sipout\LeadSipout;
use App\Models\Sipout\SipoutCallId;
use App\Traits\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Throwable;

class LeadController extends Controller
{
    use Notification;

    public function newLeadsSipOutStore(Request $request,$id){
        $phone = $request->phone ?? '';

        if ($phone == '%VAR:CID%') {
            return 201;
        }

        try {
            $CheckRussiaPhoneController = new CheckRussiaPhoneController();
            $phone = $CheckRussiaPhoneController->checkPhone($phone);
        } catch (\Exception $exception ){
            Log::error('Ошибка проверки номера ' . $request->phone . ' '. $exception);
        }

        try {

            event(new NewLeadSipout($phone));

            if (isset($request->utm_medium) && str_starts_with($request->utm_medium, '*call')){
                $typeCall = 'output';
            } else{
                $typeCall = 'inside';
                if (isset($request->utm_medium)){
                    $utm_medium = $this->utmMediumFind($id,$request->utm_medium);
                }
            }

            $utm_medium = $utm_medium ?? $request->utm_medium ?? null;

            $project = Project::query()->find($id);
            if ($phone == null) {
                return 200;
            }

//            if (count($project->getTestPhone)>0){
//                foreach ($project->getTestPhone as $key){
//                    if ($phone == $key->phone)
//                        return 201;
//                }
//            }

            $datas ['phone'] = $phone;
            $datas ['project_id'] = $id;
            $datas ['utm_source'] = 'zvonobot';
            $datas ['utm_medium'] = $utm_medium;
            $datas ['addWeb'] = '0';

        $newLeadController = new NewLeadController();
        $newLeadController->newLeeds($phone,$project,$datas);

            $data = [
                'created_at' => now(),
                'updated_at' => now(),
                'phone' => $phone,
                'project_id' => $id,
                'utm_source' => 'zvonobot',
                'utm_medium' => $utm_medium,
                'typeCall' => $typeCall,
            ];
            LeadSipout::query()->where('project_id',$id)->insert([$data]);
            $sheetDate = [[
                'date' => now()->format('d-m-y'),
                'phone' => $phone,
                'utm_source' => 'zvonobot',
                'utm_medium' => $utm_medium,
                'typeCall' => $typeCall,
            ]];

            LeadToGoogleSheet::dispatch($sheetDate,$id);

        } catch (Throwable $e){
            report($e);
        }

        return 200;
    }

    public function stopListStore(Request $request,$id){

        $phone = $request->phone ?? '';
        try {
            $CheckRussiaPhoneController = new CheckRussiaPhoneController();
            $phone = $CheckRussiaPhoneController->checkPhone($phone);
        } catch (\Exception $exception ){
            Log::error('Ошибка проверки номера ' . $request->phone);
        }

        BlackList::query()->updateOrCreate([
            'project_id' => $id,
            'phone' => $phone,
        ],[
            'phone' => $phone,
        ]);
            return 200;

    }

      function utmMediumFind($id,$utm_medium){
          $SipoutCallId = SipoutCallId::query()->where(['phone' => $utm_medium,'project_id' => $id])->first();
          return $SipoutCallId ? $SipoutCallId->callId : $utm_medium;
      }

}
