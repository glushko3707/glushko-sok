<?php

namespace App\Http\Controllers\Sipout;

use App\Http\Controllers\Controller;
use App\Services\Sipout\Service;

class BaseController extends Controller
{
    public $service;

    public function __construct(Service $service)
    {
        $this->service = $service;
    }
}
