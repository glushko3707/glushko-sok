<?php

namespace App\Http\Controllers\Sipout;

use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Models\Project;
use App\Models\Sipout\SipOutSetting;
use App\Models\Sipout\TestPhone;
use App\Traits\SipOut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SipoutSettingController extends BaseController
{
    use SipOut;
    public function sipoutSettingIndex($id)
    {
        $project = Project::query()->find($id);

        $sipOutSetting = SipOutSetting::query()->where('project_id', $id)->first(); // Вынесем общий запрос в переменную, чтобы не повторять его дважды
        if (isset($sipOutSetting->api_key)) { // Проверяем существование api_key у найденной записи
            $project->sipOutSetting = $sipOutSetting->api_key; // Присваиваем значение api_key свойству sipOutSetting
        }
        if (isset($sipOutSetting->sheetId)) { // Проверяем существование api_key у найденной записи
            $project->sheet = $sipOutSetting->sheetId; // Присваиваем значение sheetId свойству sheet
        }
        if (isset($sipOutSetting->timeDifferent)) { // Проверяем существование api_key у найденной записи
            $project->timeDifferent = $sipOutSetting->timeDifferent; // Присваиваем значение sheetId свойству sheet
        }

        if (isset($project->getTestPhone) && count($project->getTestPhone)>0) { // Проверяем существование api_key у найденной записи
            $project->testPhone = $project->getTestPhone->first()->phone; // Присваиваем значение sheetId свойству sheet
        }
        return view('sipout.setting.SettingSipout', compact('id', 'project'));
    }
    public function sipoutSettingStore(Request $request, $id)
    {
        if (isset($request->sipOutSetting) && !empty($request->sipOutSetting)) {
            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://lk.sipout.net/userapi/?key=' . $request->sipOutSetting . '&method=balance&action=get');

            if ($response->json()["result"] != "ok") {
                return back()->withErrors(["Неправильный sipOutSetting"]);
            }

            SipOutSetting::query()->updateOrCreate([
                "project_id" => $id,
            ], [
                "api_key" => $request->sipOutSetting,
            ]);
        }


        if (!empty($request->timeDifferent)) {
            SipOutSetting::query()->updateOrCreate([
                "project_id" => $id,
            ], [
                "timeDifferent" => $request->timeDifferent,
            ]);
        }

        if (!empty($request->testPhone)){
            $CheckRussiaPhoneController = new CheckRussiaPhoneController();
            $phone = $CheckRussiaPhoneController->checkPhone($request->testPhone);
                if ($phone != null){
                    TestPhone::query()->updateOrCreate([
                        "project_id" => $id,
                    ], [
                        "phone" => $request->testPhone,
                    ]);
            }
        }


        return back();
    }

}
