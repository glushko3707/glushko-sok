<?php

namespace App\Http\Controllers\Sipout;

use App\Http\Controllers\Controller;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\Wa\WebWa;
use App\Models\Notification\Wa\WebWaNumber;
use App\Models\Notification\WebCall;
use App\Models\Notification\WebSms;
use App\Models\Project;
use App\Models\SmsSetting;
use App\Models\Waba\WabaMessage;
use App\Models\Waba\wabaSetting;
use App\Models\Waba\WabaTemplates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Throwable;

class WelcomeMessage extends Controller
{
    public function index($id){
        $sms = WebSms::query()->where('project_id',$id)->get();
        $wa = WebWa::query()->where('project_id',$id)->get();

        $project = Project::query()->find($id);

        $wabaTemplates = $project->getWabaTemplates;
        $notificationSetting = $project->getNotificationSetting;
        if (!empty($notificationSetting)){
            $notificationSetting->wabaTemplates = $wabaTemplates;
            $notificationSetting->wabaSetting = $project->getWabaSetting;
        }



        if (empty($notificationSetting)){
            $notificationSetting = NotificationSetting::query()->updateOrCreate([
                'project_id' => $id,
            ],[
                'project_id' => $id,
            ]);
        }
        $getSmsSetting = $project->getSmsSetting;
        if (empty($getSmsSetting)){
            $getSmsSetting = SmsSetting::query()->updateOrCreate([
                'project_id' => $id
            ],[
                'project_id' => $id,
                'service' => 'smsAero'
            ]);
        }

        try{
        if (isset($notificationSetting->smsAero_key) && isset($notificationSetting->smsAeroEmail) && !empty($notificationSetting->smsAero_key) && !empty($notificationSetting->smsAeroEmail) ) {
           $response = Http::withHeaders([
                    "content-type" => "application/json",
                    'accept' => 'application/json',
                ])->post('https://'.$notificationSetting->smsAeroEmail.':'.$notificationSetting->smsAero_key.'@gate.smsaero.ru/v2/balance');

            if (isset($response->json()['data']['balance'])) {
                $stat['sms'] = round($response->json()['data']['balance']);
            } else{$stat['sms'] = 'ошибка';}


            $responses = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://'.$notificationSetting->smsAeroEmail.':'.$notificationSetting->smsAero_key.'@gate.smsaero.ru/v2/sign/list');

            if ($responses->json()['success'] == true && $responses->json()['data']['totalCount'] > 0) {
                $smsAeroSenderName = collect($responses->json()['data'])->where('extendStatus','=', 'active')->pluck('name');
            }

        }

        } catch (Throwable $e){
        }

        $stat = $stat ?? '';

        if ($notificationSetting->welcomeSms == 1){

            $welcome['activeFirstSms'] = true;
            $welcome['sms'] = collect($sms)->whereNull('timeSend')->first()->text ?? '';
        } else {
            $welcome['activeFirstSms'] = false;
            $welcome['sms'] = '';
        }

        if ($notificationSetting->welcomeWa == 1){
            $welcome['activeFirstWa'] = true;
            $welcome['wa'] = collect($wa)->where('type','=','welcome')->first()->text ?? '';

            $webWanumber = WebWaNumber::query()->where(['project_id' => $id,'active' => 1])->get();
            $welcome['waCount'] = count($webWanumber);
        } else {
            $welcome['activeFirstWa'] = false;
            $welcome['wa'] = '';
        }

//        if (isset($notificationSetting->welcomeWaba) && $notificationSetting->welcomeWaba == 1){
//            $welcome['activeFirstWaba'] = true;
//            $notificationSetting->wabaToken = $waba->token;
//            $welcome['waba'] = $waba->welcome_pipeline_step_id ?? '';
//        } else {
//            $welcome['activeFirstWaba'] = false;
//            $welcome['waba'] = '';
//        }

        $smsAeroSenderName = $smsAeroSenderName ?? null;

        return view('sipout.welcomeMessage.welcomeMessage',compact('id','welcome','stat','notificationSetting','smsAeroSenderName','getSmsSetting'));
    }

    public function firstSmsActive(Request $request,$id){
        $project = Project::query()->find($id);

        $notificationSetting = $project->getNotificationSetting;

        if (isset($request->smsWelcome)){
            $smsWelcome = 1;
        } else {
            $smsWelcome = 0;
        }

        SmsSetting::query()->updateOrCreate(
            ['project_id' => $id],
            [
                'welcomeSms' => $smsWelcome,
            ]);

        NotificationSetting::query()->updateOrCreate(
            ['project_id' => $id],
            [
                'welcomeSms' => $smsWelcome,
            ]);

        try {
            if (isset($request->smsAero_key) && isset($request->emailSmsAero) && !empty($request->smsAero_key) && !empty($request->emailSmsAero) ) {
                $notificationSetting->smsAero_key =  $request->smsAero_key;
                $notificationSetting->smsAeroEmail = $request->emailSmsAero;
                $notificationSetting->smsAeroSenderName = $request->smsAeroSenderName ?? null;

                $smsSetting = $project->getSmsSetting;
                $smsSetting->smsAero_key = $project->smsAero_key;
                $smsSetting->smsAeroEmail = $project->smsAeroEmail;
                $smsSetting->smsAeroSenderName = $project->smsAeroSenderName;

                $response = Http::withHeaders([
                    "content-type" => "application/json",
                    'accept' => 'application/json',
                ])->post('https://'.$request->emailSmsAero.':'.$request->smsAero_key.'@gate.smsaero.ru/v2/balance');



                $notificationSetting->save();
                $smsSetting->save();

                if ($response->json()['success'] != "true") {
                    return redirect()->back()->withErrors(['Неправильный smsAero_key или emailSmsAero']);
                }
            }
        } catch (Throwable $e) {
            report($e);
        }


        return back();
    }
    public function firstWaActive(Request $request,$id){

        if (isset($request->waWelcome)){
            $waWelcome = 1;
        } else {
            $waWelcome = 0;
        }

        NotificationSetting::query()->updateOrCreate(['project_id' => $id],[
            'welcomeWa' => $waWelcome,
        ]);

        return back();
    }

    public function firstWabaActive(Request $request,$id){

        if (isset($request->wabaWelcome)){
            $waWelcome = 1;
        } else {
            $waWelcome = 0;
        }

        NotificationSetting::query()->updateOrCreate(['project_id' => $id],[
            'welcomeWaba' => $waWelcome,
        ]);

        return back();
    }

    public function firstWabaUpdateText(Request $request,$id){
        wabaSetting::query()->updateOrCreate([
                'project_id' => $id
            ], [
            'welcome_template_name' => $request->wabaWelcomeStore ?: null,
        ]);
        return back();
    }

    public function firstSmsUpdateText(Request $request,$id){
                WebSms::query()->updateOrCreate([
                    'project_id' => $id, 'timeSend' => null, 'type' =>  'welcome',
                ],[
                    'timeSend' => null, 'text' =>  $request->smsWelcomeStore,
                ]);
                return 200;
    }

    public function firstWaUpdateText(Request $request,$id){
        WebWa::query()->updateOrCreate([
            'project_id' => $id, 'type' =>  'welcome',
        ],[
            'text' =>  $request->waWelcomeStore,
        ]);
        return 200;
    }

}
