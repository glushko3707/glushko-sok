<?php

namespace App\Http\Controllers\Sipout;

use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Jobs\Sipout\AddPhone;
use App\Jobs\Sipout\AddPhoneToSipout;
use App\Jobs\Sipout\Databases\MinusUsedPhone;
use App\Jobs\Sipout\Databases\UpdateCategoryTable;
use App\Jobs\Sipout\Databases\UpdateUsedPhone;
use App\Jobs\Sipout\DeletePhoneDatabase;
use App\Jobs\Sipout\GetPhone;
use App\Models\BlackList;
use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use App\Services\Sipout\Service;
use App\Traits\SipOut;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use League\Csv\Reader;

class ContactDatabaseController extends BaseController
{
    use SipOut;

    public function addPhoneToDatabase($phoneWeb,$id,$category,$minus)
    {
        $blackList = BlackList::query()->get();

        $phoneWeb = collect($phoneWeb);
        $datas = $phoneWeb->map(function ($stats) {
            try {
                $CheckRussiaPhoneController = new CheckRussiaPhoneController();
                $phone = $CheckRussiaPhoneController->checkPhone($stats);
            } catch (\Exception $exception){
                $phone = null;
            }
            return $phone;
        })->filter();

        $datas = $datas->diff($blackList);

        $categoryDatabase = CategoryDatabase::query()
            ->where('project_id', $id)
            ->where('category_name', $category)
            ->first();


        $category_id = $categoryDatabase
            ? $categoryDatabase->id
            : CategoryDatabase::query()->updateOrCreate([
                'category_name' => $category,
                'project_id' => $id,
            ], [
                'used_count' => 0,
                'count' => 0,
            ])->id;


        if ($minus != null){
            $minusPhone = CategoryDatabase::query()
                ->where('project_id', $id)
                ->whereIn('category_name', $minus)
                ->pluck('id');
            $minusPhone->push($category_id);
            $ContactDatabase = ContactDatabase::query()->where('project_id',$id)->whereIn('category_databases_id',$minusPhone)->pluck('phone');
        } else {
            $ContactDatabase = ContactDatabase::query()->where('project_id',$id)->where('category_databases_id','=',$category_id)->pluck('phone');
        }

        if (isset($datas)){
            $newContact = collect($datas)->diff($ContactDatabase);
        } else{
            $newContact = [];
        }

        if (count($newContact) > 0){
            $data = $newContact->map(function ($stats) use($category_id,$id) {
                return [
                    'phone' => $stats,
                    'project_id' => $id,
                    'created_at' => now(),
                    'updated_at' => now(),
                    'category_databases_id' => $category_id,
                ];
            })->toArray();
        }

        if (isset($data)){
            ContactDatabase::query()->insert($data);
            CategoryDatabase::query()->find($category_id)->increment('count',count($data));
        }
    }

    public function contactDatabaseIndex($id){

        // Получаем массив имен файлов в папке контакта
        $contactDownload = array_map('basename', Storage::files('public/project/contact/'.$id.'/'));

        // Если массив пуст, присваиваем ему пустой массив
        $contactDownload = $contactDownload ?: [];

        $contacts = CategoryDatabase::query()->where('project_id',$id)->get();

        $contacts_work = Cache::get('contacts_work_' . $id);

        if ($contacts_work == null) {
            $contacts_work = 0;
        }

        return view('sipout.phoneDataBases.dataBases',compact('id','contacts','contactDownload','contacts_work'));
    }
    public function contactDatabaseStore(Request $request,$id){

        $request->validate([
            'category_id' => 'required|min:1',
        ], [
            'category_id' => 'Обязательно для заполнения',
        ]);


        $CategoryDatabase = CategoryDatabase::query()->find($request->category_id);
        $CategoryNotUsed = $CategoryDatabase['count'] - $CategoryDatabase['used_count'];
        $limit = min($CategoryNotUsed, $request->count*$request->countFile);

        $CategoryDatabase->increment('used_count', $limit);

//        $this->getPhone($request->category_id,$request->count,$request->countFile,$id);
        GetPhone::dispatch($request->category_id,$request->count,$request->countFile,$id);

        return back();

    }
    public function contactAddDatabaseIndex($id){
        $category = CategoryDatabase::query()->where('project_id',$id)->get();
        return view('sipout.phoneDataBases.dataBasesAdd',compact('id','category'));
    }

    public function contactAddDatabaseStore(Request $request,$id){

            if ($request->anotherСategory != null) {
                $category = $request->anotherСategory;
            } else {
                $validatedData = $request->validate([
                    'category' => 'required|min:1',
                ], [
                    'category' => 'Обязательно для заполнения',
                ]);
                $category = $request->category ?? null;
            }
        $minus = $request->minus ?? null;

        if ($request->has('contactOld')) {
            $getContactOld = $this->getContactOld($request);
            $phoneWebs = $getContactOld->chunk(3000);

            foreach ($phoneWebs as $web){
                AddPhone::dispatch($web,$id,$category,$minus)->onQueue('low');
            }

            return redirect(route('contactDatabase.index',$id));
        }


        $files = $request->file('phoneWeb');
        foreach ($files as $phoneWebs){

        $phoneWeb =  collect(file($phoneWebs))->unique();

        $phoneWebs = $phoneWeb->chunk(3000);
        foreach ($phoneWebs as $web){
//            $ContactDatabaseController  = new ContactDatabaseController(new Service());
//            $ContactDatabaseController->addPhoneToDatabase($web,$id,$category,$minus);
               AddPhone::dispatch($web,$id,$category,$minus)->onQueue('low');
            }
        }

        return redirect(route('contactDatabase.index',$id));
    }

    public function contactMinusDatabaseStore(Request $request,$id){
        $validatedData = $request->validate([
            'category' => 'required|min:1',
            'minus' => 'required|min:1',
        ], [
            'category' => 'Обязательно для заполнения',
            'minus' => 'Обязательно для заполнения',
        ]);


        MinusUsedPhone::dispatch($id,$request->category,$request->minus)->onQueue('low');

        return back();
    }
    public function contactDatabaseDelete(Request $request,$id){
        CategoryDatabase::query()->find($request->category_id)->delete();
        DeletePhoneDatabase::dispatch($request->category_id,$id);
        return 200;
    }
    public function getContactOld($request){
        // Проверяем, есть ли файл в запросе
        $collect = new \Illuminate\Support\Collection();
        if ($request->hasFile('contactOld')) {
            // Получаем файл из запроса
            $files = $request->file('contactOld');
                foreach ($files as $file) {
                // Создаем объект Reader из файла
                $csv = Reader::createFromPath($file->getPathname());
                // Устанавливаем разделитель столбцов
                $csv->setDelimiter(';');
                // Получаем заголовки из первой строки
                $headers = $csv->fetchOne();
                $records = collect($csv->jsonSerialize());
                $keyPhone = array_search ('phone', $headers);
                $collect = $collect->concat($records);
                }

            if (isset($request->wait)){
                $date[] = 'wait';
            }

            if (isset($request->ring_timeout)){
                $date[] = 'ring timeout';
                $date[] = 'busy';
            }
            if (isset($request->filed_machine)){
                $date[] = 'failed ( machine )';
            }

             if (isset($request->all)){
                 return $collect->pluck($keyPhone);
             }

            // Возвращаем успешный ответ с коллекцией отфильтрованных данных
            return $collect->whereIn('1',$date)->pluck($keyPhone);
        } else {
            // Возвращаем ошибку, если файл отсутствует в запросе
            return response()->json(['error' => 'No file provided']);
        }
    }
    public function contactDatabaseCleanUsed(Request $request,$id){

        UpdateUsedPhone::dispatch($id,$request->categoryCleanUsed_id)->onQueue('low');

        CategoryDatabase::query()
            ->where([
                ['project_id', '=', $id],
                ['id', '=', $request->categoryCleanUsed_id]
            ])
            ->update(['used_count' => 0]);


        return back();
    }
    public function getContactDatabase(Request $request,$id){

        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];

        $pathPart = "public/project/contact/".$id . '/';
        $filename = Storage::path($pathPart . $request->fileName);


        if (!Storage::exists($pathPart . $request->fileName)){
            return back();
        }

        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }
    public function getContactDatabaseDelete(Request $request,$id){
        $pathPart = "public/project/contact/".$id . '/';

        if (!Storage::exists($pathPart . $request->fileName)){
            return back();
        }

       Storage::delete($pathPart . $request->fileName);


        return back();
    }
    public function getInfoContactDatabase($id){
        Return CategoryDatabase::query()
            ->where([
                ['project_id', '=', $id],
            ])
            ->get()
            ->toArray();
    }
    public function addSipoutContact(Request $request,$id){
        $count = $request->count;
        $category_id = $request->category_id;
        $call_id = $request->call_id;

//        $this->addPhoneSipout($count,$category_id,$call_id,$id);
        AddPhoneToSipout::dispatch($count,$category_id,$call_id,$id)->onQueue('low');

        return back();

    }
    public function updateCategory($id,$category_id){
        UpdateCategoryTable::dispatch($id,$category_id)->onQueue('low');
        return back();
    }
    public function blackListIndex($id){

        return view('sipout.phoneDataBases.blackList',compact('id'));
    }
    public function blackListStore(Request $request,$id){
        // Проверяем, есть ли в запросе список телефонов
        if (isset($request->phoneWebOne)){
            // Разбиваем текст по переносам строк
            $text = $request->phoneWebOne;
//            $result =  preg_split('/\r\n|[\r\n]/', $text);
            $result = preg_split("/[\s,]+/", $text);
            // Для каждой строки в списке

            foreach($result as $line) {
                // Пытаемся проверить, является ли строка валидным номером телефона
                try {
                    $CheckRussiaPhoneController = new CheckRussiaPhoneController();
                    $phone = $CheckRussiaPhoneController->checkPhone($line);
                } catch (\Exception $exception){
                    // Если нет, то пропускаем строку
                    continue;
                }
                if ($phone != null){
                    BlackList::query()->updateOrCreate([
                        'project_id' => $id,
                        'phone' => $phone,
                    ],[
                        'phone' => $phone,
                    ]);
                }

            }
        }
        return back();
    }

    public function fileMinusStore(Request $request){

        $phoneWeb = new \Illuminate\Support\Collection();
        $minusColl = new \Illuminate\Support\Collection();
        $files = $request->file('file');
        $BlackList = BlackList::query()->where('project_id','=',114)->get();
        foreach ($files as $phoneWebs){
            $phoneWeb =  collect(file($phoneWebs))->merge($phoneWeb);
        }

        if ($request->file('minus') !== null){


        $minus = $request->file('minus');
        foreach ($minus as $minussss){
            $minusColl =  collect(file($minussss))->merge($minusColl);
            $phoneWeb =  $phoneWeb->diff($minusColl);
        }

        }
        $phoneWeb =  $phoneWeb->diff($BlackList)->toArray();
        $count = count($phoneWeb);
        $filename = "file_$count.txt";
        $handle = fopen($filename, 'w');
        foreach ($phoneWeb as $row) {
            fwrite($handle, str_replace(["\r\n", "\r", "\n"], "", $row) . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }
}

