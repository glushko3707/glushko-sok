<?php

namespace App\Http\Controllers\Sipout\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification\NewLeadWeb;
use App\Models\Sipout\LeadSipout;
use App\Models\Sipout\SipoutCallId;
use Illuminate\Http\Request;

class StatAllProject extends Controller
{
    public function StatAllProject(Request $request, $id){

        $dateTo = $request->date_to;
        $dateFrom = $request->date_from;

        $newLeadWeb = LeadSipout::query();
        $SipoutCallId = SipoutCallId::query();

        if (!empty($dateTo)) {
            $newLeadWeb->whereDate('updated_at', '<=', $dateTo);
            $SipoutCallId->whereDate('date', '<=', $dateTo);
            ;}
        if (!empty($dateFrom)) {
            $newLeadWeb->whereDate('updated_at', '>=', $dateFrom);
            $SipoutCallId->whereDate('date', '>=', $dateFrom);
        }



        $newLeadWebs = $newLeadWeb->get()->groupBy('utm_medium');
        $SipoutCallIds = $SipoutCallId->get();

        $all ['money'] = round($SipoutCallId->sum('money'));
        $all ['lead'] = $newLeadWeb->count();
        if ($all ['lead'] != 0){
            $all ['leadPrice'] = round($all ['money'] / $all ['lead']);
        } else {
            $all ['leadPrice'] = 0;
        }

        $ads = [];

        foreach ($SipoutCallIds as $callIds) {


               if (!isset($ads[$callIds->project_id]['money'])){
                   $ads[$callIds->project_id]['money'] = 0;
                   $ads[$callIds->project_id]['lead'] = 0;
                   $ads[$callIds->project_id]['LeadPrice'] = 0;
               }

               $ads[$callIds->project_id]['money'] +=  $callIds['money'];

                if (isset($newLeadWebs[$callIds->callId])){
                    $ads[$callIds->project_id]['lead'] += count($newLeadWebs[$callIds->callId]);
                    $ads[$callIds->project_id]['LeadPrice'] =   round($ads[$callIds->project_id]['money']/ $ads[$callIds->project_id]['lead']);
                }
        }

        $ads_price = collect($ads)->where('lead','!=',0)->sortBy('LeadPrice');

        return view('sipout.admin.StatAllProject',compact('all','id','ads_price'));
    }
}
