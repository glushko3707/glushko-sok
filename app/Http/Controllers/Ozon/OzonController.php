<?php

namespace App\Http\Controllers\Ozon;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\CSVController;
use App\Http\Controllers\Service\ZipController;
use App\Models\Ozon\OzonStatistic;
use Carbon\Carbon;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use League\Csv\Exception;
use League\Csv\InvalidArgument;
use League\Csv\UnavailableStream;

class OzonController extends Controller
{
    /**
     * Получение OAuth токена
     *
     * @return array
     * @throws RequestException
     */
    public function getToken(): array
    {
        $url = 'https://api-performance.ozon.ru/api/client/token';

        // Эти значения лучше хранить в .env
        $clientId = '29444515-1726055997869@advertising.performance.ozon.ru';
        $clientSecret = 'l2Zlbr-q-3YnkjtA_if1VEAWraLr0zrBF1VRK4LBg3haXH4B5Siyteu4j_fOUqHlFAbY6B-UW58LvG-H1A';

        $response = Http::post($url, [
            'client_id' => $clientId,
            'client_secret' => $clientSecret,
            "grant_type" => "client_credentials"
        ]);

        // Проверка успешности
        if ($response->successful() && isset($response->json()['access_token'])) {
            return $response->json();
        }

        return [];
    }

    /**
     * @throws RequestException
     */
    public function getCompanies(): array
    {
        // Получаем токен (или берём из кэша, БД и т.д.)
        $tokenData = $this->getToken();

        $accessToken = $tokenData['access_token'] ?? null;
        if (!$accessToken) {
            throw new \Exception('Отсутствует access_token');
        }
        $url = 'https://api-performance.ozon.ru/api/client/campaign';

        $response = Http::withToken($accessToken)
            ->get($url);

        $campaigns = $response->json()['list'];
        $campaignsIds = array_column($campaigns, 'id');
        $chunks = array_chunk($campaignsIds, 10);

        $stat = $this->getStatistics('2025-01-01', '2025-01-31',$chunks[0]);

        if ($response->successful()) {
            return $response->json();
        }

        $response->throw();
    }


    /**
     * Пример получения статистики с группировкой
     *
     * @param string $dateFrom
     * @param string $dateTo
     * @param array $companyIds Список ID компаний, если нужно фильтровать
     * @return array
     * @throws RequestException
     */
    public function getStatistics(string $dateFrom, string $dateTo, array $companyIds = []): array
    {

        $tokenData = $this->getToken();

        $accessToken = $tokenData['access_token'] ?? null;
        // URL может отличаться в зависимости от версии API

        $url = 'https://api-performance.ozon.ru/api/client/statistics';

        // Пример тела запроса (payload). Конкретные поля смотрите в документации
        $body = [
            'from' => now()->subDays(30)->toIso8601String(),
            'to'   => now()->toIso8601String(),

            // Измерения (группировки), например: по дате и ID компании
            'groupBy'  => 'DATE',
        ];

        if (!empty($companyIds)) {
            $body['campaigns'] = $companyIds;
        }

        $response = Http::withToken($accessToken)
            ->post($url, $body);

        dd($response->json());

        if ($response->successful()) {
            return $response->json();
        }

        $response->throw();
    }

    public function checkUUID($accessToken,$UUID)
    {

        $url = "https://api-performance.ozon.ru/api/client/statistics/$UUID";

        // Пример тела запроса (payload). Конкретные поля смотрите в документации
        $body = [
            'UUID' => $UUID,
            ];

        $response = Http::withToken($accessToken)
            ->get($url, $body);
        if ($response->successful()) {
            if ($response->json()['state'] == 'OK' && isset($response->json()['link'])) {
                return $response->json()['link'];
            }
        }
        return false;
    }

    public function updateStatisticaHw($startDate = null)
    {
        $project_id = 599;
        $date = OzonStatistic::query()->where('project_id',$project_id)->get('date')->last();
        if (!empty($date)){
            $dateFrom = $date->date;
        } else{
            $dateFrom = now()->subDays(7)->format('Y-m-d');
        }
        $dateTo = now()->format('Y-m-d');
        $stats = $this->getReport($project_id,$dateFrom,$dateTo);
        $this->setOzonStat($project_id, (array) $stats);
        return 200;
    }
    public function getReport($project_id,$dateFrom = null, $dateTo = null) {
        $tokenData = $this->getToken();

        $accessToken = $tokenData['access_token'] ?? null;

        // Базовый URL API
        $url = "https://api-performance.ozon.ru/api/client/statistics/daily/json";

        // Формируем параметры запроса
        $queryParams = [];

        if (!is_null($dateFrom)) {
            $queryParams['dateFrom'] = $dateFrom;
        }

        if (!is_null($dateTo)) {
            $queryParams['dateTo'] = $dateTo;
        }

        // Выполняем запрос
        $response = Http::withToken($accessToken)
            ->get($url, $queryParams);
        // Получаем данные в формате JSON и преобразуем в arrows
        return $response->json()['rows']; // Возвращаем данные в формате JSON
    }

    public function setOzonStat($projectId, array $stats)
    {
        // Массивы для подготовки данных и определения, что нужно удалить
        $recordsToUpdate = [];
        $adIds = [];
        $dates = [];

        foreach ($stats as $stat) {
            // Преобразуем числовые значения, особенно если у вас разделитель запятая (например, "6,90")
            $moneySpent = isset($stat['moneySpent'])
                ? (float) str_replace(',', '.', $stat['moneySpent'])
                : 0;
            $avgBid = isset($stat['avgBid'])
                ? (float) str_replace(',', '.', $stat['avgBid'])
                : 0;

            if ($avgBid > 0){
                $impressions = (int) round($moneySpent / $avgBid);
            } else {
                $impressions = 0;
            }

            $recordsToUpdate[] = [
                'project_id' => (int) $projectId,
                'ad_id'      => $stat['id'],
                'title'      => $stat['title'] ?? null,
                'date'       => $stat['date'],
                'clicks'     => $stat['clicks'] ?? 0,
                'moneySpent' => $moneySpent,
                'avgBid'     => $avgBid,
                'impressions'=> $impressions,
            ];

            // Собираем идентификаторы и даты, чтобы удалить старые записи перед обновлением
            $adIds[] = $stat['id'];
            $dates[] = $stat['date'];
        }

        // Удаляем старые записи по проекту, совпадающим ad_id и датам
        OzonStatistic::query()->where('project_id', $projectId)
            ->whereIn('ad_id', $adIds)
            ->whereIn('date', $dates)
            ->delete();

        // Вставляем (или обновляем) новые записи
        OzonStatistic::query()->upsert(
            $recordsToUpdate,
            ['project_id', 'ad_id', 'date'],  // Поля для уникального индекса
            ['title', 'clicks', 'moneySpent', 'avgBid', 'impressions'] // Поля, которые следует обновлять
        );
    }


}
