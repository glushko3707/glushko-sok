<?php

namespace App\Http\Controllers\HelloWorld;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReportController extends Controller
{
   public function index($report)
   {
       switch ($report) {
           case 'all':
               $src = 'https://app.fabric.microsoft.com/view?r=eyJrIjoiMDk0NjUwNGMtNmVjMC00MTk3LTgzNTAtYjA0OTJjNWVjNjc0IiwidCI6IjI2MjkyMDg4LWQ2MWUtNGExNC1iZWU2LTcwNWQ0ZTRmODRhMyIsImMiOjl9';
               break;
           case 'ozon':
               $src = 'https://app.powerbi.com/view?r=eyJrIjoiMDYwMzcwNDctMzk2Zi00MjE0LWE5YjMtOTZmYmQ5MzAxNjIyIiwidCI6IjI2MjkyMDg4LWQ2MWUtNGExNC1iZWU2LTcwNWQ0ZTRmODRhMyIsImMiOjl9';
               break;
           default:
               $src = 'https://app.fabric.microsoft.com/view?r=eyJrIjoiMDk0NjUwNGMtNmVjMC00MTk3LTgzNTAtYjA0OTJjNWVjNjc0IiwidCI6IjI2MjkyMDg4LWQ2MWUtNGExNC1iZWU2LTcwNWQ0ZTRmODRhMyIsImMiOjl9';
       }

       return view('hello-world.report',compact('src'));
   }
}
