<?php

namespace App\Http\Controllers\HelloWorld;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GoogleSheets\GoogleSheet2Controller;
use App\Http\Controllers\Notification\TrafficController;
use App\Jobs\HW\UpdateFacebookStat;
use App\Models\HW\HwDataAnalytica;
use App\Models\HWChannalForUpdate;
use App\Models\Ozon\OzonStatistic;
use App\Models\Yandex\YandexDirectStatistica;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Throwable;

class UpdateCostController extends Controller
{

    public function listUpdateIndex()
    {
        return view('hello-world.hwAccountUpdateTableList');
    }
    public function listUpdateStore(Request $request)
    {
        $validate = $request->validate([
            'dateFrom' => 'required',
            'dateTo' => 'required',
        ]);

        $HWChannalForUpdate_model= HWChannalForUpdate::all();
        $dateFrom = $request->dateFrom;
        $dateTo = $request->dateTo;

        foreach ($HWChannalForUpdate_model as $channel){
            switch ($channel->channel){
                case 'facebook':
                    (new UpdateCostController())->updateAccountFacebookHw('599',$channel->toArray(),$dateFrom,$dateTo);
                case 'telegram':
            }
        }




        return back()->with('success', 'Статистика обновлена');
    }
    public function listUpdateDateIndex($account_update_id)
    {
        return view('hello-world.hwAccountUpdateTableDate',compact('account_update_id'));
    }
    public function listUpdateDateStore(Request $request,$account_update_id)
    {
      $HWChannalForUpdate_model= HWChannalForUpdate::query()->find($account_update_id);
      $dateFrom = $request->dateFrom;
      $dateTo = $request->dateTo;

       switch ($HWChannalForUpdate_model->channel){
           case 'facebook':
               (new UpdateCostController())->updateAccountFacebookHw('599',$HWChannalForUpdate_model->toArray(),$dateFrom,$dateTo);
           case 'telegram':
       }

        return redirect()->route('hw.account.update.table.list');
    }

    public function hwUpdateCost()
    {
        $id = '599';
        $dateFrom = today()->subDay()->toDateString();
        $platforms = [
            'Facebook' => 'updateFacebookHw',
            'Ozon' => 'updateOzon',
            'Yandex' => 'updateYandexFormBD',
        ];

        foreach ($platforms as $platform => $method) {
            try {
                if ($platform === 'Facebook') {
                    $this->$method($id, $dateFrom);
                } else {
                    $this->$method($id, $dateFrom, $platform);
                }
            } catch (Throwable $e) {
                report($e);
                Log::error("Ошибка при обновлении {$platform}: " . $e->getMessage());
            }
        }
    }
    
    public function hwUpdateCostMy()
    {
        $id = '599';
//        $all = $this->getAllStatFromGoogleSheet('0-10');
        $dateFrom = today()->subDays(7)->format('Y-m-d');
//        $this->updateFacebookHw($id,$dateFrom);
//        $this->updateOzon($id,$dateFrom,'Ozon');
        $this->updateYandexFormBD($id,$dateFrom,'Yandex');
    }
    public function hwSetDateToSheetUpdate($dateFrom,$sheetName,$channel)
    {
        $data = $this->getStatFormDb('599',$dateFrom,$channel);
        $data = array_values($data);
        $GoogleSheetsController = new GoogleSheet2Controller();
        $GoogleSheetsController->updateDataStartingFromA2('1h2CBSCQn5EU8IWWqc6gN7wOO3AMNgxznMN1iQJwYVHg',$sheetName,$data);
    }




    public function getStatFormDb($id,$dateFrom,$channel)
    {
        return HwDataAnalytica::query()
            ->where('project_id', $id)
            ->where('channel', $channel)
            ->get()
            ->filter(function ($item) use ($dateFrom) {
                // Преобразуем поле `date` из строки в объект Carbon
                $itemDate = Carbon::parse($item->date);
                // Сравниваем дату
                return $itemDate->timezone('Europe/Moscow')->endOfDay()->gte(Carbon::parse($dateFrom)->timezone('Europe/Moscow')->startOfDay());
            })->map(function ($items) {

                return [
                    $items->date,
                    $items->utm_source,
                    $items->utm_medium,
                    $items->utm_campaign,
                    $items->utm_content,
                    $items->impressions,
                    $items->clicks,
                    $items->cost,
                ];
            })->values()->toArray();
    }


    public function getAllStatFromGoogleSheet($listName)
    {
        $GoogleSheetsController = new GoogleSheet2Controller();
        return $GoogleSheetsController->getSheetValues('1rl_4upW110vB7ct5dWnx4JdKC0Tir7Ix5NKvkpJDIxg',$listName);
    }

     function updateFacebookHw($id,$startDate = '2025-01-15',$dateTo = null)
    {
        $accounts = $this->getFacebookAccount();
        foreach ($accounts as $account){
            UpdateFacebookStat::dispatch($id,$account,$startDate);
//            (new UpdateCostController())->updateAccountFacebookHw($id, $account, $startDate);
        }
    }

    public function updateAccountFacebookHw($id,$account,$startDate,$dateTo = null)
    {
        $currencyMoney = $this->getCurrencyMoney();
        $TrafficController = new TrafficController();
        $stat = $TrafficController->targetUpdateStat($id,$account['account_id'],$startDate,$dateTo);
        $GoogleSheetsController = new GoogleSheet2Controller();
        // очищаем таблицу, кроме первой строки
        $GoogleSheetsController->clearSheetValuesExceptHeader('1h2CBSCQn5EU8IWWqc6gN7wOO3AMNgxznMN1iQJwYVHg',$account['sheetName']);
        $accountCurrency = $account['currency'];
        $currencyMoneyUp = $currencyMoney[$accountCurrency] ?? 1;
        $getFormatStat = $this->getFormatStatFacebook($stat,$currencyMoneyUp,$account);
        $GoogleSheetsController->updateDataStartingFromA2('1h2CBSCQn5EU8IWWqc6gN7wOO3AMNgxznMN1iQJwYVHg',$account['sheetName'],$getFormatStat);
    }
    function getFacebookAccount()
    {
        return HWChannalForUpdate::query()->where('channel','facebook')->where('active',1)->get()->toArray();
    }
    function getFormatStatFacebook($dataArray,$currencyMoney,$account)
    {
        $valuesForSheets = [];

        foreach ($dataArray as $item) {
            $valuesForSheets[] = [
                $item['date_start'],
                $account['utm_source'],
                $account['utm_medium'],
                $item['adset_name'],
                $item['ad_name'],
                $item['impressions'],
                $item['clicks'],
                $item['spend'] * $currencyMoney,
            ];
        }
        return $valuesForSheets;
    }
    function getCurrencyMoney()
    {
        return [
            'SGD' => 52.3,
            'USD' => 70,
        ];
    }

    public function updateTelegramToDB($id, $dateFrom, $dateTo = null, $utm = [], $sheetName = 'ОРД закуп', $channel = 'telegram')
    {
        $dateTo = $dateTo ?? now()->format('Y-m-d');
        $GoogleSheetsController = new GoogleSheet2Controller();
        $getSheetValues = $GoogleSheetsController->getAsCollection('1Oer_2Igu0mPHJEvRcI-K5jPdUusQZTMtqBJEUP5YPBU',$sheetName);
        $getSheetValuesSheet = $this->getDataTelegramFromCollection($getSheetValues,$dateFrom,$dateTo,$utm,$channel);
        $this->setStatToDB($id,$getSheetValuesSheet);
        sleep(1);
        $UpdateCostController = new UpdateCostController();
        $UpdateCostController->hwSetDateToSheetUpdate(now()->subDay()->format('Y-m-d'),'Telegram_design','telegram_design');
    }

    function setStatToDB($id,$data)
    {
        // Преобразуем обычную коллекцию в ассоциативную
        $data = collect($data);
        $transformedCollection = $data->map(function ($item) use ($id) {
            return [
                'project_id'   => $id,
                'date'         => $item['date'] ?? null,
                'utm_source'   => $item['source'] ?? null,
                'utm_medium'   => $item['medium'] ?? null,
                'utm_campaign' => $item['campaign'] ?? null,
                'utm_content'  => $item['content'] ?? null,
                'impressions'  => $item['impressions'] ?? 0,
                'clicks'       => $item['clicks'] ?? 0,
                'cost'         => $item['cost'] ?? 0.00,
                'channel'      => $item['channel'] ?? 0.00,
                'created_at'   => now(),
                'updated_at'   => now(),
            ];
        });

        $uniqueBy = ['project_id', 'utm_source', 'utm_medium', 'utm_campaign', 'utm_content','channel'];
        $updateFields = ['impressions', 'clicks', 'cost', 'updated_at'];

        HwDataAnalytica::query()->upsert(
            $transformedCollection->toArray(), // Данные для вставки или обновления
            $uniqueBy,                         // Уникальные поля для проверки
            $updateFields                      // Поля, которые будут обновлены при совпадении
        );
    }

    function getDataTelegramFromCollection($collection,$dateFrom,$dateTo,$utm = [],$channel = 'telegram')
    {
        $collection = $collection->map(function ($row) use ($utm,$channel) {
            try {
                $row['date'] = Carbon::createFromFormat('d.m.Y', $row['Date'])->format('Y-m-d');
            } catch (\Carbon\Exceptions\InvalidFormatException $e) {
                // Если нужно полностью исключить этот элемент, возвращаем false
                return false;
            }

            $metka = $row['Метка']     ?? 'unknown';
            $lending = $row['Landing']     ?? 'unknown';

            $row['source']   = $utm['source'];
            $row['medium']      =  $utm['medium'];
            $row['campaign']     = $lending   ?: 'unknown';
            $row['content']      = $metka ?: 'unknown';
            $row['channel'] = $channel;
            $valueImpressions = $row['Просмотров'] ?? '';
            // если ключ не существует, будет ''
            $valueImpressions = preg_replace('/\D+/', '', $valueImpressions); // оставляем только цифры

            if ($valueImpressions === '') {
                $row['impressions'] = 0;
            } else {
                $row['impressions'] = (int) $valueImpressions;
            }

            $value = $row['Клики'] ?? '';
// 2) Удаляем все нецифровые символы
            $digitsOnly = preg_replace('/\D+/', '', $value);
// 3) Если после удаления цифр строка оказалась пустой, значит ставим 0
            if ($digitsOnly === '') {
                $row['clicks'] = 0;
            } else {
                // Иначе приводим к int
                $row['clicks'] = (int) $digitsOnly;
            }

            $valueCost = $row['Cost'] ?? '';
            $valueCost = preg_replace('/\D+/', '', $valueCost);
            if ($valueCost === '') {
                $row['cost'] = 0;
            } else {
                $row['cost'] = (int) $valueCost;
            }

            // Оставляем только нужные ключи:
            return collect($row)->only([
                'date',
                'source',
                'medium',
                'campaign',
                'content',
                'impressions',
                'clicks',
                'cost',
                'channel',
            ]);
        });

        $collection = $collection->reject(function ($item) {
            return $item === false;
        });

        return $this->filterCollectionByDates($collection, $dateFrom, $dateTo);
    }
     function filterCollectionByDates($collection, $dateFrom, $dateTo)
    {
        if ($dateTo == null) {
            $dateTo = now()->format('Y-m-d');
        }

        // Определяем начало и конец диапазона (включая весь день)
        $start = Carbon::parse($dateFrom)->startOfDay();
        $end = Carbon::parse($dateTo)->endOfDay();
        // Фильтруем коллекцию
        $filtered = $collection->filter(function ($item) use ($start, $end) {
            $itemDate = Carbon::parse($item['date']);
            // Проверяем, попадает ли дата в нужный диапазон (включая границы)
            return $itemDate->between($start, $end);
        });

        return $filtered->values();
    }

    public function getOzonFromDB($id,$dateFrom)
    {
        return OzonStatistic::query()
            ->where('project_id', $id)
            ->get()
            ->filter(function ($item) use ($dateFrom) {
                // Преобразуем поле `date` из строки в объект Carbon
                $itemDate = Carbon::parse($item->date);
                // Сравниваем дату
                return $itemDate->timezone('Europe/Moscow')->endOfDay()->gte(Carbon::parse($dateFrom)->timezone('Europe/Moscow')->startOfDay());
            })->map(function ($items) {

                if ($items->avgBid > 0){
                    $impressions = (int) round(($items->moneySpent * 1000) / ($items->avgBid * 1000));
                } else {
                    $impressions = 0;
                }

                return [
                    $items->date,
                    'ozon',
                    'cpc',
                    $items->ad_id,
                    $items->title,
                    $impressions,
                    $items->clicks,
                    $items->moneySpent
                ];
            })->values()->toArray();
    }
    public function getYandexFromDB($id,$dateFrom)
    {
        $stat =  YandexDirectStatistica::query()
            ->where('project_id', $id)
            ->get()
            ->filter(function ($item) use ($dateFrom) {
                // Преобразуем поле `date` из строки в объект Carbon
                $itemDate = Carbon::parse($item->Date);
                // Сравниваем дату
                return $itemDate->timezone('Europe/Moscow')->endOfDay()->gte(Carbon::parse($dateFrom)->timezone('Europe/Moscow')->startOfDay());
            })
            ->map(function ($items) {
                return [
                    $items->Date,
                    'yandex',
                    'vladislav',
                    $items->CampaignName,
                    $items->AdId,
                    $items->Impressions,
                    $items->Clicks,
                    $items->Cost
                ];
            });
           $stat = $stat->values()->toArray();
        return $stat;
    }

    function updateOzon($id,$dateFrom,$sheetName)
    {
        $getOzonFromDB = $this->getOzonFromDB($id,$dateFrom);
        $GoogleSheetsController = new GoogleSheet2Controller();
        $GoogleSheetsController->updateDataStartingFromA2('1h2CBSCQn5EU8IWWqc6gN7wOO3AMNgxznMN1iQJwYVHg',$sheetName,$getOzonFromDB);
    }
    function updateYandexFormBD($id,$dateFrom,$sheetName)
    {
        $getOzonFromDB = $this->getYandexFromDB($id,$dateFrom);
        $GoogleSheetsController = new GoogleSheet2Controller();
        $GoogleSheetsController->updateDataStartingFromA2('1h2CBSCQn5EU8IWWqc6gN7wOO3AMNgxznMN1iQJwYVHg',$sheetName,$getOzonFromDB);
    }

}
