<?php

namespace App\Http\Controllers\Waba;

use App\Http\Controllers\Controller;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\wabaSetting;
use App\Models\Waba\WabaTemplates;
use App\Models\Waba\WabaVariable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TemplatesController extends Controller
{
    public function TemplatesControllerIndex($id)
    {
        return view('waba.WabaTemplates',compact('id'));
    }

    public function updateStore($id)
    {
        $this->updateWabaTemplatesStore($id);
        return back();
    }

    public function updateWabaTemplatesStore($id){
        $availableTemplates = $this->getAvailableTemplates($id);
        $names = collect($availableTemplates)->flatMap(function ($items) {
            return collect($items)->pluck('name');
        });
        WabaTemplates::query()->where('project_id',$id)->whereNotIn('name',$names)->delete();
        $this->setTemplateWaba($availableTemplates,$id);
    }

    public function updateWabaTemplatesStoreOne($waba_id,$templateName,$id){
        $availableTemplates = $this->getWabaTemplatesInfo($waba_id,$templateName);
        $this->setTemplateWaba($availableTemplates,$id);
    }

    public function getAvailableTemplates($id)
    {
        $accessToken = WabaSetting::query()->where('project_id',$id)->get()->last()->token;
        $WabaIntance = WabaIntance::query()->where('project_id',$id)->get();
        foreach ($WabaIntance as $intance ){
            $response = Http::withToken($accessToken)->get("https://graph.facebook.com/v19.0/$intance->waba_id/message_templates", [
                'access_token' => $accessToken,
            ]);
            $data[$intance->waba_id] = $response->json()['data'] ?? $response->json();
        }
        return $data ?? null;
    }

    public function setTemplateWaba($availableTemplates,$id)
    {
        $WabaIntance = WabaIntance::query()->where('project_id',$id)->get();
        foreach ($availableTemplates as $intance_id => $templates){
            $waba_intance = $WabaIntance->where('waba_id',$intance_id)->last();
            foreach ($templates as $template) {

                $data_template = [
                    'name' => $template['name'],
                    'project_id' => $id,
                    'code' => $template['language'],
                    'waba_intance_id' => $waba_intance->id,
                ];
                foreach ($template['components'] as $components) {
                    switch ($components['type']) {
                        case 'HEADER':
                            if ($components['format'] == 'TEXT'){
                                $data_template ['url_type'] =  null;
                                $data_template ['url_media'] = null;
                            }
                            if ($components['format'] == 'IMAGE' && isset($components['example']['header_handle'][0])) {
                                $data_template ['url_type'] = 'photo';
                                $url_media = $this->setPicture($components['example']['header_handle'][0],$id,$template['name']);
                                $data_template ['url_media'] = config('app.url').$url_media;
                            }
                            break;

                        case 'BODY':
                            $text = $components['text'];
                            $data_template['text'] = $text;
                            if (str_contains($text, '{{1}}')) {
                                foreach ($components['example']['body_text'][0] as $textVariable) {
                                    $textVariable = preg_replace('/\d{11}/', '#{phone}', $textVariable);

                                    $textVariable = str_replace('https://t.me/+_fakqdA8D1U5NWZi', '#{linkInviteIntensive}', $textVariable);
                                    $textVariable = str_replace('Maks', '#{name}', $textVariable);
                                    $data_template['variable']['body_text'][] = $textVariable;

                                }
                            }
                            break;

                        case 'BUTTONS':
                            foreach ($components['buttons'] as $button) {
                                if ($button['type'] != 'QUICK_REPLY' && isset($button['example'])) {
                                    foreach ($button['example'] as $example) {
                                        $commonPart = Str::beforeLast($button['url'], '{{');
                                        $commonPart = preg_replace('/\d{11}/', '#{phone}', $commonPart);
                                        $commonPart = str_replace('Maks', '#{name}', $commonPart);
                                        $example = preg_replace('/\d{11}/', '#{phone}', $example);
                                        $example = str_replace('Maks', '#{name}', $example);
                                        $secondPart = Str::afterLast($example, $commonPart);
                                        $secondPart = preg_replace('/\d{11}/', '#{phone}', $secondPart);
                                        $secondPart = str_replace('Maks', '#{name}', $secondPart);
                                        $data_template['variable']['buttons'][] = $secondPart;

                                    }
                                }
                            }
                            break;
                    }
                }

                DB::transaction(function() use($data_template,$id) {
                    $data_template ['url_type'] = $data_template ['url_type'] ?? null;
                    $data_template ['url_media'] = $data_template ['url_media'] ?? null;
                    $WabaTemplatesModel = WabaTemplates::updateOrCreate([
                        'project_id' => $id,
                        'name' => $data_template['name'],
                        'waba_intance_id' => $data_template['waba_intance_id'],
                    ],$data_template);
                    if (isset($data_template['variable'])){
                        foreach ($data_template['variable'] as $key => $variables) {
                            $count = 1;
                            switch ($key) {
                                case 'body_text':
                                    $type = 'text';
                                    break;
                                case 'buttons':
                                    $type = 'button';
                            }

                            if (!isset($type)){
                                continue;
                            }

                            foreach ($variables as $variable) {
                                WabaVariable::query()->updateOrCreate(
                                    [
                                        'project_id' => $id,
                                        'waba_template_id' => $WabaTemplatesModel->id,
                                        'count_variable' => $count,
                                        'type' => $type,
                                    ],[
                                        'value' => $variable
                                    ]
                                );
                                $count++;
                            }

                        }
                    }

                });
            }
        }
    }


    public function getWabaTemplatesInfo($waba_id,$templateName)
    {
        $wabaIntance = WabaIntance::query()->where('waba_id',$waba_id)->get()->first();
        $getSettingWaba = $wabaIntance->getSettingWaba;
        $accessToken = $getSettingWaba->token;
        if (empty($accessToken)){
            return false;
        }

// Запрос информации по шаблону
        $url = "https://graph.facebook.com/v19.0/$waba_id/message_templates";
        $responseTemplate = Http::withToken($accessToken)
            ->get($url, [
                    'name' => $templateName,
        ]);
        $data[$waba_id] = $responseTemplate->json()['data'] ?? $responseTemplate->json();

        return $data;
    }

     function setPicture($url, $id, $pictureName)
    {
        $outputDirectory = "public/project/$id/waba/picture/";

        if (!Storage::exists($outputDirectory)) {
            Storage::makeDirectory($outputDirectory);
        }

        $filename = $outputDirectory . $pictureName . '.jpg';

        if (!Storage::exists($filename)) {
            $fileContents = file_get_contents($url);
            Storage::put($filename, $fileContents);
        }

        return Storage::url($filename);
    }


}
