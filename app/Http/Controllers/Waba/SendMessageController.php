<?php

namespace App\Http\Controllers\Waba;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\Services\SmsController;
use App\Http\Controllers\Notification\VariableController;
use App\Jobs\Waba\addQueue;
use App\Models\Waba\WabaHistory;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\WabaLinkNumber;
use App\Models\Waba\wabaSetting;
use App\Models\Waba\WabaTemplates;
use App\Traits\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SendMessageController extends Controller
{
    use Notification;

    public $token;
    public $wabaTemplateVariable;
    public $WabaIntanceModel;

    public function testMessage($waba_templates_id){
        $phone = '789181275931';
//        SendMessege::dispatch($waba_templates_id,$phone);
        $this->sendMessageWaba($waba_templates_id,$phone);
    }


    public function updateStore($id)
    {
        $this->updateWabaTemplatesStore($id);
        return back();
    }

     public function SendMessageWabaIndex($id){

        $wabaTemplates = WabaTemplates::query()->where('project_id',$id)->get();
        if (!isset(WabaSetting::query()->where('project_id',$id)->get()->last()->token)) {
            return redirect()->route('facebookEnter.index',$id);}

        $wabaTemplates = WabaTemplates::query()->where('project_id',$id)->get();
        $pipeline_steps_id = $wabaTemplates->unique('name');
        $wabaIntance = WabaIntance::query()->where('project_id',$id)->get();
        $intances_id = $wabaIntance;
        return view('waba.WabaSendMessage',compact('id','pipeline_steps_id','intances_id'));
    }



    public function SendMessageWabaStore(Request $request, $id)
    {

        $request->validate([
            'name' => 'required',
            'phone' => 'required',
        ]);

        // Инициализация массива для хранения переменных
        $variables = [];

        // Предполагаем, что $request содержит данные формы
        foreach ($request->all() as $key => $value) {
            // Проверка, начинается ли ключ с "variable_"
            if (strpos($key, 'variable_') === 0) {
                // Извлечение типа и номера переменной
                preg_match('/variable_(text|button)_(\d+)/', $key, $matches);

                if (!empty($matches)) {
                    // matches[1] содержит тип (text или button)
                    // matches[2] содержит count_variable
                    $type = $matches[1];
                    $countVariable = $matches[2];

                    // Добавление переменной в массив
                    $variables[] = [
                        'type' => $type,
                        'count_variable' => $countVariable,
                        'value' => $value,
                    ];
                }
            }
        }

        $variablesJson = json_encode($variables);

        $template_name = $request->name;
        $phones = $this->getPhone($request->phone);
        $phones = collect($phones)->unique();
        $phoneses = $phones->chunk(100);

        $timeSend = $request->timeSend ?? now();
        $waba_phone_ids = $request->waba_phone_ids;

        foreach ($phoneses as $array) {
          addQueue::dispatch($template_name, $waba_phone_ids, $array->toArray(), $timeSend, $id, $variablesJson);
//            $NotificationController = new WabaMessageController();
//            $NotificationController->sendNowMessage($template_name,$waba_phone_ids,$array->toArray(),$timeSend,$id,$variablesJson);
        }
            return back();

    }


    public function sendMessageWabaClass($waba_templates_id, $phone,$variablesJson = null){
        $wabaTemplates = WabaTemplates::query()->find($waba_templates_id);
        $getWabaIntance = $this->WabaIntanceModel = $wabaTemplates->getWabaIntance;

        $data = $this->getDataWaba($phone, $getWabaIntance,$wabaTemplates,$variablesJson);
        $response = $this->sendRequest($data, $getWabaIntance, $wabaTemplates);

        if (isset($response['messages'][0]['message_status']) && $response['messages'][0]['message_status'] == 'accepted') {
            $this->setWabaHistory($wabaTemplates,$response,$getWabaIntance);
        }

        if (isset($response['error']) && !isset($response['messages'][0]['message_status'])){
            $reserveSmsModel = $wabaTemplates->getReserveSms;
            if (!empty($reserveSmsModel)){
                $SmsController = new SmsController();
                $SmsController->sendReserveSms($reserveSmsModel,$wabaTemplates,$phone);
            }

        }

    }

    public function getDataWaba($phone,$getWabaIntance,$wabaTemplates,$variablesJson = null){
        $wabaVariable = $this->wabaTemplateVariable = $wabaTemplates->getVariable->where('project_id',$wabaTemplates->project_id);
        $data = [
            'messaging_product' => "whatsapp",
            'to' => "$phone",
            'type' => "template",
            "recipient_type" => "individual",
            'template' => [
                "namespace" => "$getWabaIntance->name_space",
                "name" => "$wabaTemplates->name",
                "language" => [
                    'code' => "$wabaTemplates->code"
                ],
            ],
        ];

        if ($wabaTemplates->url_type == 'photo') {
            $data['template']['components'][] =
                [
                    'type' => "header",
                    "parameters" => [[
                        "type" => "image",
                        "image" => ['link' => "$wabaTemplates->url_media"]
                    ]]
                ];
        }

        if (count($wabaVariable) > 0) {
            $index = 0;

            if ($variablesJson !== null) {
                $variable_json_decode = json_decode($variablesJson, true);
                if (json_last_error() !== JSON_ERROR_NONE) {
                    $variable_json_decode = [];
                }
            } else {
                $variable_json_decode = [];
            }
            foreach ($wabaVariable as $variable_template) {
                $matchedVariables = array_filter($variable_json_decode, function ($variable) use ($variable_template) {
                    return isset($variable['type'], $variable['count_variable']) &&
                        $variable['type'] == $variable_template->type && $variable['count_variable'] == $variable_template->count_variable;
                });

                if (!empty($matchedVariables)) {
                    $variable_template->value = reset($matchedVariables)['value'];
                }

                $VariableController = new VariableController();

                $text = $VariableController->setVariablesWaba($variable_template->value,$phone,$wabaTemplates->project_id);
                if ($variable_template->type =='text'){
                    $data['template']['components'][] =
                        [
                            'type' => "body",
                            "parameters" => [[
                                "type" => "text",
                                "text" => $text
                            ]]
                            ,
                        ];
                }
                if ($variable_template->type =='button'){
                    $data['template']['components'][] =
                        [
                            "type" => "button",
                            "index" => "$index",
                            "sub_type" => "url",
                            "parameters" => [
                                [
                                    "type" => "text",
                                    "text" => "$text"
                                ]
                            ]
                        ];
                    $index++;
                }
            }

        }
        return $data;
    }

    public function sendRequest($data,$getWabaIntance,$wabaTemplates){
        $wabaSetting = $wabaTemplates->getSetting;
        $waba_phone_id = $getWabaIntance->waba_phone_id;
        $token = $this->token = $wabaSetting->token;

        $url = "https://graph.facebook.com/v19.0/$waba_phone_id/messages";
        $response = Http::withHeaders([
            "Authorization" => "Bearer " . $token,
            "content-type" => "application/json",
        ])->post($url, $data);

        return $response->json();
    }
    public function setWabaHistory($wabaTemplates,$response,$getWabaIntance){

        WabaHistory::query()->create([
            'project_id' => $wabaTemplates->project_id,
            'phone' => $response['contacts'][0]['wa_id'],
            'type' => 'outgoing',
            'message' => "$wabaTemplates->name",
            'status' => $response['messages'][0]['message_status'],
            'w_id' => $response['messages'][0]['id'],
            'display_phone' => $getWabaIntance->phone,
        ]);

        WabaLinkNumber::query()->create([
            'project_id' => $wabaTemplates->project_id,
            'phone' => $response['contacts'][0]['wa_id'],
            'waba_intance_id' => $getWabaIntance->id,
        ]);
    }


}
