<?php

namespace App\Http\Controllers\Waba;

use App\Http\Controllers\Controller;
use App\Models\Facebook\FacebookSettingApi;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\wabaSetting;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use libphonenumber\PhoneNumberUtil;

class SettingController extends Controller
{
    public function wabaToken(Request $request,$id){

        $wabaToken = $request->wabaToken ?? '';
        wabaSetting::query()->where('project_id',$id)->updateOrCreate(
            ['project_id' => $id],
            ['token' => $wabaToken]
        );
        return back();
    }
    public function redirectOauth(Request $request,$id){

        return 200;
    }

    function getAccessToken($id)
    {
        return WabaSetting::query()->where('project_id',$id)->first()->token ?? null;
    }


    public function settingWaba($id)
    {
        $wabaSetting = wabaSetting::query()->where('project_id',$id)->exists();
        if (!$wabaSetting){
            return redirect()->route('facebookEnter.index',compact('id'));
        }
        $wabaIntance = WabaIntance::query()->where('project_id',$id)->get();

        return view('waba.WabaSetting',compact('id','wabaIntance'));
    }
    public function saveToken($access_token,$id)
    {

        $wabaToken = wabaSetting::query()->updateOrCreate([
            'project_id' => $id,
        ],[
            'token' => $access_token,
        ]);
        return 200;
    }

    public function exchangeToken(Request $request)
    {
        $code = $request->code;
        $id = $request->id;

        // Запрос на обмен code на accessToken через Facebook API
        $response = Http::get('https://graph.facebook.com/v19.0/oauth/access_token', [
            'client_id' => '908829377361820',
            'client_secret' => '46115f3bc715543f1141cf7a690748a4',
            'code' => $code,
        ]);

        $access_token = $response->json();
        $this->saveToken($access_token['access_token'],$id);

        return redirect()->route('setBM.index',$id);
    }

    public function setBM($id)
    {
      return view('facebook.settingWaba.setBM',['id' => $id]);
    }

    public function setWabaId(Request $request,$id)
    {
        $validated = $request->validate([
            'owner_business_id' => 'required|min:2',
        ], [
            'owner_business_id' => 'Обязательно для заполнения',
        ]);

        $owner_business_id = $request->owner_business_id;
        $getWababyBMid = $this->getWababyBMid($id,$owner_business_id);
        $waba_data = $getWababyBMid['data'];

        return view('facebook.settingWaba.setWabaId',['id' => $id,'owner_business_id' => $owner_business_id,'waba_data' => $waba_data]);
    }
    public function certificateUpdatePhone($id,$waba_phone_id)
    {

        $accessToken = $this->getAccessToken($id);
        $WabaIntance = WabaIntance::query()->where('project_id',$id)->first()->get();

        $clientData = [
            'countryName' => 'RU',
            'stateOrProvinceName' => 'Moscow',
            'localityName' => 'Moscow',
            'organizationName' => 'ИП Купряхина Юлия Викторовна',
            'organizationalUnitName' => 'IT Department',
            'commonName' => 'glushko-sok.ru',
            'emailAddress' => 'glushko-sok@yandex.ru'
        ];

        $csrContent = $this->createAndStoreCSR($clientData,$id);
        $uploadCertificate = $this->uploadCertificate($accessToken, $waba_phone_id, $csrContent);
        dd($uploadCertificate);
//        return redirect()->route('settingWaba.index',$id);

        return view('facebook.settingWaba.verificationCode',compact('waba_id'));

    }

    public function verificationCode(Request $request,$id)
    {

        $waba_phone_id = $request->phone_id;
        $accessToken = $this->getAccessToken($id);
        $WabaIntance = WabaIntance::query()->where('waba_phone_id',$waba_phone_id)->get()->first();
        $registerResponse = $this->registerPhoneNumber($accessToken, $WabaIntance->waba_phone_id, $WabaIntance->phone);
        if (isset($registerResponse['success']) && $registerResponse['success'] == 'true'){
            $WabaIntance->update(['active' => 1]);
        }
        $this->subscribed_apps($accessToken,$WabaIntance->waba_id);
        return back();
    }

    public function subscribed_apps($accessToken,$waba_id)
    {
        $registerUrl = "https://graph.facebook.com/v19.0/{$waba_id}/subscribed_apps";
        Http::withToken($accessToken)
            ->post($registerUrl);
    }
    public function verificationCodeStore(Request $request,$id)
    {
        $accessToken = $this->getAccessToken($id);
        $WabaIntance = WabaIntance::query()->where('project_id',$id)->first()->get();
        $phoneNumberId = $WabaIntance->waba_phone_id;
        $verifyPhoneUrl = "https://graph.facebook.com/v19.0/{$phoneNumberId}/verify";

        $data = [
            'code' => $request->verificationCode,
        ];

        $options = [
            'http' => [
                'header' => "Authorization: Bearer {$accessToken}\r\nContent-Type: application/json\r\n",
                'method' => 'POST',
                'content' => json_encode($data),
            ],
        ];

        $context = stream_context_create($options);
        $result = file_get_contents($verifyPhoneUrl, false, $context);

        if ($result === false) {
            throw new Exception('Error verifying phone number');
        }

        $response = json_decode($result, true);
        return $response;
    }

    public function setWabaIdStore(Request $request,$id)
    {
        $validated = $request->validate([
            'owner_business_id' => 'required|min:2',
        ], [
            'owner_business_id' => 'Обязательно для заполнения',
        ]);

        $owner_business_id = $request->owner_business_id;
        $waba_id = $request->waba_id;

        $getWababyBMid = $this->getWababyBMid($id,$owner_business_id);
        $waba_data = $getWababyBMid['data'] ?? null;
        $waba_id_data = collect($waba_data)->where('id',$waba_id)->first();
        $data = [
            'project_id' => $id,
            'owner_business_id' => $owner_business_id,
            'name_space' => $waba_id_data['message_template_namespace'] ?? '',
            'name_waba' => $waba_id_data['name'] ?? '',
            'waba_id' => $waba_id,
        ];

        $phoneData = $this->getPhoneByWabaid($id,$waba_id);
        $phoneData = $phoneData[0];
        $data ['phone'] = preg_replace('/\D+/', '', $phoneData['display_phone_number']);
        $data ['waba_phone_id'] = $phoneData['id'];

        WabaIntance::query()->updateOrCreate([
            'project_id' => $id,
            'waba_phone_id' => $data ['waba_phone_id'],
        ],$data);

        return redirect()->route('WabaMessageController.index',$id);
    }
    
    public function getWababyBMid($id,$bmId)
    {
        $accessToken = WabaSetting::query()->where('project_id',$id)->get()->last()->token;
        if ($accessToken){
            // Получение списка бизнес-менеджеров
            $response = Http::withToken($accessToken)->get("https://graph.facebook.com/v19.0/{$bmId}/owned_whatsapp_business_accounts", [
                'access_token' => $accessToken,
            ]);
            return $response->json();
        } else {
            return null;
        }
    }



    public function getPhoneByWabaid($id,$waba_id)
    {
        $accessToken = WabaSetting::query()->where('project_id',$id)->get()->last()->token;
        if ($accessToken){
            // Получение списка бизнес-менеджеров
            $response = Http::withToken($accessToken)->get("https://graph.facebook.com/v19.0/{$waba_id}/phone_numbers", [
                'access_token' => $accessToken,
            ]);

            return $response->json()['data'] ?? null;
        } else {
            return null;
        }


    }

    public function facebookEnter(Request $request,$id){
        return view('facebook.enter',compact('id'));
    }

    public function createAndStoreCSR($clientData,$id)
    {
        // Конфигурация для CSR
        $config = [
            "digest_alg" => "sha256",
            "private_key_bits" => 2048,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        ];

        // Создание нового приватного ключа
        $privateKey = openssl_pkey_new($config);

        // Данные клиента для CSR
        $dn = [
            "countryName" => $clientData['countryName'], // Пример: "RU"
            "stateOrProvinceName" => $clientData['stateOrProvinceName'], // Пример: "Moscow"
            "localityName" => $clientData['localityName'], // Пример: "Moscow"
            "organizationName" => $clientData['organizationName'], // Пример: "Client Company"
            "organizationalUnitName" => $clientData['organizationalUnitName'], // Пример: "IT Department"
            "commonName" => $clientData['commonName'], // Пример: "example.com"
            "emailAddress" => $clientData['emailAddress'], // Пример: "client-email@example.com"
        ];

        $csr = openssl_csr_new($dn, $privateKey, $config);

        // Экспорт приватного ключа и CSR в строки
        openssl_pkey_export($privateKey, $privateKeyOut);
        openssl_csr_export($csr, $csrOut);

        $outputDirectory = "public/project/$id/waba/certificates/";

        if (!Storage::exists($outputDirectory)) {
            Storage::makeDirectory($outputDirectory);
        }

        // Сохранение приватного ключа и CSR в хранилище Laravel
        Storage::put($outputDirectory .'private.key', $privateKeyOut);
        Storage::put($outputDirectory . 'certificates/request.csr', $csrOut);

        // Чтение содержимого CSR для отправки через API
        $csrContent = Storage::get( $outputDirectory . 'certificates/request.csr');

        return $csrContent;
    }

    function uploadCertificate($accessToken, $phoneNumberId, $certificateContent)
    {
        $uploadCertUrl = "https://graph.facebook.com/v19.0/{$phoneNumberId}/certificate";

        $response = Http::withToken($accessToken)
            ->post($uploadCertUrl, [
                'certificate' => $certificateContent,
            ]);

        return $response->json();
    }

    function verifyPhoneNumber($accessToken, $phoneNumberId, $verificationCode)
    {
        $verifyPhoneUrl = "https://graph.facebook.com/v19.0/{$phoneNumberId}/verify";

        $data = [
            'code' => $verificationCode,
        ];

        $options = [
            'http' => [
                'header' => "Authorization: Bearer {$accessToken}\r\nContent-Type: application/json\r\n",
                'method' => 'POST',
                'content' => json_encode($data),
            ],
        ];

        $context = stream_context_create($options);
        $result = file_get_contents($verifyPhoneUrl, false, $context);

        if ($result === false) {
            throw new Exception('Error verifying phone number');
        }

        $response = json_decode($result, true);
        return $response;
    }


    function registerPhoneNumber($accessToken, $waba_phone_id, $phoneNumber)
    {
        $registerUrl = "https://graph.facebook.com/v19.0/{$waba_phone_id}/register";
        $phoneUtil = PhoneNumberUtil::getInstance();
        $numberProto = $phoneUtil->parse($phoneNumber, "ru");
        $countryCode = $numberProto->getCountryCode();

        $phoneNumberWithoutCountryCode = preg_replace('/^\+?' . $countryCode . '/', '', $phoneNumber);

        $response = Http::withToken($accessToken)
            ->post($registerUrl, [
                'messaging_product' => 'whatsapp', // или 'VOICE'
                'pin' => '316091',
            ]);


        return $response->json();
    }

}
