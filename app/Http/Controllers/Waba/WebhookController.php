<?php

namespace App\Http\Controllers\Waba;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\Services\SmsController;
use App\Http\Controllers\Notification\VariableController;
use App\Jobs\Notification\SmsAero;
use App\Jobs\Waba\WabaSendMessegeCallback;
use App\Jobs\Waba\WabaSendMessegeOne;
use App\Models\BlackList;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Waba\WabaButton;
use App\Models\Waba\WabaHistory;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\WabaLinkNumber;
use App\Models\Waba\WabaMessage;
use App\Models\Waba\WabaQueue;
use App\Models\Waba\WabaTemplates;
use App\Traits\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Throwable;

class WebhookController extends Controller
{
    use Notification;
    public function webhook(Request $request,$id){
        try {
            if (isset($request->hub_mode) && $request->hub_mode === 'subscribe'){
                $challenge = $this->subscribeWebhook($request);
                if ($challenge){
                    return response($challenge, 200);
                }
            }

            if ($request->entry[0]['changes'][0]['field'] == 'messages'){

                $wabeRequest = $request->entry[0]['changes'][0]['value'];
                // статус сообщения


                if (isset($wabeRequest['statuses'])) {
                    $w_id = $request->entry[0]['changes'][0]['value']['statuses'][0]['id'];
                    $status = $request->entry[0]['changes'][0]['value']['statuses'][0]['status'];
                    WabaHistory::query()->where('w_id', $w_id)->update(['status' => $status]);

                    if ($status == 'failed') {
                        $this->filedWabaSmsSend($w_id);
                    }
                }

                // входящее сообщения
                if (isset($wabeRequest['messages'])){
                    $phone_number_id = $wabeRequest['metadata']['phone_number_id'];
                    $WabaIntance = WabaIntance::query()->where('waba_phone_id',$phone_number_id)->get()->first();
                    $display_phone_number = $WabaIntance->phone;
                 // текстовое сообщение
                    if(isset($wabeRequest['messages'][0]['type']) == 'request_welcome'){
                        $phone =  $wabeRequest['messages'][0]['from'];
                        $w_id =  $wabeRequest['messages'][0]['id'];

                        // временно убираем из-за одинаковых номеров на проекты VM и мой
//                        $this->newRequestWelcome($wabeRequest['messages'][0]['from'],$id);
                        $buttonHistory = WabaHistory::query()->create([
                            'project_id' => $WabaIntance->project_id,
                            'phone' => $phone,
                            'type' => 'request_welcome',
                            'message' => 'request_welcome',
                            'status' => 'request_welcome',
                            'w_id' => $w_id,
                            'display_phone' => $display_phone_number,
                        ]);

                        WabaLinkNumber::query()->updateOrCreate([
                            'project_id' => $WabaIntance->project_id,
                            'phone' => $phone,
                        ],[
                            'waba_intance_id' => $WabaIntance->id
                        ]);
                    }

                    if (isset($wabeRequest['messages'][0]['text'])){
                        $text = $wabeRequest['messages'][0]['text']['body'];
                        $phone =  $wabeRequest['messages'][0]['from'];
                        $w_id =  $wabeRequest['messages'][0]['id'];

                       $buttonHistory = WabaHistory::query()->create([
                            'project_id' => $WabaIntance->project_id,
                            'phone' => $phone,
                            'type' => 'in',
                            'message' => $text,
                            'status' => 'in',
                            'w_id' => $w_id,
                            'display_phone' => $display_phone_number,
                        ]);

                        WabaLinkNumber::query()->updateOrCreate([
                            'project_id' => $WabaIntance->project_id,
                            'phone' => $phone,
                        ],[
                            'waba_intance_id' => $WabaIntance->id
                        ]);

                       $this->buttonResponce($buttonHistory);

                       $wordsToCheck = ['ебал', 'в суд', 'заебали', 'полиции', 'Удалите мои', 'хуй', 'ФАС', 'прокуратуру', 'Статья 137', 'твари', 'пидоры'];
                       $lowercaseText = mb_strtolower($text); // Приводим текст к нижнему регистру для кейс-инсенситивной проверки

                       $containsBannedWords = collect($wordsToCheck)->contains(function ($word) use ($lowercaseText) {
                           return str_contains($lowercaseText, mb_strtolower($word));
                       });


                       if ($containsBannedWords) {
                       BlackList::query()->updateOrCreate([
                           'project_id' => $WabaIntance->project_id,
                           'phone' => $phone,
                       ],[
                           'project_id' => $WabaIntance->project_id,
                           'phone' => $phone,
                       ]);
                       WebNotificationSchedule::query()->where(['project_id' => $WabaIntance->project_id,'phone' => $phone])->delete();
                       WabaQueue::query()->where(['project_id' => $WabaIntance->project_id,'phone' => $phone])->delete();
                       }

                   }
                 // кнопки
                    if (isset($wabeRequest['messages'][0]['button'])){
                        $button= $wabeRequest['messages'][0]['button']['text'];
                        $phone =  $wabeRequest['messages'][0]['from'];
                        $w_id =  $wabeRequest['messages'][0]['id'];

                        $buttonHistory = WabaHistory::query()->create([
                            'project_id' => $WabaIntance->project_id,
                            'phone' => $phone,
                            'type' => 'in_button',
                            'message' => $button,
                            'status' => 'in',
                            'w_id' => $w_id,
                            'display_phone' => $display_phone_number,
                        ]);
                        $this->buttonResponce($buttonHistory);
                    }
                }
            }

        } catch (Throwable $e) {
        report($e);
            return 204;
        }
                return 200;
            }

    public function responseSetting($id)
    {
        return view('waba.responseWaba.responseSetting',compact('id'));
    }
    public function subscribeWebhook($request){
        // Адаптированный код под Laravel
        $verify_token = 'GLUSHKO';

        // Получить параметры из запроса на проверку вебхука
        $mode = $request->hub_mode ?? '';
        $token = $request->hub_verify_token ?? '';
        $challenge = $request->hub_challenge ?? '';

        // Проверить, что были отправлены токен и режим
        if ($mode && $token) {
            // Проверить, что отправленные токен и режим верны
            if ($mode === 'subscribe' && $token === $verify_token) {
                // Ответить с 200 OK и токеном вызова из запроса
                return $challenge;
            } else {
                // Ответить с '403 Forbidden', если токены не совпадают
                return false;
            }
        }
        return 200;
    }

    public function buttonResponce($buttonHistory){
        $ButtonText = $buttonHistory->message;

        if ($ButtonText == 'Не присылать рекламу' || $ButtonText == 'Мне не интересно') {
            BlackList::query()->updateOrCreate([
                'project_id' => $buttonHistory->project_id,
                'phone' => $buttonHistory->phone,
            ],[
            'project_id' => $buttonHistory->project_id,
            'phone' => $buttonHistory->phone,
                ]);
            WebNotificationSchedule::query()->where(['project_id' => $buttonHistory->project_id,'phone' => $buttonHistory->phone])->delete();
            WabaQueue::query()->where(['project_id' => $buttonHistory->project_id,'phone' => $buttonHistory->phone])->delete();
        }

        $WabaMessage = WabaMessage::query()->where('project_id',$buttonHistory->project_id)->where('callback',$ButtonText)->get()->first();
        if(empty($WabaMessage)){
            return false;
        }
        $this->sendMessageWabaResponce($WabaMessage->id,$buttonHistory->phone,$buttonHistory->display_phone);
        return true;
    }

    public function filedWabaSmsSend($w_id){
        $wabaHistory = WabaHistory::query()->where('w_id',$w_id)->get()->last();
        $wabaTemplatesName = $wabaHistory->message;
        $wabaTemplatesModel = WabaTemplates::query()->where('project_id',$wabaHistory->project_id)->where('name',$wabaTemplatesName)->first();
        $reserveSmsModel = $wabaTemplatesModel->getReserveSms;

        if (!empty($reserveSmsModel)){
            $SmsController = new SmsController();
            $SmsController->sendReserveSms($reserveSmsModel,$wabaTemplatesModel,$wabaHistory->phone);
        }

    }

    public function sendMessageWabaResponce($message_id, $phone, $display_phone)
    {
        $wabaMessage = WabaMessage::query()->find($message_id);

        $wabaMessageController = new WabaMessageController();
        $getButtons = $wabaMessage->getButtons;

        $variableController = new VariableController();
        $text = $variableController->setVariablesWaba($wabaMessage->text,$phone,$wabaMessage->project_id);

        if (!empty($getButtons->first())){
            foreach ($getButtons as $button){
                $createButton[] = $wabaMessageController->createButton($button->text,$button->callback);
            }
        } else {
            $createButton = [];
        }

        if (!empty($wabaMessage->url_type) && !empty($wabaMessage->url_media)){
                $media = [
                    'type' => $wabaMessage->url_type,
                    'url_media' => $wabaMessage->url_media,
                ];  }
                else{
                    $media = null;
            }

//        $wabaMessageController->sendMessageCallback($display_phone,$phone,$text,$wabaMessage->project_id,$media,$createButton);
        WabaSendMessegeCallback::dispatch($display_phone,$phone,$text,$wabaMessage->project_id,$media,$createButton);
    }

    public function newRequestWelcome($phone,$project_id)
    {
        $newLeadWeb = NewLeadWeb::query()->where('phone',$phone)->where('project_id',$project_id)->get();
        if (count($newLeadWeb) > 0){
            $sevenDaysAgo = Carbon::parse($newLeadWeb->last()->created_at)->subDays(30);

            if ($sevenDaysAgo->lessThan($sevenDaysAgo)) {
                return 200;
            }  else {
                $newLeadWeb = $newLeadWeb->whereNotNull('utm_source')->last();
                NewLeadWeb::query()->create([
                    'project_id' => $project_id,
                    'phone' => $phone,
                    'addWeb' => 0,
                    'utm_source' => $newLeadWeb->utm_source ?? null,
                    'utm_medium' => $newLeadWeb->utm_medium ?? null,
                    'date' => now()->format('Y-m-d')
                ]);
            }
        } else {
            $WabaHistory = WabaHistory::query()->where('phone',$phone)->where('project_id',$project_id)->get()->last();
            if (!empty($WabaHistory)){
                $sevenDaysAgo = Carbon::parse($WabaHistory->created_at)->subDays(30);
                if ($sevenDaysAgo->lessThan($sevenDaysAgo)) {
                    return 200;
                }  else {
                    $newLeadWeb = $newLeadWeb->whereNotNull('utm_source')->last();

                    NewLeadWeb::query()->create([
                        'project_id' => $project_id,
                        'phone' => $phone,
                        'addWeb' => 0,
                        'utm_source' => $newLeadWeb->utm_source ?? null,
                        'utm_medium' => $newLeadWeb->utm_medium ?? null,
                        'date' => now()->format('Y-m-d')
                    ]);
                }
            }

              NewLeadWeb::query()->create([
                'project_id' => $project_id,
                'phone' => $phone,
                'addWeb' => 0,
                'utm_source' => 'wa',
                'utm_medium' => 'wa',
                'date' => now()->format('Y-m-d')
            ]);
        }
        return 200;
    }

}
