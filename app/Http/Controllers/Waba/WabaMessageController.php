<?php

namespace App\Http\Controllers\Waba;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Models\Waba\WabaHistory;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\WabaLinkNumber;
use App\Models\Waba\WabaMessage;
use App\Models\Waba\WabaQueue;
use App\Models\Waba\wabaSetting;
use App\Models\Waba\WabaTemplates;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class WabaMessageController extends Controller
{
    public function index($id){
        return view('waba.WabaDialog',compact('id'));
    }

    public function saveMedia(Request $request,$id)
    {
        $message_id = $request->message_id;

        if (isset($request->replace) && $request->replace == 'yes'){
            $wabaMessage = \App\Models\Waba\WabaMessage::find($message_id);
            // Получаем относительный путь от URL
            $relativePath = Str::after($wabaMessage->url_media, 'storage/');
            // Удаляем файл
            Storage::delete('public/' . $relativePath);
        }
        $name = now()->timestamp . '.jpg';
        $request->file('media')->storeAs("public/project/$id/waba/picture/",$name);

        $url = config('app.url') . '/' . "storage/project/$id/waba/picture/" . $name;
        WabaMessage::query()->find($message_id)->update(['url_media' => $url,'url_type' => 'image']);
        return back();
    }

    public function deleteMedia(Request $request)
    {
        $message_id = $request->message_id;
        $wabaMessage = \App\Models\Waba\WabaMessage::find($message_id);

        // Получаем относительный путь от URL
        $relativePath = Str::after($wabaMessage->url_media, 'storage/');

        // Удаляем файл
//        $delete = Storage::get('public/' . $relativePath);
       $delete = Storage::delete('public/' . $relativePath);

        if ($delete && $wabaMessage) {
            $wabaMessage->url_media = null;
            $wabaMessage->url_type = null;
            $wabaMessage->save();
        }
        WabaMessage::query()->find($message_id)->update(['url_media' => null,'url_type' => null]);
        return back();

    }

    public function sendMessageWabaOne($phone,$text,$id,$media = null,$buttons = null){
        $instance_model = $this->getWabaIntanceFromPhone($phone,$id);
        $waba_phone_id = $instance_model->waba_phone_id;
        $wabaSetting = wabaSetting::query()->where('project_id', $id)->get()->first();
        $token = $wabaSetting->token;

        $data = [
            'messaging_product' => "whatsapp",
            'to' => $phone,
            'recipient_type' => "individual",
        ];

        if ($media) {
            $data['type'] = $media['type'];
            $data['image'] = [
                "link" => $media['url_media'],
                "caption" => $text,
            ];
        } elseif (!empty($buttons)) {
            $data['type'] = "interactive";
            $data['interactive'] = [
                "type" => "button",
                "body" => [
                    "text" => $text,
                ],
                "action" => [
                    "buttons" => $buttons
                ]
            ];
        } else {
            $data['type'] = "text";
            $data['text'] = [
                "preview_url" => "true",
                "body" => $text,
            ];
        }

        $url = "https://graph.facebook.com/v19.0/$waba_phone_id/messages";
        $response = Http::withHeaders([
            "Authorization" => "Bearer " . $token,
            "content-type" => "application/json",
        ])->post($url, $data);
        $response = $response->json();

        if (isset($response['messages'][0]['id'])) {

            WabaHistory::query()->create([
                'project_id' => $instance_model->project_id,
                'phone' => $response['contacts'][0]['wa_id'],
                'type' => 'outgoing',
                'message' => "$text",
                'status' => 'send',
                'w_id' => $response['messages'][0]['id'],
                'display_phone' => $instance_model->phone,
            ]);
        }

    }
    public function sendMessageCallback($display_phone,$phone,$text,$id,$media = null,$buttons = null){
        $instance_model = WabaIntance::query()->where('project_id',$id)->where('phone',$display_phone)->get()->first();
        $waba_phone_id = $instance_model->waba_phone_id;
        $wabaSetting = wabaSetting::query()->where('project_id', $id)->get()->first();
        $token = $wabaSetting->token;

        $data = $this->createMessageData($phone, $text, $media, $buttons);

        $url = "https://graph.facebook.com/v19.0/$waba_phone_id/messages";
        $response = Http::withHeaders([
            "Authorization" => "Bearer " . $token,
            "content-type" => "application/json",
        ])->post($url, $data);
        $response = $response->json();

        if (isset($response['messages'][0]['id'])) {

            WabaHistory::query()->create([
                'project_id' => $instance_model->project_id,
                'phone' => $response['contacts'][0]['wa_id'],
                'type' => 'outgoing',
                'message' => "$text",
                'status' => 'send',
                'w_id' => $response['messages'][0]['id'],
                'display_phone' => $instance_model->phone,
            ]);
        }

    }

    function getWabaIntanceFromPhone($phone,$id){
        $WaSendMessage = WabaLinkNumber::query()->where('project_id', $id)->where('phone', $phone)->get()->last();
        $webWaNumber = WabaIntance::query()->where('project_id', $id)->get();
        if (isset($WaSendMessage->waba_intance_id)) {
            $instance = $WaSendMessage->waba_intance_id;
            $instance_id_model = $webWaNumber->where('id', $instance)->first();
            if (!empty($instance_id_model)) {
                return $instance_id_model;
            } else {
                return $webWaNumber->sortBy('count_today')->first();
            }
        } else {

            return $webWaNumber->sortBy('count_today')->first();
        }

    }

    public function createButton($text,$callback) {
            return [
                "type" => "reply",
                "reply" => [
                    "id" => $callback, // Генерация уникального ID для кнопки
                    "title" => $text
                ]
            ];
        }


    function checkWabaIntances($template_name,$waba_phone_ids,$id){
        $waba_intance_id = WabaTemplates::query()->where('project_id',$id)->where('name',$template_name)->pluck('waba_intance_id')->toArray();

        $query = WabaIntance::query()->whereIn('id', $waba_intance_id);
        if (!is_null($waba_phone_ids)) {
            $query->whereIn('waba_phone_id', $waba_phone_ids);
        }
        return $query->get();

    }

    public function createMessageData($phone, $text, $media = null, $buttons = null) {
        $data = [
            'messaging_product' => "whatsapp",
            'to' => $phone,
            'recipient_type' => "individual",
        ];

        if ($media && $buttons) {
            $data['type'] = "interactive";
            $data['interactive'] = [
                "type" => "button",
                "body" => [
                    "text" => $text,
                ],
                "action" => [
                    "buttons" => $buttons
                ],
                "header" => [
                    "type" => $media['type'],
                    "image" => [
                        "link" => $media['url_media']
                    ]
                ]
            ];
        } elseif ($media) {
            $data['type'] = $media['type'];
            $data['image'] = [
                "link" => $media['url_media'],
                "caption" => $text,
            ];
        } elseif ($buttons) {
            $data['type'] = "interactive";
            $data['interactive'] = [
                "type" => "button",
                "body" => [
                    "text" => $text,
                ],
                "action" => [
                    "buttons" => $buttons
                ]
            ];
        } else {
            $data['type'] = "text";
            $data['text'] = [
                "preview_url" => "true",
                "body" => $text,
            ];
        }

        return $data;
    }


    public function getWabaIntance($phone, $wabaIntances, $id)
    {
        $WaSendMessage = WabaLinkNumber::query()->where('project_id', $id)->where('phone', $phone)->get()->last();
        $webWaNumber = WabaIntance::query()->where('project_id', $id)->whereIn('waba_phone_id', $wabaIntances->pluck('waba_phone_id'))->get();
        if (isset($WaSendMessage->waba_intance_id)) {
            $instance = $WaSendMessage->waba_intance_id;
            $instance_id_model = $webWaNumber->where('id', $instance)->first();
            if (!empty($instance_id_model)) {
                return $instance_id_model;
            } else {
                return $webWaNumber->sortBy('count_today')->first();
            }
        } else {
            return $webWaNumber->sortBy('count_today')->first();
        }

    }
    public function getTemplate($instance_model,$template_name){
        return WabaTemplates::query()->where('waba_intance_id',$instance_model->id)->where('name',$template_name)->get()->last();
    }
    public function sendMessageStore(Request $request,$id){

        $request->validate([
            'text' => 'required',
            'phone' => 'required',
        ],[
            'text' => 'text обязательно для заполнения',
            'phone' => 'phone обязательно для заполнения',
        ]);
        $phone = $request->phone;
        $text = $request->text;

        $this->sendMessageWabaOne($phone,$text,$id);
    }


    // мспользуется для тестов
    public function sendNowMessage($template_name, $waba_intances, $phones, $timeSend, $id,$variablesJson = null){
        $WabaMessageController = new WabaMessageController();

        $wabaIntances = $WabaMessageController->checkWabaIntances($template_name, $waba_intances,$id);
        $CheckRussiaPhoneController = new CheckRussiaPhoneController();
        $phones = $CheckRussiaPhoneController->checkBlackList($phones,$id);

        if (empty($phones)){
            return 200;
        }
        if ($wabaIntances->first() == null) {
            return back()->withErrors(['phone' => 'Не правильный интанс']);
        }
        foreach ($phones as $phone) {
            $WabaMessageController = new WabaMessageController();
            $instance_model = $WabaMessageController->getWabaIntance($phone, $wabaIntances, $id);
            $template_model = $WabaMessageController->getTemplate($instance_model, $template_name);

            $SendMessageController = new SendMessageController();
            $SendMessageController->sendMessageWabaClass($template_model->id ,$phone, $variablesJson);
        }
        return 200;
    }

    public function addQueue($template_name, $waba_phone_ids, $phones, $timeSend, $id,$variable = null)
    {
        $WabaMessageController = new WabaMessageController();
        $wabaIntances = $WabaMessageController->checkWabaIntances($template_name,$waba_phone_ids,$id);
        $CheckRussiaPhoneController = new CheckRussiaPhoneController();
        $phones = $CheckRussiaPhoneController->checkBlackList($phones,$id);

        if (empty($phones)){
            return 200;
        }
        if ($wabaIntances->first() == null) {
            return back()->withErrors(['phone' => 'Не правильный Intance']);
        }
        foreach ($phones as $phone) {
            $WabaMessageController = new WabaMessageController();
            $instance_model = $WabaMessageController->getWabaIntance($phone, $wabaIntances, $id);
            $template_model = $WabaMessageController->getTemplate($instance_model, $template_name);

            WabaQueue::query()->updateOrCreate([
                'project_id' => $id,
                'phone' => $phone,
                'waba_templates_id' => $template_model->id,
            ],[
                'variable' => $variable,
                'project_id' => $id,
                'dateSend' => $timeSend,
                'phone' => $phone,
                'waba_intance_id' => $instance_model->id,
                'waba_templates_id' => $template_model->id,
            ]);

            if (Carbon::parse($instance_model->updated_at)->isToday()) {
                $instance_model->increment('count_today');
            } else {
                $instance_model->count_today = 1;
                $instance_model->updated_at = now();
            }
            $instance_model->save();
        }
        return 200;
    }
}


