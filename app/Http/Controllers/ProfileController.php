<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileUpdateRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;
use YooKassa\Client;

class ProfileController extends Controller
{
    /**
     * Display the user's profile form.
     */
    public function edit(Request $request): View
    {
        return view('profile.edit', [
            'user' => $request->user(),
        ]);
    }

    /**
     * Update the user's profile information.
     */
    public function update(ProfileUpdateRequest $request): RedirectResponse
    {
        $request->user()->fill($request->validated());

        if ($request->user()->isDirty('email')) {
            $request->user()->email_verified_at = null;
        }

        $request->user()->save();

        return Redirect::route('profile.edit')->with('status', 'profile-updated');
    }

    /**
     * Delete the user's account.
     */
    public function destroy(Request $request): RedirectResponse
    {
        $request->validateWithBag('userDeletion', [
            'password' => ['required', 'current_password'],
        ]);

        $user = $request->user();

        Auth::logout();

        $user->delete();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return Redirect::to('/');
    }

    public function project(Request $request){
        $permissionsProject = auth()->user()->getPermissionsProject;

        return view('profile.projects', [
            'user' => $request->user(),
            'permissionsProject' => $permissionsProject,
        ]);
    }


    public function paymentService(Request $request){

        $sum = $this->getSum();

        $description = 'Оплата за сервис аналитики LEAD PRO';
        $merchant_customer_id = auth()->user()->id;
        $metadata = ['purpose' => 'LeadPro'];

        $confirmationUrl = $this->createKassa($sum,$description,$merchant_customer_id,$metadata);


        return view('auth.payment.payment', [
            'confirmationUrl' => $confirmationUrl,
            'sum' => $sum,
        ]);
    }

    function getSum(){
        $user = auth()->user();
        $projects = $user->getPermissionsProject;

        $availibleModule = $this->getAvailibleModule($projects);

        $sum = 499;
//        $sum = 499 + collect($availibleModule)->sum('price');

        if (count($projects) <= 5 && count($projects) > 3) {
            $sum = $sum * 2;
            if ($sum > 1290){
                $sum = 1290;
            }
        }

        return $sum;
    }

    function getAvailibleModule($projects){

        $availibleModule = [];

        foreach ($projects as $project){
            $getPermissionModul = $project->getPermissionModuleFull;

            if (count($getPermissionModul) == 0){
                continue;
            }

            foreach ($getPermissionModul as $module) {
                $availibleModule[$module->id]['price'] = $module->price;
                $availibleModule[$module->id]['name'] = $module->name;
                $availibleModule[$module->id]['module_id'] = $module->id;
            }
        }

        return $availibleModule;

    }
    function createKassa($sum,$description,$merchant_customer_id,$metadata = ['text' => 'unknown']) {

        $client = new Client();
        $client->setAuth('820111', 'live_xueyiUBPI-ES-DMpCGP8LsRQyRHausGnPxjDoynPO3Q');

        $idempotenceKey = uniqid('', true);
        $response = $client->createPayment(
            array(
                'amount' => array(
                    'value' => $sum,
                    'currency' => 'RUB',
                ),
                'payment_method_data' => array(
                    'type' => 'bank_card',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => 'https://glushko-sok.ru/projects',
                ),
                'description' => $description,
                'merchant_customer_id' => $merchant_customer_id,
                'metadata' => $metadata,
            ),
            $idempotenceKey
        );

        //get confirmation url
        $confirmationUrl = $response->getConfirmation()->getConfirmationUrl();
        $url_components = parse_url( $confirmationUrl, $component = -1 );
        parse_str($url_components['query'], $params);
        return $confirmationUrl;
    }

}
