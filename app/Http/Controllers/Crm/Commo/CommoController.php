<?php

namespace App\Http\Controllers\Crm\Commo;

use App\Http\Controllers\Controller;
use App\Models\Crm\CrmSettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Kommo\KommoAPI;
use Kommo\KommoAPIException;
use Kommo\KommoContact;
use Kommo\KommoLead;

class CommoController extends Controller
{
    public $clientIdAmo;
    public $clientSecret;
    public $subdomain;
    public $statuses;
    public $setting;
    /**
     * @throws KommoAPIException
     */
    public function oAuthStoresFirst($id,$clientId,$clientSecret,$authCode,$redirectUri,$subdomain){
        return KommoAPI::oAuth2($subdomain, $clientId, $clientSecret, $redirectUri, $authCode);
    }
    public function oAuthIndex(Request $request,$id)
    {
        $code = $request->code;
        $subdomain = Str::before($request->referer,'.kommo.com');
        $this->setAmoSetting($id, 'subdomain', $subdomain);
        $client_id = $request->client_id;
        $redirectUri = "https://glushko-sok.ru/AmoCrm/redirection/$id";
        $this->getAmoSetting($id);
        $this->oAuthStoresFirst($id,$client_id,$this->clientSecret,$code,$redirectUri,$subdomain);
        return 200;
    }

    public function setAmoSetting($id, $fieldName, $fieldValue)
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
        $settings = json_decode($CrmSettingModel->settings, true);
        $settings[$fieldName] = $fieldValue;
        $CrmSettingModel->settings = json_encode($settings);
        $CrmSettingModel->save();
    }

    public function getAmoSetting($id)
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
        if (!empty($CrmSettingModel->settings)){
            $setting = $CrmSettingModel->settings;
            $settings = json_decode($setting, true);
            $this->clientSecret = $settings['clientSecret'] ?: null;
            $this->clientIdAmo = $settings['clientIdAmo'] ?: null;
            $this->subdomain = $settings['subdomain'] ?: null;
        }
    }
    public function getAmoStatus($id)
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
        $setting = $CrmSettingModel->settings;
        $settings = json_decode($setting, true);
        KommoAPI::oAuth2($settings['subdomain']);
        $pipelines = KommoAPI::getAccount('pipelines',$settings['subdomain']);
        return $pipelines['_embedded']['pipelines'];
    }

    public function oAuthStoresSecond($id)
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
        $setting = $CrmSettingModel->settings;
        $this->setting = $setting;

        $statuses = $CrmSettingModel->statuses ?? null;
        if (!empty($statuses)){
            $this->statuses = json_decode($statuses,true);
        }
        $settings = json_decode($setting, true);
        $this->clientSecret = $settings['clientSecret'] ?? null;
        $this->clientIdAmo = $settings['clientIdAmo'] ?? null;
        $this->subdomain = $settings['subdomain'] ?? null;
        return KommoAPI::oAuth2($settings['subdomain']);
    }

    public function saveContactAndDealFromPhone($id,$phone,$data = null){

        if (empty($this->subdomain)){
           $this->oAuthStoresSecond($id);
        }

        $data['contact']['phone'] = $phone;
        $contactId = $this->createContact($data, $id);

        if (!empty($data['new_lead_status'])){
            $this->createDeal($contactId,$data['new_lead_status']);
        } else {
            $this->createDeal($contactId,$this->statuses['new_lead_status']);
        }
    }

    public function customFields($id)
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
        $setting = $CrmSettingModel->settings;
        $settings = json_decode($setting, true);
        KommoAPI::oAuth2($settings['subdomain']);
        return KommoAPI::getAccount('custom_fields',$settings['subdomain'])['_embedded']['custom_fields'];
    }

    function getPhoneFromContactField($customFieldsContact)
    {
        return array_column($customFieldsContact, 'id', 'code')['PHONE'] ?? null;
    }

    function createContact($data, $id)
    {
        if (empty($this->subdomain) || empty($this->statuses)){
            $this->oAuthStoresSecond($id);
        }

        $statuses = $this->statuses;

        //            dd(KommoAPI::getContacts(['phone' => '79259126938'],'false',$settings['subdomain']));
        // Создание контакта

        $contact = new KommoContact([
            'name' => $data['contact']['name'] ?? $data['contact']['phone'],
        ]);

        // Установка дополнительных полей
        $contact->setCustomFields([
            $statuses['phone_id_status'] => [[
                'value' => $data['contact']['phone'],
                'enum'  => 'WORK'
            ]],
        ]);

        // Сохранение контакта
        return $contact->save();
    }

    function createDeal($contactId,$statusId)
    {
        // Создание новой сделки
        $lead1 = new KommoLead([
            'name'                => 'Новый лид_' . $contactId,
//            'pipeline'            => [ 'id' => $pipelineId ],
            'status_id'           => $statusId,
        ]);

        // Привязка контакта
        $lead1->addContacts($contactId);

        // Сохранение сделки и получение ее ID
        $leadId = $lead1->save();
    }

}
