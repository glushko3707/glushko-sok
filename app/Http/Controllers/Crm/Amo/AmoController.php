<?php

namespace App\Http\Controllers\Crm\Amo;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm\Commo\CommoController;
use App\Models\Crm\CrmSettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Kommo\KommoAPI;
use Kommo\KommoAPIException;
use Kommo\KommoContact;

class AmoController extends Controller
{
    public $clientIdAmo;
    public $clientSecret;
    public $subdomain;
    /**
     * @throws KommoAPIException
     */
    public function oAuthStoresFirst($id,$clientId,$clientSecret,$authCode,$redirectUri,$subdomain){
        return KommoAPI::oAuth2($subdomain, $clientId, $clientSecret, $redirectUri, $authCode);
    }
    public function oAuthIndex(Request $request,$id)
    {
        $code = $request->code;
        $subdomain = Str::before($request->referer,'.kommo.com');
        $this->setAmoSetting($id, 'subdomain', $subdomain);
        $client_id = $request->client_id;
        $redirectUri = "https://glushko-sok.ru/AmoCrm/redirection/$id";
        $this->getAmoSetting($id);
        $this->oAuthStoresFirst($id,$client_id,$this->clientSecret,$code,$redirectUri,$subdomain);
        return 200;
    }

    public function setAmoSetting($id, $fieldName, $fieldValue)
    {
        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
        $settings = json_decode($CrmSettingModel->settings, true);
        $settings[$fieldName] = $fieldValue;
        $CrmSettingModel->settings = json_encode($settings);
        $CrmSettingModel->save();
    }

    public function getAmoSetting($id)
        {
            $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
            if (!empty($CrmSettingModel->settings)){
                $setting = $CrmSettingModel->settings;
                $settings = json_decode($setting, true);
                $this->clientSecret = $settings['clientSecret'] ?: null;
                $this->clientIdAmo = $settings['clientIdAmo'] ?: null;
                $this->subdomain = $settings['subdomain'] ?: null;
            }
        }
        public function getAmoStatus($id)
        {
            $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
            $setting = $CrmSettingModel->settings;
            $settings = json_decode($setting, true);
            KommoAPI::oAuth2($settings['subdomain']);
            $pipelines = KommoAPI::getAccount('pipelines',$settings['subdomain']);
            return $pipelines['_embedded']['pipelines'];
        }

        public function oAuthStoresSecond($id)
        {
            $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
            $setting = $CrmSettingModel->settings;
            $settings = json_decode($setting, true);
            return KommoAPI::oAuth2($settings['subdomain']);
        }

        public function findContactFromPhone($id){
            $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
            $setting = $CrmSettingModel->settings;
            $settings = json_decode($setting, true);
            KommoAPI::oAuth2($settings['subdomain']);

//            dd(KommoAPI::getContacts(['phone' => '79259126938'],'false',$settings['subdomain']));
            // Создание контакта

            $contact1 = new KommoContact([
                'name' => $data['contact']['name'] ?: $data['contact']['phone'],
            ]);

            // Установка дополнительных полей
            $contact1->setCustomFields([
                '6532343' => 41,
                '123456' => [[
                    'value' => '+79451112233',
                    'enum'  => 'WORK'
                ]],
                '123467' => [[
                    'value' => 'hans@example.com',
                    'enum'  => 'WORK'
                ]]
            ]);

            // Сохранение контакта
            $contact1Id = $contact1->save();
    }


        public function pipelineSetting($id)
        {
            $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->exists();
            if (empty($CrmSettingModel)){
                return redirect()->route('serviceSetting.index', $id);
            }
            return view('crm.pipelineSetting', compact('id'));
        }
    }
