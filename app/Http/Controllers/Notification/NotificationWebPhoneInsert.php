<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\NotificationSetting;
use App\Traits\Notification;
use Illuminate\Http\Request;

class NotificationWebPhoneInsert extends Controller
{
    use Notification;

    public function insertForm($id){
        return view('webinar.notification.insertPhone',compact('id'));
    }


    public function changeDateWeb(Request $request,$id){
        $date = $request->changeDateWeb;
        NotificationSetting::query()->updateOrCreate([
            'project_id' => $id,
        ],[
            'dateWeb' => $date,
        ]);

        return back();
    }

    public function insertFormStore(Request $request,$id){
        if ($request->hasFile('phoneWeb'))
            foreach(file($request->file('phoneWeb')) as $line) {
                $NotificationController = new NotificationController();
                $NotificationController->addNotification($line,$id,'Insert');
            }

        if (isset($request->phoneWebOne)){
            $text = $request->phoneWebOne;
            $result =  preg_split('/\r\n|[\r\n]/', $text);
            foreach($result as $line) {
                $result = preg_replace("/[^,.0-9]/", '', $line);
                $NotificationController = new NotificationController();
                $NotificationController->addNotification($result,$id,'Insert');
            }
        }
        return redirect(route('plannedNotificztion',$id));
    }

    public function addNotificationRequest(Request $request,$id){
        if (isset($request->phone)){
            $phone = $request->phone;
            $phone = $this->onlyDigits($phone);
            if ($phone == ''){
                return 200;
            }
            $utm_source = $request->utm_source ?? null;
            $NotificationController = new NotificationController();
            $NotificationController->addNotification($phone,$id,$utm_source);
        }
        return 200;
    }

    function onlyDigits($text) {
        return preg_replace('/\D/', '', $text);
    }
    public function insertNewLeadStore ($id){
        $usersForWEb = NewLeadWeb::query()->where('project_id',$id)->where('addWeb','=',0)->get();
        if (empty($usersForWEb)){die();}
        foreach($usersForWEb as $user) {
            $NotificationController = new NotificationController();
            $NotificationController->addNotification($user->phone,$id,'Insert');
        }

        return back();
    }



}

