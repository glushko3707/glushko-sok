<?php

namespace App\Http\Controllers\Notification\Wa;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\Wa\WebWaNumber;
use App\Models\Project;
use App\Traits\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WaViewController extends Controller
{
    use Notification;

    public function instanceSetting($id) {

        $wa = WebWaNumber::query()->where('project_id',$id)->get();

        return view('webinar.wa.WaSetting',compact('id','wa'));
    }
    public function instanceUpdate(Request $request, $id) {

        $wa = WebWaNumber::query()->find($request->id);
        $wa->instance_phone = $request->instance_phone;
        $wa->instance_id = $request->instance_id;

        if (isset($request->active) && $request->active != null) {
            $wa->active = 1;
        } else {
            $wa->active = 0;
        }

        $wa->update();

        $instance_id = $wa->get()->first();

        return back();
    }
    public function instanceCheck($id,$instance_id) {

        $project = Project::query()->find($id);
        $getNotificationSetting = $project->getNotificationSetting;

        $waModel = WebWaNumber::query()->where('project_id',$id)->where('instance_id',$instance_id)->get()->first();

        $url = "https://whatsmonster.ru/api/reconnect?instance_id=$instance_id&access_token=$getNotificationSetting->wapico_key";

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->get($url)->json();

        if ($response['status'] == 'success'){
            $waModel->active = 1;
            $waModel->update();
        } else {
            $waModel->active = 0;
            $waModel->update();
        }
        return back();
    }
    public function instanceGetQr($id,$instance_id) {

        $project = Project::query()->find($id);
        $getNotificationSetting = $project->getNotificationSetting;

        $url = "https://whatsmonster.ru/api/get_qrcode?instance_id=$instance_id&access_token=$getNotificationSetting->wapico_key";

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->get($url)->json();


        return ['qr' => $response['base64']];

    }
    public function addInstance($id) {
        return view('webinar.wa.addIntance',compact('id'));
    }
    public function addInstanceStore($id) {
        $project = Project::query()->find($id);
        $getNotificationSetting = $project->getNotificationSetting;

        $url = "https://whatsmonster.ru/api/create_instance?access_token=$getNotificationSetting->wapico_key";
        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->get($url)->json();

        if ($response['status'] == 'success' && $response['message'] == 'Instance ID generated successfully') {
            return $response['instance_id'];
        } else {
            return $response['message'];
        }

    }
    public function deleteInstance($id,$instance_id) {

        $project = Project::query()->find($id);
        $getNotificationSetting = $project->getNotificationSetting;

        $waModel = WebWaNumber::query()->where('project_id',$id)->where('instance_id',$instance_id)->get()->first();

        $url = "https://whatsmonster.ru/api/reset_instance?instance_id=$instance_id&access_token=$getNotificationSetting->wapico_key";

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->get($url)->json();

        $waModel->delete();

        return back();

    }
    public function addInstanceDatabaseStore(Request $request,$id){

        WebWaNumber::query()->updateOrCreate([
            'project_id' => $id,
            'instance_phone' => $request->instance_phone,
        ],[
            'instance_id' => $request->instance_id,
            'active' => 1,
        ]);

        $this->setWebhook($id,$request->instance_id);

        return redirect(route('instanceSetting.index',$id));
    }
    public function instanceTestMessage(Request $request,$id) {

        $project = Project::query()->find($id);
        $instance_id = $request->instance_id;

        $CheckRussiaPhoneController = new CheckRussiaPhoneController();
        $phone = $CheckRussiaPhoneController->checkPhone($request->phone);

        $notificationSetting = $project->getNotificationSetting;
        $text = 'Тестовое сообщение';

        $data  = [
            'number' => $phone,
            'type' => 'text',
            'message' => $text,
            'instance_id' => $instance_id,
            'access_token' => $notificationSetting->wapico_key,
        ];

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post("https://whatsmonster.ru/api/send?number=$phone&type=text&message=$text&instance_id=$instance_id&access_token=$notificationSetting->wapico_key",$data)->json();


        return 200;
    }

    function setWebhook($id,$instance_id){
        $project = Project::query()->find($id);
        $getNotificationSetting = $project->getNotificationSetting;
        $date = [
            'webhook_url' => route('waWebhook.store',['id' => $id,'instance_id' => $instance_id]),
            'enable' => true,
            'instance_id' => $instance_id,
            'access_token' => $getNotificationSetting->wapico_key,
        ];
        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->get("https://whatsmonster.ru/api/set_webhook",$date)->json();
    }

    public function waWebhook($id,$instance_id){
        return 200;
    }

    public function waTokenUpdate(Request $request,$id){
        $validator = $request->validate([
            'wapico_key' => 'required|min:3',
        ],[
            'wapico_key' => 'Обязательно для заполнения',
        ]);


        NotificationSetting::query()->updateOrCreate([
            'project_id' => $id
        ],[
            'wapico_key' => $request->wapico_key,
            ]);

        return to_route('instanceSetting.index',$id);
    }

    public function SendWaMessageOne($id){

        return view('webinar.wa.WaSendMessageBlackWa',compact('id'));
    }


    public function SendWaMessageStore(Request $request,$id){

        $validator = $request->validate([
            'phone' => 'required|min:3',
            'text' => 'required|min:3',

        ],[
            'phone' => 'Обязательно для заполнения',
            'text' => 'Обязательно для заполнения',
        ]);

        $phone = $request->phone;
        $CheckRussiaPhoneController = new CheckRussiaPhoneController();
        $phone = $CheckRussiaPhoneController->checkPhone($phone);
        if(!$phone){
            return back()->withErrors(['phone' => 'Не правильный номер']);
        }
        $project = Project::query()->find($id);
        $text = $request->text;

        $WaMessageController =  new WaMessageController();
        $result = $WaMessageController->sendWaJob($text,$phone,$project);

        if ($result) {
            // Возвращаемся назад с сообщением об успехе
            return redirect()->back()->with('success', 'Ваше сообщение было успешно отправлено!');
        } else {
            // Возвращаемся назад с сообщением об ошибке
            return redirect()->back()->with('error', 'Произошла ошибка при отправке сообщения!');
        }
    }
}
