<?php

namespace App\Http\Controllers\Notification\Wa;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\VariableController;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\Wa\WaSendMessage;
use App\Models\Notification\Wa\WebWaNumber;
use App\Models\WhatsUpQueue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class WaMessageController extends Controller
{
    public function sendWaJob($text, $phone, $project)
    {

        $lead = NewLeadWeb::query()->where('project_id', $project->id)->where('phone', $phone)->get()->last();

        if (!empty($lead)) {
            $VariableController = new VariableController();
            $text = $VariableController->setVariables($text, $phone, $project->id);
        }

        $instance_id_model = $this->getWaIntance($phone, $project);
        $checkQueue = $this->checkQueueMiddlewire($text, $phone, $instance_id_model->instance_id, $project->id);
        if ($checkQueue) {
            return 315;
        }

        if (empty($instance_id_model)) {
            return 400;
        }

        $notificationSetting = $project->getNotificationSetting;

        $data = [
            'number' => $phone,
            'type' => 'text',
            'message' => $text,
            'instance_id' => $instance_id_model->instance_id,
            'access_token' => $notificationSetting->wapico_key,
        ];

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post("https://whatsmonster.ru/api/send?number=$phone&type=text&message=$text&instance_id=$instance_id_model->instance_id&access_token=$notificationSetting->wapico_key", $data)->json();
        if (isset($response['status'])) {
            if ($response['status'] == 'success') {
                $this->updateIntanceCount($data, $project, $instance_id_model);
            } else {
                WebWaNumber::query()->updateOrCreate([
                    'instance_id' => $instance_id_model->instance_id,
                    'project_id' => $project->id,
                ], [
                    'active' => 0,
                ]);
                return 400;
            }
        }

        return 200;
    }

    public function getWaIntance($phone, $project)
    {
        $WaSendMessage = WaSendMessage::query()->where('project_id', $project->id)->where('phone', $phone)->get()->last();
        $webWaNumber = WebWaNumber::query()->where('project_id', $project->id)->get();

        if (isset($WaSendMessage->instance_id)) {
            $instance = $WaSendMessage->instance_id;

            $instance_id_model = $webWaNumber->where('instance_id', $instance)->first();
            if (!empty($instance_id_model) && $instance_id_model->active == 1) {
                return $instance_id_model;
            } else {
                return $webWaNumber->where('active', "=", 1)->sortBy('updated_at')->first();
            }
        } else {
            return $webWaNumber->where('active', "=", 1)->sortBy('updated_at')->first();
        }

    }
    public function checkQueueMiddlewire($text, $phone, $instance_id, $id, $sec = 3)
    {
        $instance_id_model = WebWaNumber::query()->where(['project_id' => $id, 'instance_id' => $instance_id])->first();
        $updated_at = $instance_id_model->updated_at;
        $checkQueueMiddleware = Carbon::parse($updated_at)->lt(now()->subSeconds($sec));
        if (!$checkQueueMiddleware) {
            WhatsUpQueue::query()->updateOrCreate([
                'project_id' => $id,
                'phone' => $phone,
            ], [
                'instance_id' => $instance_id,
                'text' => $text,
                'time' => now()->addMinutes(3),
            ]);
            return true;
        }
        return false;
    }

    public function updateIntanceCount($data, $project, $instance_id_model)
    {
        WaSendMessage::query()->insert([
            'project_id' => $project->id,
            'phone' => $data['number'],
            'instance_id' => $data['instance_id'],
        ]);

        //
        WebWaNumber::query()->updateOrCreate([
            'project_id' => $project->id,
            'instance_id' => $data['instance_id'],
        ], [
            'count' => $instance_id_model->count + 1,
        ]);

    }
}
