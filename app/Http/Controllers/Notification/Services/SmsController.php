<?php

namespace App\Http\Controllers\Notification\Services;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\VariableController;
use App\Jobs\Notification\SmsAero;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\SmsAeroHistory;
use App\Models\Project;
use App\Models\SmsSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;

class SmsController extends Controller
{
    public function smsAeroSetting(Request $request,$id){

        $request->validate([
            'smsAeroEmail' => 'required|email',
            'smsAero_key' => 'required|min:3',
        ],[
            'smsAeroEmail' => 'Обязательно для заполнения',
            'smsAero_key' => 'Обязательно для заполнения',
        ]);

        $project = Project::query()->find($id);

        try{
            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://'.$request->smsAeroEmail.':'.$request->smsAero_key.'@gate.smsaero.ru/v2/balance');
        } catch (Throwable $e){
            report($e);
        }

        if (isset($response) && !isset($response->json()['data']['balance'])){
            return back()->withErrors([
                'smsAeroEmail' => 'Не корректные данные',
                'smsAero_key' => 'Не корректные данные',
            ]);
        }

            $date = [
                'smsAeroEmail' => $request->smsAeroEmail,
                'smsAero_key' => $request->smsAero_key
                ];

        if(isset($request->smsAeroSenderName)){
            $date ['smsAeroSenderName'] = $request->smsAeroSenderName;
        }

        $notificationSetting = $project->getNotificationSetting;
        $getSmsSetting = $project->getSmsSetting;

        if (empty($notificationSetting)){
            $notificationSetting = NotificationSetting::query()->updateOrCreate([
                'project_id' => $id,
            ],[
                'project_id' => $id,
            ]);
        }
        if (empty($getSmsSetting)){
            $getSmsSetting = SmsSetting::query()->updateOrCreate([
                'project_id' => $id
            ],[
                'project_id' => $id,
                'service' => 'smsAero'
            ]);
        }

        if (isset($date)){
            $notificationSetting->update($date);
            $getSmsSetting->update($date);
        }
        return back();
    }

    /**
     * Отправляет SMS с использованием сервиса SMSAero.
     *
     * @param array $date Дата и время отправки SMS, с ключами:
     *     - smsAeroSenderName (строка): Имя отправителя SMS.
     *     - phone (строка): Номер телефона, на который будет отправлено SMS.
     *     - text (строка): Текстовое содержимое SMS.
     *     - timeSend (строка): Дата и время отправки SMS в формате ISO.
     *     - smsAeroEmail (строка): Email аккаунта SMSAero.
     *     - smsAero_key (строка): Ключ аккаунта SMSAero.
     * @return void
     */

    public function sendSmsAeroJob($date)
    {
        $datas = [];

        if (isset($date['smsAeroSenderName'])) {
            $datas ['sign'] = $date['smsAeroSenderName'];
        } else {
            $datas ['sign'] = 'SMS Aero';
        }

        if (isset($date['timeSend'])) {
            $datas ['dateSend'] = $date['timeSend'];
        }

        $datas ['number'] = $date['phone'];
        $datas ['text'] = $date['text'];
        $datas ['shortLink'] = 1;

        $response = Http::get('https://'.$date['smsAeroEmail'].':'.$date['smsAero_key'].'@gate.smsaero.ru/v2/sms/send',$datas)->json();

        if (isset($response['success']) && $response['success'] == true) {

            $cost = $response['data']['cost'] ?? null;
            SmsAeroHistory::query()->create( [
                'project_id' => $date['id'],
                'phone' => $date['phone'],
                'cost' => $cost,
                'text' => $date['text'],
                'status' => 'success',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        } else {
            $error_message = json_encode($response);

            Log::error('SmsAero API Error', ['error' => $error_message]);

            SmsAeroHistory::query()->create([
                'project_id' => $date['id'],
                'phone' => $date['phone'],
                'text' => $date['text'],
                'cost' => 0,
                'status' => 'error',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }

    }

    public function sendReserveSms($reserveSmsModel,$wabaTemplatesModel,$phone)
    {
        $project_telegram = $wabaTemplatesModel->getProject;
        $notificationSetting = $project_telegram->getNotificationSetting;
        $VariableController = new VariableController();
        $text = $VariableController->setVariables($reserveSmsModel->text, $phone, $project_telegram->id);


        $date = [
            'smsAeroSenderName' => $notificationSetting->smsAeroSenderName,
            'smsAeroEmail' => $notificationSetting->smsAeroEmail,
            'smsAero_key' => $notificationSetting->smsAero_key,
            'phone' => $phone,
            'text' => $text,
            'timeSend' => now(),
            'id' => $notificationSetting->project_id,
        ];
        SmsAero::dispatch($date);
    }
}
