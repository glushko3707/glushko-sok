<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\Services\SmsController;
use App\Http\Controllers\Notification\Wa\WaMessageController;
use App\Http\Controllers\Waba\WabaMessageController;
use App\Jobs\Notification\P1sms;
use App\Jobs\Notification\SendWhatsUp;
use App\Jobs\Waba\addQueue;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\Wa\WebWa;
use App\Models\SmsSetting;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\wabaSetting;

class WelcomeMessageController extends Controller
{
    public function sendWelcomeWa($phone, $project)
    {
        $waMessage = WebWa::query()->where('project_id', $project->id)->where('type', '=', 'welcome')->get()->first();

        if (config('app.name') == 'Laravel_test') {
           $WaMessageController =  new WaMessageController();
           $WaMessageController->sendWaJob($waMessage->text, $phone, $project);
        } else {
            SendWhatsUp::dispatch($waMessage->text, $phone, $project);
        }
    }

    public function sendWelcomeWaba($phone, $project)
    {
        $welcome_template_name = wabaSetting::query()->where('project_id', $project->id)->get()->first()->welcome_template_name;
        $timeSend = now();
        $waba_intances = WabaIntance::query()->where('project_id',$project->id)->pluck('waba_phone_id');
        if (config('app.name') == 'Laravel_test')  {
            $NotificationController = new WabaMessageController();
            $NotificationController->addQueue($welcome_template_name,$waba_intances->toArray(),[$phone],$timeSend,$project->id);
        } else {
            addQueue::dispatch($welcome_template_name,$waba_intances,[$phone],$timeSend,$project->id);
        }
    }

    public function sendWelcomeSms($smsStep, $phone, $project)
    {
        $project_id = $project->id;
        $smsSetting = SmsSetting::query()->where('project_id', $project_id)->get()->first();


        if (isset($smsSetting->service)) {
            switch ($smsSetting->service) {
                case 'smsAero':
                    $this->sendWelcomeSmsAero($smsStep, $phone, $project);
                    break;
                case 'p1sms':
                    $this->sendWelcomeSmsP1sms($smsStep, $phone, $project);
                    break;
            }
        }
    }

    public function sendWelcomeSmsAero($smsStep, $phone, $project)
    {
        $notificationSetting = $project->getNotificationSetting;

        $VariableController = new VariableController();
        $text = $VariableController->setVariables($smsStep->text, $phone, $project->id);

        $data = [
            'id' => $notificationSetting->project_id,
            'phone' => $phone,
            'text' => $text,
            'smsAeroSenderName' => $notificationSetting->smsAeroSenderName,
            'smsAeroEmail' => $notificationSetting->smsAeroEmail,
            'smsAero_key' => $notificationSetting->smsAero_key,
        ];

        $send = new SmsController();
        $send->sendSmsAeroJob($data);

    }


    public function sendWelcomeSmsP1sms($smsStep, $phone, $project)
    {

        $getSmsSetting = $project->getSmsSetting;
        $lead = NewLeadWeb::query()->where('project_id', $project->id)->where('phone', $phone)->get()->last();

        $VariableController = new VariableController();
        $text = $VariableController->setVariables($smsStep->text, $phone, $project->id);

        $date = [
                'sms' => [[
                'phone' => $phone,
                'text' => $text,
                'channel' => 'char',
                'sender' => $getSmsSetting->p1smsSender,
            ]],
            'apiKey' => $getSmsSetting->p1smsApiKey,
        ];

        P1sms::dispatch($date, $project->id);

    }

}
