<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Jobs\Notification\WebNotification;
use App\Models\Notification\JobTelegramNotification;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Project;
use App\Models\SmsSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Jenssegers\Date\Date;
use Throwable;

class PiplelineController extends Controller
{
    public function firstSmsControl(Request $request,$id){
        $project = Project::query()->find($id);

        $notificationSetting = $project->getNotificationSetting;

        if (isset($request->smsWelcome)){
            $smsWelcome = 1;
        } else {
            $smsWelcome = 0;
        }

        SmsSetting::query()->updateOrCreate(
            ['project_id' => $id],
            [
                'welcomeSms' => $smsWelcome,
            ]);

        NotificationSetting::query()->updateOrCreate(
            ['project_id' => $id],
                [
            'welcomeSms' => $smsWelcome,
        ]);

        try {
            if (isset($request->smsAero_key) && isset($request->emailSmsAero) && !empty($request->smsAero_key) && !empty($request->emailSmsAero) ) {
                $notificationSetting->smsAero_key =  $request->smsAero_key;
                $notificationSetting->smsAeroEmail = $request->emailSmsAero;
                $notificationSetting->smsAeroSenderName = $request->smsAeroSenderName ?? null;

                $smsSetting = $project->getSmsSetting;
                $smsSetting->smsAero_key = $project->smsAero_key;
                $smsSetting->smsAeroEmail = $project->smsAeroEmail;
                $smsSetting->smsAeroSenderName = $project->smsAeroSenderName;

                $response = Http::withHeaders([
                    "content-type" => "application/json",
                    'accept' => 'application/json',
                ])->post('https://'.$request->emailSmsAero.':'.$request->smsAero_key.'@gate.smsaero.ru/v2/balance');



                $notificationSetting->save();
                $smsSetting->save();

                if ($response->json()['success'] != "true") {
                    return redirect()->back()->withErrors(['Неправильный smsAero_key или emailSmsAero']);
                }
            }
        } catch (Throwable $e) {
            report($e);
        }




        return back();
    }

    public function firstWaControl(Request $request,$id){

        if (isset($request->waWelcome)){
            $waWelcome = 1;
        } else {
            $waWelcome = 0;
        }

        NotificationSetting::query()->updateOrCreate(['project_id' => $id],[
            'welcomeWa' => $waWelcome,
        ]);

        return back();
    }

    public function getDateWeb($id){
        $NotificationSetting= NotificationSetting::query()->where('project_id','=',$id)->get()->first();

        if (isset($NotificationSetting->dateWeb)){
            $dateWebinar = Date::parse($NotificationSetting->dateWeb)->locale('ru')->format('j F');
        } else {
            $dateWebinar = '';
        }


        return [
            'dateWeb' => Carbon::parse($NotificationSetting->dateWeb)->format('d.m.Y'),
            'dateWebText' => $dateWebinar,
        ];
    }


}
