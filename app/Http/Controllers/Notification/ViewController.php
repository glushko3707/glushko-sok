<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\ServiceSetting;
use App\Jobs\Notification\WebNotification;
use App\Models\Crm\CrmSettingModel;
use App\Models\GlushkoTelegramConnect;
use App\Models\linkChangeModel;
use App\Models\Notification\JobTelegramNotification;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\Wa\WebWa;
use App\Models\Notification\WebCall;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Notification\WebSms;
use App\Models\Project;
use App\Models\SmsSetting;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\WabaTemplates;
use App\Models\Webinar\WebinarUser;
use App\Traits\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Throwable;

class ViewController extends Controller
{
use Notification;

    public function pipelineNotificztion($project_id){
        $id = $project_id;
        $webNotificationPipleline = WebNotificationPipleline::query()
            ->where('project_id',$project_id)
            ->whereNotNull('timeSend')
            ->orderBy('timeSend')
            ->get();

        $webNotificationPiplelineNoTime = WebNotificationPipleline::query()
            ->where('project_id',$project_id)
            ->whereNull('timeSend')
            ->orderBy('timeSend')
            ->get();

        $sms = WebSms::query()->where('project_id',$project_id)->get();
        $call = WebCall::query()->where('project_id',$project_id)->get();
        $wa = WebWa::query()->where('project_id',$project_id)->get();

        $project_telegram = Project::query()->find($id);
        $notificationSetting = $project_telegram->getNotificationSetting;
        $getWabaTemplates = $project_telegram->getWabaTemplates;
        $notificationSetting->wabaTemplates = $getWabaTemplates->unique('name');

        //        if (!empty($call)) {
//            if (!isset($notificationSetting->zvonobot_key) || empty($notificationSetting->zvonobot_key)){
//                return to_route('serviceSetting.index', $id)->withErrors(['zvonobot' => 'не указан API KEY ZVONOBOT']);
//            }
//        }

        if (isset($notificationSetting->zvonobot_key) && !empty($notificationSetting->zvonobot_key)) {
            $data ['apiKey'] = $notificationSetting->zvonobot_key;
            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://lk.zvonobot.ru/apiCalls/userInfo',$data);

            if ($response->json()['status'] == "success") {
                $stat['zvonobot'] =  round($response->json()['data']['balance']);
            } else {$stat['zvonobot'] = 'ошибка key';}
        } else {$stat['zvonobot'] = 'нет api';}


        if (isset($notificationSetting->smsAero_key) && isset($notificationSetting->smsAeroEmail) && !empty($notificationSetting->smsAero_key) && !empty($notificationSetting->smsAeroEmail) ) {

            try{
                $response = Http::withHeaders([
                    "content-type" => "application/json",
                    'accept' => 'application/json',
                ])->post('https://'.$notificationSetting->smsAeroEmail.':'.$notificationSetting->smsAero_key.'@gate.smsaero.ru/v2/balance');

                if (isset($response->json()['data']['balance'])) {
                    $stat['sms'] = round($response->json()['data']['balance']);
                } else{$stat['sms'] = 'ошибка';}

                } catch (Throwable $e){
            }
        }


        if (!isset($notificationSetting->welcomeSms)) {
            NotificationSetting::query()->updateOrCreate([
                'project_id' => $id
            ],[
                'welcomeWa' => 0,
            ]);
        }


        if ($notificationSetting->welcomeSms == 1){
            $welcome['activeFirstSms'] = true;
            $welcome['sms'] = collect($sms)->whereNull('timeSend')->first()->text ?? '';
        } else {
            $welcome['activeFirstSms'] = false;
            $welcome['sms'] = '';
        }


        if ($notificationSetting->welcomeWa == 1){
            $welcome['activeFirstWa'] = true;
            $welcome['wa'] = collect($wa)->where('type','=','welcome')->first()->text ?? '';
        } else {
            $welcome['activeFirstWa'] = false;
            $welcome['wa'] = '';
        }


        foreach ($webNotificationPipleline as $keys => $val){
            if ($val->content_type == 'SMS') {
                $smsId = $sms->find($val->content_id);
                $timeSend = $smsId->timeSend;
                $webNotificationPipleline[$keys]['content'] = $smsId->text;

                $webNotificationPipleline[$keys]['timeProgram'] = $val->timeSend;
                $webNotificationPipleline[$keys]['timeSend'] = $timeSend;
                if (Carbon::parse($val->timeSend)->eq(Carbon::parse($timeSend)) == false){
                    $webNotificationPipleline[$keys]['timeProgram'] = 'ВНИМАНИЕ! запрос позже отправки';
                };

            } elseif ($val->content_type == 'CALL'){
                $callId = $call->find($val->content_id);
                $timeSend = $callId->timeSend;
                $webNotificationPipleline[$keys]['content'] = $callId->campaign_id;

                $webNotificationPipleline[$keys]['timeProgram'] = $val->timeSend;
                $webNotificationPipleline[$keys]['timeSend'] = $timeSend;

                if ($timeSend == null) {
                    $webNotificationPipleline[$keys]['timeSend'] =  $webNotificationPipleline[$keys]['timeProgram'];
                } else {
                    if (Carbon::parse($val->timeSend)->eq(Carbon::parse($timeSend)) == false){
                        $webNotificationPipleline[$keys]['timeProgram'] = 'ВНИМАНИЕ! запрос позже отправки';
                    };
                }


            } elseif ($val->content_type == 'WA') {
                $waId = $wa->find($val->content_id);
                $webNotificationPipleline[$keys]['content'] = $waId->text;

                $webNotificationPipleline[$keys]['timeSend'] = $val->timeSend;
                $webNotificationPipleline[$keys]['timeProgram'] = '';
            }
            elseif ($val->content_type == 'WABA') {
                $getWabaTemplate = $val->getWabaTemplate->where('project_id',$id)->first();

                $webNotificationPipleline[$keys]['content'] = $getWabaTemplate->name;
                if ($val->type == 'nextDay') {
                    $webNotificationPipleline[$keys]['nextDay'] = true;
                }
                $webNotificationPipleline[$keys]['timeSend'] = $val->timeSend;
                $webNotificationPipleline[$keys]['timeProgram'] = '';
            }

        }

        return view('webinar.notification.webNotificationPipeline',compact('id','stat','webNotificationPipleline','welcome','notificationSetting','webNotificationPiplelineNoTime'));
    }
    public function createNotification(Request $request,$id){
        $project_telegram = Project::query()->find($id);
        $notificationSetting = $project_telegram->getNotificationSetting;
        $getWabaTemplates = $project_telegram->getWabaTemplates;
        $notificationSetting->wabaTemplates = $getWabaTemplates->unique('name');

        return view('webinar.notification.addWebStep',compact('id','notificationSetting'));
    }
    public function createNotificationStore(Request $request,$id){

        DB::transaction(function() use($request,$id) {
            $notification_type = $request->notification_type;

            $timeSend = Carbon::parse($request->timeSend)->subMinutes(15);

            if ($notification_type == 'SMS') {
                $contentId = WebSms::query()->create([
                    'project_id' => $id,
                    'text' => $request->text_post,
                    'timeSend' => Carbon::parse($request->timeSend),
                ]);
            }

            if ($notification_type == 'CALL') {
                $contentId = WebCall::query()->create([
                    'project_id' => $id,
                    'campaign_id' => $request->campaign_id,
                    'timeSend' => Carbon::parse($request->timeSend),
                ]);
            }

            if ($notification_type == 'WA') {
                $contentId = WebWa::query()->create([
                    'project_id' => $id,
                    'text' => $request->text_post,
                ]);
            }

            if ($notification_type == 'WABA') {
                $contentId = new Collection();
                $contentId->id = $request->waba;
                $timeSend = Carbon::parse($request->timeSend);
            }

        $date = [
            'project_id' => $id,
            'content_id' => $contentId->id,
            'content_type' => $notification_type,
            'timeSend' => $timeSend,
            'priority' => rand(1,10),
            'type' => $request->type,
        ];

            WebNotificationPipleline::query()->create($date);

        });

        return redirect(route('pipelineNotificztion',$id));

    }
    public function plannedNotificztionAction(Request $request,$id){
        $notification = WebNotificationSchedule::query()->find($id);

        $jobId = JobTelegramNotification::query()->updateOrCreate([
            'project_id' => $notification->project_id,
            'phone' => $notification->phone,
            'step_id' => $notification->step_id,
        ],[
            'user_id' => $notification->user_id,
            'timeSend' => now(),
        ])->id;

//        $this->completeAction($jobId);
        WebNotification::dispatch($jobId);

        return back();
    }
    public function createNotificationDelete(Request $request,$id){

        $WebNotificationPipleline = WebNotificationPipleline::query()->find($request->step_id);

        DB::transaction(function() use($request,$id,$WebNotificationPipleline) {

            if ($WebNotificationPipleline->content_type == 'SMS'){
                $sms = $WebNotificationPipleline->getSms();
                $sms = $sms->first()->delete();
            }

            if ($WebNotificationPipleline->content_type == 'WA'){
                $wa = $WebNotificationPipleline->getWhatsUp();
                $wa = $wa->first()->delete();
            }

            if ($WebNotificationPipleline->content_type == 'CALL'){
                $call = $WebNotificationPipleline->getCall();
                $call = $call->first()->delete();
            }

            WebNotificationPipleline::query()->find($request->step_id)->delete();

        });

        return back();

    }
    public function notificationStore(Request $request,$id){
        if (isset($request->messege_id)){
            $webNotificationPipleline = WebNotificationPipleline::query()->find($request->messege_id);
            if($request->type == 'text'){
                if ($webNotificationPipleline['content_type'] == 'SMS'){
                    $sms = WebSms::query()
                        ->find($webNotificationPipleline->content_id);
                    $sms->text = $request->text;
                    $sms->update();
                }
                if ($webNotificationPipleline['content_type'] == 'CALL'){
                    $call = WebCall::query()
                        ->find($webNotificationPipleline->content_id);
                    $call->campaign_id = $request->text;
                    $call->update();
                }
                if ($webNotificationPipleline['content_type'] == 'WA'){
                    $wa = WebWa::query()
                        ->find($webNotificationPipleline->content_id);
                    $wa->text = $request->text;
                    $wa->update();
                }

                if ($webNotificationPipleline['content_type'] == 'WABA'){
                    $webNotificationPipleline->content_id = $request->text;
                    $webNotificationPipleline->save();
                }

            }

            if($request->type == 'timeSend'){


                if ($webNotificationPipleline['content_type'] == 'SMS'){

                    $sms = WebSms::query()
                        ->find($webNotificationPipleline->content_id);
                    $sms->timeSend = Carbon::parse($request->text);
                    $sms->update();

                    $webNotificationPipleline->timeSend =  Carbon::parse($request->text)->subMinutes(15);
                    $webNotificationPipleline->update();

                }
                if ($webNotificationPipleline['content_type'] == 'CALL'){
                    $call = WebCall::query()
                        ->find($webNotificationPipleline->content_id);
                    $call->timeSend = $request->text;
                    $call->update();

                    $webNotificationPipleline->timeSend =  Carbon::parse($request->text)->subMinutes(15);
                    $webNotificationPipleline->update();
                }

                if ($webNotificationPipleline['content_type'] == 'WA'){
                    $webNotificationPipleline->timeSend =  Carbon::parse($request->text);
                    $webNotificationPipleline->update();
                }


            }



        }

    }
    public function notificationWaStore(Request $request,$id){
        if (isset($request->waWelcomeStore)){
            $wa = WebWa::query()
                ->where('project_id',$id)
                ->where('type','=','welcome')
                ->first();

            if (!isset($wa->text)){
                WebWa::query()->updateOrCreate([
                    'project_id' => $id,
                    'type' => 'welcome',
                ],[
                    'type' => 'welcome',
                    'text' =>  $request->waWelcomeStore,
                ]);
            } else{
                $wa->text = $request->waWelcomeStore;
                $wa->update();
            }
        }

    }

    public function serviceSetting($id){
        $project = Project::query()->find($id);

        $notificationSetting = $project->getNotificationSetting;

        if (empty($notificationSetting)){
            $notificationSetting = NotificationSetting::query()->updateOrCreate([
                'project_id' =>$id
            ],[
                'project_id' =>$id
            ]);
        }

        $getSmsSetting = $project->getSmsSetting;
        if (empty($getSmsSetting)){
            $getSmsSetting = SmsSetting::query()->updateOrCreate([
                'project_id' =>$id
            ],[
                'service' => 'smsAero'
            ]);
        }

        $notificationSetting->service = $getSmsSetting->service;
        $notificationSetting->p1smsApiKey = $getSmsSetting->p1smsApiKey ?? '';
        $notificationSetting->p1smsSender = $getSmsSetting->p1smsSender ?? '';

        if (isset($notificationSetting->zvonobot_key) && !empty($notificationSetting->zvonobot_key)) {
            $data ['apiKey'] = $notificationSetting->zvonobot_key;
            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://lk.zvonobot.ru/apiCalls/userInfo',$data);

            if ($response->json()['status'] == "success") {
                $stat['zvonobot'] =  round($response->json()['data']['balance']);
            } else {$stat['zvonobot'] = 'ошибка key';}
        } else {$stat['zvonobot'] = 'нет';}

        try {
            if (isset($notificationSetting->smsAero_key) && isset($notificationSetting->smsAeroEmail) && !empty($notificationSetting->smsAero_key) && !empty($notificationSetting->smsAeroEmail) ) {

            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://'.$notificationSetting->smsAeroEmail.':'.$notificationSetting->smsAero_key.'@gate.smsaero.ru/v2/balance');

            if (isset($response->json()['data']['balance'])) {
                $stat['sms'] = round($response->json()['data']['balance']);
            } else{$stat['sms'] = 'ошибка';}


                $responses = Http::withHeaders([
                    "content-type" => "application/json",
                    'accept' => 'application/json',
                ])->post('https://'.$notificationSetting->smsAeroEmail.':'.$notificationSetting->smsAero_key.'@gate.smsaero.ru/v2/sign/list');

                if ($responses->json()['success'] == true && $responses->json()['data']['totalCount'] > 0) {
                    $smsAeroSenderName = collect($responses->json()['data'])->where('extendStatus','=', 'active')->pluck('name');
                }


        }
            // Проверка `$value` ...
        } catch (Throwable $e) {
            report($e);
        }

        $ServiceSetting = new ServiceSetting($id);
        $ServiceSetting->getWabaSetting($id);
        $smsAeroSenderName = $smsAeroSenderName ?? null;

        $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
        if (!empty($CrmSettingModel->settings)){
          $crmSettings = json_decode($CrmSettingModel->settings, true);
          $notificationSetting->CrmSettings = $crmSettings;
        }
        return view('webinar.serviceSetting',compact('id','project','stat','notificationSetting','smsAeroSenderName','getSmsSetting','ServiceSetting'));
    }

    public function serviceSettingStore(Request $request,$id){

        $telegram = Project::query()->find($id);
        $notificationSetting = $telegram->getNotificationSetting;
        $getSmsSetting = $telegram->getSmsSetting;
        $getSmsSetting->service = $request->service ?? '';
        $getSmsSetting->save();

        if (isset($request->wapico_key) && !empty($request->wapico_key)) {
            {
                $notificationSetting->wapico_key = $request->wapico_key;
                $notificationSetting->save();
            }
        }

        if (!isset($notificationSetting->welcomeWa)) {
            NotificationSetting::query()->updateOrCreate([
                'project_id' => $id
            ],[
                'welcomeWa' => 0,
            ]);
        }



        if (!isset($notificationSetting->welcomeSms)) {
            NotificationSetting::query()->updateOrCreate([
                'project_id' => $id
            ],[
                'welcomeSms' => 0,
            ]);
        }

        if (isset($request->zvonobot_key) && !empty($request->zvonobot_key)) {
            $notificationSetting->zvonobot_key = $request->zvonobot_key;
            $data ['apiKey'] = $request->zvonobot_key;
            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://lk.zvonobot.ru/apiCalls/userInfo',$data);

            if ($response->json()['status'] != "success") {
                return redirect()->back()->withErrors(['Неправильный Zvonobot_key']);
            }
        }
        if (isset($request->sipOutSetting) && !empty($request->sipOutSetting)) {
            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://lk.sipout.net/userapi/?key='. $request->sipOutSetting .'&method=balance&action=get');

            if ($response->json()['result'] != 'ok'){
                return redirect()->back()->withErrors(['Неправильный sipOutSetting']);
            }

            if (isset($response->json()['data']['summary']['balance'])){
                $balance = $response->json()['data']['summary']['balance'];
            } elseif (isset($response->json()['data']['balance'])) {
                $balance = $response->json()['data']['balance'];
            }
        }
        if (isset($request->smart_token) && !empty($request->smart_token)) {
            {
                $date['limitation'] = 10;
                $date['page'] = 1;

                $response = Http::withHeaders([
                    "Authorization" =>  "Bearer ".$request->smart_token,
                ])->get("https://api.smartsender.com/v1/operators",$date);
            }

            if (!isset($response->json()['collection'])){
                return redirect()->back()->withErrors(['Неправильный smart_token']);
            }

            $notificationSetting->smart_token = $request->smart_token;
            $notificationSetting->save();

        }

        try {
          if (isset($request->smsAero_key) && isset($request->emailSmsAero) && !empty($request->smsAero_key) && !empty($request->emailSmsAero) ) {
            $notificationSetting->smsAero_key = $request->smsAero_key;
            $notificationSetting->smsAeroEmail = $request->emailSmsAero;
            $notificationSetting->smsAeroSenderName = $request->smsAeroSenderName;

            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://'.$request->emailSmsAero.':'.$request->smsAero_key.'@gate.smsaero.ru/v2/balance');


            if ($response->json()['success'] != "true") {
                return redirect()->back()->withErrors(['Неправильный smsAero_key или emailSmsAero']);
            }
        }
        } catch (Throwable $e) {
            report($e);
        }
        $notificationSetting->save();

        if (isset($request->p1smsApiKey)) {
            $apiKey = ['apiKey' => $request->p1smsApiKey];
            $response = \Illuminate\Support\Facades\Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->get('https://admin.p1sms.ru/apiUsers/getUserBalanceInfo',$apiKey)->json();

            if (isset($response['status'])){
                $getSmsSetting->p1smsApiKey = $request->p1smsApiKey ?? '';
                $getSmsSetting->p1smsSender = $request->p1smsSender ?? '';
                $getSmsSetting->save();
            }
        }


        return back();
    }

    public function plannedNotificztion($project_id){

        $id = $project_id;
        $webNotificationPlaned = WebNotificationSchedule::query()->where('project_id',$project_id)->orderBy('timeSend')->get();
        $webNotificationPipleline = WebNotificationPipleline::query()->where('project_id',$project_id)->whereNotNull('timeSend')->orderBy('timeSend')->get();
        $stat ['countPeapleToday'] = $webNotificationPlaned->unique('phone')->count();
        $usersForWEb = NewLeadWeb::query()->where('project_id',$project_id)->where('addWeb','=',0)->get();
        $stat ['countPeapleCanToday'] = $usersForWEb->count() ?? 0;
        foreach ($webNotificationPlaned as $key => $Notification) {
            if (!isset($webNotificationPipleline->where('id',$Notification['step_id'])->first()->content_type)){
                $Notification->delete();
                continue;
            }
            $webNotificationPlaned[$key]['type'] = $webNotificationPipleline->where('id',$Notification['step_id'])->first()->content_type;
        }
        $project_telegram = Project::query()->find($id);
        $notificationSetting = $project_telegram->getNotificationSetting;
        if (!empty($notificationSetting->dateWeb)){
            $stat ['dateWeb'] = $notificationSetting->dateWeb;
        }

        return view('webinar.notification.WebplannedNotificztion',compact('id','webNotificationPlaned','stat','project_telegram'));
    }
    public function downloadShedule($project_id){

        $usersForWEb = NewLeadWeb::query()->where('project_id',$project_id)->where('addWeb','=',0)->get();

        $usersForWEb = $usersForWEb->pluck('phone')->toArray();
        $fileName = 'planned_phones.csv';
        $headers = [
            'Content-type' => 'text/csv',
            'Content-Disposition' => sprintf('attachment; filename="%s"', $fileName),
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0',
        ];
        $callback = function () use ($usersForWEb) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['phone']);
            foreach ($usersForWEb as $number) {
                fputcsv($file, [$number]);
            }
            fclose($file);
        };
        return response()->stream($callback, 200, $headers);

    }

    public function addWebLead($project_id){
        $usersForWEb = NewLeadWeb::query()->where('project_id',$project_id)->where('addWeb','=',0)->update(['addWeb' => 1]);
        return back();
    }

    public function plannedNotificztionDelete($id){
        WebNotificationSchedule::query()->find($id)->delete();
        return back();
    }

//    public function saveFile(Request $request,$id){
//        // Получаем данные из БД
//        $usersForWEb = NewLeadWeb::query()->where('project_id',$id)->whereDate('created_at','>=',now()->subDays(1))->get()->unique('phone');
//
//
//        // Создаем файл и записываем данные в него
//        $filename = 'file.txt';
//        $handle = fopen($filename, 'w');
//        foreach ($usersForWEb as $row) {
//            fwrite($handle, $row->phone . PHP_EOL);
//        }
//        fclose($handle);
//        // Отправляем файл для скачивания
//        $headers = [
//            'Content-Type' => 'text/plain',
//        ];
//        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
//
//    }

    public function linkSetting (Request $request,$id){
        $linkChangeModel = linkChangeModel::query()->where('project_id',$id)->get();

        return view('webinar.notification.linkChangeSetting',compact('linkChangeModel','id'));
    }

    public function linkChangeSetting (Request $request,$id){

        if ($request->id == 'new'){
            linkChangeModel::query()->updateOrCreate([
                'project_id' => $id,
                'utm_source' => $request->utm_source,
                'linkName' => $request->linkName,
            ],[
                'link' => $request->link,
            ]);

            return back();

        }

            linkChangeModel::query()->updateOrCreate([
                'project_id' => $id,
                'id' => $request->id,
            ],[
                'link' => $request->link,
                'utm_source' => $request->utm_source,
                'linkName' => $request->linkName,
            ]);

        return back();


    }
}

