<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Models\BlackList;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Webinar\LeadForWeb;
use Carbon\Carbon;

class NotificationController extends Controller
{
    /**
     * Добавляет уведомление для указанного номера телефона и проекта.
     *
     * @param string $phone Номер телефона.
     * @param int $project_id Идентификатор проекта.
     * @param string|null $utm_source Источник UTM (необязательный).
     * @return string
     */

    public function addNotification(string $phone, int $project_id, ?string $utm_source = null, $NewLeadWeb = null): string
    {
        $phone = trim($phone, '+');


        if (!$phone || BlackList::where('phone', $phone)->exists()) {
            return 200;
        }

        usleep(100000);

        if (empty($NewLeadWeb)){
            $NewLeadWeb = NewLeadWeb::where('project_id', $project_id)->where('phone', $phone)->get()->last();
        }

        $LeadForWeb = LeadForWeb::where('project_id', $project_id)->where('phone', $phone)->get()->last();

        // возвращает первое не нулевое значение
        $utm['source'] = $utm_source ?: $NewLeadWeb->utm_source ?? $LeadForWeb->utm_source ?? '';
        $utm['medium'] = $NewLeadWeb->utm_medium ?? $LeadForWeb->utm_medium ?? '';
        $notification  = $this->issetNotification($project_id,$phone);

        if (!$notification) {
            $attempt = ($LeadForWeb->attempt ?? 0) + 1;
            $this->setNotificationSchedule($project_id,$phone);
        } else {
            $attempt = ($LeadForWeb->attempt ?? 1);
        }

        // устанавливаем UTM и attempt
        $this->setLeadForWebUtmAndAttempt($phone,$utm,$project_id,$attempt,$NewLeadWeb);
        // устанавливаем уведомления



        return 'ok';

    }

    function issetNotification($project_id,$phone)
    {
        return webNotificationSchedule::query()->where('project_id', $project_id)->where('phone', $phone)->exists();
    }

    /**
     * Обновляет или создает записи в LeadForWeb и NewLeadWeb с информацией о пользователе.
     *
     * @param string $phone Номер телефона пользователя.
     * @param array $utm Массив данных UTM.
     * @param int $project_id Идентификатор проекта.
     * @param int $attempt Количество попыток взаимодействия.
     * @return void
     */

    function setLeadForWebUtmAndAttempt(string $phone, array $utm, int $project_id, int $attempt, $NewLeadWeb = null): void
    {
            LeadForWeb::query()->updateOrCreate([
                'phone' => $phone,
                'project_id' => $project_id,
                'dateWeb' => now()->format('Y-m-d'),
            ], [
                'attempt' => $attempt,
                'utm_source' => $utm ['source'] ?? '',
                'utm_medium' => $utm ['medium']  ?? '',
            ]);

            if (!empty($NewLeadWeb)){
                $NewLeadWeb->addWeb = 1;
                $NewLeadWeb->save();
            }

            NewLeadWeb::query()
                ->where('phone',$phone)
                ->where('project_id',$project_id)
                ->update(['addWeb' => '1']);
    }

    /**
     * Устанавливает расписание уведомлений для телефона.
     *
     * @param int $project_id Идентификатор проекта.
     * @param string $phone Номер телефона.
     * @return void
     */

    public function setNotificationSchedule(int $project_id, string $phone)
    {
        $notificationPipleline = WebNotificationPipleline::query()->where('project_id', $project_id)->whereNotNull('timeSend')->get();

        foreach ($notificationPipleline as $step) {
            $timeToSend = Carbon::parse($step->timeSend);
            $hour = $timeToSend->hour;
            $minutes = $timeToSend->minute;
            $phone = trim($phone, '+');

            if ($timeToSend->gt(now()) || $step->type == 'nextDay') { //если дата еще не прошла

                if ($step->type == 'nextDay'){
                    $timeSend = now()->addDay()->setTime($hour, $minutes);
                } else {
                    $timeSend = now()->setTime($hour, $minutes);
                }

                WebNotificationSchedule::query()->updateOrCreate([
                    'phone' => $phone,
                    'project_id' => $project_id,
                    'step_id' => $step->id,
                ], [
                    'timeSend' => $timeSend,
                ]);
            }

        }
    }

}
