<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Jobs\Google\LeadToGoogleSheet;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\WebSms;
use App\Models\Project;
use App\Traits\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LeadController extends Controller
{
    use Notification;

    public $utm_source;
    public $utm_medium;

    public function newLead(Request $request,$id)
    {
        $phone = $request->phone ?? '';
        try {
            $CheckRussiaPhoneController = new CheckRussiaPhoneController();
            $phone = $CheckRussiaPhoneController->checkPhone($phone);
        } catch (\Exception $exception ){
            Log::error('Ошибка проверки номера');
        }

        $project = Project::query()->find($id);

        if ($phone != false) {
            $this->newLeadsAddWeb($project,$phone);
            $phone = trim($phone,'+');

            $issetNewLeadWeb = NewLeadWeb::query()->where('project_id', '=',$id)->where('phone', '=',$phone)->exists();

            $data ['phone'] = $phone;
            $data ['project_id'] = $id;
            $data ['utm_source'] = $request->utm_source ?? null;
            $data ['utm_medium'] = $request->utm_medium ?? null;
            $data ['addWeb'] = '0';


            $sheetDate = [[
                'date' => now()->format('d-m-y'),
                'phone' => $phone,
                'utm_source' => $request->utm_medium ?? '',
                'utm_medium' => $request->utm_medium ?? null,
                'typeCall' => 'inside',
            ]];

            LeadToGoogleSheet::dispatch($sheetDate,$id);


            if ($phone == '79293762528' || $phone == '79259126938' || $phone == '79181275931') {
                $issetNewLeadWeb = false;
            }

            if (!$issetNewLeadWeb) {
                NewLeadWeb::query()->updateOrCreate([
                    'phone' => $phone,
                    'project_id' => $id,
                ], $data);

                $notificationSetting = NotificationSetting::query()->where('project_id',$project->id)->get()->first();
                if ($notificationSetting->welcomeSms == '1') {
                    $sms = WebSms::query()->where('project_id', $id)->whereNull('timeSend')->get()->first();
                    $WelcomeMessageController = new WelcomeMessageController();
                    $WelcomeMessageController->sendWelcomeSms($sms, $phone,$project);
//                    $this->sendWelcomeSms($sms, $phone,$project);
                }
            }
        }
        return 200;
    }

    public function getUtmFormPhone($project_id,$phone){
        $NewLeadWeb = NewLeadWeb::query()->where('project_id',$project_id)->where('phone',$phone)->get()->last();
        if (!empty($NewLeadWeb)){
            $this->utm_source = $NewLeadWeb->utm_source;
            $this->utm_medium = $NewLeadWeb->utm_medium;

        }
    }

    public function newLeadsAddWeb($project, $phone)
    {
        $notificationSetting = $project->getNotificationSetting;
        if (isset($notificationSetting->dateWeb) && Carbon::parse($notificationSetting->dateWeb)->isToday()) {
            $NotificationController = new NotificationController();
            $NotificationController->addNotification($phone, $project->id);
        }
    }

}
