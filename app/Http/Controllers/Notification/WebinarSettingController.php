<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Jobs\Notification\Zvonobot;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WebinarSettingController extends Controller
{
    public function autoWebActive(Request $request,$id){
//        $validator = $request->validate([
//            'autoWeb' => 'required',
//        ],[
//            'autoWeb' => 'Обязательно для заполнения',
//        ]);

        $projectModel =  Project::query()->find($id);
        $notificationSettingModel =  $projectModel->getNotificationSetting;

     if (isset($request->autoWeb)){
        $notificationSettingModel->autoWeb = 1;
        $notificationSettingModel->dateWeb = now()->format('Y-m-d');
     }   else {
         $notificationSettingModel->autoWeb = 0;
     }

        $notificationSettingModel->save() ;

        return back();

    }
    public function zvonobotKey(Request $request,$id){

        $validator = $request->validate([
            'zvonobot_key' => 'required',
        ],[
            'zvonobot_key' => 'Обязательно для заполнения',
        ]);

        $projectModel =  Project::query()->find($id);
        $notificationSettingModel =  $projectModel->getNotificationSetting;

        $zvonobot_key = $request->zvonobot_key ?? '';

        if (!empty($zvonobot_key)) {
            $data ['apiKey'] = $zvonobot_key;
            $response = Http::withHeaders([
                "content-type" => "application/json",
                'accept' => 'application/json',
            ])->post('https://lk.zvonobot.ru/apiCalls/userInfo',$data);

            if ($response->json()['status'] != "success") {
                return redirect()->back()->withErrors(['Неправильный Zvonobot_key']);
            }
        }


        $notificationSettingModel->zvonobot_key = $zvonobot_key;
        $notificationSettingModel->save() ;

        return back();
    }

    public function zvonobotPhoneOutput(Request $request,$id){

        $validator = $request->validate([
            'phoneOutput' => 'required',
        ],[
            'phoneOutput' => 'Обязательно для заполнения',
        ]);

        $projectModel =  Project::query()->find($id);
        $notificationSettingModel =  $projectModel->getNotificationSetting;
        $notificationSettingModel->zvonobotPhoneOutput = $request->phoneOutput;
        $notificationSettingModel->save();

        return back();
    }

    public function zvonobotPhoneOutputTest(Request $request,$id){
        $validator = $request->validate([
            'testPhone' => 'required',
            'audioId' => 'required',
        ],[
            'testPhone' => 'Обязательно для заполнения',
            'audioId' => 'Обязательно для заполнения',
        ]);

        $project = Project::query()->find($id);
        $notificationSetting = $project->getNotificationSetting;

        $data ['apiKey'] = $notificationSetting->zvonobot_key;
        $data ['record'] = ['id' => $request->audioId];
        $data ['phone'] = $request->testPhone;
        $data ['plannedAt'] = now()->addMinutes(2)->timestamp;

        $data ['dutyPhone'] = '1';
        Zvonobot::dispatch($id, $data);

    }
}
