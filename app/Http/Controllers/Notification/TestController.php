<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Intensive\IntensiveUserController;
use App\Http\Controllers\Notification\Services\SmsController;
use App\Jobs\Notification\WebNotification;
use App\Jobs\TestJob;
use App\Models\Intensive\IntensiveUser;
use App\Models\Notification\JobTelegramNotification;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Notification\WebSms;
use App\Models\Project;
use App\Models\Webinar\LeadForWeb;
use App\Traits\Notification;
use App\Models\Notification\NotificationSetting;
use Carbon\Carbon;


class
TestController extends Controller
{
    use Notification;

        public function requestTest()
        {
            return 200;
        }

        public function requestDrag()
        {
            return view('test.testDrop');
        }
    public function testPlanned($id)
    {
        $notification = WebNotificationSchedule::find($id);
                $jobId = JobTelegramNotification::query()->updateOrCreate([
                    'project_id' => $notification->project_id,
                    'phone' => $notification->phone,
                    'step_id' => $notification->step_id,
                ],[
                    'user_id' => $notification->user_id,
                    'timeSend' => $notification->timeSend,
                ])->id;
                usleep( 1000); // 1/10 секунды
                WebNotification::dispatch($jobId);
    }

    public function testCompleteAction($schedules_id)
    {
        $NotificationSchedule = WebNotificationSchedule::query()->find($schedules_id);
        $NotificationStep = WebNotificationPipleline::query()->find($NotificationSchedule->step_id);
        if ($NotificationStep->content_type == 'CALL'){
            $this->sendCall($NotificationStep,$NotificationSchedule);
        }
        if ($NotificationStep->content_type == 'SMS'){
            $this->sendSms($NotificationStep->content_id,$NotificationSchedule);
        }
        if ($NotificationStep->content_type == 'WA'){
            $this->sendWhatsUp($NotificationStep,$NotificationSchedule);
        }

    }

    public function testKernels(){
        $notificationSettings = NotificationSetting::query()->whereNotNull('dateWeb')->get();

        foreach ($notificationSettings as $project_telegram){
            if (Carbon::parse($project_telegram->dateWeb)->isToday()){
                $leadForWeb = LeadForWeb::query()->where('project_id','=',$project_telegram->project_id)->whereDate('dateWeb','=',today()->format('Y-m-d'))->get();

                foreach ($leadForWeb as $user){
                    $utm_source = $user->utm_source ?? '';
                    $NotificationController = new NotificationController();
                    $NotificationController->addNotification($user->phone, $project_telegram->project_id, $utm_source);
                }

                $leadForWebs = NewLeadWeb::query()->where('project_id','=',$project_telegram->project_id)->where('addWeb', '=',0)->get();
                foreach ($leadForWebs as $user){
                    $utm_source = $user->utm_source ?? '';
                    $NotificationController = new NotificationController();
                    $NotificationController->addNotification($user->phone, $project_telegram->project_id, $utm_source);
                }

            }
            sleep(1);
        }
    }

    public function sendWelcomeSmsAeroTest()
    {
        $project = Project::query()->find('114');
        $phone = '79259126938';
        $smsStep = WebSms::query()->where('project_id', $project->id)->whereNull('timeSend')->get()->first();

        $notificationSetting = $project->getNotificationSetting;

        $VariableController = new VariableController();
        $text = $VariableController->setVariables($smsStep->text, $phone, $project->id);

        $data = [
            'phone' => $phone,
            'text' => $text,
            'smsAeroSenderName' => $notificationSetting->smsAeroSenderName,
            'smsAeroEmail' => $notificationSetting->smsAeroEmail,
            'smsAero_key' => $notificationSetting->smsAero_key,
        ];

        $send = new SmsController();
        $send->sendSmsAeroJob($data);

    }



    public function testRate($id){
        TestJob::dispatch('123');
    }

    public function testAddNotification($phone, $project_id){
        $NotificationController = new NotificationController();
        $NotificationController->addNotification($phone, $project_id);
    }

}
