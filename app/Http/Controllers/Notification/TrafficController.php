<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Facebook\ApiConvertion;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Models\Facebook\FacebookStat;
use App\Models\Notification\TildaLead;
use App\Models\Offer;
use App\Models\Payment;
use App\Models\Sipout\SipoutCallId;
use App\Models\Webinar\LeadForWeb;
use App\Models\Webinar\WebinarUser;
use App\Models\Yandex\YandexDirectStatistica;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TrafficController extends Controller
{
        public function WebinarReaching($id){
//            $glushko = '79259126938';
//            $LeadForWeb = LeadForWeb::query()->where('project_id',$id)->where('phone',$glushko)->delete();
//
//            $LinkInvitePhoneController = New LinkInvitePhoneController();
//            $link = $LinkInvitePhoneController->createInviteLink($id);
//
            return view('webinar.webinarReaching',compact('id'));
    }

    public function  trafficAnalyticaTargetNoAdmin($id)
    {
        return view('traffic.trafficAnaliticaNoAdminFacebook',compact('id'));
    }

    public function trafficAnalyticaTarget($id){
        return view('traffic.trafficAnalyticaFacebook',compact('id'));
    }

    public function trafficAnalyticaYandex($id){
        return view('traffic.trafficAnalyticaYandex',compact('id'));
    }

    public function targetUpdateStatStore($id)
    {
        $FacebookStat = FacebookStat::query()->where('project_id', $id)->pluck('account_id')->unique();
        foreach ($FacebookStat as $account_id) {
            if ($account_id == '708424221102216'){
                continue;
            }
            $this->targetUpdateStat($id, $account_id);
        }
        return back()->with('success', 'Статистика обновлена');
    }



    public function targetUpdateStat($id,$ad_account_id,$startDate = null, $dateTo = null){
        $ApiConvertion = new ApiConvertion();

        $date = FacebookStat::query()->where('account_id',$ad_account_id)->where('project_id',$id)->get('date')->last();
        $ad_account_id = "act_$ad_account_id";
        if (!empty($date) && empty($startDate)) {
            $startDate = $date->date;
        } else {
            if ($startDate == null){
                $startDate = '2025-01-01';
            }
        }
        $startDate = Carbon::createFromDate($startDate);
        $day = $startDate->toDateString();
        $stat = $ApiConvertion->getStatFacebook($id,$day,$ad_account_id);

        $ApiConvertion->setAdStat($id,$stat);
        return $stat;
        }

    public function targetLeads($id){
//            $facebook = FacebookStat::query()->where('project_id',$id)->with('getPayment')->get();
        return view('traffic.trafficLeads',compact('id'));
    }

        public function targetLeadsUkolova($id){
//            $facebook = FacebookStat::query()->where('project_id',$id)->with('getPayment')->get();
            return view('traffic.trafficLeadsUkolova',compact('id'));
        }

        public function getAdIdUkolova($id,$date_from,$date_to,$search)
        {
            $facebook_ad_ids = FacebookStat::where('project_id', $id);

            if (!empty($date_to)) {$facebook_ad_ids->whereDate('date', '<=', $date_to);}
            if (!empty($date_from)) {$facebook_ad_ids->whereDate('date', '>=', $date_from);}

            if (!empty($search)) {$facebook_ad_ids->where(function($q) use ($search) {
                $q->where('ad_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('adset_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('campaign_name', 'LIKE', '%' . $search . '%');
            });}

            $facebook_ad_id =  $facebook_ad_ids
                ->with(['getLeadsUkolova'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('created_at', '>=', $date_from)
                        ->whereDate('created_at', '<=', $date_to);
                },'getWebinarUserUkolova'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('date', '>=', $date_from)
                        ->whereDate('date', '<=', $date_to);
                }])
                ->get()
                ->map(function ($stats) {
                    return [
                        'lead' => $stats->getLeadsUkolova->count(),
                        'webinar' => $stats->getWebinarUserUkolova->count(),
                        'clicks' => $stats->clicks,
                        'impressions' => $stats->impressions,
                        'spend' => $stats->spend,
                        'clickAll' => $stats->clickAll,
                        'clickLink' => $stats->clickLink,
                        'account_id' => $stats->account_id,
                        'account_name' => $stats->account_name,
                        'campaign_id' => $stats->campaign_id,
                        'campaign_name' => $stats->campaign_name,
                        'adset_id' => $stats->adset_id,
                        'adset_name' => $stats->adset_name,
                        'ad_name' => $stats->ad_name,
                        'ad_id' => $stats->ad_id,
                        'payment' => 0,
                        'paymentMain' => 0,
                    ];
                });

            return $facebook_ad_id;
        }

        public function getAdId($id,$date_from,$date_to,$search)
        {
            $facebook_ad_ids = FacebookStat::where('project_id', $id);

            if (!empty($date_to)) {$facebook_ad_ids->whereDate('date', '<=', $date_to);}
            if (!empty($date_from)) {$facebook_ad_ids->whereDate('date', '>=', $date_from);}

            if (!empty($search)) {$facebook_ad_ids->where(function($q) use ($search) {
                $q->where('ad_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('adset_name', 'LIKE', '%' . $search . '%')
                    ->orWhere('campaign_name', 'LIKE', '%' . $search . '%');
            });}

            $facebook_ad_id =  $facebook_ad_ids
                ->with(['getLeads'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('created_at', '>=', $date_from)
                        ->whereDate('created_at', '<=', $date_to);
                },'getWebinarUser'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('date', '>=', $date_from)
                        ->whereDate('date', '<=', $date_to);
                }])
                ->get()
                ->map(function ($stats) {
                    return [
                        'lead' => $stats->getLeads->count(),
                        'webinar' => $stats->getWebinarUser->count(),
                        'clicks' => $stats->clicks,
                        'impressions' => $stats->impressions,
                        'spend' => $stats->spend,
                        'clickAll' => $stats->clickAll,
                        'clickLink' => $stats->clickLink,
                        'account_id' => $stats->account_id,
                        'account_name' => $stats->account_name,
                        'campaign_id' => $stats->campaign_id,
                        'campaign_name' => $stats->campaign_name,
                        'adset_id' => $stats->adset_id,
                        'adset_name' => $stats->adset_name,
                        'ad_name' => $stats->ad_name,
                        'ad_id' => $stats->ad_id,
                        'payment' => 0,
                        'paymentMain' => 0,
                    ];
                });
            return $facebook_ad_id;
        }

        public function getYandex($id,$date_from,$date_to,$search)
        {
            $ad_ids = YandexDirectStatistica::query()->where('project_id', $id);

            if (!empty($date_to)) {$ad_ids->whereDate('date', '<=', $date_to);}
            if (!empty($date_from)) {$ad_ids->whereDate('date', '>=', $date_from);}

            if (!empty($search)) {$ad_ids->where(function($q) use ($search) {
                $q->where('CampaignName', 'LIKE', '%' . $search . '%')
                    ->orWhere('AdGroupId', 'LIKE', '%' . $search . '%')
                    ->orWhere('AccountName', 'LIKE', '%' . $search . '%')
                    ->orWhere('CampaignId', 'LIKE', '%' . $search . '%');
            });}

            $ad_id =  $ad_ids
                ->with(['getLeads'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('created_at', '>=', $date_from)
                        ->whereDate('created_at', '<=', $date_to);
                },'getLeadsCampaign'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('created_at', '>=', $date_from)
                        ->whereDate('created_at', '<=', $date_to)
                    ;
                },'getWebinarUser'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('date', '>=', $date_from)
                        ->whereDate('date', '<=', $date_to);
                },'getWebinarUserCampaign'  => function ($query) use ($date_from,$date_to) {
                    $query->whereDate('date', '>=', $date_from)
                        ->whereDate('date', '<=', $date_to);
                }
                ,'getLeads.getPayments','getLeadsCampaign.getPayments'])
                ->get()
                ->map(function ($stats) {
                    $payments = $stats->getLeads
                        ->map(function ($leads) {
                            $paymentCollection = $leads->getPayments->where('product', 'Оплата интенсив по телеграм с Юлией Купр')
                                ->whereIn('status', ['succeeded', 'по карте'])
                                ->filter(function($payment) use ($leads) {
                                    return $payment->created_at > $leads->created_at;
                                });
                            $paymentMainCollection = $leads->getPayments->whereIn('product',['Внесите предоплату 1500 руб., чтобы забронировать место по тарифу ПРОФИ','Предоплата 100к на телеграм с Юлией Купр'] )
                                ->whereIn('status', ['succeeded', 'по карте'])
                                ->filter(function($payment) use ($leads) {
                                    return $payment->created_at > $leads->created_at;
                                });;


                            if ($paymentCollection->isNotEmpty()) {
                                $leads->payment = $paymentCollection->count();
                            }

                            if ($paymentMainCollection->isNotEmpty()) {
                                $leads->paymentMain = $paymentMainCollection->count();
                            }

                            return $leads;
                        });

                    $paymentsCampaign = $stats->getLeadsCampaign
                        ->map(function ($leadss) {
                            $paymentCollection = $leadss->getPayments->where('product', 'Оплата интенсив по телеграм с Юлией Купр')
                                ->whereIn('status', ['succeeded', 'по карте'])
                                ->filter(function($payment) use ($leadss) {
                                    return $payment->created_at > $leadss->created_at;
                                });
                            $paymentMainCollection = $leadss->getPayments->whereIn('product',['Внесите предоплату 1500 руб., чтобы забронировать место по тарифу ПРОФИ','Предоплата 100к на телеграм с Юлией Купр'] )
                                ->whereIn('status', ['succeeded', 'по карте'])
                                ->filter(function($payment) use ($leadss) {
                                    return $payment->created_at > $leadss->created_at;
                                });;


                            if ($paymentCollection->isNotEmpty()) {
                                $leadss->payment = $paymentCollection->count();
                            }

                            if ($paymentMainCollection->isNotEmpty()) {
                                $leadss->paymentMain = $paymentMainCollection->count();
                            }

                            return $leadss;
                        });

                    $lead = $stats->getLeads->count() ?: $stats->getLeadsCampaign->count();
                    $webinar = $stats->getWebinarUser->count() ?: $stats->getWebinarUserCampaign->count();
                    $payment = $payments->sum('payment') ?: $paymentsCampaign->sum('payment');

                    return [
                        'lead' => $lead,
                        'webinar' => $webinar,
                        'webinarWarm' => $stats->getWebinarUser->whereIn('warm',['succeeded','hot'])->count(),
                        'clicks' => $stats->Сlicks,
                        'impressions' => $stats->impressions,
                        'spend' => $stats->Cost,
                        'account_name' => $stats->AccountName,
                        'campaignLeads' => $stats->getLeadsCampaign->count(),
                        'campaign_id' => $stats->CampaignId,
                        'campaign_name' => $stats->CampaignName,
                        'adset_id' => $stats->AdGroupId,
                        'adset_name' => $stats->AdGroupName,
                        'ad_id' => $stats->AdId,
                        'payment' => $payment,
                        'paymentMain' => $payments->sum('paymentMain'),
                    ];
                });

            return $ad_id;
        }

        public function getFacebook($id,$date_from,$date_to,$search)
    {
        $ad_ids = FacebookStat::query()->where('project_id', $id);

        if (!empty($date_to)) {$ad_ids->whereDate('date', '<=', $date_to);}
        if (!empty($date_from)) {$ad_ids->whereDate('date', '>=', $date_from);}

        if (!empty($search)) {$ad_ids->where(function($q) use ($search) {
            $q->where('ad_name', 'LIKE', '%' . $search . '%')
                ->orWhere('adset_name', 'LIKE', '%' . $search . '%')
                ->orWhere('account_name', 'LIKE', '%' . $search . '%')
                ->orWhere('campaign_name', 'LIKE', '%' . $search . '%');
        });}

        $ad_id =  $ad_ids
            ->with(['getLeads'  => function ($query) use ($date_from,$date_to) {
                $query->whereDate('created_at', '>=', $date_from)
                    ->whereDate('created_at', '<=', $date_to);
            },'getWebinarUser'  => function ($query) use ($date_from,$date_to) {
                $query->whereDate('date', '>=', $date_from)
                    ->whereDate('date', '<=', $date_to);
            },'getLeads.getPayments'])
            ->get()
            ->map(function ($stats) {
                $payments = $stats->getLeads
                    ->map(function ($leads) {
                        $paymentCollection = $leads->getPayments
                            ->whereIn('status', ['succeeded', 'по карте'])
                            ->filter(function($payment) use ($leads) {
                                return $payment->created_at > $leads->created_at;
                            });
                        if ($paymentCollection->isNotEmpty()) {
                            $leads->payment = $paymentCollection->count();
                        }
                        return $leads;
                    });
                return [
                    'lead' => $stats->getLeads->count(),
                    'webinar' => $stats->getWebinarUser->count(),
                    'webinarWarm' => $stats->getWebinarUser->whereIn('warm',['succeeded','hot'])->count(),
                    'clicks' => $stats->clickLink,
                    'impressions' => $stats->impressions,
                    'spend' => $stats->spend,
                    'account_id' => $stats->account_id,
                    'account_name' => $stats->account_name,
                    'campaign_id' => $stats->campaign_id,
                    'campaign_name' => $stats->campaign_name,
                    'adset_id' => $stats->adset_id,
                    'adset_name' => $stats->adset_name,
                    'ad_id' => $stats->ad_id,
                    'ad_name' => $stats->ad_name,
                    'payment' => $payments->sum('payment'),
                    'paymentMain' => $payments->sum('paymentMain'),
                ];
            });

        return $ad_id;
    }



}
