<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Intensive\IntensiveUserController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Models\Intensive\IntensiveUser;
use App\Models\linkChangeModel;
use App\Models\Notification\GlobalVariable;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\Variable;
use App\Models\Notification\Wa\WebWa;
use App\Models\Notification\WebSms;
use App\Models\Project;
use App\Models\Webinar\LeadForWeb;
use Throwable;

class VariableController extends Controller
{

    public function setVariables($text,$phone,$project_id){
        $hasBraces = $this->hasBraces($text);
        if (count($hasBraces) > 0) {
            foreach ($hasBraces as $variable) {

                if ($variable == 'phone') {
                    $text = str_replace('#{phone}', $phone, $text);
                }

                if ($variable == 'name') {
                    $name = LeadForWeb::where('phone', $phone)
                        ->whereNotNull('user_name')
                        ->value('user_name') ?? '';
                    if ($name != ''){
                        $name = explode(' ', trim($name))[0];
                        $name = $this->transliterate($name);
                    }
                    $text = str_replace('#{name}',$name, $text);
                }

                if ($variable == 'linkInviteIntensive') {
                    $IntensiveUser = IntensiveUser::query()->where('project_id', $project_id)->where('phone', $phone)->with('getLinkMainChat')->get()->last();
                    if (!empty($IntensiveUser )){
                        $IntensiveUserController =  new IntensiveUserController() ;
                        $link = $IntensiveUserController->getLinkIntensive($IntensiveUser);
                        if ($link == null){
                            return null;
                        }
                        $text = str_replace('#{linkInviteIntensive}', $link, $text);
                    } else {
                        return null;
                    }
                }



                if ($variable == 'linkInviteUser') {
                    $LinkInvitePhoneController = new LinkInvitePhoneController();
                    try {
                        $link = $LinkInvitePhoneController->createInviteLinkAndSetPhone($project_id,$phone);
                    } catch (Throwable $e) {
                        report($e);
                        if ($project_id == 114){
                            $link = 'https://t.me/+ZK4ocxUrL7E3ZTI6';
                        }
                    }
                    $text = str_replace('#{linkInviteUser}', $link, $text);
                }
            }
        }
        return $text;
    }

    function setVariablesUser($phone, $project_id, $step)
    {
        $leadForWeb = LeadForWeb::query()->where('project_id', $project_id)->where('phone', $phone)->get()->last();

        if ($step->content_type == 'SMS') {
            $sms = WebSms::query()->find($step->content_id);

            $hasBraces = $this->hasBraces($sms->text);

            if (count($hasBraces) > 0) {
                foreach ($hasBraces as $variblal) {
                    $varible = substr($variblal, 1);

                    if ($varible == 'linkWeb') {
                        $variable_value = $this->shortLink($phone, $project_id, $leadForWeb);
                    }

                    if ($varible == 'linkInvite') {
                        $projectTelegram = Project::query()->find($project_id);

                        $linkChangeModel = linkChangeModel::query()->where('project_id', $projectTelegram->id)->get();
                        $lead = NewLeadWeb::query()->where('project_id', $project_id)->where('phone', $phone)->get()->last();

                        $linkChange = $linkChangeModel->where('utm_source', $lead->utm_source)->where('linkName', '=', $varible)->first();
                        if (empty($linkChange->link)) {
                            $linkChange = $linkChangeModel->where('utm_source', 'all')->where('linkName', '=', $varible)->first();
                        }
                        $variable_value = $linkChange->link;
                    }


                    Variable::query()->updateOrCreate([
                        'lead_for_web_id' => $leadForWeb->id,
                        'project_id' => $project_id,
                        'variable_name' => $varible,
                    ], [
                        'variable_value' => $variable_value,
                    ]);

                }
            }
        }

        if ($step->content_type == 'WA') {
            $wa = WebWa::query()->find($step->content_id);
            $hasBraces = $this->hasBraces($wa->text);

            if (count($hasBraces) > 0) {
                foreach ($hasBraces as $variblal) {
                    $varible = substr($variblal, 1);

                    if ($varible == 'linkWeb') {
                        $variable_value = $this->shortLink($phone, $project_id, $leadForWeb);
                    }

                    if ($varible == 'linkInvite') {
                        $projectTelegram = Project::query()->find($project_id);
                        $linkChangeModel = linkChangeModel::query()->where('project_id', $projectTelegram->id)->get();
                        $lead = NewLeadWeb::query()->where('project_id', $project_id)->where('phone', $phone)->get()->last();
                        $linkChange = $linkChangeModel->where('utm_source', $lead->utm_source)->where('linkName', '=', $varible)->first();
                        if (empty($linkChange->link)) {
                            $linkChange = $linkChangeModel->where('utm_source', 'all')->where('linkName', '=', $varible)->first();
                        }
                        $variable_value = $linkChange->link;
                    }

                    Variable::query()->updateOrCreate([
                        'lead_for_web_id' => $leadForWeb->id,
                        'project_id' => $project_id,
                        'variable_name' => $varible,
                    ], [
                        'variable_value' => $variable_value,
                    ]);

                }
            }
        }

    }

    function hasBraces($string)
    {
        // Создаем пустой массив для хранения найденных слов
        $words = array();
        // Используем регулярное выражение для поиска всех слов в фигурных скобках
        preg_match_all('/#\{([^\}]+)\}/', $string, $matches);

        // Перебираем все найденные совпадения
        foreach ($matches[1] as $match) {
            // Добавляем слово в массив
            $words[] = $match;
        }
        // Возвращаем массив слов
        return $words;
    }


    public function setVariablesWaba($text,$phone,$project_id){
        $hasBraces = $this->hasBraces($text);
        if (count($hasBraces) > 0) {
            foreach ($hasBraces as $variable) {

                if ($variable == 'phone') {
                    $text = str_replace('#{phone}', $phone, $text);
                }

                if ($variable == 'name') {
                    $name = LeadForWeb::where('phone', $phone)
                        ->whereNotNull('user_name')
                        ->value('user_name') ?? '';
                    if ($name != ''){
                        $name = explode(' ', trim($name))[0];
                        $name = $this->transliterate($name);
                    }
                    $text = str_replace('#{name}',$name, $text);
                }

                if ($variable == 'linkInviteIntensive') {
                    $IntensiveUser = IntensiveUser::query()->where('project_id', $project_id)->where('phone', $phone)->with('getLinkMainChat')->get()->last();
                    if (!empty($IntensiveUser )){
                        $IntensiveUserController =  new IntensiveUserController() ;
                        $link = $IntensiveUserController->getLinkIntensive($IntensiveUser);
                        if ($link == null){
                            return null;
                        }
                        $text = str_replace('#{linkInviteIntensive}', $link, $text);
                    } else {
                        return null;
                    }
                }



                if ($variable == 'linkInviteUser') {
                    $LinkInvitePhoneController = new LinkInvitePhoneController();
                    try {
                        $link = $LinkInvitePhoneController->createInviteLinkAndSetPhone($project_id,$phone);
                    } catch (Throwable $e) {
                        report($e);
                        if ($project_id == 114){
                            $link = 'https://t.me/+ZK4ocxUrL7E3ZTI6';
                        }
                    }
                    $text = str_replace('#{linkInviteUser}', $link, $text);
                }
            }
        }
        return $text;
    }

    public function shortLink($phone, $project_id, $leadForWeb)
    {
        $linksWeb = GlobalVariable::query()->where('project_id', $project_id)->where('variable_name', '=', 'linkWeb')->get()->first();
        $date['linkFull'] = $linksWeb->variable_value . 'utm_source=' . $leadForWeb->utm_source . '&utm_medium=' . $leadForWeb->utm_medium . '&phone=' . $phone;

        $url = 'https://kupr.pro/api/shortLink/add';

        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->post($url, $date);

        if (strlen($response->body()) < 10) {
            return $response->body();
        } else {
            return 'error';
        }

    }

    function transliterate($text) {
        $map = [
            'А' => 'A', 'Б' => 'B', 'В' => 'V', 'Г' => 'G', 'Д' => 'D', 'Е' => 'E', 'Ё' => 'E', 'Ж' => 'Zh', 'З' => 'Z', 'И' => 'I', 'Й' => 'Y',
            'К' => 'K', 'Л' => 'L', 'М' => 'M', 'Н' => 'N', 'О' => 'O', 'П' => 'P', 'Р' => 'R', 'С' => 'S', 'Т' => 'T', 'У' => 'U', 'Ф' => 'F',
            'Х' => 'Kh', 'Ц' => 'Ts', 'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ъ' => '', 'Ы' => 'Y', 'Ь' => '', 'Э' => 'E', 'Ю' => 'Yu', 'Я' => 'Ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i', 'й' => 'y',
            'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'kh', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya'
        ];

        return preg_match('/[\p{Cyrillic}]/u', $text) ? strtr($text, $map) : $text;
    }
}
