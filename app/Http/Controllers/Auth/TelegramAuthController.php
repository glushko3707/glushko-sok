<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class TelegramAuthController extends Controller
{
    public function storeTelegram(Request $request)
    {
        $request->validate([
            'id' => 'required',
        ]);

        $name = $request->username ?? $request->first_name ?? $request->id;
        $telegram_id = $request->id;
        $email = $telegram_id . '@mail.ru';

        // проверяем существует ли такой пользователь в базе с таким telegram_id
        $user = User::where('telegram_id', $telegram_id)->first();
        if ($user) {
            Auth::login($user);
            return back();
        }
        $user = User::updateOrCreate([
            'name' => $name,
            'email' => $email,
            'telegram_id' => $telegram_id,
            'password' => Hash::make(Str::random(10)),
        ],[
            'name' => $name,
            'email' => $email,
            'telegram_id' => $telegram_id,
        ]);

        event(new Registered($user));
        Auth::login($user);
        return back();
    }
}
