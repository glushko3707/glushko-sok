<?php

namespace App\Http\Controllers\Webinar;

use App\Http\Controllers\Controller;
use App\Models\Notification\NewLeadWeb;
use App\Models\Payment;
use App\Models\Webinar\WebinarUser;
use Illuminate\Http\Request;

class LeadStatController extends Controller
{
    public function leadStat(Request $request,$id){

        $dateTo = $request->date_to;
        $dateFrom = $request->date_from;

        $newLeadWeb = NewLeadWeb::query()->where('project_id',$id);
        $webinarUser = WebinarUser::query()->where('project_id',$id);
        $payment = Payment::query()->where('project_id',$id)->whereIn('status',['succeeded','по карте'])->where('sum','=','490');
        $paymentFull = Payment::query()->where('project_id',$id)->whereIn('status',['succeeded','по карте'])->where('sum','=','2500');

        if (!empty($dateTo)) {
            $newLeadWeb->whereDate('updated_at', '<=', $dateTo);
            $webinarUser->whereDate('date', '<=', $dateTo);
            ;}
        if (!empty($dateFrom)) {
            $newLeadWeb->whereDate('updated_at', '>=', $dateFrom);
            $webinarUser->whereDate('date', '>=', $dateFrom);
            }

        $newLeadWeb = $newLeadWeb->get()->groupBy('utm_source');
        $webinarUser = $webinarUser->get()->groupBy('utm_source');
        $payment = $payment->pluck('phone')->toArray();
        $paymentFull = $paymentFull->pluck('phone')->toArray();

        foreach ($newLeadWeb as $key => $value){

            if ($key == '' || $key == 'Insert'){
                continue;
            }
            $leadWeb[$key]['all'] = count($value);
            if (isset($webinarUser[$key])) {
                $leadWeb[$key]['webinar'] = count($webinarUser[$key] ?? 0);
            } else {
                $leadWeb[$key]['webinar'] = 0;
            }
            $leadWeb[$key]['paymentFull'] = count($value->whereIn('phone',$paymentFull));

            $leadWeb[$key]['paymentWeb'] = count($value->whereIn('phone',$payment));
        }

        return view('webinar.trafficStat',compact('id','leadWeb'));
    }
}
