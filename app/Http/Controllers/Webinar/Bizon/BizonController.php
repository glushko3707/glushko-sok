<?php

namespace App\Http\Controllers\Webinar\Bizon;

use App\Bizon365\{Bizon365API};
use App\Events\WebinarVisit;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Facebook\ApiConvertion;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Jobs\Webinar\AddUserAfterWeb;
use App\Models\Lead\Ukolova;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\TildaLead;
use App\Models\Payment;
use App\Models\Webinar\LeadForWeb;
use App\Models\Webinar\WebinarUser;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Throwable;


class BizonController extends Controller
{
    public function after(Request $request,$id){
        try {
            $data = $request->getContent();
            $data = json_decode($data);
            if (isset($data->webinarId)){
                $webinarId = $data->webinarId;
            }
        } catch (Throwable $e){
            report($e);
            return 201;
        }

        try{
            $webinarId = $webinarId ?? $data->webinarId;
            $webinarUser = $this->webinarIdUpdateStore($id,$webinarId);
        } catch (Throwable $e){
            report($e);
            return 201;
        }

        try{
            if (!empty($webinarUser) && is_array($webinarUser)){
                foreach ($webinarUser as $userWeb){
                    event(new WebinarVisit($userWeb, $id));
                }
            }
        } catch (Throwable $e){
            report($e);
            return 202;
        }



        return 200;
    }

    public function getWebinarLint ($id){

        return view('webinar.webinarList',compact('id'));

    }

//    public function getWebinarForInsert($viewers,$id){
//
//        if ($id == 498) {
//            $ApiConvertion = new ApiConvertion();
//            $ukolova = new \App\Http\Controllers\Tilda\Ukolova();
//            $ukolovaModel = Ukolova::query()->get(['_fbc','_fbp','phone']);
//        }
//
//        $LeadForWeb = LeadForWeb::query()->where('project_id',$id)->get();
//
//        foreach ($viewers as $viewer){
//
//           $phone = $viewer['phone'] ?? '' ;
//           $phone = trim($phone,'+');
//
//            if ($id == 498) {
//                $data = $ukolova->getLeadInfo($phone,$ukolovaModel);
//                $data ['event'] = 'webinarVisit';
//                if ($data){
////                    $ApiConvertion->setEventPixel($id,$data);
//                    sleep(1);
//                }
//            } else {
//               $lead = $LeadForWeb->where('phone',$phone)->whereNotNull('utm_source')->last();
//                if (isset($lead->utm_source) && !empty($lead->utm_source)){
//                    $utm_source = $lead->utm_source;
//                }
//            }
//
//
//            if (Carbon::parse($viewer['created'])->timezone('Europe/Moscow')->format('H') < 16 && $id == 114){
//                $date = today()->subDay()->format('Y-m-d');
//            } else if ($id == 114){
//                $date = today()->format('Y-m-d');
//            } else {
//                $date = Str::before($viewer['created'], 'T') ?? now()->format('Y-m-d');
//            };
//
//            $username = $viewer['username'] ?? '' ;
//            $region = $viewer['region'] ?? '' ;
//            $utm_source = $utm_source ?? $viewer['utm_source'] ?? '' ;
//            $utm_campaign = $viewer['utm_campaign'] ?? '' ;
//            $buttons = $viewer['buttons'][0]['id'] ?? '' ;
//            $banners = $viewer['banners'][0]['id'] ?? '' ;
//            $utm_medium = $viewer['utm_medium'] ?? '' ;
//            $utm_content = $viewer['utm_content']?? '' ;
//            $platform_id = $viewer['platform_id'] ?? '' ;
//            $timeWeb = 0;
//            foreach ($viewer['vi'] as $time){
//                $timeWeb = $timeWeb + ($time['e'] - $time['s']);
//            }
//
//            $timeWeb = (int)round($timeWeb/60,);
//
//            if ($viewer['playVideo'] == '0'){
//                $warm = 'cold';
//            } elseif (empty($buttons) && $timeWeb < 20 ) {
//                $warm = 'normal';
//            } elseif (empty($buttons) && $timeWeb > 20 ) {
//                $warm = 'hot';
//            } elseif (!empty($buttons)) {
//                $warm = 'hot';
//            }
//
//            $warm = $warm ?? '';
//
//            $usersWeb[] = [
//                'date' => $date,
//                'phone' => $phone,
//                'username' => $username,
//                'region' => $region,
//                'utm_source' => $utm_source,
//                'utm_campaign' => $utm_campaign,
//                'utm_medium' => $utm_medium,
//                'utm_content' => $utm_content,
//                'buttons' => $buttons,
//                'banners' => $banners,
//                'ssId' => $platform_id,
//                'timeWeb' => $timeWeb,
//                'warm' => $warm,
//            ];
//        }
//
//        $collectUser = new Collection();
//            if (isset($usersWeb) && count($usersWeb) > 0){
//            foreach ($usersWeb as $key => $user){
//
//                $userCol = $collectUser->where('phone','=',$user['phone'])->first();
//                if (!empty($userCol) && $user['phone'] != null && $user['phone'] != ''){
//                      $user['timeWeb'] = $userCol['timeWeb'] + $user['timeWeb'];
//                    if ($userCol['banners'] != ''){
//                      $user['banners'] = $userCol['banners'];
//                    }
//                } else {
//                    $collectUser->push($user);
//                }
//            }
//                return $collectUser->toArray();
//            } else {
//                return null;
//            }
//    }
    /**
     * Обрабатывает информацию о зрителях вебинара и агрегирует данные для вставки.
     *
     * @param array $viewers Массив данных зрителей вебинара.
     * @param int $id Идентификатор вебинара.
     * @return array|null Агрегированные данные о пользователях или null, если данные отсутствуют.
     */

    public function getWebinarForInsert(array $viewers, int $id) : ?array
    {
        $apiConvertion = $ukolova = $ukolovaModel = null;
        if ($id == 498) {
            $apiConvertion = new ApiConvertion();
            $ukolova = new \App\Http\Controllers\Tilda\Ukolova();
            $ukolovaModel = Ukolova::query()->get(['_fbc', '_fbp', 'phone']);
        }

        $leadForWebCollection = LeadForWeb::where('project_id', $id)->get();
        $usersWeb = [];

        foreach ($viewers as $viewer) {
            $phone = trim($viewer['phone'] ?? '', '+');

            if ($id == 498) {
                $data = $ukolova->getLeadInfo($phone, $ukolovaModel);
                if ($data) {
                    $data['event'] = 'webinarVisit';
                     $apiConvertion->setEventPixel($id, $data);
                    sleep(1);
                }
            } else {
                $lead = $leadForWebCollection->where('phone', $phone)
                    ->whereNotNull('utm_source')->last();
                $utm_source = $lead->utm_source ?? '';
            }


            $date = $this->determineDate($viewer['created'], $id);

            $username = $viewer['username'] ?? '';
            $region = $viewer['region'] ?? '';
            $utm_source = $utm_source ?? $viewer['utm_source'] ?? '';
            $utm_campaign = $viewer['utm_campaign'] ?? '';
            $utm_medium = $viewer['utm_medium'] ?? '';
            $utm_content = $viewer['utm_content'] ?? '';
            $platform_id = $viewer['platform_id'] ?? '';
            $buttons = $viewer['buttons'][0]['id'] ?? '';
            $banners = $viewer['banners'][0]['id'] ?? '';
            $playVideo = $viewer['playVideo'] ?? 0;
            $timeWeb = collect($viewer['vi'])->sum(fn($time) => $time['e'] - $time['s']);
            $timeWeb = (int)round($timeWeb / 60);

            $warm = $this->determineWarm($playVideo, $buttons, $timeWeb);

            $usersWeb[] = compact(
                'date', 'phone', 'username', 'region', 'utm_source', 'utm_campaign',
                'utm_medium', 'utm_content', 'buttons', 'banners', 'platform_id',
                'timeWeb', 'warm'
            );
        }
        return $this->aggregateUsers($usersWeb);
    }

    /**
     * Определяет дату на основе предоставленного времени создания и идентификатора вебинара.
     *
     * @param string $created Время создания зрителя в формате строки.
     * @param int $id Идентификатор вебинара.
     * @return string Дата в формате 'Y-m-d'.
     */

    private function determineDate(string $created, int $id): string
    {
        if ($id == 114) {
            return Carbon::parse($created)->timezone('Europe/Moscow')->format('H') < 16
                ? today()->subDay()->format('Y-m-d')
                : today()->format('Y-m-d');
        }
        return Str::before($created, 'T') ?? now()->format('Y-m-d');
    }

    private function determineWarm($playVideo, $buttons, $timeWeb)
    {
        if ($playVideo == '0') {
            return 'cold';
        } elseif (empty($buttons) && $timeWeb < 20) {
            return 'normal';
        } elseif (empty($buttons) && $timeWeb > 20) {
            return 'hot';
        } elseif (!empty($buttons)) {
            return 'hot';
        }
        return '';
    }

    /**
     * Агрегирует массив пользователей, суммируя время и объединяя баннеры для пользователей с одинаковыми номерами телефонов.
     *
     * @param array $usersWeb Массив пользователей, каждый из которых является экземпляром UserWeb.
     * @return array|null Агрегированный массив пользователей или null, если массив пуст.
     */
    private function aggregateUsers(array $usersWeb): ?array
    {
        $collectUser = new Collection();

        foreach ($usersWeb as $user) {
            $existingUser = $collectUser->firstWhere('phone', $user['phone']);

            if ($existingUser && $user['phone']) {
                $user['timeWeb'] += $existingUser['timeWeb'];
                $user['banners'] = $existingUser['banners'] ?: $user['banners'];
            } else {
                $collectUser->push($user);
            }
        }

        return $collectUser->isNotEmpty() ? $collectUser->toArray() : null;
    }


    public function WebinarStat($id){
        $this->updateWebinarUser($id);
        return view('webinar.webinarStat',compact('id'));
    }

    public function WebinarStatUpdate(Request $request){
        WebinarUser::query()->find($request->id)->update(['warm' => $request->status]);
        return back();
    }

    public function WebinarStatUpdateUtm(Request $request,$id){

        if(empty($request->date_from)){
            return back();
        }
        $date_from = $request->date_from;

        $webinarUsers = WebinarUser::query()->where('project_id',$id);
        $webinarUsers->whereDate('created_at', '>=', $date_from);
        $webinarUsers = $webinarUsers->where('phone','!=','')->get();
        $LeadForWeb = LeadForWeb::query()->where('project_id',$id)->get();
        foreach ($webinarUsers as $user){
            if (empty($user->utm_source) && !empty($user->phone)){
                $lastLead = $LeadForWeb->where('phone',$user->phone)->where('utm_source','!=','')->last();
                if (!empty($lastLead)){
                    $user->utm_source = $lastLead->utm_source ?? '';
                    $user->save();
                }
            }
        }

        return back();
    }



    function updateWebinarUser($id){
        // Загрузить связанные модели WebinarUser с помощью метода with
        $Payment = Payment::query()->where('project_id', $id)->with('webinarUsers')->get();

        foreach ($Payment as $key) {
            // Фильтровать коллекцию WebinarUser по дате и телефону
            $WebinarUser = $key->webinarUsers->filter(function ($item) use ($key) {
                return Carbon::parse($item['date'])->isSameDay($key->created_at) && $item['phone'] == $key->phone;
            });

            if ($WebinarUser->isNotEmpty()) {
                // Обновить поле warm у первой модели WebinarUser
                $WebinarUser->first()->update(['warm' => $key->status]);
            }
        }

    }

    public function enterRoom(\Illuminate\Http\Request $request,$id){
        if ($request->phone != null){
            $CheckRussiaPhoneController = new CheckRussiaPhoneController();
            $phone = $CheckRussiaPhoneController->checkPhone($request->phone);
            WebinarUser::query()->updateOrCreate([
                'project_id' => $id,
                'date' => now()->format('Y-m-d'),
                'phone' => $phone,
            ],[
                'phone' => $phone,
            ]);
        }

        return 200;
    }
    public function trafficStat(\Illuminate\Http\Request $request,$id){

        $dateFrom = $request->date_from ?? null;
        $dateTo = $request->date_to ?? null;

        $LeadForWebQuery = LeadForWeb::query()
            ->where('project_id',$id);

        if (!empty($dateTo)) {$LeadForWebQuery->whereDate('updated_at', '<=', $dateTo);}
        if (!empty($dateFrom)) {$LeadForWebQuery->whereDate('updated_at', '>=', $dateFrom);}
        $LeadForWeb = $LeadForWebQuery->get()->groupBy('dateWeb');
        $LeadForWeb = $LeadForWeb->reverse();

        if (!empty($LeadForWeb)){


            foreach ($LeadForWeb as $day => $value){
                $yandex = $value->where('utm_source','=','yandex');
                $tg = $value->where('utm_source','=','tg');
                $zvonobot = $value->where('utm_source','=','zvonobot');

                $warm = $value->where('warm','=','hot')->whereIn('warm',['hot','succeeded','open','по карте']);
                $payment = Payment::query()->whereDate('date','=',$day)->count();
                $convertionWarm = round(count($warm)/count($value)*100);
                $convertionPay =  round($payment/count($value)*100);

                $webinarUser[$day] = [
                    'count' => count($value),
                    'yandex' => count($yandex),
                    'tg' => count($tg),
                    'zvonobot' => count($zvonobot),
                    'hot' => count($warm),
                    'payment' => $payment,
                    'convertionPay' => $convertionPay,
                    'convertionWarm' => $convertionWarm,
                ];
            }
        }

        $webinarUser = $webinarUser ?? [];
        return view('webinar.trafficStat',compact('id','webinarUser','LeadForWeb'));
    }
    public function trafficStatSave(\Illuminate\Http\Request $request,$id){

        $dateFrom = $request->date_from ?? null;
        $dateTo = $request->date_to ?? null;

        $LeadForWebQuery = LeadForWeb::query()
            ->where('project_id',$id);

        if (!empty($dateTo)) {$LeadForWebQuery->whereDate('updated_at', '<=', $dateTo);}
        if (!empty($dateFrom)) {$LeadForWebQuery->whereDate('updated_at', '>=', $dateFrom);}
        $LeadForWeb = $LeadForWebQuery->get();

        // Создаем файл и записываем данные в него
        $filename = 'file.txt';
        $handle = fopen($filename, 'w');
        foreach ($LeadForWeb as $row) {
            fwrite($handle, $row->phone . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }

    public function tildaLeadStat(\Illuminate\Http\Request $request,$id){
            $dateFrom = $request->date_from ?? null;
            $dateTo = $request->date_to ?? null;

            $LeadForWebQuery = TildaLead::query()
                ->where('project_id',$id);

            if (!empty($dateTo)) {$LeadForWebQuery->whereDate('updated_at', '<=', $dateTo);}
            if (!empty($dateFrom)) {$LeadForWebQuery->whereDate('updated_at', '>=', $dateFrom);}
            $LeadForWeb = $LeadForWebQuery->get()->groupBy('date');
            $LeadForWeb = $LeadForWeb->reverse();

            if (!empty($LeadForWeb)){


                foreach ($LeadForWeb as $day => $value){
                    $yandex = $value->where('utm_source','=','yandex');
                    $tg = $value->where('utm_source','=','tg_blog');
                    $zvonobot = $value->where('utm_source','=','zvonobot');

                    $warm = $value->where('warm','=','hot')->whereIn('warm',['hot','succeeded','open','по карте']);
                    $payment = Payment::query()->whereDate('date','=',$day)->count();
                    $convertionWarm = round(count($warm)/count($value)*100);
                    $convertionPay =  round($payment/count($value)*100);

                    $webinarUser[$day] = [
                        'count' => count($value),
                        'yandex' => count($yandex),
                        'tg' => count($tg),
                        'zvonobot' => count($zvonobot),
                        'hot' => count($warm),
                        'payment' => $payment,
                        'convertionPay' => $convertionPay,
                        'convertionWarm' => $convertionWarm,
                    ];
                }
            }

            $webinarUser = $webinarUser ?? [];

            return view('webinar.tildaStat',compact('id','webinarUser','LeadForWeb'));

        }


    public function tildaLeadStatSave(\Illuminate\Http\Request $request,$id){

        $dateFrom = $request->date_from ?? null;
        $dateTo = $request->date_to ?? null;

        $LeadForWebQuery = TildaLead::query()
            ->where('project_id',$id);

        if (!empty($dateTo)) {$LeadForWebQuery->whereDate('updated_at', '<=', $dateTo);}
        if (!empty($dateFrom)) {$LeadForWebQuery->whereDate('updated_at', '>=', $dateFrom);}
        $LeadForWeb = $LeadForWebQuery->get();

        // Создаем файл и записываем данные в него
        $filename = 'file.txt';
        $handle = fopen($filename, 'w');
        foreach ($LeadForWeb as $row) {
            fwrite($handle, $row->phone . PHP_EOL);
        }
        fclose($handle);
        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];
        return response()->download($filename, null, $headers)->deleteFileAfterSend(true);
    }

    public function bizonApiSetting(\Illuminate\Http\Request $request,$id){

        $request->validate([
            'bizon_key' => 'required',
        ], [
            'bizon_key' => 'Обязательно для заполнения',
        ]);

        $bizon_key = $request->bizon_key ?? '';

        NotificationSetting::query()->updateOrCreate([
            'project_id' => $id,
        ],[
            'bizon_key' => $bizon_key,
        ]);

        return back();
    }

    public function webinarIdUpdateIndex($id,$webinarId){
        $this->webinarIdUpdateStore($id,$webinarId);
        return redirect()->route('webinarStat.index',$id);
    }

    /**
     * Обновляет и сохраняет информацию о вебинаре.
     *
     * @param int $id Идентификатор проекта.
     * @param string $webinarId Идентификатор вебинара.
     * @return array|null Массив данных о пользователях или null, если данные отсутствуют.
     */

    public function  webinarIdUpdateStore(int $id, string $webinarId): ?array
    {
        $BizonSetting = NotificationSetting::query()->where('project_id','=',$id)->get()->first();
        $bizon365 = new Bizon365API($BizonSetting->bizon_key);

        $viewers = $bizon365->getAllWebinarViewers($webinarId);
        $userWeb  = $this->getWebinarForInsert($viewers,$id);
        if ($userWeb == null) {
            return null;
        } else {
//            $this->addUserAfterWeb($userWeb, $id);
            AddUserAfterWeb::dispatch($userWeb, $id);
        }
        return $userWeb;
    }

    /**
     * Добавляет или обновляет информацию о пользователях после вебинара.
     *
     * @param array $userWeb Массив данных о пользователях вебинара.
     * @param int $id Идентификатор проекта.
     * @return void
     */
    public function addUserAfterWeb(array $userWeb, int $id): void
    {
        $records = collect($userWeb)->map(function ($user) use ($id) {
            return [
                'date' => Carbon::parse($user['date'])->format('Y-m-d'),
                'phone' => trim($user['phone'] ?? ''),
                'project_id' => $id,
                'username' => $user['username'] ?? null,
                'utm_source' => $user['utm_source'] ?? null,
                'utm_campaign' => $user['utm_campaign'] ?? null,
                'utm_medium' => $user['utm_medium'] ?? null,
                'buttons' => $user['buttons'] ?? null,
                'banners' => $user['banners'] ?? null,
                'ssId' => $user['ssId'] ?? null,
                'timeWeb' => $user['timeWeb'] ?? null,
                'warm' => $user['warm'] ?? null,
            ];
        });
        $records->each(function ($record) use ($id) {
            WebinarUser::updateOrCreate(
                [
                    'phone' => $record['phone'],
                    'project_id' => $record['project_id'],
                    'date' => $record['date'],
                ],
                $record
            );
        });
    }
}
