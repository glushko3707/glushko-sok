<?php

namespace App\Http\Controllers\Service;

use App\Models\Project;
use App\Models\Service\Module;
use App\Models\Service\PermissionModule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function serviceModuleIndex(Request $request,$id){

        $modules = Module::query()->where('tag','!=','links')->get();
        $project = Project::query()->find($id);

        $user = auth()->user();
        $projects = $user->getPermissionsProject;

        $availibleModule = $this->getAvailibleModule($projects);


        $getPermissionModul = $project->getPermissionModule;

            foreach ($modules as $module){
                if (count($getPermissionModul->where('module_id',$module->id)) > 0){
                    $module->active = true;
                    $module->sum = $module->price;
                    continue;
                }
                if (isset($availibleModule[$module->id])){
                    $module->availible = true;
                }
            }

        $sum = 499 + collect($availibleModule)->sum('price');

        if (count($projects) <= 5 && count($projects) > 2) {
            $sum = $sum * 2;
        }

        return view('ServiceProject.Module.module',compact(
            'id','modules','sum',
        ));
    }

    public function serviceModuleStore(Request $request,$id){

        $tag = $request->input('tag');
        $module = Module::query()->where('tag',$tag)->get()->first();

        if ($request->input('value') == 'on'){
            PermissionModule::query()->updateOrCreate([
                'module_id' => $module->id,
                'project_id' => $id,
            ],[
                'project_id' => $id,
                ]);
        } else {
            PermissionModule::query()->where(['project_id' => $id,'module_id' => $module->id,])->delete();
        }

        return back();
    }

    function getAvailibleModule($projects){

        $availibleModule = [];

        foreach ($projects as $project){
            $getPermissionModul = $project->getPermissionModuleFull;

            if (count($getPermissionModul) == 0){
                continue;
            }

            foreach ($getPermissionModul as $module) {
                $availibleModule[$module->id]['price'] = $module->price;
                $availibleModule[$module->id]['name'] = $module->name;
                $availibleModule[$module->id]['module_id'] = $module->id;
            }
        }

        return $availibleModule;

    }

}
