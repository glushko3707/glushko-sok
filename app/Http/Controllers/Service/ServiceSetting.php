<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;

class ServiceSetting extends Controller
{
    public $id;
    public $ProjectModel;
    public $waba_token; public $welcomeWaba; public $welcome_pipeline_step_id;


    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getWabaSetting($project_id){
        $ProjectModel = Project::query()->find($project_id);
        $this->ProjectModel = $ProjectModel;
        $WabaSettingModel = $ProjectModel->getWabaSetting;
        $this->waba_token = $WabaSettingModel->token ?? null;
        $this->welcomeWaba = $WabaSettingModel->welcomeWaba ?? null;
        $this->welcome_pipeline_step_id = $WabaSettingModel->welcome_pipeline_step_id ?? null;
    }



}
