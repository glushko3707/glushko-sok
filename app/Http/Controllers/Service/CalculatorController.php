<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CalculatorController extends Controller
{
      public function diagnostic()
      {
          return view('ServiceProject.Calculator.diagnostic');
      }

    public function webinar()
    {
        return view('ServiceProject.Calculator.webinar');
    }

    public function threeDay()
    {
        return view('ServiceProject.Calculator.threeDay');
    }


}
