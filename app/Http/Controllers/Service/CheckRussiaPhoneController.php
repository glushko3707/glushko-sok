<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Models\BlackList;

class CheckRussiaPhoneController extends Controller
{
    public function checkPhone($phone)
    {
        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
        $swissNumberProto = $phoneUtil->parse($phone, 'RU');
        $isValid = $phoneUtil->isValidNumber($swissNumberProto);

        if (!$isValid) {
            return null;
        } //проверяем правильный ли номер
        $geocoder = \libphonenumber\geocoding\PhoneNumberOfflineGeocoder::getInstance();
        $region = $geocoder->getDescriptionForNumber($swissNumberProto, "en_EN");

        if ($region == 'Russia') // проверяем, что номер российский
        {
            return trim($phoneUtil->format($swissNumberProto, \libphonenumber\PhoneNumberFormat::E164), '+');
        } else {
            return null;
        }
    }

    public function checkBlackList($phones,$id){
        $BlackList = BlackList::query()->where('project_id',$id)->pluck('phone');
        return collect($phones)->diff($BlackList)->toArray();
    }
}
