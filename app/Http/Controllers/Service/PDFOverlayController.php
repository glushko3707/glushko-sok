<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Sipout\SipOutSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;
use Intervention\Image\Interfaces\AnalyzerInterface;
use Intervention\Image\Laravel\Facades\Image;
use setasign\Fpdi\Fpdi;
use ZipArchive;

class PDFOverlayController extends Controller
{



    public function indexOverlayPngOnPdfs($id)
    {
        $directory['signature'] = "public/project/$id/signature/";
        $directory['signatureName'] = "public/project/$id/signatureName/";

        $signaturePath['signature'] = Storage::files($directory['signature']);
        $signaturePath['signatureName'] = Storage::files($directory['signatureName']);

        $files = array_map('basename', Storage::files("public/project/$id/pdf/"));

        return view('ServiceProject.overlayPngOnPdfs', ['id' => $id, 'signaturePath' => $signaturePath, 'contactDownload' => $files]);
    }
    public function overlayPngOnPdfs(Request $request,$id)
    {

        $signatureHas = $request->has('signature');

        if ($signatureHas){
            $request->file('signature')->store("public/project/$id/signature/");
            return back();
        }

        $signatureHas = $request->has('signatureName');

        if ($signatureHas){
            $request->file('signatureName')->store("public/project/$id/signatureName/");
            return back();
        }


        $projectModel = Project::find($id);
        $getSipProject = $projectModel->getSipProject;
        $signatureName = $getSipProject->signatureName;

        if (!$signatureName){
            return back();
        }


        $pdfPaths = collect($request->file('pdfs'))->map(function ($file) {
            $originalName = $file->getClientOriginalName();
            return $file->storeAs('public/pdfs', $originalName);
        });

        $outputDirectory = "public/project/$id/pdf/";

        if (!Storage::exists($outputDirectory)) {
            Storage::makeDirectory($outputDirectory);
        }

        $outputFiles = [];
        $pngPath = "public/project/$id/signature/";


        foreach ($pdfPaths as $pdfPath) {
            $pdf = new FPDI();
            $pageCount = $pdf->setSourceFile(Storage::path($pdfPath));
            $templateId = $pdf->importPage(1);
            $size = $pdf->getTemplateSize($templateId);

            $pdf->AddPage($size['orientation'], [$size['width'], $size['height']]);
            $pdf->useTemplate($templateId);

            $directory = "public/project/$id/signature/";
            $signaturePath = Storage::files($directory)[0];
            $pdf->Image(Storage::path($signaturePath), 90, 244,'12', '12', 'PNG');

            $directory = "public/project/$id/signatureName/";
            $signaturePath = Storage::files($directory)[0];
            $pdf->Image(Storage::path($signaturePath), 170, 242,'14', '14', 'PNG');

            $outputPath = $outputDirectory . Str::random(10) . '_' . basename($pdfPath);
            $pdf->Output('F', Storage::path($outputPath));

            $outputFiles[] = $outputPath;
        }



        return back();
    }

    public function getPDFFile(Request $request,$id){

        // Отправляем файл для скачивания
        $headers = [
            'Content-Type' => 'text/plain',
        ];

        $pathPart = "public/project/$id/pdf/";
        $filename = Storage::path($pathPart . $request->fileName);


        if (!Storage::exists($pathPart . $request->fileName)){
            return back();
        }

        return response()->download($filename, null, $headers)->deleteFileAfterSend();

    }

}
