<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use League\Csv\Exception;
use League\Csv\InvalidArgument;
use League\Csv\Reader;
use League\Csv\UnavailableStream;

class CSVController extends Controller
{
    /**
     * @throws UnavailableStream
     * @throws InvalidArgument
     * @throws Exception
     */
    public function reedCsv($file,$delimiter = ';')
    {
            $csv = Reader::createFromPath($file->getPathname());
            $csv->setDelimiter($delimiter);
            return $csv;
    }

    public function getHeaders($csv,$offset = 0)
    {
        if ($offset != 0){
            $csv->setHeaderOffset($offset);
        }
        $headers = $csv->fetchOne();
    }

   public function getBody($csv,)
   {
       $collect = new \Illuminate\Support\Collection();
       // Получаем заголовки из первой строки
       $records = collect($csv->jsonSerialize());
       $collect = $collect->concat($records);
   }
}
