<?php

namespace App\Http\Controllers\Service;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ZipController extends Controller
{
    public function unzip($zipPath, $extractDir)
    {
        $zip = new \ZipArchive();
        if ($zip->open($zipPath) === true) {
            // Куда распаковывать (пусть это будет папка "unzipped")
            // Создаём директорию, если нужно
            if (!file_exists($extractDir)) {
                mkdir($extractDir, 0777, true);
            }
            // Распаковываем содержимое
            $zip->extractTo($extractDir);
            $zip->close();

            unlink($zipPath);
        }
    }
}
