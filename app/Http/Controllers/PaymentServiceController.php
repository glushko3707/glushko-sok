<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Service\Module;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use YooKassa\Client;

class PaymentServiceController extends Controller
{
    public function paymentServiceIndex($id){

        $sum = $this->getSum();

        $description = 'Оплата за сервис аналитики LEAD PRO';
        $merchant_customer_id = auth()->user()->id;
        $metadata = ['purpose' => 'LeadPro'];

        $confirmationUrl = $this->createKassa($sum,$description,$merchant_customer_id,$metadata);
        return view('ServiceProject.paymentService',compact(
            'sum','id','confirmationUrl'
        ));
    }

    function createKassa($sum,$description,$merchant_customer_id,$metadata = ['text' => 'unknown']) {

        $client = new Client();
        $client->setAuth('820111', 'live_xueyiUBPI-ES-DMpCGP8LsRQyRHausGnPxjDoynPO3Q');

        $idempotenceKey = uniqid('', true);
        $response = $client->createPayment(
            array(
                'amount' => array(
                    'value' => $sum,
                    'currency' => 'RUB',
                ),
                'payment_method_data' => array(
                    'type' => 'bank_card',
                ),
                'confirmation' => array(
                    'type' => 'redirect',
                    'return_url' => 'https://glushko-sok.ru/projects',
                ),
                'description' => $description,
                'merchant_customer_id' => $merchant_customer_id,
                'metadata' => $metadata,
            ),
            $idempotenceKey
        );

        //get confirmation url
        $confirmationUrl = $response->getConfirmation()->getConfirmationUrl();
        $url_components = parse_url( $confirmationUrl, $component = -1 );
        parse_str($url_components['query'], $params);
        return $confirmationUrl;
    }

    function getSum(){
        $user = auth()->user();
        $projects = $user->getPermissionsProject;

        $availibleModule = $this->getAvailibleModule($projects);

        $sum = 499 + collect($availibleModule)->sum('price');

        if (count($projects) <= 5 && count($projects) > 2) {
            $sum = $sum * 2;
        }

        return $sum;
    }
    function getAvailibleModule($projects){

        $availibleModule = [];

        foreach ($projects as $project){
            $getPermissionModul = $project->getPermissionModuleFull;

            if (count($getPermissionModul) == 0){
                continue;
            }

            foreach ($getPermissionModul as $module) {
                $availibleModule[$module->id]['price'] = $module->price;
                $availibleModule[$module->id]['name'] = $module->name;
                $availibleModule[$module->id]['module_id'] = $module->id;
            }
        }

        return $availibleModule;

    }

    public function promoCodeActive(Request $request){
        $validator = $request->validate([
            'promocode' => 'required|min:3',
        ]);


            if ($request->promocode == 'kupr_2024') {
                $user = auth()->user();
                $user->update(['expire' => now()->addDays(14)]);
                return redirect(route('projects'));

        }

        return back()->withErrors(['promocode' => 'Промокод не действительный']);

    }

}
