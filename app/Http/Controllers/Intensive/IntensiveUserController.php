<?php

namespace App\Http\Controllers\Intensive;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\Services\SmsController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Jobs\Notification\Zvonobot;
use App\Jobs\Yandex\MetrikaOfflineConversonUpload;
use App\Models\Intensive\IntensiveChat;
use App\Models\Intensive\IntensiveList;
use App\Models\Intensive\IntensiveUser;
use App\Models\Notification\NewLeadWeb;
use App\Models\Project;
use App\Models\Telegram\UserChannel;
use App\Models\Webinar\LeadForWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use function Livewire\before;

class IntensiveUserController extends Controller
{
    public function userIndex($intensive_ids,$id){
       $code = 'kpr32';

       $intensive_id = Str::before($intensive_ids, $code);

       return view('Intensive.intensiveUser',compact('id','intensive_id'));
    }

    public function deleteUserStore($iduser_intensive_id)
    {
        IntensiveUser::query()->find('id',$iduser_intensive_id)->delete();
    }
    public function adminIndex($intensive_id,$id){
//        $this->updateInfoUser($intensive_id,$id);
//        $this->updateChatInviteInfo($intensive_id,$id);
        return view('Intensive.intensiveAdmin',compact('id','intensive_id'));
    }

    public function adminIndexUpdate($intensive_id,$id){
        $this->updateInfoUser($intensive_id,$id);
        $this->updateChatInviteInfo($intensive_id,$id);
        return back();
    }

    public function updateInfoUser($intensive_id,$id){
        $intensiveUsers = IntensiveUser::query()
            ->where('project_id', $id)
            ->whereNull('telegram_id')->orWhereNull('name')
            ->where('intensive_list_id', $intensive_id)
            ->with(['getLeadsForWebs','getLinkMainChat','getUserChannel'])
            ->get();


        $updates = $intensiveUsers->map(function ($intensiveUser) {
            return [
                'phone' => $intensiveUser->phone ?: null,
                'telegram_id' => $intensiveUser->getLeadsForWebs->telegram_id ?? $intensiveUser->getUserChannel->first()->telegram_id ?? null,
                'telegram_username' => $intensiveUser->getLeadsForWebs->telegram_username ?? $intensiveUser->getUserChannel->first()->telegram_username ?? null,
                'name' => $intensiveUser->name ?: $intensiveUser->getUserChannel->first()->fullName ?? $intensiveUser->getLeadsForWebs->user_name ??  null
            ];
        })->map(function ($data) {
            // Убираем ключи, где значения равны 0
            return collect($data)->filter(function ($value) {
                return $value != 0;
            })->toArray();
        })->filter(function ($data) {
            // Убираем пустые массивы
            return !empty($data);
        });;

        // Массовое обновление, если есть, что обновлять
        if (!empty($updates)) {
            foreach ($updates as $phone => $data) {
                IntensiveUser::query()->where('phone', $data['phone'])
                    ->where('intensive_list_id', $intensive_id)
                    ->where('project_id', $id)
                    ->update($data);
            }
        }

    }
    public function updateChatInviteInfo($intensive_id,$id){
       $IntensiveList =  IntensiveList::query()->find($intensive_id);
       $getIntensiveChats = $IntensiveList->getIntensiveChats;
        $intensiveUsers = IntensiveUser::query()
            ->where('project_id', $id)
            ->where('intensive_list_id', $intensive_id)
            ->with('getUserChannel')
            ->get();
// Подготовка массива для массового обновления
        $updates = [];
        foreach ($intensiveUsers as $user) {
            if (!empty($user->getUserChannel->first()->telegram_id)) {
                if (!empty($user->getUserChannel->where('channel_id',$IntensiveList->main_chat_id)->first()->telegram_id)){
                    $updates[$user->phone] = ['chat_main_active' => 1];
                }
                if (!empty($user->getUserChannel->whereIn('channel_id',$getIntensiveChats->pluck('chat_telegram_id'))->first()->telegram_id)){
                    $updates[$user->phone] = ['chat_second_active' => 1];
                }
            }
        }
// Массовое обновление, если есть, что обновлять
        if (!empty($updates)) {
            foreach ($updates as $phone => $data) {
                $IntensiveUser = IntensiveUser::query()
                    ->where('project_id', $id)
                    ->where('intensive_list_id', $intensive_id)
                    ->where('phone', $phone)->get()->last();
                $IntensiveUser->update($data);
            }
        }
    }


    public function addUserToNextIntensive($phone,$project_id){
        $ListController = new ListController();
        $NextIntensive = $ListController->getNextIntensive($project_id);
        $IntensiveUser = IntensiveUser::query()->updateOrCreate([
            'project_id' => $project_id,
            'intensive_list_id' => $NextIntensive->id,
            'phone' => $phone,
        ],[
            'project_id' => $project_id,
            'intensive_list_id' => $NextIntensive->id,
            'phone' => $phone,
        ]);

        // загрузка офлайн конверсий в яндекс
        MetrikaOfflineConversonUpload::dispatch($project_id, $phone, 'paymentIntensive');

        return [
            'NextIntensive' => $NextIntensive,
            'IntensiveUser' => $IntensiveUser,
        ];
    }

    public function distributedByFaculty($intensive_id, $project_id)
    {

        // Получаем список участников без факультета
        $allIntensiveUsersNotFacultet = IntensiveUser::query()
            ->where('intensive_list_id', $intensive_id)
            ->where('project_id', $project_id)
            ->whereNull('faculty_name')
            ->orWhere('faculty_name', '')
            ->get();

        if ($allIntensiveUsersNotFacultet->isEmpty()) {
            return back()->with('error', 'Нет пользователей без факультета в интенсиве');
        }

        // Получаем список факультетов с участниками интенсива
        $ListController = New ListController();
        $intensiveChatWithUser = $ListController->getIntensiveChatWithUser($project_id, $intensive_id);

        // считаем в каком интенсиве сколько пользователей в каждом факультете
        $getIntensiveChatsCountUsers = $ListController->getIntensiveChatsCountUsers($intensiveChatWithUser);


        foreach ($allIntensiveUsersNotFacultet as $user) {

            // Находим факультет с наименьшим количеством пользователей для распределения
            $fucultetMin = $getIntensiveChatsCountUsers->min();
            $facultyNamesMin = $getIntensiveChatsCountUsers->filter(function ($count) use ($fucultetMin) {
                return $count == $fucultetMin;
            })->keys()->first();
            // Обновляем факультет пользователя и сохраняем
            $user->faculty_name = $facultyNamesMin;
            $user->save();

            // Обновляем подсчет пользователей на факультете
            $getIntensiveChatsCountUsers->put($facultyNamesMin, $getIntensiveChatsCountUsers[$facultyNamesMin] + 1);
        }
            return back()->with('success', 'Пользователи распределены по факультетам');
    }

    public function getLinkIntensive(IntensiveUser $user)
    {
        $name_link = 'intensive_' . $user->phone;
        $link = $user->getLinkMainChat->where('link_name',$name_link)->first()->link ?? null;
        if ($link == null){
            $LinkInvitePhoneController = new LinkInvitePhoneController();
            $IntensiveList = IntensiveList::find($user->intensive_list_id);
            $link = $LinkInvitePhoneController->createInviteLinkIntensivePhone($user->project_id,$IntensiveList->main_chat_id,$user->phone);
        }
        return $link;
    }

    public function addUserStore(Request $request, $intensive_id, $id)
    {

       // валидация
        $validated = $request->validate([
            'phone' => 'required'
        ], [
            'phone' => 'Обязательно для заполнения',

        ]);

        $intensive = IntensiveList::query()->find($intensive_id);

        $phone = preg_replace('/\D+/', '', $request->phone);

        $IntensiveUser = IntensiveUser::query()->updateOrCreate([
            'project_id' => $id,
            'intensive_list_id' => $intensive_id,
            'phone' => $phone,
        ],[
            'telegram_id' => $request->telegram_id ?? null,
        ]);

        $LinkInvitePhoneController = new LinkInvitePhoneController();
        $LinkInvitePhoneController->createInviteLinkIntensivePhone($id,$intensive->main_chat_id,$phone);
        $IntensiveUser->payment_method = 'kassa';
        $IntensiveUser->save();

        return back();
    }

    public function setIntensiveNotification(Request $request, $intensive_id, $id)
    {
        $IntensiveList = IntensiveList::find($intensive_id);
        $getUsers = $IntensiveList->getUsers;
        $SmsController = new SmsController();

        $project = Project::query()->find($id);
        $notificationSetting = $project->getNotificationSetting;
        $timeStart = Carbon::parse($request->timeStart)->timestamp;

        $text = $request->text ?? null;
        foreach ($getUsers as $user) {
            if (!empty($text)){
            $phone = $user->phone;
            $data = [
                'id' => $id,
                'phone' => $phone,
                'text' => $text,
                'smsAeroSenderName' => $notificationSetting->smsAeroSenderName,
                'smsAeroEmail' => $notificationSetting->smsAeroEmail,
                'smsAero_key' => $notificationSetting->smsAero_key,
                'timeSend' => $timeStart
            ];
            $SmsController->sendSmsAeroJob($data);
            }
            if (!empty($request->campaign_id)){
                $data ['apiKey'] = $notificationSetting->zvonobot_key;
                $data ['record'] = ['id' => $request->campaign_id];
                $data ['phone'] = $phone;
                $data ['dutyPhone'] = '1';
                $data ['plannedAt'] = $timeStart;
                Zvonobot::dispatch($id, $data);
            }

        }
        return back();
    }
}
