<?php

namespace App\Http\Controllers\Intensive;

use App\Http\Controllers\Controller;
use App\Http\Controllers\saleBot\SalebotUserController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Models\Intensive\IntensiveChat;
use App\Models\Intensive\IntensiveList;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IntensiveCreateController extends Controller
{
    public function IntensiveCreatesIndex($id){
        $today = Carbon::today();
        $nextMonday = ($today->dayOfWeek == Carbon::MONDAY) ? $today->addWeek() : $today->next(Carbon::MONDAY);
        $data ['date_intensive'] = $nextMonday->format('Y-m-d');
        return view('Intensive.intensiveCreate',compact('id','data'));
    }

    public function IntensiveChatCreatesIndex($intensive_id,$id){
        $today = Carbon::today();
        $nextMonday = ($today->dayOfWeek == Carbon::MONDAY) ? $today->addWeek() : $today->next(Carbon::MONDAY);
        $data ['date_intensive'] = $nextMonday->format('Y-m-d');
        $data ['intensive_id'] = $intensive_id;
        $IntensiveList = IntensiveList::query()->find($intensive_id);
        $getIntensiveChats = $IntensiveList->getIntensiveChats;
        $count = $getIntensiveChats->count();
        if ($count <= 4){
            $fakultet = ['Когтевран 💙','Пуффендуй 💛','Гриффиндор ❤️','Слизерин 💚'];
            $data['chat_name'] = $fakultet[$count];
            $data['chat_name_id'] = $count;
        }

        return view('Intensive.intensiveCreateChat',compact('id','data'));
    }

    public function IntensiveCreatesStore(Request $request,$id){

        $validated = $request->validate([
            'main_chat_id' => 'required|regex:/^-10/',
            'date_intensive' => [
                'required',
                'date',
                'after:' . Carbon::now()->toDateTimeString(),
            ],
        ], [
            'main_chat_id.required' => 'Обязательно для заполнения',
            'main_chat_id.regex' => 'ID чата должен начинаться -10******)',
            'date_intensive.required' => 'Дата интенсива обязательна для заполнения',
            'date_intensive.after' => 'Дата интенсива должна быть в будущем',
        ]);

        $IntensiveListModel = $this->intensiveCreate($request->main_chat_id,$request->date_intensive,$id);
        return redirect()->route('IntensiveChatCreatesIndex',[$IntensiveListModel->id,$id]);
    }

    public function IntensiveChatCreatesStore(Request $request,$intensive_id,$id){
        $validated = $request->validate([
            'chat_id' => 'required|regex:/^-10/',
            'chat_name' => 'required'
        ], [
            'chat_id.required' => 'Обязательно для заполнения',
            'chat_id.regex' => 'ID чата должен начинаться -10******)',
            'chat_name' => 'Обязательно для заполнения'
        ]);
        $IntensiveListModel = IntensiveList::query()->find($intensive_id);
        $variables ['chat_name_id'] = $request->chat_name_id ?? '';
        $variables ['chat_name'] = $request->chat_name ?? '';
        $variables ['intensive_id'] = $intensive_id ?? '';
        $intensive_user_sheet = "https://glushko-sok.ru/kupr/user/" . $IntensiveListModel->id . "kpr32/$id";
        $variables ['intensive_user_sheet'] = $intensive_user_sheet ?? '';

        if ($request->chat_name_id <= 4){
            $fakultet = ['Когтевран 💙','Пуффендуй 💛','Гриффиндор ❤️','Слизерин 💚'];
            $dataInsert['chat_name'] = $fakultet[$request->chat_name_id] ?? '';
        }
       $LinkInvitePhoneController = new LinkInvitePhoneController();
       $link = $LinkInvitePhoneController->createInviteLinkIntensive($id,$request->chat_id);

       $IntensiveChatModel = IntensiveChat::query()->updateOrCreate([
            'project_id' => $id,
            'chat_telegram_id' => $request->chat_id,
            'intensive_list_id' => $intensive_id,
        ],[
            'chat_name' =>  $dataInsert['chat_name'] ?? '',
            'chat_link' =>  $link,
       ]);

        $this->salebotChatActive($request->chat_id,$id,'chatCreateStore',$variables);

        return redirect()->route('listSettingIndex',[$intensive_id,$id]);
    }

    public function intensiveCreate($main_chat_id,$date_intensive,$id){
        $LinkInvitePhoneController = new LinkInvitePhoneController();
        $link = $LinkInvitePhoneController->createInviteLinkIntensive($id,$main_chat_id);
        $IntensiveListModel = IntensiveList::query()->updateOrCreate([
            'project_id' => $id,
            'date_intensive' => $date_intensive,
        ],[
            'main_chat_id' => $main_chat_id,
            'link_invite' => $link,
        ]);
        $dateNextIntensiveShort = Carbon::parse($date_intensive)->format('d.m');
        $intensive_user_sheet = "https://glushko-sok.ru/kupr/user/" . $IntensiveListModel->id . "kpr32/$id";

        $variables = [
            'dateNextIntensive' => Carbon::parse($date_intensive)->format('d.m.Y'),
            'dateNextIntensiveShort' => $dateNextIntensiveShort,
            "intensive_user_sheet" => $intensive_user_sheet,
        ];

        $this->salebotChatActive($main_chat_id,$id,'mainCreateStore',$variables);
        return $IntensiveListModel;
    }

    public function salebotChatActive($chat_id,$id,$actionName,array $variables = null){
        $SalebotUserController = new SalebotUserController();
        $SalebotUserController->actionRequest($id,$chat_id,$actionName,$variables);
    }

    public function createNextIntensive($id){
        $today = Carbon::today();
        $nextMonday = ($today->dayOfWeek == Carbon::MONDAY) ? $today->addWeek() : $today->next(Carbon::MONDAY);
        $data ['date_intensive'] = $nextMonday->format('Y-m-d');
        return  IntensiveList::query()->updateOrCreate([
            'project_id' => $id,
            'date_intensive' => $data,
        ],[
            'project_id' => $id,
            'date_intensive' => $data,
        ]);
    }

}
