<?php

namespace App\Http\Controllers\Intensive;

use App\Http\Controllers\Controller;
use App\Models\Intensive\GoogleForm;
use App\Models\Intensive\IntensiveList;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class IntensiveSettingController extends Controller
{
    public function IntensiveSettingIndex($id){
        return view('intensive.intensiveSetting',compact('id'));
    }


    public function getNextIntensive(){

    }

    public function GoogleFormWebhook(Request $request,$id){
        $content = $request->getContent();
        $answer = json_decode($content, true);
        $phoneQuery = 'Напиши свой номер телефона ';
        $phone = preg_replace('/\D+/', '', $answer[$phoneQuery]);
        $phone = preg_replace('/^8/', '7', $phone); // Замена первой цифры 8 на 7

        GoogleForm::query()->create([
            'phone' => $phone,
            'project_id' => $id,
            'date' => Carbon::now()->format('Y-m-d'),
            'answers' => $content
        ]);

        return 200;
    }


}
