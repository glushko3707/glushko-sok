<?php

namespace App\Http\Controllers\Intensive;

use App\Http\Controllers\Controller;
use App\Models\Intensive\IntensiveChat;
use App\Models\Intensive\IntensiveList;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ListController extends Controller
{
    public function listIndex($id){
        return view('Intensive.IntensiveList',compact('id'));
    }



    public function listSettingIndex($intensive_id,$id){
        $IntensiveListModel = IntensiveList::query()->find($intensive_id);
        $data['getIntensiveChats'] = $IntensiveListModel->getIntensiveChats;
        $data['date_intensive'] = $IntensiveListModel->date_intensive;
        $data['main_chat_id'] = $IntensiveListModel->main_chat_id;
        $data['intensive_id'] = $IntensiveListModel->id;
        $data['IntensiveListModel'] = $IntensiveListModel;
        return view('Intensive.IntensiveListSetting',compact('id','data'));
    }
    public function getNextIntensive($id){
        $currentDateTime = Carbon::now();
        // Установка времени 15:00
        $comparisonTime = Carbon::createFromTimeString('15:00:00');

        if ($currentDateTime->greaterThan($comparisonTime)) {
            $NextIntensive = IntensiveList::query()->where('project_id',$id)->whereDate('date_intensive','>', today()->format('Y-m-d'))->get()->first();
        } else {
            $NextIntensive = IntensiveList::query()->where('project_id',$id)->whereDate('date_intensive','>=', today()->subDay()->format('Y-m-d'))->get()->first();
        }

        if (empty($NextIntensive)){
            $IntensiveCreateController = new IntensiveCreateController();
            $NextIntensive = $IntensiveCreateController->createNextIntensive($id);
        }
        return $NextIntensive;
    }

    public function getListChat($intensive_id)
    {
        $IntensiveList = \App\Models\Intensive\IntensiveList::query()->find($intensive_id);
        return $IntensiveList->getIntensiveChats;
    }

    public function getIntensiveChatWithUser($project_id, $intensive_id)
    {
            return IntensiveChat::query()->where('project_id', $project_id)->where('intensive_list_id', $intensive_id)->with('getIntensiveUser')->get();
    }

    public function getIntensiveChatsCountUsers($intensiveChatWithUser)
    {
        return $intensiveChatWithUser->groupBy('chat_name')->map(function ($users, $key) {
            return count($users->first()->getIntensiveUser);
        });
    }

}
