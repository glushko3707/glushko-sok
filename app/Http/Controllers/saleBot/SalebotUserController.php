<?php

namespace App\Http\Controllers\saleBot;

use App\Http\Controllers\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class SalebotUserController extends Controller
{
    public $client_id;
    public $id;
    public $salebot_api_key;
    public $telegram_bot_name;


    public function getClientIdCollect($id,$telegram_id){
        if (empty($this->salebot_api_key) || empty($this->telegram_bot_name)){
            $this->setApiKeyAndBotName($id);
        }
        $ProjectModel = Project::query()->find($id);
        $TelegramSettingModel = $ProjectModel->telegramSettingModel;
        $salebot_api_key = $TelegramSettingModel->salebot_api_key;
        $telegram_bot_name = $TelegramSettingModel->telegram_bot_name;
        $date = ['platform_ids' => [$telegram_id],'group_id' => $telegram_bot_name];

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post("https://chatter.salebot.pro/api/$salebot_api_key/find_client_id_by_platform_id",$date)->json();

        return $response[0] ?? null;
    }

    function setApiKeyAndBotName($id){
        $ProjectModel = Project::query()->find($id);
        $TelegramSettingModel = $ProjectModel->telegramSettingModel;
        $this->salebot_api_key = $TelegramSettingModel->salebot_api_key;
        $this->telegram_bot_name = $TelegramSettingModel->telegram_bot_name;
    }

    public function setVariable($id,$client_id, array $variable){
        if (empty($this->salebot_api_key) || empty($this->telegram_bot_name)){
            $this->setApiKeyAndBotName($id);
        }
        $data = [
            'client_id' => $client_id,
            'variables' => $variable
        ];
        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post("https://chatter.salebot.pro/api/$this->salebot_api_key/save_variables",$data)->json();
    }

        public function actionRequest($id,$platform_id,$actionName,array $variables = null){
            if (empty($this->salebot_api_key) || empty($this->telegram_bot_name)){
                $this->setApiKeyAndBotName($id);
            }
            $data = [
                'message' => $actionName,
                'user_id' => $platform_id,
                'group_id' => $this->telegram_bot_name,
            ];

            if (!empty($variables)){
                foreach ($variables as $variableName => $variableValue){
                    $data[$variableName] = $variableValue;
                }
            }

            $response = Http::withHeaders([
                "content-type" => "application/json",
            ])->post("https://chatter.salebot.pro/api/$this->salebot_api_key/tg_callback",$data)->json();
        }
}
