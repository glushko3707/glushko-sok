<?php

namespace App\Http\Controllers\saleBot;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Crm\Commo\CommoController;
use App\Models\Crm\CrmSettingModel;
use Illuminate\Http\Request;
use Throwable;

class SalebotCrmController extends Controller
{
    public function addToCrm($id, Request $request)
    {
        try {
                $CrmSettingModel = CrmSettingModel::query()->where('project_id', $id)->first();
                $crm_type = $CrmSettingModel->crm_type;

                // получить все переменные, которые есть в request
                $phone = $request->phone;
                $data ['new_lead_status'] = $request->new_lead_status ?? null;

                if (isset($request->name)){
                    $data ['contact']['name'] = $request->name;
                }

                if ($crm_type == 'Commo'){
                    $commo = new CommoController();
                    $commo->saveContactAndDealFromPhone($id, $phone, $data);
                }

        } catch (Throwable $e) {
            report($e);
        }

        return 200;
    }
}
