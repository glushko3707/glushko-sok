<?php

namespace App\Http\Controllers\saleBot;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Notification\NotificationController;
use App\Traits\Notification;
use Illuminate\Http\Request;

class SalebotLeadController extends Controller
{
    use Notification;
        public function phoneWebinarLead(Request $request,$id){
            $target = $request->target ?? '';
            $phone = $request->phone ?? '';
            $utm_source = $request->utm_source ?? '';

            if ($target == 'webinar'){
                $NotificationController = new NotificationController();
                $NotificationController->addNotification($phone, $id,$utm_source);
            }
            return 200;
    }
}
