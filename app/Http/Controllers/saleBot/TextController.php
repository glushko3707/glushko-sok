<?php

namespace App\Http\Controllers\saleBot;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TextController extends Controller
{
    public function textInput(){
        return view('saleBot.textarea');
    }
    public function textInputStore(Request $request){
        $lines = explode("\r\n", $request->text);

        $exit = '';
 foreach ($lines as $line){
     if ($exit == ''){
         $exit = $line . '"';
         continue;
     }
     if ($line == ''){
         $exit = $exit  . '+"\n"';

     }
     else {
         $exit = $exit . '+"\n"+'  . '"' . $line. '"';
     }
 }
        return $exit;
    }

}
