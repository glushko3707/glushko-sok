<?php

namespace App\Http\Controllers\saleBot;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GoogleSheets\GoogleSheetsController;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Jobs\Google\AddRowToGoogleSheet;
use App\Models\Project;
use App\Models\Telegram\TelegramSetting;
use Illuminate\Http\Request;

class SalebotAddToSheet extends Controller
{
    public function webhookFromSalebot(Request $request,$id)
    {
        $project = Project::find($id);
        $sheet_id = $this->getSheetId($project);

        $date_of_creation = $request->date_of_creation ?? '';
        $action = $request->action ?? '';
        $platform_id = $request->platform_id ?? '';

        $phone = $request->phone ?? '';
        $CheckRussiaPhoneController = New CheckRussiaPhoneController();
        $phone = $CheckRussiaPhoneController->checkPhone($phone);

        if (!$phone){
            $phone = '';
        }

        $full_name = $request->full_name ?? '';
        $tg_username = $request->tg_username ?? '';
        $group = $request->group ?? '';
        $utm_source = $request->utm_source ?? '';

        $sheetDate = [[now()->toDateTimeString(), $date_of_creation,$action,(string)$platform_id,(string)$phone,$full_name,$tg_username,$group,$utm_source]];

        foreach ($request->all() as $key => $value) {
            if (!in_array($key, ['date_of_creation', 'action', 'platform_id', 'phone', 'full_name', 'tg_username', 'group', 'utm_source'])) {
                $sheetDate[0][] = $value;
            }
        }
        AddRowToGoogleSheet::dispatch($sheetDate,$sheet_id,'users_salebot');

        return 200;
    }

    public function getSheetId(Project $projectModel)
    {
        $telegramSettingModel = $projectModel->telegramSettingModel;
        if (empty($telegramSettingModel->sheet_id)) {
            $GoogleSheetsController = new GoogleSheetsController;
            $sheet_id = $GoogleSheetsController->createSheets('salebot_users_' . $projectModel->id);
            $GoogleSheetsController->updateTitleSheets($sheet_id,'Sheet1','users_salebot');
            $GoogleSheetsController->relocateSheets($sheet_id,'1p6NHEFmmK-kWd1SvEYmDsNttXAkJPqhX');
            $values = [['date', 'date_of_creation','action','platform_id','phone','full_name','tg_username','group','utm_source']];
            $GoogleSheetsController->inputRow($sheet_id,$values,'users_salebot');
            $telegramSettingModel = TelegramSetting::updateOrCreate([
                'project_id' => $projectModel->id
            ],[
                'sheet_id' => $sheet_id
            ]);
        }

        return $telegramSettingModel->sheet_id;
    }

}
