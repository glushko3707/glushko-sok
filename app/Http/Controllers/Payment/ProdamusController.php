<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Jobs\Google\AddRowToGoogleSheet;
use App\Models\Notification\TildaLead;
use App\Rules\PhoneNumber;
use Illuminate\Http\Request;

class ProdamusController extends Controller
{
   public function paymentLink($phone,$id)
   {
       $linktoform = 'kupr.payform.ru';

// Секретный ключ. Можно найти на странице настроек,
// в личном кабинете платежной формы.
       $secret_key = 'f18be14624ec24d2923d21e2fdc807add26bac12d163502d9ed511dc2b9308be';

       $data = [

//           'order_id' => '1',

           'customer_phone' => $phone,
//           'customer_email' => 'ИМЯ@prodamus.ru',

           // перечень товаров заказа
           'products' => [
               [
                   'name' => 'Аудит бизнеса с Екатериной Уколовой',
                   'price' => 22,
                   'quantity' => 1,
                   'tax' => [
                       'tax_type' => 0,
                   ],
                   'paymentMethod' => 1,
                   'paymentObject' => 4,
               ],
           ],

           // дополнительные данные
           'customer_extra' => 'После оплаты с Вами свяжется менеджер и расскажет о времени аудита с Екатериной Уколовой',

           // для интернет-магазинов доступно только действие "Оплата"
           'do' => 'pay',

           // url-адрес для возврата пользователя без оплаты
           //           (при необходимости прописать свой адрес)
           'urlReturn' => 'https://sales-code.club/audit_ukolova',

           // url-адрес для возврата пользователя при успешной оплате
           //           (при необходимости прописать свой адрес)
           'urlSuccess' => 'https://chat.whatsapp.com/BYYUqba4QmK3hSQSusoA3O',

           'urlNotification' => 'https://glushko-sok.ru/prodamus/response/' . $id,

           'available_payment_methods' => ['AC','ACUSDSOM','SBP'],
           'currency' => 'usd',

           'npd_income_type' => 'FROM_INDIVIDUAL',
       ];

       $data['signature'] = $this->create($data, $secret_key);
       $link = sprintf('%s?%s', $linktoform, http_build_query($data));
       return 'https://' . $link;

   }

   public function form(Request $request,$id)
   {
       $phone = $request->phone ?? $request->Phone ?? '';

       $lead = TildaLead::query()->where('project_id',$id)->where('phone',$phone)->get()->last();

       $utm_source = $lead->utm_source ?? $request->utm_source ?? '';
       $utm_medium = $lead->utm_medium ?? $request->utm_medium ?? '';
       $utm_campaign = $lead->utm_campaign ?? $request->utm_campaign ?? '';
       $utm_term = $lead->utm_term ?? $request->utm_term ?? '';
       $utm_content = $lead->utm_content ?? $request->utm_content ?? '';

       $nisha = $request->nisha ?? '';
       $oborot = $request->oborot ?? '';
       $name = $request->name ?? '';


       return view('payment.prodamusUkolova',[
           'phone' => $phone,'nisha' => $nisha,'oborot' => $oborot,'name' => $name,'id' => $id,
           'utm_source' => $utm_source,
           'utm_medium' => $utm_medium,
           'utm_campaign' => $utm_campaign,
           'utm_term' => $utm_term,
           'utm_content' => $utm_content,
       ]);
   }

   public function formStore(Request $request,$id)
   {
       $request->validate([
           'phone' => ['required',new PhoneNumber],
           'name' => 'required|min:2',
           'nisha' => 'required|min:2',
           'oborot' => 'required|min:2',
       ], [
           'phone' => 'Введите корректный номер телефона',
           'name' => 'Обязательно для заполнения',
           'nisha' => 'Обязательно для заполнения',
           'oborot' => 'Обязательно для заполнения',
       ]);

       $phone = $request->phone ;
       $phone = preg_replace('/[\s\-\+\(\)]/', '', $phone);

       $nisha = $request->nisha ;
       $oborot = $request->oborot ;


       $lead = TildaLead::query()->where('project_id',$id)->where('phone',$phone)->get()->last();
       $utm_source = $request->utm_source ?? $lead->utm_source ?? '';
       $utm_medium = $request->utm_medium ?? $lead->utm_medium ?? '';
       $utm_campaign = $request->utm_campaign ?? $lead->utm_campaign ?? '';
       $utm_term = $request->utm_term ?? $lead->utm_term ?? '';
       $utm_content = $request->utm_content ?? $lead->utm_content ?? '';
       $name = $request->name ?? $lead->name ?? '' ;

       $price = '22' ;
       $PaymentLink = $this->paymentLink($phone,$id);

       $sheetDate = [[now()->toDateTimeString(), 'phone', $name, $nisha, $oborot, $price, $utm_source, $utm_medium,$utm_campaign,$utm_term,$utm_content,]];
       $sheetId = '1YNv3O2FoRvE04Vy54TlEwomWLH11r-t4R1idVo4xdD0';
       $sheetName = 'Заявки_на_оплату';

       AddRowToGoogleSheet::dispatch($sheetDate,$sheetId,$sheetName);

       return redirect($PaymentLink);
   }

   public function response()
   {
       return 200;
   }

    static function create($data, $key, $algo = 'sha256') {
        if (!in_array($algo, hash_algos()))
            return false;
        $data = (array) $data;
        array_walk_recursive($data, function(&$v){
            $v = strval($v);
        });
        self::_sort($data);
        if (version_compare(PHP_VERSION, '8.1.3', '<')) {
            $data = preg_replace_callback('/((\\\u[01-9a-fA-F]{4})+)/', function ($matches) {
                return json_decode('"'.$matches[1].'"');
            }, json_encode($data));
        }
        else {
            $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        }
        return hash_hmac($algo, $data, $key);
    }

    static function verify($data, $key, $sign, $algo = 'sha256') {
        $_sign = self::create($data, $key, $algo);
        return ($_sign && (strtolower($_sign) == strtolower($sign)));
    }

    static private function _sort(&$data) {
        ksort($data, SORT_REGULAR);
        foreach ($data as &$arr)
            is_array($arr) && self::_sort($arr);
    }




}
