<?php

namespace App\Http\Controllers\Payment;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Intensive\IntensiveUserController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use JustCommunication\TinkoffAcquiringAPIClient\API\InitRequest;
use JustCommunication\TinkoffAcquiringAPIClient\Exception\TinkoffAPIException;
use JustCommunication\TinkoffAcquiringAPIClient\TinkoffAcquiringAPIClient;
use Throwable;

class PaymentController extends Controller
{

    // удалить
    public function setLeadIdDate()
    {
       Payment::query()->with('getLead')->get()->each(function($dataToInsert){
           $lead = $dataToInsert->getLead->where('created_at', '<',$dataToInsert->created_at)->last();
           if (!empty($lead)){
               $dataToInsert->lead_id = $lead->id;
               $dataToInsert->lead_date = \Carbon\Carbon::parse($lead->created_at)->format('Y-m-d');
               $dataToInsert->save();
           }
       });
//        $newLead = NewLeadWeb::query()->where('phone', $dataToInsert['phone'])->where('project_id', $id)->get()->last();
//        if (!empty($newLead)){
//            $dataToInsert ['lead_id'] = $newLead->id;
//            $dataToInsert ['lead_date'] = \Carbon\Carbon::parse($newLead->created_at)->format('Y-m-d');
//        }
    }

    public function view($id){
        return view('payment.view.paymentView',compact('id'));
    }

    public function viewAdd($id){
        $products = Payment::query()
            ->where('project_id', $id)
            ->pluck('sum','product');
        return view('payment.view.paymentAdd',compact('id','products'));
    }

    public function paymentAddControllerStore(Request $request,$id){

        $request->validate([
            'phoneWebOne' => 'required|min:2',
            'product' => 'required|min:2',
            'sum' => 'required',
        ], [
            'phoneWebOne' => 'Обязательно для заполнения',
            'product' => 'Обязательно для заполнения',
        ]);

        $sum = $request->sum;
        $text = $request->phoneWebOne;
        $phones  = preg_split('/\r\n|[\r\n]/', $text);
        $product = $request->product;
        $date = Carbon::parse($request->date)->format('Y-m-d') ?? now()->format('Y-m-d');

        $dataToInsert = array_map(function($phone) use ($product,$id,$date,$sum) {
            $phone = preg_replace('/\D+/', '', $phone);
            return [
                'phone' => $phone,
                'project_id' => $id,
                'orderId' => 'na',
                'status' => 'succeeded',
                'sum' => $sum,
                'date' => $date,
                'product' => $product,
                'created_at' => now(),
                'updated_at' => now(),
            ];
        }, $phones);

        Payment::query()->insert($dataToInsert);

        try {
            if ($product == 'Оплата интенсив по телеграм с Юлией Купр' || $product == 'Недельный интенсив с Юлией Купр'){
                $product_id = 3;
            }
            if (!empty($product_id) && ($product_id == 3 || $product_id == 4)){
                $IntensiveUserController = new IntensiveUserController();
                $addUserToNextIntensive = $IntensiveUserController->addUserToNextIntensive($dataToInsert['phone'],$id);
                $NextIntensive = $addUserToNextIntensive ['NextIntensive'];
                $IntensiveUser = $addUserToNextIntensive ['IntensiveUser'];

                $LinkInvitePhoneController = new LinkInvitePhoneController();
                $LinkInvitePhoneController->createInviteLinkIntensivePhone($id,$NextIntensive->main_chat_id,$dataToInsert['phone']);
                $IntensiveUser->save();
            }
        } catch (Throwable $e) {
            report($e);
        }

        return redirect()->route('viewAdd.index',$id);
    }

    public function paymentControllerUpdate(Request $request){
        Payment::query()->find($request->id)->update(['status' => $request->status]);
        return back();
    }

    public function installmentIndex(Request $request,$sum){
        return view('payment.rassrochka',compact('sum'));
    }

    public function tbankIndex($id, Request $request){
    $sum = $request->sum ?? null;
        return view('payment.tpayment',compact('sum','id'));
    }

    public function tbankStore( Request $request,$id)
    {
        $request->validate([
            'phone' => 'required',
            'sum' => 'required',
        ]);
        $onlyDigitsPhone = preg_replace('/\D+/', '', $request->phone);

        $paymentURL = $this->tTBankPaymentRedirect($onlyDigitsPhone,$id,$request->sum);

       return redirect($paymentURL);
    }

    public function tTBankPaymentRedirect($phone,$id,$sum)
    {
        $client = new TinkoffAcquiringAPIClient('1693466161142', 'wqsqdcfejo3pkrbw');
        // Запрос на создание платежа на 100 рублей и внутренним номером `order-1234`
        $initRequest = new InitRequest($sum*100, now()->timestamp);
        // необязательные параметры
        $initRequest
            ->setLanguage($initRequest::LANGUAGE_RU)
            ->setDescription('Оплата за консультации по продвижению в Telegram')
            ->setNotificationURL(route('tbankNotifications',['id' => $id]))
//            ->setSuccessURL('https://domain.tld/_api/success/124')
//            ->setFailURL('https://domain.tld/_api/fail/124')
            ->setData([
                'phone' => $phone
            ])
        ;

        $initRequest->addData('Bar', 'baz');
        $response = $client->sendInitRequest($initRequest);
        return $response->getPaymentURL();
    }

}
