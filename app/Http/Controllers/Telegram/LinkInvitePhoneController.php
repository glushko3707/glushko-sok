<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\Telegram\LinkInvitePhone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Klev\TelegramBotApi\Methods\CreateChatInviteLink;
use Klev\TelegramBotApi\Telegram;


class LinkInvitePhoneController extends Controller
{
    public $link;
    public $LinkInvitePhoneModel;

    public function createInviteLink($id,$phone = null){
        $project = Project::query()->find($id);
        $telegramSettingModel = $project->telegramSettingModel;

        if ($phone && $phone != 'booking'){
           $LinkInvitePhoneExist= LinkInvitePhone::query()->where(['project_id' => $id])->where('phone',$phone)->whereDate('created_at',today())->exists();
        }

        if (isset($LinkInvitePhoneExist) && $LinkInvitePhoneExist){

            $LinkInvitePhoneModel = LinkInvitePhone::query()->where(['project_id' => $id])->where('phone',$phone)->whereDate('created_at',today())->get()->first();
            $this->link = $LinkInvitePhoneModel->link;
            $this->LinkInvitePhoneModel = $LinkInvitePhoneModel;
            return $LinkInvitePhoneModel->link;
        }


        if ($phone && $phone != 'booking'){
            $LinkInvitePhoneModel = LinkInvitePhone::query()->create(['project_id' => $id,'phone' => $phone]);
        } else {
            $LinkInvitePhoneModel = LinkInvitePhone::query()->create(['project_id' => $id]);
        }

        if (!empty($telegramSettingModel->telegram_api_key)){
            $bot = new Telegram($telegramSettingModel->telegram_api_key);} else {return null;}
            $CreateChatInviteLink = new CreateChatInviteLink($telegramSettingModel->chat_id);
            $CreateChatInviteLink->creates_join_request = true;
            $CreateChatInviteLink->name = 'user' . "_$LinkInvitePhoneModel->id";
            $result = $bot->createChatInviteLink($CreateChatInviteLink);
            $link = $result->invite_link;
            $LinkInvitePhoneModel->link = $link;
            $LinkInvitePhoneModel->link_name = 'user' . "_$LinkInvitePhoneModel->id";
            $LinkInvitePhoneModel->save();

            $this->link = $link;
            $this->LinkInvitePhoneModel = $LinkInvitePhoneModel;

        return $link;
    }
    public function checkIsset($project_id,$phone){
        $LinkInvitePhone = LinkInvitePhone::query()->where('project_id',$project_id)->where('phone',$phone)->get()->last();
    }
    public function createInviteLinkAndSetPhone($project_id,$phone){
        // проверяем наличие ссылок для этого пользователя
        $LinkInvitePhone = LinkInvitePhone::query()->where('project_id',$project_id)->where('phone',$phone)->exists();
        if ($LinkInvitePhone){
            $LinkInvitePhone = LinkInvitePhone::query()->where('project_id',$project_id)->where('phone',$phone)->get();
            $this->link = $link = $LinkInvitePhone->last()->link;
            return $link;
        }

        $id = $project_id;
        $project = Project::query()->find($id);
        $telegramSettingModel = $project->telegramSettingModel;
        $LinkInvitePhoneModel = LinkInvitePhone::query()->create(['project_id' => $id]);
        if (!empty($telegramSettingModel->telegram_api_key)){
            $bot = new Telegram($telegramSettingModel->telegram_api_key);} else {return null;}
        $CreateChatInviteLink = new CreateChatInviteLink($telegramSettingModel->chat_id);
        $CreateChatInviteLink->creates_join_request = true;
        $CreateChatInviteLink->name = 'user' . "_$LinkInvitePhoneModel->id";
        $result = $bot->createChatInviteLink($CreateChatInviteLink);
        $link = $result->invite_link;
        $LinkInvitePhoneModel->link = $link;
        $LinkInvitePhoneModel->link_name = 'user' . "_$LinkInvitePhoneModel->id";
        $LinkInvitePhoneModel->phone = $phone;
        $LinkInvitePhoneModel->save();

        $this->link = $link;
        $this->LinkInvitePhoneModel = $LinkInvitePhoneModel;

        return $link;
    }

    public function getEmptyLink($id){
        // Используем блокировку, чтобы избежать конкурентного доступа к таблице
        DB::beginTransaction();
        try {
            // Получаем последнюю запись из таблицы
            $LinkInvitePhoneModels = LinkInvitePhone::query()->where('project_id',$id)->whereNull('phone');

            $LinkInvitePhoneModel = $LinkInvitePhoneModels->first();
           if (empty($LinkInvitePhoneModel)){
               $LinkInvitePhoneController = new LinkInvitePhoneController();
               $LinkInvitePhoneController->createInviteLink($id);
               $LinkInvitePhoneModel = $LinkInvitePhoneController->LinkInvitePhoneModel;
           } else {
               if ($LinkInvitePhoneModels->count() < 10){
                   $this->deleteBooking($id);
                   $this->createInviteLink($id);
               }
           }


            $LinkInvitePhoneModel->phone = 'booking';
            $Link = $LinkInvitePhoneModel->link;
            $LinkInvitePhoneModel->save();
            // Фиксируем изменения в базе данных
            DB::commit();
        } catch (\Exception $e) {
            // Откатываем изменения в случае ошибки
            DB::rollBack();
            // Выводим сообщение об ошибке
        }
         if (!empty($Link)){
             return $Link;
         } else {
             return null;
         }
    }

    public function getLinkSite($id, Request $request){

        $phone = $request->phone ?: 'booking';
        // Используем блокировку, чтобы избежать конкурентного доступа к таблице
        DB::beginTransaction();
        try {
            // Получаем последнюю запись из таблицы
            $LinkInvitePhoneModels = LinkInvitePhone::query()->where('project_id',$id)->whereNull('phone');

            $LinkInvitePhoneModel = $LinkInvitePhoneModels->first();
            if (empty($LinkInvitePhoneModel)){
                $LinkInvitePhoneController = new LinkInvitePhoneController();
                $LinkInvitePhoneController->createInviteLink($id,$phone);
                $LinkInvitePhoneModel = $LinkInvitePhoneController->LinkInvitePhoneModel;
            } else {
                if ($LinkInvitePhoneModels->count() < 10){
                    $this->deleteBooking($id);
                    $this->createInviteLink($id);
                }
            }

            $LinkInvitePhoneModel->phone = $phone;
            $Link = $LinkInvitePhoneModel->link;
            $LinkInvitePhoneModel->save();
            // Фиксируем изменения в базе данных
            DB::commit();
        } catch (\Exception $e) {
            // Откатываем изменения в случае ошибки
            DB::rollBack();
            // Выводим сообщение об ошибке
        }
        if (!empty($Link)){
            return $Link;
        } else {
            return null;
        }
    }


    public function setPhoneToLink($id,$phone,$link){
        $LinkInvitePhoneModel = LinkInvitePhone::query()->updateOrCreate([
            'project_id' => $id,
            'link' => $link,
        ],[
            'phone' => $phone,
        ]);
        $this->createInviteLink($id);
    }
    function deleteBooking(){
        LinkInvitePhone::query()->whereDate('updated_at','<',now()->subDays(3)->format('Y-m-d'))->where('activate',0)->where('phone','=','booking')->update(['phone'=> null]);
    }

    public function setPhoneToLinkIntensive($id,$phone,$link){
        $LinkInvitePhoneModel = LinkInvitePhone::query()->updateOrCreate([
            'project_id' => $id,
            'link' => $link,
        ],[
            'phone' => $phone,
            'link_name' => 'intensive_' . $phone,
        ]);
    }

    public function activateLink($id,$link_name,$telegram_id,$telegram_username){
        $LinkInvitePhoneModel = LinkInvitePhone::query()->updateOrCreate([
            'project_id' => $id,
            'link_name' => $link_name,
        ],[
            'activate' => 1,
            'telegram_id' => $telegram_id,
            'telegram_username' => $telegram_username,

        ]);

        return $LinkInvitePhoneModel;
    }


    public function createInviteLinkIntensive($id,$chat_id){
        $project = Project::query()->find($id);
        $telegramSettingModel = $project->telegramSettingModel;
        if (!empty($telegramSettingModel->telegram_api_key)){
            $bot = new Telegram($telegramSettingModel->telegram_api_key);} else {return null;}
        $CreateChatInviteLink = new CreateChatInviteLink($chat_id);
        $CreateChatInviteLink->name = 'glushko-sok';
        $result = $bot->createChatInviteLink($CreateChatInviteLink);
        $link = $result->invite_link;

        return $link;
    }

    public function createInviteLinkIntensivePhone($id,$chat_id,$phone){
        $project = Project::query()->find($id);
        $telegramSettingModel = $project->telegramSettingModel;
        if (!empty($telegramSettingModel->telegram_api_key)){
        $bot = new Telegram($telegramSettingModel->telegram_api_key);} else {return null;}
        $CreateChatInviteLink = new CreateChatInviteLink($chat_id);
        $CreateChatInviteLink->name = 'intensive_' . $phone;
        $result = $bot->createChatInviteLink($CreateChatInviteLink);
        $link = $result->invite_link;
        $this->setPhoneToLinkIntensive($id,$phone,$link);
        return $link;
    }

}
