<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TelegramSetting extends Controller
{
    public function telegramSetting($id){
        $telegramSetting = \App\Models\Telegram\TelegramSetting::query()->where('project_id',$id)->get()->first();
        return view('telegram.telegramSetting',['id' => $id,'telegramSetting' => $telegramSetting]);
    }
}
