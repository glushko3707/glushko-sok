<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Intensive\IntensiveUserController;
use App\Http\Controllers\Notification\LeadController;
use App\Http\Controllers\saleBot\SalebotUserController;
use App\Models\GlushkoTelegramConnect;
use App\Models\Project;
use App\Models\Telegram\SalebotUser;
use App\Models\Telegram\UserChannel;
use App\Models\Webinar\LeadForWeb;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class TelegramMemberController extends Controller
{
    public function joinMember(Request $request,$id){
        sleep(1);
        $requestObject = json_decode($request->tg_request)->chat_member;
        $project = Project::query()->find($id);
        $telegramConnect = $project->telegramConnect;
        $project_telegram_service = $telegramConnect->telegram_project ?? '';
        $channel_id = $requestObject->chat->id;
        $telegram_id = $requestObject->old_chat_member->user->id;
        $username = $requestObject->old_chat_member->user->username ?? '';
        $invite_link = $requestObject->invite_link->invite_link ?? '';
        $firstName = $requestObject->old_chat_member->user->first_name ?? '';
        $lastName = $requestObject->old_chat_member->user->last_name ?? '';
        $fullName = $firstName . ' ' . $lastName;
        $link_name = $requestObject->invite_link->name ?? '';

        $UserChannelExists = UserChannel::query()
            ->where('telegram_id', $telegram_id)
            ->where('project_id', $project_telegram_service)
            ->where('channel_id', $channel_id)
            ->where('link_name', $link_name)
            ->exists();

        if (!$UserChannelExists){
        UserChannel::query()->updateOrCreate([
            'project_id' => $project_telegram_service,
            'telegram_id' => $telegram_id,
            'channel_id' => $channel_id,
            'link_name' => $link_name,
        ],[
            'project_id' => $project_telegram_service,
            'telegram_id' => $telegram_id,
            'channel_id' => $channel_id,
            'link_name' => $link_name,
            'link' => $invite_link,
            'telegram_username' => $username,
            'fullName' => $fullName,
        ]);
        }
        $data = [
            'project_id' => $project_telegram_service,
            'telegram_id' => $telegram_id,
            'channel_id' => $channel_id,
            'link_name' => $link_name,
            'link' => $invite_link,
            'telegram_username' => $username,
            'fullName' => $fullName,
        ];

//        $this->chatMember($data);
        try {
            $LinkInvitePhoneController = new LinkInvitePhoneController();
            $LinkInvitePhoneModel = $LinkInvitePhoneController->activateLink($id,$link_name,$telegram_id,$username);
        } catch (\Exception $e) {}

        if (isset($LinkInvitePhoneModel->phone) && !empty($LinkInvitePhoneModel->phone)){
            try {
                $SalebotUserController = new SalebotUserController();
                $SalebotUserController->setApiKeyAndBotName($id);
                $getClientIdCollect = $SalebotUserController->getClientIdCollect($id,$telegram_id);

                $this->setNewLeadWebTelegramId($id,$LinkInvitePhoneModel->phone,$telegram_id,$data['telegram_username']);

                $leadController = new LeadController();
                $leadController->getUtmFormPhone($id,$LinkInvitePhoneModel->phone);

                $variable ['client.phone'] = "$LinkInvitePhoneModel->phone";
                if (!empty($leadController->utm_source)){
                    $variable ['utm_source'] = $leadController->utm_source;
                }
                if (!empty($leadController->utm_medium)){
                    $variable ['utm_medium'] = $leadController->utm_medium;
                }

                $SalebotUserController->setVariable($id,$getClientIdCollect['id'],$variable);
            } catch (\Exception $e) {}
        }


        return 200;
    }



    public function chatMember($data){
        $project_id = $data['project_id'];
        $ProjectModel = Project::query()->find($project_id);
        $telegramSettingModel = $ProjectModel->telegramSettingModel;
        if ($telegramSettingModel->chat_id != $data['channel_id']){
            $IntensiveListModel = $ProjectModel->getIntensiveList;
            $IntensiveList = $IntensiveListModel->where('project_id',$project_id)->where('main_chat_id',$data['channel_id'])->exists();

            $ChatIntensiveList = $ProjectModel->getChatIntensiveList;
            $ChatIntensive = $ChatIntensiveList->where('project_id',$project_id)->where('main_chat_id',$data['channel_id'])->exists();

            if ($IntensiveList){
                $this->newIntensiveMember($data,$IntensiveListModel);
            } elseif ($ChatIntensive){
                $this->newChatIntensiveMember($data,$ChatIntensiveList);
            }
        }
    }

    public function newIntensiveMember($data,$IntensiveListModel){
        $IntensiveList = $IntensiveListModel->where('project_id',$data['project_id'])->where('main_chat_id',$data['channel_id'])->get();
        $Intensive = $IntensiveList->last();
        $users = $Intensive->getUsers;

        $users->where('telegram_username',$data['telegram_username'])->orWhere('telegram_id',$data['telegram_id'])->get();
    }

    public function newChatIntensiveMember($data,$ChatIntensiveList){
        $ChatIntensive = $ChatIntensiveList->where('project_id',$data['project_id'])->where('main_chat_id',$data['channel_id'])->get();
        $Chat = $ChatIntensive->last();
    }

    function setNewLeadWebTelegramId($id, $phone, $telegram_id, $telegram_username){
        LeadForWeb::query()->where('project_id',$id,)->where('phone',$phone)->update([
            'telegram_id' => $telegram_id,
            'telegram_username' => $telegram_username,
        ]);
    }

    // ручное сопоставление в таблицах
    public function enterTelegramId($id){
        $telegram_project = GlushkoTelegramConnect::query()->where('project_id',$id)->first()->telegram_project;
        $getChatIntensiveList = UserChannel::query()->where('project_id',$telegram_project)->with('getLinkInvitePhone')->get();

        $filteredChatIntensiveList = $getChatIntensiveList->filter(function ($userChannel) {
            return optional($userChannel->getLinkInvitePhone)->activate === 1;
        });

        $filteredChatIntensiveList = $filteredChatIntensiveList->filter(function ($userChannel) {
            return optional($userChannel->getLinkInvitePhone)->phone != '';
        });

        $filteredChatIntensiveList = $filteredChatIntensiveList->filter(function ($userChannel) {
            return optional($userChannel->getLinkInvitePhone)->phone != 'booking';
        });

        foreach ($filteredChatIntensiveList as $user){
           $data[] = ['phone' => $user->getLinkInvitePhone->phone,'telegram_id' => $user->telegram_id,'telegram_username' => $user->telegram_username];
             $this->setNewLeadWebTelegramId($id,$user->getLinkInvitePhone->phone,$user->telegram_id,$user->telegram_username);
        }
    }

    public function newUserSalebot(Request $request,$project_id)
    {
        $data = [
            'phone' => $request->phone ?? null,
            'project_id' => $project_id,
            'telegram_id' => $request->telegram_id ?? null,
            'salebot_id' => $request->client_id ?? null,
            'name' => $request->name ?? null,
            'pipelineName' => $request->pipelineName ?? null,
        ];

        $existingRecord = SalebotUser::query()->where('phone', $data['phone'])
            ->where('project_id', $data['project_id'])
            ->whereDate('created_at', Carbon::today())
            ->first();

        if (!$existingRecord) {
            SalebotUser::query()->create($data);
        }

        LeadForWeb::query()->where('project_id',$project_id)->where('phone',$data['phone'])->update([
            'telegram_id' => $data['telegram_id'],
        ]);
        return 200;
    }

}
