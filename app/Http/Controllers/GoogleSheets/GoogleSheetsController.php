<?php

namespace App\Http\Controllers\GoogleSheets;

use App\Http\Controllers\Controller;
use App\Models\Sipout\SipOutSetting;
use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
use Google_Service_Drive_Permission;
use Google_Service_Sheets;
use Google_Service_Sheets_BatchUpdateSpreadsheetRequest;
use Google_Service_Sheets_Request;
use Google_Service_Sheets_Spreadsheet;
use Google_Service_Sheets_ValueRange;
use Illuminate\Support\Facades\Storage;

class GoogleSheetsController extends Controller
{
    public $client;
    public $service;
    public $spreadsheetId;
    public $sheetName;


    public function getSheet($sheetId)
    {
        if (empty($this->service) || empty($this->client)){
            $this->connectGoogle();
        }

        $spreadsheet = $this->service->spreadsheets->get($sheetId)->getSheets();
        dd($spreadsheet);
    }

    public function connectGoogle(){
        $client = new Google_Client();
        $client->setApplicationName('Google Sheets API');
        $client->addScope(['https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/spreadsheets']);
        $client->setAuthConfig(Storage::path('secret/secretGoogle.json'));
        $service = new Google_Service_Sheets($client);
        $this->client = $client;
        $this->service = $service;
    }

    public function createSheets($sheetName){
        if (empty($this->service) || empty($this->client)){
            $this->connectGoogle();
        }

        $spreadsheet = new Google_Service_Sheets_Spreadsheet([
            'properties' => [
                'title' => $sheetName
            ]
        ]);
        $spreadsheet = $this->service->spreadsheets->create($spreadsheet, [
            'fields' => 'spreadsheetId'
        ]);

// Получаем ID созданной таблицы и
        $spreadsheetId = $spreadsheet->spreadsheetId;

// Создаем разрешение для всех с ролью редактора
        $permission = new Google_Service_Drive_Permission([
            'type' => 'anyone',
            'role' => 'writer'
        ]);
        $driveService = new Google_Service_Drive($this->client);
        $driveService->permissions->create($spreadsheetId, $permission);
        $this->spreadsheetId = $spreadsheetId;
        return $spreadsheetId;
    }

    public function updateTitleSheets($spreadsheetId,$oldName,$newName){

        if (empty($this->service) || empty($this->client)){
            $this->connectGoogle();
        }

        $spreadsheet = $this->service->spreadsheets->get($spreadsheetId);
        foreach ($spreadsheet->getSheets() as $sheet) {
            if ($sheet->getProperties()->getTitle() == "$oldName") {
                $sheetId = $sheet->getProperties()->getSheetId();
                break;
            }
        }

    // Создаем запрос на переименование листа sheets на leads
        $requests = [
            new Google_Service_Sheets_Request([
                'updateSheetProperties' => [
                    'properties' => [
                        'sheetId' => $sheetId,
                        'title' => $newName
                    ],
                    'fields' => 'title'
                ]
            ])
        ];
        $this->sheetName = $newName;

        $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
            'requests' => $requests
        ]);
        $response = $this->service->spreadsheets->batchUpdate($spreadsheetId, $batchUpdateRequest);
    }

    public function relocateSheets($spreadsheetId,$folderId){
        if (empty($this->service) || empty($this->client)){
            $this->connectGoogle();
        }
        $driveService = new Google_Service_Drive($this->client);
        $file = $driveService->files->get($spreadsheetId, ['fields' => 'parents']);
        $previousParents = join(',', $file->parents);


        // Обновляем родителей таблицы на папку leads
        $file = new Google_Service_Drive_DriveFile();
        $driveService->files->update($spreadsheetId, $file, [
            'addParents' => $folderId,
            'removeParents' => $previousParents,
            'fields' => 'id, parents'
        ]);
    }
    public function inputRow($spreadsheetId,$values,$sheetName) {
        if (empty($this->service) || empty($this->client)){
            $this->connectGoogle();
        }

        $service = $this->service;

        foreach ($values as $key){
            $value[] = array_values($key);
            $count =  count($key);
        }

        $body = new Google_Service_Sheets_ValueRange([
            'values' => $value
        ]);

        $params = [
            'valueInputOption' => 'RAW'
        ];

        // Диапазон столбца, в котором ищем последнюю заполненную ячейку
        $rangeFind = $sheetName.'!A:A';
        // Получаем данные о листе с помощью метода get
        $response = $service->spreadsheets_values->get($spreadsheetId, $rangeFind);
        // Получаем массив значений из ответа с помощью метода getValues
        $valun = $response->getValues();

        // Отфильтровываем пустые ячейки с помощью функции array_filter и функции strlen в качестве коллбэка
        $english = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

        if ($valun != null) {
            $lastCol = count($valun) + 1;
            $range = $sheetName . '!A'.$lastCol.':'. $english[$count] .  $lastCol + count($values);
        } else {
            $range = $sheetName . '!A1:'. $english[$count] . '1';
        }

        $result = $service->spreadsheets_values->append(
            $spreadsheetId,
            $range,
            $body,
            $params
        );
        return $result;
    }

    /**
     * Получаем данные с листа по ID таблицы и названию листа.
     *
     * @param  string $spreadsheetId  ID Google-таблицы
     * @param  string $sheetName      Название листа (вкладки)
     * @param  string $range          Диапазон (по умолчанию 'A1:Z')
     * @return array
     */
    public function getSheetValues(string $spreadsheetId, string $sheetName, string $range = 'A1:Z'): array
    {
        if (empty($this->service) || empty($this->client)){
            $this->connectGoogle();
        }
        // Например: "Лист1!A1:Z"
        $rangeString = $sheetName . '!' . $range;

        $response = $this->service
            ->spreadsheets_values
            ->get($spreadsheetId, $rangeString);

        // Получаем массив значений (двумерный массив [строка][столбец])
        return $response->getValues() ?? [];
    }

}
