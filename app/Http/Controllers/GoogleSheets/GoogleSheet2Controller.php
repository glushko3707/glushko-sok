<?php

namespace App\Http\Controllers\GoogleSheets;

use App\Http\Controllers\Controller;
use Google\Client;
use Google\Exception;
use Google\Service\Sheets;

use Google\Service\Sheets\BatchUpdateSpreadsheetRequest;
use Google\Service\Sheets\ClearValuesRequest;
use Google\Service\Sheets\ValueRange;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;

class GoogleSheet2Controller extends Controller
{
    protected Client $client;
    protected Sheets $sheetsService;

    /**
     * @throws Exception
     */
    public function __construct()
    {
        // Инициализируем клиент Google
        $this->client = new Client();
        // Указываем, что будем использовать аутентификацию сервисного аккаунта
        $this->client->setAuthConfig(Storage::path('secret/secretGoogle.json'));

        // Добавляем нужные scopes
        $this->client->setScopes([
            Sheets::SPREADSHEETS, // чтение/запись Google Sheets
        ]);

        $this->sheetsService = new Sheets($this->client);
    }

    public function getSheetValues(string $spreadsheetId, string $sheetName, string $range = 'A1:Z'): array
    {
        // Например: "Лист1!A1:Z"
        $rangeString = $sheetName . '!' . $range;

        $response = $this->sheetsService
            ->spreadsheets_values
            ->get($spreadsheetId, $rangeString);

        // Получаем массив значений (двумерный массив [строка][столбец])
        return $response->getValues() ?? [];
    }
    public function getAsCollection(string $spreadsheetId, string $range): Collection
    {
        // 1) Считываем строки из Google Sheet
        $response = $this->sheetsService
            ->spreadsheets_values
            ->get($spreadsheetId, $range);
        $rows = $response->getValues(); // двумерный массив
        if (count($rows) < 1) {
            return collect();
        }

        // 2) Первая строка - заголовки
        $headers = $rows[0];
        $headers[0] = 'Date';
        // 3) Остальные
        $dataRows = array_slice($rows, 1);

        // 4) Фильтруем и мапим
        return collect($dataRows)
            ->map(function ($row) use ($headers) {
                // Дополняем/подрезаем
                if (count($row) < count($headers)) {
                    $row = array_pad($row, count($headers), null);
                } else {
                    $row = array_slice($row, 0, count($headers));
                }

                return array_combine($headers, $row);
            });
    }
    public function getAsCollectionAll(string $spreadsheetId, string $range): Collection
    {
        // 1) Считываем строки из Google Sheet
        $response = $this->sheetsService
            ->spreadsheets_values
            ->get($spreadsheetId, $range);
        $rows = $response->getValues(); // двумерный массив
        if (count($rows) < 1) {
            return collect();
        }

        // 2) Первая строка - заголовки
        $headers = $rows[0];
        if (empty($headers[0])) {
            $headers[0] = 'Date';
        }

        // 3) Остальные строки
        $dataRows = array_slice($rows, 1);

        // 4) Фильтруем и мапим
        return collect($dataRows)
            ->map(function ($row) use ($headers) {
                // Дополняем/подрезаем
                if (count($row) < count($headers)) {
                    $row = array_pad($row, count($headers), null);
                } else {
                    $row = array_slice($row, 0, count($headers));
                }

                return array_combine($headers, $row);
            });
    }

    public function clearSheetValuesExceptHeader(string $spreadsheetId, string $sheetName): void
    {
        // Допустим, вы хотите очистить все ячейки, начиная со 2-й строки (A2 и далее).
        // Если не знаете, сколько столбцов, возьмите "A2:ZZZ" или как вам удобнее.
        $range = $sheetName . '!A2:ZZZ';

        $clearRequest = new ClearValuesRequest();

        $this->sheetsService
            ->spreadsheets_values
            ->clear($spreadsheetId, $range, $clearRequest);
    }
    public function deleteSheetExceptFirstRow(string $spreadsheetId, string $sheetName): void
    {
        // 1) Получаем информацию о всей таблице
        $spreadsheet = $this->sheetsService->spreadsheets->get($spreadsheetId);

        // Найдём нужный лист по имени
        $targetSheet = null;
        foreach ($spreadsheet->getSheets() as $sheet) {
            if ($sheet->getProperties()->getTitle() === $sheetName) {
                $targetSheet = $sheet;
                break;
            }
        }

        if (!$targetSheet) {
            throw new \Exception("Лист '{$sheetName}' не найден в таблице {$spreadsheetId}.");
        }

        // 2) Узнаём sheetId и общее число строк
        $sheetId = $targetSheet->getProperties()->getSheetId();
        $rowCount = $targetSheet->getProperties()->getGridProperties()->getRowCount();

        // Если на листе 1 или 0 строк, то и удалять нечего
        if ($rowCount <= 1) {
            // Можно вернуть или выбросить исключение,
            // но логичнее просто игнорировать.
            return;
        }

        // 3) Формируем запрос на удаление (deleteDimension)
        // Удаляем строки, начиная со 2-й (startIndex=1) до конца (endIndex=$rowCount)
        $deleteRequest = [
            'deleteDimension' => [
                'range' => [
                    'sheetId'   => $sheetId,
                    'dimension' => 'ROWS',
                    'startIndex' => 1,
                    'endIndex'   => $rowCount,
                ],
            ],
        ];

        // 4) Выполняем batchUpdate
        $batchUpdateRequest = new BatchUpdateSpreadsheetRequest([
            'requests' => [$deleteRequest],
        ]);

        $this->sheetsService->spreadsheets->batchUpdate($spreadsheetId, $batchUpdateRequest);

        // Всё — теперь на листе осталась только первая строка.
    }

    public function updateDataStartingFromA2(string $spreadsheetId, string $sheetName, array $values)
    {
        // Формируем ValueRange
        $valueRange = new ValueRange([
            'values' => $values
        ]);

        // Указываем опцию, что пользователь "вводит" данные (чтобы Google Sheets сам конвертировал их)
        $options = [
            'valueInputOption' => 'USER_ENTERED'
        ];

        // Обновляем (перезаписываем) данные в диапазоне sheetName!A2
        return $this->sheetsService->spreadsheets_values->update(
            $spreadsheetId,
            "{$sheetName}!A2",
            $valueRange,
            $options
        );
    }
}
