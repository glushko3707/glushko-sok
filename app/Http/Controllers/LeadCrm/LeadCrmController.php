<?php

namespace App\Http\Controllers\LeadCrm;

use App\Http\Controllers\Controller;
use App\Models\LeadCrm\LeadCrm;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\TildaLead;
use App\Models\Sipout\LeadSipout;
use App\Models\Webinar\LeadForWeb;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LeadCrmController extends Controller
{
    public function sipoutLeadsIndex(Request $request, $id){

        return view('sipout.lead',compact('id'));

    }

    public function sipoutLeadsClientIndex($client_rand_id){
        $client_rand_id_after = Str::beforeLast($client_rand_id,'ad3fsdf');
        $id = Str::after($client_rand_id_after,'dasd213c32fwrdasd');

        return view('sipout.leadClient',compact('id'));
    }

    public function sipoutLeadsGet($id){

        $LeadForWeb = LeadForWeb::query()->where('project_id', $id)->get('phone');
        $LeadSipout = LeadSipout::query()->where('project_id', $id)->get('phone');
        $TildaLead = TildaLead::query()->where('project_id', $id)->get('phone');

        $allLeads = $LeadForWeb->push($LeadSipout)->push($TildaLead);
        $phones = $allLeads->whereNotNull('phone')->pluck('phone');

        // Преобразуем коллекцию в строку, разделяя значения символом переноса строки
        $phones_string = $phones->join("\n"); // Измененная строка

// Создаем имя файла с текущей датой и временем
        $filename = 'phones_' . date('Y-m-d_H-i-s') . '.txt';

// Устанавливаем заголовки для скачивания файла
        header('Content-Type: text/plain');
        header('Content-Disposition: attachment; filename="' . $filename . '"');
        header('Content-Length: ' . strlen($phones_string));

        echo $phones_string;
    }
    public function crmLeadStore(Request $request){
        $method = $request->methods;
        $value = $request->value;
        $id = $request->id;

        $lead = LeadCrm::query()->find($id);

        if ($method == 'comment'){
            $lead->comment = $value;
        }

        if ($method == 'dateTask'){
            $lead->dateTask = $value;
        }

        if ($method == 'status'){
            $lead->status = $value;
        }

        $lead->save();
    }
}
