<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Jobs\Google\NewProjectSheetJob;
use App\Jobs\Sipout\Databases\DeleteProjectDatebases;
use App\Models\Permissoin;
use App\Models\Project;
use App\Models\Sipout\ContactDatabase;
use App\Models\Sipout\LeadSipout;
use App\Models\Sipout\SipoutCallId;
use App\Models\Sipout\SipOutSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    public function createProjectIndex(){
        return view('sipout.addProject');
    }

    public function createProjectStore(Request $request){

        $validatedData = $request->validate([
            'name' => 'required|min:2',
        ], [
            'name' => 'Обязательно',
        ]);


        $date['name'] = $request->name;
        $date['active'] = 1;

        $project = Project::query()->create($date);

        NewProjectSheetJob::dispatch($project->id,$request->name);


        $path = "image/".$request->name;

        if(!Storage::exists($path)){
            Storage::makeDirectory($path);
        }

        Permissoin::query()->insert([[
            'project_id' => $project->id,
            'user_id' => auth()->user()->id,
        ]]);

        return back();
    }

    public function getPermissionModule($id){
        $project = Project::query()->find($id);
        $getPermissionModul = $project->getPermissionModule;

        return $getPermissionModul->pluck('module_id');
    }

    public function getPermissionProject(){
        return auth()->user()->getPermissionsProject;
    }
    public function getPermissionProjectJson(){
        $project = auth()->user()->getPermissionsProject;
        return $project->toArray();
    }

    public function destroyProjectStore($id){
           LeadSipout::query()->where('project_id','=',$id)->delete();
           SipoutCallId::query()->where('project_id','=',$id)->delete();
           SipOutSetting::query()->where('project_id','=',$id)->delete();
           Permissoin::query()->where('project_id','=',$id)->delete();
           DeleteProjectDatebases::dispatch($id);
       return redirect(route('projects'));

       }


}
