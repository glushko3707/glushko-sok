<?php

namespace App\Http\Controllers\Smartsender;

use App\Http\Controllers\Controller;
use App\Models\Smartsender\SmartsenderLeads;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;

class SmartsenderLeadsController extends Controller
{

    public function newLeads(Request $request,$id){
        $ssId =  $request->ssId;
        $fullName =  $request->fullName ?? '';
        $utm_source =  $request->utm_source ?? '';
        $utm_medium =  $request->utm_medium ?? '';
        $telegram_username =  $request->telegram_username ?? '';

        SmartsenderLeads::query()->updateOrCreate([
            'project_id' => $id,
            'ssId' => $ssId,
            'utm_source' => $utm_source,
        ],[
            'fullName' => $fullName,
            'utm_source' => $utm_source,
            'utm_medium' => $utm_medium,
            'telegram_username' => $telegram_username,
        ]);

        return 200;
    }

    public function activeSmartsender(Request $request,$id){
        $ssId =  $request->ssId;

        try {
            SmartsenderLeads::query()->where(['project_id' => $id,'ssId' => $ssId,])->update(['active' => 1,]);
        } catch (\Exception $exception ){
            return 201;
        }

        return 200;
    }
}
