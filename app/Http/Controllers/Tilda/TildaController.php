<?php

namespace App\Http\Controllers\Tilda;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Facebook\ApiConvertion;
use App\Http\Controllers\Lead\NewLeadController;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Jobs\Google\LeadToGoogleSheet;
use App\Models\Notification\TildaLead;
use App\Models\Project;
use App\Traits\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Throwable;

class TildaController extends Controller
{
    use Notification;
    public function newLead(Request $request,$id)
    {
        try {

        if (!isset($request->Phone) && !isset($request->phone)) {
            return 200;
        }

       $COOKIES = $request->COOKIES ?? '';
       parse_str(str_replace(";", "&", $COOKIES), $output);
//            '_fbp' => $output['_fbp'] ?? null,
//                '_fbc' => $output['_fbc'] ?? null,

        $project = Project::query()->find($id);

        $data['_fbp'] = $output['_fbp'] ?? null;
        $data['_fbс'] = $output['_fbp'] ?? null;
//        Установка ClientID
        $data['_ym_uid'] = $output['_ym_uid'] ?? null;
//        Установка Яндекс Клик
        $data['_yclid'] = $output['yclid'] ?? null;

        $data['value_1'] = $request->profession ?? null;
        $data['name'] = $request->name ?? $request->Name ?? '';
        $data['user_name'] = $request->name ?? $request->Name ?? '';
        $data['utm_source'] = $request->utm_source ?? '';
        $data['utm_medium'] = $request->utm_medium ?? '';
        $data['utm_campaign'] = $request->utm_campaign ?? '';
        $data['utm_content'] = $request->utm_content ?? '';
        $data['utm_term'] = $request->utm_term ?? '';
        $data['project_id'] = $id;
        $data['url'] = $output['previousUrl'] ?? $output['currentUrl'] ?? null;
        $data['url'] = str_replace('https://', '', $data['url']);


        $CheckRussiaPhoneController = new CheckRussiaPhoneController();
        $phone = $CheckRussiaPhoneController->checkPhone($request->Phone);
            if (isset($request->link)){
                try {
                $LinkInvitePhoneController = new LinkInvitePhoneController();
                $LinkInvitePhoneController->setPhoneToLink($id,$phone,$request->link);
                } catch (\Exception $exception ){
                    Log::error('Ошибка таблицы лидов' . $request->phone . ' '. $exception);
                }
            }

            if ($phone == null) {
                $data['phone'] = $request->Phone ?? $request->phone;
                $data['phone'] = preg_replace('/\D+/', '', $data['phone']);
            } else {
                $phone = trim($phone, '+');
                $data['phone'] = $phone;
            }

            // Проверяем наличие записи с тем же phone и project_id, созданной сегодня
            $existingLead = TildaLead::query()
                ->where('phone', $data['phone'])
                ->where('project_id', $data['project_id'])
                ->whereDate('created_at', today())
                ->first();

            if ($existingLead) {
                // Обновляем существующую запись
                $existingLead->update($data);
            } else {
                // Создаем новую запись
                TildaLead::query()->create($data);
            }

        $newLeadController = new NewLeadController();
        $newLeadController->newLeeds($data['phone'],$project,$data);

//        $this->newLeeds($data['phone'],$project,$data);

            $sheetDate = [[
                'date' => now()->format('d-m-y'),
                'phone' => $phone,
                'utm_source' => $data['utm_source'],
                'utm_medium' => $data['utm_medium'],
                'typeCall' => 'context',
            ]];

            LeadToGoogleSheet::dispatch($sheetDate,$id);

            } catch (\Exception $exception ){
        Log::error('Ошибка входящего лида тильда' . $request->phone . ' '. $exception);
        }

        return 200;

    }

    public function newLeadAllProject(Request $request,$id)
    {
        try {

            if (!isset($request->Phone) && !isset($request->phone)) {
                return 200;
            }

            $data['utm_source'] = $request->utm_source ?? '';
            $data['utm_medium'] = $request->utm_medium ?? '';
            $data['utm_campaign'] = $request->utm_campaign ?? '';
            $data['utm_content'] = $request->utm_content ?? '';
            $data['utm_term'] = $request->utm_term ?? '';
            $data['project_id'] = $id;
            $data['phone'] = trim($request->Phone ?? $request->phone ?? '', '+');
            TildaLead::query()->create($data);

        } catch (\Exception $exception ){
            Log::error('Ошибка входящего лида тильда' . $request->phone . ' '. $exception);
        }

        return 200;

    }


    public function LeadTildaAnalyticUkolova(Request $request,$id){
        try {
        $COOKIES = $request->COOKIES ?? '';
        parse_str(str_replace(";", "&", $COOKIES), $output);

        $data ['_fbp'] = $output['_fbp'] ?? null;
        $data ['_fbc'] = $output['_fbc'] ?? null;
        $data ['phone'] = $request->Phone ?? $request->phone ?? null;
        $data['utm_source'] = $request->utm_source ?? '';
        $data['utm_medium'] = $request->utm_medium ?? '';
        $data['utm_campaign'] = $request->utm_campaign ?? '';
        $data['utm_content'] = $request->utm_content ?? '';
        $data['utm_term'] = $request->utm_term ?? '';
        $data['project_id'] = $id;
        $data ['turnover'] = $request->oborot ?? $request->oborot_2 ?? $request->Ваш_оборот_мес ?? null;

            if ($id == 498) {
                $ukolova = new \App\Http\Controllers\Tilda\Ukolova();
                $ukolova->setRequest($request,$id,$data);
            }
        $ApiConvertion = new ApiConvertion();

        $data ['event'] = 'lead';
        $ApiConvertion->sendEvent($id,$data);

        } catch (Throwable $e){
        }
        return 200;
    }

    public function LeadTildaAnalytic(Request $request,$id){
        try {

        $COOKIES = $request->COOKIES ?? '';
        parse_str(str_replace(";", "&", $COOKIES), $output);

            // Преобразование телефона
            $phone = preg_replace('/[\s\-\+\(\)]/', '', $request->input('Phone', $request->input('phone')));

            $data = array_merge([
                '_fbp' => $output['_fbp'] ?? null,
                '_fbc' => $output['_fbc'] ?? null,
                'phone' => $phone,
                'project_id' => $id,
                'utm_source' => $request->utm_source ?? null,
                'utm_medium' => $request->utm_medium ?? null,
                'utm_content' => $request->utm_content ?? null,
                'utm_campaign' => $request->utm_campaign ?? null,
                'utm_term' => $request->utm_term ?? null,
                'name' => $request->name ?? $request->Name ?? null,
                'email' => $request->email ?? $request->Email ?? null,
            ]);

        TildaLead::query()->create($data);
        } catch (Throwable $e){

        }
        try {
            $ApiConvertion = new ApiConvertion();
            $data ['event'] = 'lead';
            $ApiConvertion->setEventPixel($id,$data);
        } catch (Throwable $e){

        }
        return 200;
    }

}
