<?php

namespace App\Http\Controllers\Tilda;

use App\Http\Controllers\Controller;
use App\Http\Controllers\GoogleSheets\GoogleSheet2Controller;
use App\Models\Notification\TildaLead;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Locale;

class Ukolova extends Controller
{

    public function getLeads()
    {
//        $GoogleSheet2Controller  = new GoogleSheet2Controller();
//        $getAsCollectionAll = $GoogleSheet2Controller->getAsCollectionAll('1VXhZH4_l8fqzv1-Su1JLbBg_twLNrvXpNyeIorwipQs', 'Все реги!A1:Z');
//        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
//
//        $collection = collect($getAsCollectionAll)->map(function ($item) use ($phoneUtil) {
//            try {
//                $phoneNumberProto = $phoneUtil->parse($item['Phone'], null);
//                $locale = 'ru'; // Локализация
//                $data ['country'] = $phoneUtil->getRegionCodeForNumber($phoneNumberProto);
//                $data ['country'] = Locale::getDisplayRegion('-' . $data ['country'], $locale);
//
//            } catch (\libphonenumber\NumberParseException $e) {
//            }
//
//            return [
//                'user_name'    => $item['name'] ?? null,
//                'phone'        => isset($item['Phone']) ? str_replace([' ', '-', '+', '(', ')'], '', $item['Phone']) : null,
//                'web_name'     => $item['web_number'] ?? null,
//                'dateWeb'      => isset($item['sent']) ? Carbon::parse($item['sent'])->format('Y-m-d') : null,
//                'timeWeb'      => isset($item['sent']) ? Carbon::parse($item['sent'])->format('H:i:s') : null,
//                'created_at'      => isset($item['sent']) ? Carbon::parse($item['sent']) : null,
//                'updated_at'      => now(),
//                'project_id'   => '498',
//                'country'      => $data ['country'] ?? null,
//                '_fbp'         => $item['_fbp'] ?? null,
//                '_fbc'         => $item['_fbc'] ?? null,
//                'turnover'     => $item['oborot'] ?? $item['oborot_2'] ?? null,
//                'role'         => $item['role'] ?? null,
//                'utm_source'   => $item['utm_source'] ?? null,
//                'utm_medium'   => $item['utm_medium'] ?? null,
//                'utm_content'  => $item['utm_content'] ?? null,
//                'utm_campaign' => $item['utm_campaign'] ?? null,
//                'utm_term'     => $item['utm_term'] ?? null,
//            ];
//        });
//
////        dd($collection->toArray());
//        // Вставка всех данных разом
//      \App\Models\Lead\Ukolova::query()->insert($collection->toArray());

//       $ukolova = \App\Models\Lead\Ukolova::query()->whereDate('created_at', '>=', Carbon::now()->subDays(1))->get();
//        foreach ($ukolova as $lead) {
//            $existingLead = TildaLead::query()
//                ->where('phone', $lead->phone)
//                ->where('project_id', $lead->project_id)
//                ->whereDate('created_at', '>=', Carbon::now()->subDays(1)->startOfDay()) // Только если TildaLead был создан вчера или сегодня
//                ->first();
//
//            if ($existingLead) {
//                $existingLead->update($lead->only([
//                    'phone', 'web_name', 'user_name', 'dateWeb', 'timeWeb', 'project_id', 'country',
//                    '_fbp', '_fbc', 'turnover', 'role', 'utm_source', 'utm_medium', 'utm_content',
//                    'utm_campaign', 'utm_term'
//                ]));
//            } else {
//                TildaLead::create($lead->only([
//                    'phone', 'web_name', 'user_name', 'dateWeb', 'timeWeb', 'project_id', 'country',
//                    '_fbp', '_fbc', 'turnover', 'role', 'utm_source', 'utm_medium', 'utm_content',
//                    'utm_campaign', 'utm_term'
//                ]));
//            }
//        }

        return 200;
    }
    public function setRequest($request,$id,$data = []){
        $data ['dateWeb'] = $this->getWebinarDate();
        $data ['timeWeb'] = $this->getWebinarTime();
        $phone = $request->phone ?? $request->Phone ?? '';
        if (empty($phone)){
            return 200;
        }
        $data ['phone'] = str_replace([' ','-','+', '(', ')'], '', $phone);
        $data ['user_name'] = $request->name ?? null;
        $data ['project_id'] = $id;
        $data ['web_name'] = $request->tags ?? null;
        $data['role'] = $request->role ?? '';
        $data ['turnover'] = $request->oborot ?? $request->oborot_2 ?? $request->Ваш_оборот_мес ?? null;
        $data ['utm_source'] = $request->utm_source ?? null;
        $data ['utm_medium'] = $request->utm_medium ?? null;
        $data ['utm_content'] = $request->utm_content ?? null;
        $data ['utm_campaign'] = $request->utm_campaign ?? null;
        $data ['utm_term'] = $request->utm_term ?? null;

        $phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();

        try {
            $phoneNumberProto = $phoneUtil->parse($phone, null);
            $locale = 'ru'; // Локализация
            $data ['country'] = $phoneUtil->getRegionCodeForNumber($phoneNumberProto);
            $data ['country'] = Locale::getDisplayRegion('-' . $data ['country'], $locale);

        } catch (\libphonenumber\NumberParseException $e) {
        }

        \App\Models\Lead\Ukolova::query()->create($data);
        TildaLead::query()->create($data);

        return 200;
    }


    public function getWebinarDate()
    {
        $currentHour = date('H');
        if ($currentHour < 20) {
            return date('Y-m-d'); // Возвращает сегодняшнюю дату
        } else {
            return date('Y-m-d', strtotime('+1 day'));  // Возвращает дату следующего дня
        }
    }

    public function getWebinarTime()
    {
        $currentHour = date('H');

        if ($currentHour < 10) {
            return '10:00';
        } elseif ($currentHour < 12) {
            return '12:00';
        } elseif ($currentHour < 14) {
            return '14:00';
        } elseif ($currentHour < 16) {
            return '16:00';
        } elseif ($currentHour < 18) {
            return '18:00';
        } elseif ($currentHour < 20) {
            return '20:00';
        } else {
            return '10:00';
        }
    }

    function getLeadInfo($phone,$model){
        $userModel = $model->where('phone',$phone)->last();
        if (!empty($userModel)){
            $data ['_fbc'] = $userModel->_fbc ?? null;
            $data ['_fbp'] = $userModel->_fbp ?? null;
            $data ['phone'] = $phone;
            return $data;
        } else {
            return null;
        }
    }
}
