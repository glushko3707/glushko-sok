<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\HttpFoundation\Response;

class PreventMultipleRequests
{
    // Время блокировки запроса в секундах
    protected $lockTime = 10;

    // Ключ для хранения информации о блокировке в кэше
    protected function getLockKey($request)
    {
        return 'request_lock_' . md5($request->url() . $request->ip());
    }

    // Проверяем, заблокирован ли запрос
    protected function isLocked($request)
    {
        return Cache::has($this->getLockKey($request));
    }

    // Блокируем запрос на определенное время
    protected function lock($request)
    {
        Cache::put($this->getLockKey($request), true, $this->lockTime);
    }

    // Обрабатываем входящий запрос
    public function handle($request, Closure $next)
    {
        // Если запрос заблокирован, возвращаем ошибку 429 (Too Many Requests)
        if ($this->isLocked($request)) {
            return back();
        }

        // Иначе выполняем запрос и блокируем его на будущее
        $response = $next($request);
        $this->lock($request);

        return $response;
    }
}

