<?php

namespace App\Http\Middleware;

use App\Models\Permissoin;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class PermissionProject
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (auth()->user()->role == 'superAdmin'){
            return $next($request);
        }
        $id = $request->route('id');
        $clients = Permissoin::query();
        if (isset($id)) {$clients->where('project_id',$id);}
        $clients = $clients->where('user_id',auth()->user()->id)->get();

        if (!isset($clients)) {
            return redirect(route('projects'))->with('permission', 'У вас нет доступа');
        }

        return $next($request);
    }
}
