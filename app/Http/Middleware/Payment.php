<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Payment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = auth()->user();

        if (empty($user->expire)){
            $user->update([
                'expire' => now()->addDays(14)
            ]);
        }

        if (\Illuminate\Support\Carbon::parse($user->expire)->isPast()){
            return redirect(route('paymentService'));
        }

        return $next($request);

    }
}
