<?php

namespace App\Listeners;

use App\Events\WebinarVisit;
use App\Jobs\Waba\addQueue;
use App\Jobs\Yandex\MetrikaOfflineConversonUpload;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Payment;
use Illuminate\Support\Carbon;

class WebinarUserVisitAction
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(WebinarVisit $event): void
    {
        $userWeb = $event->userWeb;
        $id= $event->id;


        $payment = Payment::query()->where('project_id',$id)->where('phone',$userWeb['phone'])->where('status','=','succeeded')->whereDate('created_at', Carbon::today())->exists();

        if (!$payment){
            $this->afterWebinarSendMessage($userWeb['platform_id'],$userWeb['phone'],$id);
        }

        if (!empty($userWeb['phone'])){
            MetrikaOfflineConversonUpload::dispatch($id, $userWeb['phone'], 'visitWeb');
        }

        if ($userWeb['warm'] == 'hot' || $userWeb['warm'] == 'succeeded'){
            MetrikaOfflineConversonUpload::dispatch($id, $userWeb['phone'], 'webinarWarm');
        }
    }

    function afterWebinarSendMessage($platformId,$phone,$id): void
    {

        if (Carbon::now()->hour > 15) {
            $WebNotificationPipeline = WebNotificationPipleline::query()->where('project_id', $id)->where('type','=','afterWebinar')->get();
        } else {
            $WebNotificationPipeline = WebNotificationPipleline::query()->where('project_id', $id)->where('type','=','morningAfterWeb')->get();
        }

        if (isset($WebNotificationPipeline) && $WebNotificationPipeline->count() > 0){
            foreach ($WebNotificationPipeline as $step){
                switch ($step['content_type']) {
                    case 'WABA':
                    addQueue::dispatch($step['content_id'],null,$phone,now(),$id);
//                        $NotificationController = new WabaMessageController();
//                        $NotificationController->addQueue($step['content_id'],null,$phone,now(),$id);
                    case 'SALEBOT':
                }
            }
        }
    }
}
