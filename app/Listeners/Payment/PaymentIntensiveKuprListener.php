<?php

namespace App\Listeners\Payment;

use App\Events\Payment\PaymentIntensiveKupr;
use App\Http\Controllers\saleBot\SalebotUserController;
use App\Http\Controllers\Waba\WabaMessageController;
use App\Jobs\Notification\WebNotification;
use App\Jobs\Waba\addQueue;
use App\Jobs\Waba\WabaSendMessegeOne;
use App\Jobs\Yandex\MetrikaOfflineConversonUpload;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Waba\WabaHistory;
use App\Models\Webinar\LeadForWeb;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class PaymentIntensiveKuprListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */


    public function handle(PaymentIntensiveKupr $event): void
    {
        $data = $event->data;
        $project_id = $event->project_id;
        $phone = $data['phone'];
        $variables['linkInviteIntensive'] = $data['linkInviteIntensive'] ?? null;
        MetrikaOfflineConversonUpload::dispatch($project_id, $phone, 'paymentIntensive');

        WebNotificationSchedule::query()->where('project_id',$project_id)->where('phone',$phone)->delete();

        $lead = LeadForWeb::query()->where('phone', $phone)->where('project_id',$project_id)->whereNotNull('telegram_id')->get()->last();
        if (!empty($lead)){
            $SalebotUserController = new SalebotUserController();
            $variables['linkInviteIntensive'] = $data['linkInviteIntensive'] ?? null;
            $SalebotUserController->actionRequest($project_id,$lead->telegram_id,'payment_intensive_290',$variables);
        }
        if (!empty($data['linkInviteIntensive'])){
            $WabaHistor = WabaHistory::query()->where('phone', $phone)->where('project_id',$project_id)->where('message','=','payment_intensive')->exists();
            if(!$WabaHistor){
                addQueue::dispatch('payment_intensive','',[$phone],now()->addMinutes(2),$project_id);
            }
        }
    }
}
