<?php

namespace App\Services\Sipout;

use App\Models\Offer;
use App\Models\Sipout\LeadSipout;
use App\Models\Sipout\SipoutCallId;
use Carbon\Carbon;

class Service
{
    public function updateSipoutCall($project,$priceSec,$call,$timeDifferent = 15)
    {

        $callCalleds = $call->filter(function ($item) {
            return preg_match('/^\*call/', $item['called']);
        });


        $callPhone = $callCalleds->groupBy('called');
        if ($callPhone->isEmpty()){
            return 200;
        }

        $date = Carbon::parse($callPhone->last()->last()['date'])->format('Y-m-d');

        foreach ($callPhone as $key => $value) {
            SipoutCallId::query()->updateOrCreate([
                'project_id' => $project['id'],
                'callId' => $key,
                'date' => $date,
            ],[
                'money' => $value->sum('duration') * $priceSec,
                'timePhone' => $value->sum('duration'),
            ]);
        }


        return 200;
    }

    public function uploadSipoutCall($collection,$id,$timeDifferent){

        $formatted_collection = $collection->transform(function ($item) {
            $item['cost'] = str_replace(',', '.', $item['cost']);
            return $item;
        })->groupBy('from_number');

        foreach ($formatted_collection as $key => $value){

            if (isset($value[0])){
                $date = Carbon::parse($value[0]['dt'])->format('Y-m-d');
            }

            $phoneId[$key] = [
                'cost' => $value->sum('cost'),
                'time' => $value->sum('duration'),
            ];

            $exists = SipoutCallId::query()->where( 'project_id', '=', $id,)->where( 'phone', '=', $key)->get();

            if ($exists->isEmpty()){
               continue;
            } else {
                $callId = $exists->last()->callId;
            }

            SipoutCallId::query()->updateOrCreate([
                'project_id' => $id,
                'callId' => $callId,
            ],[
                'date' => $date,
                'money' => $value->sum('cost'),
                'timePhone' => $value->sum('duration'),
            ]);
        }


        return back();
    }

    public function getLastStat($sipoutCallId,$leads){

        $date = Carbon::parse($sipoutCallId->last()->date);
        $sipoutCallId = $sipoutCallId->filter(function ($item) use ($date) {
            return Carbon::parse($item['date'])->isSameDay($date);
        });
        $leads = $leads->filter(function ($item) use ($date) {
            return Carbon::parse($item['updated_at'])->isSameDay($date);
        });
        if ($leads->count() > 0){
            $todayLeadPrice = $sipoutCallId->sum('money') / $leads->count();
        } else {
            $todayLeadPrice = 0;
        }
        if ($date->isToday()) {
            $date = 'Сегодня';
        } elseif ($date->isYesterday()) {
            $date = 'Вчера ('.$date->format('d.m.Y').')';
        } else{
            $date = $date->format('d.m.Y');
        }

        return [
            'todaySpend' => round($sipoutCallId->sum('money')),
            'todayLeads' => $leads->count(),
            'todayLeadPrice' => round($todayLeadPrice),
            'date' => $date,
        ];

    }

    public function getStatPhone($id,$dateFrom,$dateTo){
        $leads = LeadSipout::query()
            ->where('project_id','=',$id)
            ->whereNotNull('utm_medium');
        $Offer = Offer::query()->where('project_id',$id)->get(['offer_name','call_id']);

        if ($dateTo) {$leads->whereDate('updated_at', '<=', $dateTo);}
        if ($dateFrom) {$leads->whereDate('updated_at', '>=', $dateFrom);}

        $leads = $leads->get();

        $this->inputPhoneChange($leads,$id);

        $leads = $leads->groupBy('utm_medium');

        $callId = SipoutCallId::query()
            ->where('project_id','=',$id)
            ->orderBy('created_at',);
        if ($dateTo) {$callId->whereDate('created_at', '<=', $dateTo);}
        if ($dateFrom) {$callId->whereDate('created_at', '>=', $dateFrom);}
        $callId = $callId->get()->groupBy('callId');

        $callId = $callId->reverse();
        foreach ($callId as $value => $key) {
            $number [$key->first()->callId]['id'] = $value;
            $number [$key->first()->callId]['date'] = $key->first()->date;
            $number [$key->first()->callId]['phone'] = $key->first()->phone;
            $number [$key->first()->callId]['callId'] = $key->first()->callId;
            $number [$key->first()->callId]['rashod'] = round($key->sum('money'));
            $number [$key->first()->callId]['timePhone'] = $key->sum('timePhone');
            $number [$key->first()->callId]['comment'] = $key->first()->comment;
            $number [$key->first()->callId]['value'] = $key->first()->id;
            $number [$key->first()->callId]['callId_for'] = substr($key->first()->callId,1);

            $offers = $Offer->where('call_id',$key->first()->callId);
            if (count($offers) > 0) {$number[$key->first()->callId]['offer'] = $offers->first()->offer_name;}

            if (isset($leads[$key->first()->callId])) {$number [$key->first()->callId]['newleads'] = $leads[$key->first()->callId]->count();} else{$number [$key->first()->callId]['newleads'] = 0;}
            if (isset($leads[$key->first()->callId])) {$number [$key->first()->callId]['leadPrice'] =  round($number [$key->first()->callId]['rashod']/$leads[$key->first()->callId]->count(),2);} else{$number [$key->first()->callId]['leadPrice'] = 0;}
        }

        if (isset($number)){
            $number = collect($number);
            $all['rashod'] = round($number->sum('rashod')) ;
            $all[ 'newleads'] = $number->sum('newleads') ;
            if ($all['newleads'] != 0) {
                $all ['leadPrice'] =  round($all ['rashod'] / $all ['newleads']);
            } else {$all ['leadPrice'] = 0;}

        } else {
            $all = [];
        }
        if (!isset($number)){$number = [];}
        if (!empty($number)){
            $number = $number->where('rashod','>',3);
        }

        return [
            'number' => $number,
            'all' => $all,

        ];
    }

    function getStatDays($id){
        $money = SipoutCallId::query()
            ->where('project_id','=',$id)
            ->orderByDesc('created_at')->get()->groupBy(function($events) {
                return \Illuminate\Support\Carbon::parse($events->created_at)->format('d-m-y'); // А это то-же поле по нему мы и будем группировать
            });

        $leads = LeadSipout::query()
            ->where('project_id','=',$id)
            ->orderByDesc('created_at')->get()->groupBy(function($events) {
                return \Illuminate\Support\Carbon::parse($events->created_at)->format('d-m-y'); // А это то-же поле по нему мы и будем группировать
            });

        if (isset($money)){
            foreach ($money as $day => $value){
                $stat[$day] = ['rashod' => round($value->sum('money')),];
                if (isset($leads[$day])) {
                    $stat[$day]['newleads'] =  $leads[$day]->count();
                    $stat[$day]['leadPrice'] =  round($stat[$day]['rashod']/$stat[$day]['newleads']);
                } else{
                    $stat[$day]['newleads'] = 0;
                    $stat[$day]['leadPrice'] = 0;
                }
            }
        }
        if (isset($stat)){
            $stat = collect($stat);
            $all['rashod'] = round($stat->sum('rashod'));
            $all['newleads'] = $stat->sum('newleads');
            if (isset($all['newleads']) && $all['newleads'] > 0 ){
                $all['leadPrice'] =  round($all['rashod']/$all['newleads']);
            } else{
                $all['leadPrice'] = 0;
            }
        }

        if (!isset($stat)){$stat = [];}
        if (!isset($all)){$all = [];}

        return[
            'stat' => $stat,
            'all' => $all
        ];
    }

     function inputPhoneChange($leads,$id)
    {
        $leadProne = $leads->filter(function ($item) {
            return !preg_match('/^\*call/', $item['utm_medium']);
        });


        if (count($leadProne) > 0) {
            $sipoutCallIds = SipoutCallId::query()
                ->where('project_id','=',$id)->get();

            foreach ($leadProne as $keyn => $valuesen){
                if (isset($sipoutCallIds->where('phone',$valuesen->utm_medium)->last()->callId)) {
                    LeadSipout::query()->find($valuesen->id)
                        ->update(['utm_medium' => $sipoutCallIds->where('phone',$valuesen->utm_medium)->last()->callId]);
                }
            }
        }
    }


    public function getNeedUpdate($sipoutCallIdLast){
        $threshold = Carbon::now()->subMinutes(5)->toDateTimeString();
        if (isset($sipoutCallIdLast->updated_at) && $sipoutCallIdLast->updated_at <= $threshold) {
            return true;
        } else {
           return false;
        }
    }
}
