<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class DeleteOldLogs extends Command
{
    protected $signature = 'logs:delete';

    protected $description = 'Delete logs older than 14 days';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $files = Storage::files(storage_path('logs/'));
        $now = now();

        foreach ($files as $file) {
            if ($now->diffInDays(Storage::lastModified($file)) > 14) {
                Storage::delete($file);
            }
        }
    }
}