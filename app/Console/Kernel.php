<?php

namespace App\Console;

use App\Http\Controllers\HelloWorld\UpdateCostController;
use App\Http\Controllers\Notification\NotificationController;
use App\Http\Controllers\Ozon\OzonController;
use App\Http\Controllers\Yandex\Direct\DirectController;
use App\Jobs\Facebook\AdsUpdate;
use App\Jobs\Notification\SendMessageWhatsUp;
use App\Jobs\Notification\WebNotification;
use App\Jobs\Sipout\UpdateProject;
use App\Jobs\Waba\SendMessege;
use App\Models\Facebook\FacebookStat;
use App\Models\Notification\JobTelegramNotification;
use App\Models\Notification\NotificationSetting;
use App\Models\Notification\WebNotificationSchedule;
use App\Models\Sipout\LeadSipout;
use App\Models\Waba\WabaQueue;
use App\Models\Webinar\LeadForWeb;
use App\Models\WhatsUpQueue;
use App\Traits\Notification;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    use Notification;

    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command('telescope:prune --hours=48')->daily();
        $schedule->command('logs:delete')->daily();

        $schedule->call(function () {
            (new UpdateCostController())->hwUpdateCost();
        })->cron('4 5,12,17,23 * * *');

        $schedule->call(function () {
           $UpdateCostController = new UpdateCostController();

            // дизайн
            $utm = ['source' => 'telegram', 'medium' => 'proga',];
            $sheetName = 'ОРД закуп';
            $channel = 'telegram';

            $UpdateCostController->updateTelegramToDB('599',now()->subDay()->format('Y-m-d'),null,$utm,$sheetName,$channel);
            sleep(1);
            $UpdateCostController->hwSetDateToSheetUpdate(now()->subDay()->format('Y-m-d'),'Telegram',$channel);

            // дизайн
            $utm = ['source' => 'telegram_design', 'medium' => 'posev',];
            $sheetName = 'Дизайн';
            $channel = 'telegram_design';

            $UpdateCostController->updateTelegramToDB('599',now()->subDay()->format('Y-m-d'),null,$utm,$sheetName,$channel);
            sleep(1);
            $UpdateCostController->hwSetDateToSheetUpdate(now()->subDay()->format('Y-m-d'),'Telegram_design',$channel);
        })->dailyAt('00:02');

        $schedule->call(function () {
            NotificationSetting::query()->where('autoWeb','=',1)->update(['dateWeb' => now()->addDays()->format('Y-m-d')]);
        })->dailyAt('19:00');

        $schedule->call(function () {
            $notifications = WebNotificationSchedule::query()->where('timeSend','<',now()->timezone('Europe/Moscow'))->get();
            if (count($notifications) > 0) {
                foreach ($notifications as $notification) {
                    $jobId = JobTelegramNotification::query()->updateOrCreate([
                        'project_id' => $notification->project_id,
                        'phone' => $notification->phone,
                        'step_id' => $notification->step_id,
                    ],[
                        'user_id' => $notification->user_id,
                        'timeSend' => $notification->timeSend,
                    ])->id;
                    usleep( 10000); // 1/10 секунды
                    WebNotification::dispatch($jobId);
                    WebNotificationSchedule::query()->where('id',$notification->id)->delete();
                }
            }

            $whatsUpQueue = WhatsUpQueue::query()->where('time','<',now()->timezone('Europe/Moscow'))->get();

            if (count($whatsUpQueue) > 0) {
                foreach ($whatsUpQueue as $whatsUp) {
                    usleep(300000);
                    SendMessageWhatsUp::dispatch($whatsUp->text,$whatsUp->phone,$whatsUp->instance_id,$whatsUp->id);
                    WhatsUpQueue::query()->find($whatsUp->id)->delete();
                }
            }

            $wabaQueue = WabaQueue::query()->where('dateSend','<',now()->timezone('Europe/Moscow'))->get();

            if (count($wabaQueue) > 0) {
                foreach ($wabaQueue as $whatsUpWaba) {
                    SendMessege::dispatch($whatsUpWaba->waba_templates_id,$whatsUpWaba->phone,$whatsUpWaba->variable);
                    WabaQueue::query()->find($whatsUpWaba->id)->delete();
                }
            }

        })->everyMinute();

        $schedule->call(function () {
            // обновляем озон
            (new OzonController)->updateStatisticaHw();
           // затраты Facebook
            $FacebookStat = FacebookStat::query()->where('project_id', 599)->pluck('account_id')->unique();
            foreach ($FacebookStat as $account_id) {
                AdsUpdate::dispatch(599, $account_id);
            }
            AdsUpdate::dispatch(498, '299258346473085');
            $directController = new DirectController();
            $directController->updateYandexDirect('599');
        })->hourly();


// добавляем напоминания в сегодня
        $schedule->call(function () {
            $notificationSettings = NotificationSetting::query()->whereNotNull('dateWeb')->get();

            foreach ($notificationSettings as $project_telegram){

                if (Carbon::parse($project_telegram->dateWeb)->isToday()){
                    $leadForWeb = LeadForWeb::query()->where('project_id','=',$project_telegram->project_id)->whereDate('dateWeb','=',today()->format('Y-m-d'))->get();
                    foreach ($leadForWeb as $user){
                        $utm_source = $user->utm_source ?? '';
                        $NotificationController = new NotificationController();
                        $NotificationController->addNotification($user->phone, $project_telegram->project_id, $utm_source);
                    }
                }
                sleep(1);
            }

        })->timezone('Europe/Moscow')->dailyAt('0:30');

        $schedule->call(function () {
            $leadSipout = LeadSipout::query()->whereDate('created_at',today())->get('project_id')->unique('project_id')->pluck('project_id');
           foreach ($leadSipout as $project){
               sleep(10);
               UpdateProject::dispatch($project);
           }
        })->timezone('Europe/Moscow')->dailyAt('23:10');
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
