<?php

namespace App\Traits;

use App\Http\Controllers\Notification\NotificationController;
use App\Http\Controllers\Notification\VariableController;
use App\Http\Controllers\Notification\Wa\WaMessageController;
use App\Http\Controllers\Notification\WelcomeMessageController;
use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use App\Http\Controllers\Waba\WabaMessageController;
use App\Jobs\Notification\SendWhatsUp;
use App\Jobs\Notification\SmsAero;
use App\Jobs\Notification\Zvonobot;
use App\Jobs\Waba\addQueue;
use App\Models\BlackList;
use App\Models\LeadCrm\LeadCrm;
use App\Models\linkChangeModel;
use App\Models\Notification\GlobalVariable;
use App\Models\Notification\JobTelegramNotification;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\SmsAeroHistory;
use App\Models\Notification\Variable;
use App\Models\Notification\Wa\WaSendMessage;
use App\Models\Notification\Wa\WebWa;
use App\Models\Notification\Wa\WebWaNumber;
use App\Models\Notification\WebNotificationPipleline;
use App\Models\Notification\WebSms;
use App\Models\Payment;
use App\Models\Project;
use App\Models\SmsSetting;
use App\Models\Waba\WabaHistory;
use App\Models\Waba\WabaIntance;
use App\Models\Waba\WabaLinkNumber;
use App\Models\Waba\WabaQueue;
use App\Models\Waba\wabaSetting;
use App\Models\Waba\WabaTemplates;
use App\Models\Webinar\LeadForWeb;
use App\Models\Webinar\WebinarUser;
use App\Models\WhatsUpQueue;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Throwable;


trait Notification
{





    public function sendSms($content_id, $NotificationSchedule)
    {
        $project_id = $NotificationSchedule->project_id;
        $smsSetting = SmsSetting::query()->where('project_id', $project_id)->get()->first();

        if (isset($smsSetting->service)) {
            if ($smsSetting->service == 'smsAero') {
                $this->sendSmsAero($content_id, $NotificationSchedule);
            }
        }
    }

    public function sendSmsAero($content_id, $NotificationSchedule)
    {
        $smsStep = WebSms::query()->find($content_id);
        $timeToSended = Carbon::parse($smsStep->timeSend);
        $project_id = $smsStep->project_id;

        $project_telegram = Project::query()->find($project_id);
        $notificationSetting = $project_telegram->getNotificationSetting;

        $hour = $timeToSended->hour;
        $minutes = $timeToSended->minute;
        $timeSend = now()->setTime($hour, $minutes)->timestamp;

        $VariableController = new VariableController();
        $text = $VariableController->setVariables($smsStep->text,$NotificationSchedule->phone, $project_telegram->id);

        $date = [
            'smsAeroSenderName' => $notificationSetting->smsAeroSenderName,
            'smsAeroEmail' => $notificationSetting->smsAeroEmail,
            'smsAero_key' => $notificationSetting->smsAero_key,
            'phone' => $NotificationSchedule->phone,
            'text' => $text,
            'timeSend' => $timeSend,
            'id' => $notificationSetting->project_id,
        ];

        // проверяем, если лид упал сегодня, то не отправляем дневное СМС
        if (!empty($smsStep->type) && $smsStep->type == 'day') {
            $leadWeb = NewLeadWeb::query()->where('project_id', $project_id)->where('phone', '=', $NotificationSchedule->phone)->get()->last();
            if (!empty($leadWeb) && $leadWeb->created_at->isToday()) {
                Log::alert('Sms заявка получена сегодня');
                return 200;
            }
        }


        SmsAero::dispatch($date);
//        $this->sendSmsAeroJob($date);

        return 200;
    }

    public function completeAction($jobId)
    {
        $NotificationSchedule = JobTelegramNotification::query()->where('id', $jobId)->get()->first();
        $NotificationStep = WebNotificationPipleline::query()->find($NotificationSchedule->step_id);
        $data ['phone'] = $NotificationSchedule->phone;

        if (isset($NotificationStep->type)) {
            switch ($NotificationStep->type) {
                case 'new_noToday':
                    $leadWeb = NewLeadWeb::query()->where('project_id', $NotificationSchedule->project_id)->where('phone', '=', $data['phone'])->get()->last();
                    if (!empty($leadWeb) && $leadWeb->created_at->isToday()) {
                        JobTelegramNotification::query()->where('id', $jobId)->first()->delete();
                        return 200;
                    }
                    break;
                case 'onWeb':
                    $WebinarUser = WebinarUser::query()->where('project_id', '=', $NotificationSchedule->project_id)->whereDate('date', '=', now()->format('Y-m-d'))->where('phone', '=', $data['phone'])->exists();
                    if ($WebinarUser) {
                        JobTelegramNotification::query()->where('id', $jobId)->first()->delete();
                        return 200;
                    }
                    break;
                case 'nextDay':
                    $payment = Payment::query()->where('project_id', '=', $NotificationSchedule->project_id)->whereDate('date', '=', now()->subDay()->format('Y-m-d'))->where('phone', '=', $data['phone'])->whereIn('phone', ['succeeded','по карте'])->exists();
                    if ($payment) {
                        JobTelegramNotification::query()->where('id', $jobId)->first()->delete();
                        return 200;
                    }
                    break;
            }
        }

        if ($NotificationStep->content_type == 'CALL') {
            $this->sendCall($NotificationStep, $NotificationSchedule);
        }
        if ($NotificationStep->content_type == 'SMS') {
            $this->sendSms($NotificationStep->content_id, $NotificationSchedule);
        }
        if ($NotificationStep->content_type == 'WA') {
            $this->sendWhatsUp($NotificationStep, $NotificationSchedule);
        }

        if ($NotificationStep->content_type == 'WABA') {
            $this->sendWaba($NotificationStep, $NotificationSchedule);
        }

        JobTelegramNotification::query()->where('id', $jobId)->first()->delete();

        return 200;
    }

    public function sendCall($NotificationStep, $NotificationSchedule)
    {

        $callStep = $NotificationStep->getCall;

        if (!empty($callStep->timeSend)) {
            $timeToSended = Carbon::parse($callStep->timeSend);
            $hour = $timeToSended->hour;
            $minutes = $timeToSended->minute;
            $timeSend = now()->setTime($hour, $minutes)->timestamp;
            $data ['plannedAt'] = $timeSend;
        }

        $id = $callStep->project_id;
        $project = Project::query()->find($id);
        $notificationSetting = $project->getNotificationSetting;

        $data ['apiKey'] = $notificationSetting->zvonobot_key;
        $data ['record'] = ['id' => $callStep->campaign_id];
        $data ['phone'] = $NotificationSchedule->phone;
        $data ['dutyPhone'] = '1';

        if (!empty($callStep->type) && $callStep->type == 'day') {
            $leadWeb = NewLeadWeb::query()->where('project_id', $id)->where('phone', '=', $data ['phone'])->get()->last();
            if (!empty($leadWeb) && $leadWeb->created_at->isToday()) {
                Log::alert('Заявка получена сегодня');
                return 200;
            }
        }

        Zvonobot::dispatch($id, $data);

        sleep(1);

        return 200;

    }

    public function sendWhatsUp($NotificationStep, $NotificationSchedule)
    {
        $waMessage = $NotificationStep->getWhatsUp;
        $project = Project::query()->find($waMessage->project_id);
        $phone = $NotificationSchedule->phone;

        if (!empty($waMessage->type) && $waMessage->type == 'day') {
            $leadWeb = NewLeadWeb::query()->where('project_id', $waMessage->project_id)->where('phone', '=', $phone)->get()->last();
            if (!empty($leadWeb) && $leadWeb->created_at->isToday()) {
                Log::alert('Sms заявка получена сегодня');
            }
        } else {
            if (config('app.env') == 'local') {
                $WaMessageController = new WaMessageController();
                $WaMessageController->sendWaJob($waMessage->text, $phone, $project);
            } else {
                SendWhatsUp::dispatch($waMessage->text, $phone, $project);
            }
        }
    }

    public function sendWaba($NotificationStep, $NotificationSchedule)
    {
        $waMessage = $NotificationStep->getWabaTemplate->where('project_id',$NotificationSchedule->project_id)->first();
        $project = Project::query()->find($waMessage->project_id);
        $phone = $NotificationSchedule->phone;
        $timeSend = now();
        $welcome_template_name = $NotificationStep->content_id;

        $waba_intances = WabaIntance::query()->where('project_id',$project->id)->pluck('waba_phone_id');

        if (config('app.name') == 'Laravel_test') {
            $NotificationController = new WabaMessageController();
            $NotificationController->addQueue($welcome_template_name,$waba_intances->toArray(),[$phone],$timeSend,$project->id);
        } else {
            addQueue::dispatch($welcome_template_name,$waba_intances,[$phone],$timeSend,$project->id);
        }

    }

    public function sendWelcomeWa($phone, $project)
    {
        $waMessage = WebWa::query()->where('project_id', $project->id)->where('type', '=', 'welcome')->get()->first();

        if (config('app.name') == 'Laravel_test') {
            $WaMessageController = new WaMessageController();
            $WaMessageController->sendWaJob($waMessage->text, $phone, $project);
        } else {
            SendWhatsUp::dispatch($waMessage->text, $phone, $project);
        }

    }


    public function sendWelcomeWaba($phone, $project)
    {
        $welcome_template_name = wabaSetting::query()->where('project_id', $project->id)->get()->first()->welcome_template_name;
        $timeSend = now();
        $waba_intances = WabaIntance::query()->where('project_id',$project->id)->pluck('waba_phone_id');
        if (config('app.name')== 'Laravel_test') {
            $NotificationController = new WabaMessageController();
            $NotificationController->addQueue($welcome_template_name,$waba_intances->toArray(),[$phone],$timeSend,$project->id);
        } else {
            addQueue::dispatch($welcome_template_name,$waba_intances,[$phone],$timeSend,$project->id);
        }

    }


    public function sendWaJob($text, $phone, $project)
    {

        $lead = NewLeadWeb::query()->where('project_id', $project->id)->where('phone', $phone)->get()->last();

        if (!empty($lead)) {
            $VariableController = new VariableController();
            $text = $VariableController->setVariables($text,$phone,$project->id);
        }

        $instance_id_model = $this->getWaIntance($phone, $project);
        $checkQueue = $this->checkQueueMiddlewire($text, $phone, $instance_id_model->instance_id, $project->id);
        if ($checkQueue) {
            return 315;
        }

        if (empty($instance_id_model)) {
            return 400;
        }

        $notificationSetting = $project->getNotificationSetting;

        $data = [
            'number' => $phone,
            'type' => 'text',
            'message' => $text,
            'instance_id' => $instance_id_model->instance_id,
            'access_token' => $notificationSetting->wapico_key,
        ];

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post("https://whatsmonster.ru/api/send?number=$phone&type=text&message=$text&instance_id=$instance_id_model->instance_id&access_token=$notificationSetting->wapico_key", $data)->json();
        if (isset($response['status'])) {
            if ($response['status'] == 'success') {
                $this->updateIntanceCount($data, $project, $instance_id_model);
            } else {
                WebWaNumber::query()->updateOrCreate([
                    'instance_id' => $instance_id_model->instance_id,
                    'project_id' => $project->id,
                ], [
                    'active' => 0,
                ]);
                return 400;
            }
        }

        return 200;
    }

    public function sendWabaJob()
    {

    }

    public function sendWaMessage($text, $phone, $instance_id, $id)
    {

        $project = Project::query()->find($id);
        $notificationSetting = $project->getNotificationSetting;
        $instance_id_model = WebWaNumber::query()->where(['project_id' => $id, 'instance_id' => $instance_id])->first();

        $checkQueue = $this->checkQueueMiddlewire($text, $phone, $instance_id_model->instance_id, $project->id);
        if ($checkQueue) {
            return 315;
        }

        $data = [
            'number' => $phone,
            'type' => 'text',
            'message' => $text,
            'instance_id' => $instance_id,
            'access_token' => $notificationSetting->wapico_key,
        ];

        $response = Http::withHeaders([
            "content-type" => "application/json",
        ])->post("https://whatsmonster.ru/api/send?number=$phone&type=text&message=$text&instance_id=$instance_id&access_token=$notificationSetting->wapico_key", $data)->json();


        if (isset($response['status'])) {
            if ($response['status'] == 'success') {
                $this->updateIntanceCount($data, $project, $instance_id_model);
            } else {
                WebWaNumber::query()->updateOrCreate([
                    'instance_id' => $instance_id_model->instance_id,
                    'project_id' => $project->id,
                ], [
                    'active' => 0,
                ]);
                return 400;
            }
        }

        return 200;
    }

    public function checkQueueMiddlewire($text, $phone, $instance_id, $id, $sec = 3)
    {
        $instance_id_model = WebWaNumber::query()->where(['project_id' => $id, 'instance_id' => $instance_id])->first();
        $updated_at = $instance_id_model->updated_at;
        $checkQueueMiddleware = Carbon::parse($updated_at)->lt(now()->subSeconds($sec));
        if (!$checkQueueMiddleware) {
            WhatsUpQueue::query()->updateOrCreate([
                'project_id' => $id,
                'phone' => $phone,
            ], [
                'instance_id' => $instance_id,
                'text' => $text,
                'time' => now()->addMinutes(3),
            ]);
            return true;
        }
        return false;
    }

    public function newLeeds($phone, $project, $data)
    {
        $notificationSetting = $project->getNotificationSetting;
        $data ['date'] = today()->format('Y-m-d');

        $data ['utm_medium'] = $data ['utm_medium'] ?? '';

        if (empty($data ['utm_medium']) && !empty($data ['utm_campaign'])){
            $data ['utm_medium'] = $data ['utm_campaign'];
        }

        $NewLeadWeb = NewLeadWeb::query()->updateOrCreate([
            'phone' => $phone,
            'project_id' => $project->id,
            'date' => $data['date'] ?? '',
        ], $data);

        LeadCrm::query()->updateOrCreate([
            'new_lead_web_id' => $NewLeadWeb->id,
            'project_id' => $project->id,
        ], [
            'new_lead_web_id' => $NewLeadWeb->id,
            'dateTask' => today(),
        ]);

        if ($notificationSetting != null) {
            // отправляем приветственное смс если оно есть

                if ($notificationSetting->welcomeWa == '1') {
                    $blackList = BlackList::query()->where('phone', $phone)->exists();

                    if (!$blackList) {
                        $this->sendWelcomeWa($phone, $project);
                    }
                }


                if ($notificationSetting->welcomeWaba == '1') {
                    $blackList = BlackList::query()->where('phone', $phone)->exists();
                    if (!$blackList) {
                        $this->sendWelcomeWaba($phone, $project);
                    }
                }


                if ($notificationSetting->welcomeSms == '1') {
                    $sms = WebSms::query()->where('project_id', $project->id)->whereNull('timeSend')->get()->first();
                    $blackList = BlackList::query()->where('phone', $phone)->exists();
                    if (!$blackList) {
                        $WelcomeMessageController = new WelcomeMessageController();
                        $WelcomeMessageController->sendWelcomeSms($sms, $phone,$project);
                    }
                }


            // добавляем инфо про вебинар
            if (isset($notificationSetting->dateWeb)) {
                $dateWeb = Carbon::parse($notificationSetting->dateWeb);

//                if (!$dateWeb->isToday() && $dateWeb->isPast()){
//                    $dateWeb = '01.01.2000';
//                }

                LeadForWeb::query()->updateOrCreate([
                    'phone' => $phone,
                    'project_id' => $project->id,
                    'dateWeb' => $dateWeb,
                    ], [
                    'utm_source' => $data ['utm_source'],
                    'utm_medium' => $data ['utm_medium'],
                    'user_name' => $data ['name'] ?? '',
                ]);
            }

            // добавляем уведомления если вебинар сегодня
            if (isset($notificationSetting->dateWeb) && Carbon::parse($notificationSetting->dateWeb)->isToday()) {
                $NotificationController = new NotificationController();
                $NotificationController->addNotification($phone, $project->id, $data['utm_source']);
            }
        }
    }

    public function sendSmsAeroJob($date)
    {
        $datas = [];

        if (isset($date['smsAeroSenderName'])) {
            $datas ['sign'] = $date['smsAeroSenderName'];
        }

//        $response = $smsaero_api->send([$date['phone']], $date['text'], $date['timeSend'],);

        $datas ['number'] = $date['phone'];
        $datas ['text'] = $date['text'];
        $datas ['shortLink'] = 1;
        $datas ['dateSend'] = $date['timeSend'];

        $response = Http::get('https://'.$date['smsAeroEmail'].':'.$date['smsAero_key'].'@gate.smsaero.ru/sms/send',$datas);


        if (isset($response['success']) && $response['success'] == true) {

            $cost = $response['data'][0]['cost'] ?? '';

            SmsAeroHistory::query()->create( [
                'project_id' => $date['id'],
                'phone' => $date['phone'],
                'cost' => $cost,
                'text' => $date['text'],
                'status' => 'success',
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
    }

    public function sendP1smsJob($date, $project_id)
    {

        $url = 'https://admin.p1sms.ru/apiSms/create';

        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->post($url, $date)->json();
        if (isset($response['status']) && $response['status'] == 'success') {

            SmsAeroHistory::query()->updateOrCreate([
                'project_id' => $project_id,
                'phone' => $response['data'][0]['phone'] ?? '',
            ], [
                'status' => 'success',
            ]);
        }

    }


    public function getWaIntance($phone, $project)
    {
        $WaSendMessage = WaSendMessage::query()->where('project_id', $project->id)->where('phone', $phone)->get()->last();
        $webWaNumber = WebWaNumber::query()->where('project_id', $project->id)->get();

        if (isset($WaSendMessage->instance_id)) {
            $instance = $WaSendMessage->instance_id;

            $instance_id_model = $webWaNumber->where('instance_id', $instance)->first();
            if (!empty($instance_id_model) && $instance_id_model->active == 1) {
                return $instance_id_model;
            } else {
                return $webWaNumber->where('active', "=", 1)->sortBy('updated_at')->first();
            }
        } else {
            return $webWaNumber->where('active', "=", 1)->sortBy('updated_at')->first();
        }

    }
    public function getWabaIntance($phone, $wabaIntances, $id)
    {
        $WaSendMessage = WabaLinkNumber::query()->where('project_id', $id)->where('phone', $phone)->get()->last();
        $webWaNumber = WabaIntance::query()->where('project_id', $id)->whereIn('waba_phone_id', $wabaIntances->pluck('waba_phone_id'))->get();
        if (isset($WaSendMessage->waba_intance_id)) {
            $instance = $WaSendMessage->waba_intance_id;
            $instance_id_model = $webWaNumber->where('id', $instance)->first();
            if (!empty($instance_id_model)) {
                return $instance_id_model;
            } else {
                return $webWaNumber->where('count_today', '<=', 240)->sortByDesc('count_today')->first();
            }
        } else {
            return $webWaNumber->where('count_today', '<=', 240)->sortByDesc('count_today')->first();
        }

    }

    public function updateIntanceCount($data, $project, $instance_id_model)
    {
        WaSendMessage::query()->insert([
            'project_id' => $project->id,
            'phone' => $data['number'],
            'instance_id' => $data['instance_id'],
        ]);

        //
        WebWaNumber::query()->updateOrCreate([
            'project_id' => $project->id,
            'instance_id' => $data['instance_id'],
        ], [
            'count' => $instance_id_model->count + 1,
        ]);

    }

    public function shortLink($phone, $project_id, $leadForWeb)
    {
        $linksWeb = GlobalVariable::query()->where('project_id', $project_id)->where('variable_name', '=', 'linkWeb')->get()->first();
        $date['linkFull'] = $linksWeb->variable_value . 'utm_source=' . $leadForWeb->utm_source . '&utm_medium=' . $leadForWeb->utm_medium . '&phone=' . $phone;

        $url = 'https://kupr.pro/api/shortLink/add';

        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->post($url, $date);

        if (strlen($response->body()) < 10) {
            return $response->body();
        } else {
            return 'error';
        }

    }

    function setVariablesUser($phone, $project_id, $step)
    {
        $leadForWeb = LeadForWeb::query()->where('project_id', $project_id)->where('phone', $phone)->get()->last();

        if ($step->content_type == 'SMS') {
            $sms = WebSms::query()->find($step->content_id);

            $hasBraces = $this->hasBraces($sms->text);

            if (count($hasBraces) > 0) {
                foreach ($hasBraces as $variblal) {
                    $varible = substr($variblal, 1);

                    if ($varible == 'linkWeb') {
                        $variable_value = $this->shortLink($phone, $project_id, $leadForWeb);
                    }

                    if ($varible == 'linkInvite') {
                        $projectTelegram = Project::query()->find($project_id);

                        $linkChangeModel = linkChangeModel::query()->where('project_id', $projectTelegram->id)->get();
                        $lead = NewLeadWeb::query()->where('project_id', $project_id)->where('phone', $phone)->get()->last();

                        $linkChange = $linkChangeModel->where('utm_source', $lead->utm_source)->where('linkName', '=', $varible)->first();
                        if (empty($linkChange->link)) {
                            $linkChange = $linkChangeModel->where('utm_source', 'all')->where('linkName', '=', $varible)->first();
                        }
                        $variable_value = $linkChange->link;
                    }


                    Variable::query()->updateOrCreate([
                        'lead_for_web_id' => $leadForWeb->id,
                        'project_id' => $project_id,
                        'variable_name' => $varible,
                    ], [
                        'variable_value' => $variable_value,
                    ]);

                }
            }
        }

        if ($step->content_type == 'WA') {
            $wa = WebWa::query()->find($step->content_id);
            $hasBraces = $this->hasBraces($wa->text);

            if (count($hasBraces) > 0) {
                foreach ($hasBraces as $variblal) {
                    $varible = substr($variblal, 1);

                    if ($varible == 'linkWeb') {
                        $variable_value = $this->shortLink($phone, $project_id, $leadForWeb);
                    }

                    if ($varible == 'linkInvite') {
                        $projectTelegram = Project::query()->find($project_id);

                        $linkChangeModel = linkChangeModel::query()->where('project_id', $projectTelegram->id)->get();
                        $lead = NewLeadWeb::query()->where('project_id', $project_id)->where('phone', $phone)->get()->last();

                        $linkChange = $linkChangeModel->where('utm_source', $lead->utm_source)->where('linkName', '=', $varible)->first();
                        if (empty($linkChange->link)) {
                            $linkChange = $linkChangeModel->where('utm_source', 'all')->where('linkName', '=', $varible)->first();
                        }
                        $variable_value = $linkChange->link;
                    }

                    Variable::query()->updateOrCreate([
                        'lead_for_web_id' => $leadForWeb->id,
                        'project_id' => $project_id,
                        'variable_name' => $varible,
                    ], [
                        'variable_value' => $variable_value,
                    ]);

                }
            }
        }

    }

    function hasBraces($string)
    {

        // Создаем пустой массив для хранения найденных слов
        $words = array();
        // Используем регулярное выражение для поиска всех слов в фигурных скобках
        preg_match_all('/\{(\$\w+)\}/', $string, $matches);

        // Перебираем все найденные совпадения
        foreach ($matches[1] as $match) {
            // Добавляем слово в массив
            $words[] = $match;
        }
        // Возвращаем массив слов
        return $words;
    }

    public function sendMessageWaba($waba_templates_id, $phone)
    {
        $wabaTemplates = WabaTemplates::query()->find($waba_templates_id);
        $wabaVariable = $wabaTemplates->getVariable;
        $wabaSetting = $wabaTemplates->getSetting;
        $token = $wabaSetting->token;
        $getWabaIntance = $wabaTemplates->getWabaIntance;
        $waba_phone_id = $getWabaIntance->waba_phone_id;

        $date = [
            'messaging_product' => "whatsapp",
            'to' => "$phone",
            'type' => "template",
            "recipient_type" => "individual",
            'template' => [
                "namespace" => "$getWabaIntance->name_space",
                "name" => "$wabaTemplates->name",
                "language" => [
                    'code' => "$wabaTemplates->code"
                ],
            ],
        ];

        if ($wabaTemplates->url_type == 'photo') {
            $date['template']['components'][] =
                [
                    'type' => "header",
                    "parameters" => [[
                        "type" => "image",
                        "image" => ['link' => "$wabaTemplates->url_media"]
                    ]]
                ];
        }

        if (count($wabaVariable) > 0) {
            $index = 0;
            foreach ($wabaVariable as $variable){
                $VariableController = new VariableController();
                $VariableController->setVariablesWaba($variable->value,$phone,$wabaTemplates->project_id);

                if ($variable->type =='text'){
                    $date['template']['components'][] =
                        [
                            'type' => "body",
                            "parameters" => [[
                                "type" => "text",
                                "text" => $variable->value
                            ]]
                        ,
                    ];
                }
                if ($variable->type =='button'){
                    $date['template']['components'][] =
                          [
                              "type" => "button",
                "index" => "$index",
                "sub_type" => "url",
                "parameters" => [
                    [
                        "type" => "text",
                        "text" => "$variable->value"
                    ]
                ]
            ];
                    $index++;
                }
            }

        }

        $url = "https://graph.facebook.com/v19.0/$waba_phone_id/messages";
        $response = Http::withHeaders([
            "Authorization" => "Bearer " . $token,
            "content-type" => "application/json",
        ])->post($url, $date);

        $response = $response->json();

        if (isset($response['messages'][0]['message_status']) && $response['messages'][0]['message_status'] == 'accepted') {

            WabaHistory::query()->create([
                'project_id' => $wabaTemplates->project_id,
                'phone' => $response['contacts'][0]['wa_id'],
                'type' => 'outgoing',
                'message' => $wabaTemplates->name,
                'status' => $response['messages'][0]['message_status'],
                'w_id' => $response['messages'][0]['id'],
                'display_phone' => $getWabaIntance->phone,
            ]);

            WabaLinkNumber::query()->create([
                'project_id' => $wabaTemplates->project_id,
                'phone' => $response['contacts'][0]['wa_id'],
                'waba_intance_id' => $getWabaIntance->id,
            ]);

        }
    }


    function getWabaIntanceFromPhone($phone,$id){
        $WaSendMessage = WabaLinkNumber::query()->where('project_id', $id)->where('phone', $phone)->get()->last();
        $webWaNumber = WabaIntance::query()->where('project_id', $id)->get();
        if (isset($WaSendMessage->waba_intance_id)) {
            $instance = $WaSendMessage->waba_intance_id;
            $instance_id_model = $webWaNumber->where('id', $instance)->first();
            if (!empty($instance_id_model)) {
                return $instance_id_model;
            } else {
                return $webWaNumber->sortBy('count_today')->first();
            }
        } else {

            return $webWaNumber->sortBy('count_today')->first();
        }

    }

    public function sendMessageWabaOne($phone,$text,$id){

        $instance_model = $this->getWabaIntanceFromPhone($phone, $id);
        $waba_phone_id = $instance_model->waba_phone_id;
        $wabaSetting = wabaSetting::query()->where('project_id', $id)->get()->first();
        $token = $this->token = $wabaSetting->token;


        $date = [
            'messaging_product' => "whatsapp",
            'to' => "$phone",
            'type' => "text",
            "recipient_type" => "individual",
            'text' => [
                "preview_url" => "true",
                "body" => "$text",
            ],
        ];

        $url = "https://graph.facebook.com/v19.0/$waba_phone_id/messages";
        $response = Http::withHeaders([
            "Authorization" => "Bearer " . $token,
            "content-type" => "application/json",
        ])->post($url, $date);

        $response = $response->json();

        if (isset($response['messages'][0]['id'])) {

            WabaHistory::query()->create([
                'project_id' => $instance_model->project_id,
                'phone' => $response['contacts'][0]['wa_id'],
                'type' => 'outgoing',
                'message' => "$text",
                'status' => 'send',
                'w_id' => $response['messages'][0]['id'],
                'display_phone' => $instance_model->phone,
            ]);
        }

    }

    public function addQueue($template_name, $waba_intances, $phones, $timeSend, $id)
    {
        $WabaMessageController = new WabaMessageController();
        $wabaIntances = $WabaMessageController->checkWabaIntances($template_name, $waba_intances,$id);

        $CheckRussiaPhoneController = new CheckRussiaPhoneController();
        $phones = $CheckRussiaPhoneController->checkBlackList($phones,$id);


        if (empty($phones)){
            return 200;
        }
        if ($wabaIntances->first() == null) {
            return back()->withErrors(['phone' => 'Не правильный интанс']);
        }
        foreach ($phones as $phone) {
            $WabaMessageController = new WabaMessageController();
            $instance_model = $WabaMessageController->getWabaIntance($phone, $wabaIntances, $id);
            $template_model = $WabaMessageController->getTemplate($instance_model, $template_name);

            WabaQueue::query()->updateOrCreate([
                'project_id' => $id,
                'phone' => $phone,
                'waba_templates_id' => $template_model->id,
            ],[
                'project_id' => $id,
                'dateSend' => $timeSend,
                'phone' => $phone,
                'waba_intance_id' => $instance_model->id,
                'waba_templates_id' => $template_model->id,
            ]);


            if (Carbon::parse($instance_model->updated_at)->isToday()) {
                $instance_model->increment('count_today');
            } else {
                $instance_model->update(['count_today' => 1]);
            }
            usleep(10000);
        }
        return 200;
    }

    public function setVariableWaba($text,$phone,$project_id){
        $hasBraces = $this->hasBraces($text);

        if (count($hasBraces) > 0) {
            foreach ($hasBraces as $variblal) {
                $varible = substr($variblal, 1);

                if ($varible == 'phone') {
                    $text = str_replace('{$phone}', $phone, $text);
                }
            }
        }
        return $text;
    }

    public function checkBlackList($phones,$id){
        $BlackList = BlackList::query()->where('project_id',$id)->pluck('phone');
        return collect($phones)->diff($BlackList)->toArray();
    }

    public function checkWabaIntances($template_name,$wabaIntances,$id){
        $waba_intance_id = WabaTemplates::query()->where('project_id',$id)->where('name',$template_name)->pluck('waba_intance_id')->toArray();
        $wabaIntance = WabaIntance::query()
            ->whereIn('id',$waba_intance_id)
            ->whereIn('waba_phone_id',$wabaIntances)
            ->get();
        return $wabaIntance;
    }

    public function getTemplate($instance_model,$template_name){
        return WabaTemplates::query()->where('waba_intance_id',$instance_model->id)->where('name',$template_name)->get()->last();
    }

    /**
     * Функция позволяет получить массив номеров, проверить их и выдать валидные Русские номера в формате Массива
     * @param  array  $ArrayPhone
     * @return array  $phoneAdd
     */

    public function getPhone($ArrayPhone){
        // Разбиваем текст по переносам строк

        $result = preg_split("/[\s,]+/", $ArrayPhone);
        // Для каждой строки в списке

        foreach($result as $line) {
            // Пытаемся проверить, является ли строка валидным номером телефона
            try {
                $CheckRussiaPhoneController = new CheckRussiaPhoneController();
                $phone = $CheckRussiaPhoneController->checkPhone($line);
            } catch (\Exception $exception) {
                // Если нет, то пропускаем строку
                continue;
            }
            if ($phone != null) {
                $phoneAdd[] = $phone;
            }
        }
        return $phoneAdd;
    }

        public function setVariablesSms($text, $lead, $projectTelegram,$phone = null)
    {

        if (str_contains($text, '{$phone}')) {
            $text = str_replace('{$phone}', $phone, $text);
        }

        if (str_contains($text, '{$linkWeb}')) {
            $linkName = 'linkWeb';

            $linkChangeModel = linkChangeModel::query()->where('project_id', $projectTelegram->id)->get();

            $linkChange = $linkChangeModel->where('utm_source', $lead->utm_source)->where('linkName', '=', $linkName)->first();
            if (empty($linkChange->link)) {
                $linkChange = $linkChangeModel->where('utm_source', 'all')->where('linkName', '=', $linkName)->first();
            }
            $link = $linkChange->link;


            $text = str_replace('{$' . $linkName . '}', $link, $text);
        }
        if (str_contains($text, '{$linkInvite}')) {
            $linkName = 'linkInvite';

            $linkChangeModel = linkChangeModel::query()->where('project_id', $projectTelegram->id)->get();

            $linkChange = $linkChangeModel->where('utm_source', $lead->utm_source)->where('linkName', '=', $linkName)->first();
            if (empty($linkChange->link)) {
                $linkChange = $linkChangeModel->where('utm_source', 'all')->where('linkName', '=', $linkName)->first();
            }
            $link = $linkChange->link;

            $text = str_replace('{$' . $linkName . '}', $link, $text);
        }

        if (str_contains($text, '{$linkInviteUser}')) {
            $LinkInvitePhoneController = new LinkInvitePhoneController();
            try {
                $link = $LinkInvitePhoneController->createInviteLinkAndSetPhone($projectTelegram->id,$lead->phone);
            } catch (Throwable $e) {
                report($e);
                if ($projectTelegram->id == 114){
                    $link = 'https://t.me/+ZK4ocxUrL7E3ZTI6';
                }
            }
            $text = str_replace('{$linkInviteUser}', $link, $text);
        }

//        if ($varible == 'linkInviteUser') {
//            $LinkInvitePhoneController = new LinkInvitePhoneController();
//            try {
//                $link = $LinkInvitePhoneController->createInviteLinkAndSetPhone($project_id,$phone);
//            } catch (Throwable $e) {
//                report($e);
//                if ($id == 114){
//                    $link = 'https://t.me/+ZK4ocxUrL7E3ZTI6';
//                }
//            }
//            $text = str_replace('{$linkInviteUser}', $link, $text);
//        }


        return $text;
    }



}


