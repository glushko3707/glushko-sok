<?php

namespace App\Traits;

use App\Jobs\Sipout\Databases\UpdateCategoryTable;
use App\Models\Project;
use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use App\Models\Sipout\SipoutCallId;
use Illuminate\Support\Facades\Storage;


trait SipOut
{
    public function getPhone($category_id,$count,$countFile,$id){

            $database = $this->getPhones($category_id,$count*$countFile,$id);

            if ($database != null) {
                 $ContactDatabase = $database['contactDatabase'];
            }

            $CategoryDatabase = $database['categoryDatabase'];

            $ContactDatabasen = $ContactDatabase->chunk($count);
            $index = 0;
            foreach ($ContactDatabasen as $ContactDatabasens){
            $filenames = "$CategoryDatabase->category_name" . '_' . $ContactDatabasens->count() . '_'  . $index . '_' . now()->timestamp . '.txt';
            $index++;

            $pathPart = "public/project/contact/".$id . '/';
            $path = Storage::path($pathPart);

            if (!is_dir($path)) {
                Storage::makeDirectory($pathPart);
                Storage::setVisibility($pathPart,'public');
            }
                $data = $ContactDatabasens->pluck('phone')->toArray();
                $data = implode("\n", $data);

                Storage::put($pathPart . $filenames, $data,'public');
                usleep(10000);
            }

        $idUsed = $ContactDatabase->pluck('id');
        ContactDatabase::query()
            ->where('project_id',$id)
            ->whereIn('id',$idUsed)
            ->update(['used' => 1]);

        UpdateCategoryTable::dispatch($id,$category_id)->onQueue('low');
    }

    public function getPhones($category_id,$count,$id){
        $CategoryDatabase = CategoryDatabase::query()->find($category_id);

        $ContactDatabaseNotNull = ContactDatabase::query()
            ->where([
                ['project_id', '=', $id],
                ['category_databases_id', '=', $category_id],
                ['used', '=', 0]
            ]);

        $ContactDatabase = $ContactDatabaseNotNull->limit($count)->get();

        return [
            'contactDatabase' => $ContactDatabase,
            'limit' => count($ContactDatabase),
            'categoryDatabase' => $CategoryDatabase,
            'contactCount' => count($ContactDatabase),
        ];

    }

    public function addPhoneSipout($count,$category_id,$call_id,$id){

        $project = Project::query()->find($id);
        $sipProject = $project->getSipProject;
        $api_key = $sipProject->api_key;
        $sipoutCallId = SipoutCallId::query()->find($call_id);

        $database = $this->getPhones($category_id,$count,$id);


        if ($database != null) {
            $ContactDatabase = $database['contactDatabase'];

        $date['phones'] = $ContactDatabase->pluck('phone');

        $url = 'https://lk.sipout.net/userapi/?key='.$api_key.'&method=ai&action=add&ai=' . trim($sipoutCallId->callId, "*");
        $response = \Illuminate\Support\Facades\Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->post($url,$date);

            if ($response->json()['result'] == 'ok'){
                $idUsed = $ContactDatabase->pluck('id');
                ContactDatabase::query()
                    ->where('project_id',$id)
                    ->whereIn('id',$idUsed)
                    ->update(['used' => 1]);

                UpdateCategoryTable::dispatch($id,$category_id)->onQueue('low');
                return true;
            } else {
                return false;
            }
    }
        return false;
    }

//    public function sipoutStatisticUpdateStore($id){
//        $timeDifferent = 30;
//        $days = 0;
//        $dayFrom = now()->subDays($days)->format('d.m.Y');
//        $dayTo = now()->format('d.m.Y');
//        $project = Project::query()->find($id);
//        $sipProject = $project->getSipProject;
//        $api_key = $sipProject->api_key;
//
//        if (!isset($sipProject->api_key)){
//            return redirect(route('sipoutSetting.index',$id))->withErrors('SipOut Api Key не заполнен');;
//        }
//
//        $priceSec = 0.016;
//        $url = 'https://lk.sipout.net/userapi/?key='.$api_key.'&method=call_stat&action=get_list&ds='.$dayFrom.'&de=' . $dayTo;
//
//        $response = \Illuminate\Support\Facades\Http::withHeaders([
//            "content-type" => "application/json",
//            'accept' => 'application/json',
//        ])->get($url);
//
//        if (!isset($response->json()['data']['list'])){
//            return 'ошибка возврата статистики звонков';
//        }
//
//        $call = collect($response->json()['data']['list']);
//
//
//        if (count($call) > 0){
//            $this->service->updateSipoutCall($project,$priceSec,$call,$timeDifferent);
//        }
//
//        return back();
//    }


}
