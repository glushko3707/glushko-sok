<?php

namespace App\Jobs\Facebook;

use App\Http\Controllers\Notification\TrafficController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AdsUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */

    public $id;
    public $account_id;

    public function __construct($id,$account_id)
    {
        $this->id = $id;
        $this->account_id = $account_id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $TrafficController = new TrafficController();
        $TrafficController->targetUpdateStat($this->id, $this->account_id);
    }
}
