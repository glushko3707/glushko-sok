<?php

namespace App\Jobs\Waba;

use App\Http\Controllers\Waba\WabaMessageController;
use App\Traits\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class addQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Notification;

    public $template_name;
    public $waba_phone_ids;
    public $timeSend;
    public $phones;
    public $id;
    public $variable;


    /**
     * Create a new job instance.
     */
    public function __construct($template_name,$waba_phone_ids,$phones,$timeSend,$id,$variable = null)
    {
        $this->template_name =  $template_name;
        $this->waba_phone_ids = $waba_phone_ids;
        $this->timeSend = $timeSend ;
        $this->phones = $phones;
        $this->id = $id;
        $this->variable = $variable;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $template_name = $this->template_name;
        $waba_phone_ids = $this->waba_phone_ids;
        $timeSend = $this->timeSend;
        $phones = $this->phones;
        $id = $this->id;
        $variable = $this->variable;
        $NotificationController = new WabaMessageController();
        $NotificationController->addQueue($template_name,$waba_phone_ids,$phones,$timeSend,$id,$variable);
    }
}
