<?php

namespace App\Jobs\Waba;

use App\Http\Controllers\Waba\WabaMessageController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WabaSendMessegeOne implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $phone;
    public $text;
    public $project_id;

    /**
     * Create a new job instance.
     */
    public function __construct($phone,$text,$project_id)
    {
        $this->phone = $phone;
        $this->text = $text;
        $this->project_id = $project_id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $WabaMessage = new WabaMessageController();
        $WabaMessage->sendMessageWabaOne($this->phone,$this->text,$this->project_id);
    }
}
