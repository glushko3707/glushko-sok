<?php

namespace App\Jobs\Waba;

use App\Http\Controllers\Waba\SendMessageController;
use App\Models\Waba\WabaTemplates;
use App\Traits\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\RateLimitedMiddleware\RateLimited;
use Throwable;

class SendMessege implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Notification;

    /**
     * Create a new job instance.
     */

    public $phone;
    public $waba_templates_id;
    public $tries = 30;
    public $waba_intance_id;
    public $variable;

    public function __construct($waba_templates_id,$phone,$variable = null)
    {
        $this->waba_templates_id = $waba_templates_id;
        $WabaTemplates = WabaTemplates::query()->find($waba_templates_id);
        $this->waba_intance_id = $WabaTemplates->waba_intance_id;
        $this->phone = $phone;
        $this->variable = $variable;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            $SendMessageController = new SendMessageController();
            $SendMessageController->sendMessageWabaClass($this->waba_templates_id ,$this->phone,$this->variable);
        } catch (Throwable $e) {
            report($e);
        }
    }


    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->key($this->waba_intance_id)
            ->allow(1)
            ->everySeconds(rand(5,9))
            ->releaseAfterSeconds(rand(10,30));
        return [$rateLimitedMiddleware];
    }
}
