<?php

namespace App\Jobs\Waba;

use App\Http\Controllers\Waba\WabaMessageController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WabaSendMessegeCallback implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $phone;
    public $text;
    public $project_id;
    public $display_phone;
    public $media;
    public $createButton;


    /**
     * Create a new job instance.
     */
    public function __construct($display_phone,$phone,$text,$project_id,$media = null,$createButton = null)
    {
        $this->display_phone = $display_phone;
        $this->phone = $phone;

        $this->text = $text;
        $this->project_id = $project_id;

        $this->media = $media;
        $this->createButton = $createButton;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $WabaMessage = new WabaMessageController();
        $WabaMessage->sendMessageCallback($this->display_phone,$this->phone,$this->text,$this->project_id,$this->media,$this->createButton);
    }

}
