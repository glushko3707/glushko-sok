<?php

namespace App\Jobs\Webinar;

use App\Http\Controllers\Webinar\Bizon\BizonController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddUserAfterWeb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public array $userWeb;
    public int $id;

    /**
     * Create a new job instance.
     */
    public function __construct($userWeb, $id)
    {
        $this->userWeb = $userWeb;
        $this->id = $id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $BizonController = new BizonController();
        $BizonController->addUserAfterWeb($this->userWeb, $this->id);
    }
}
