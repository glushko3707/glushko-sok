<?php

namespace App\Jobs\Google;

use App\Http\Controllers\GoogleSheets\GoogleSheetsController;
use App\Models\Sipout\SipOutSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NewProjectSheetJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    public $name;


    /**
     * Create a new job instance.
     */
    public function __construct($id,$name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        $sheet = new GoogleSheetsController();
        $sheet->createSheets($this->name);
        $sheet->updateTitleSheets($sheet->spreadsheetId,'Sheet1','leads');
        $sheet->relocateSheets($sheet->spreadsheetId,'1p6NHEFmmK-kWd1SvEYmDsNttXAkJPqhX');

        $values = [['date', 'phone', 'utm_source', 'utm_medium','typeCall']];

        $sheet->inputRow($sheet->spreadsheetId,$values,$sheet->sheetName);

        SipOutSetting::query()->updateOrCreate([
            'project_id' => $this->id,
        ],[
            'sheetId' => $sheet->spreadsheetId
        ]);
    }
}
