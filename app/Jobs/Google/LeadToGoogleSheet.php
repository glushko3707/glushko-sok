<?php

namespace App\Jobs\Google;

use App\Http\Controllers\GoogleSheets\GoogleSheetsController;
use App\Models\Sipout\SipOutSetting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class LeadToGoogleSheet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sheetDate;
    private $id;

    /**
     * Create a new job instance.
     */
    public function __construct($sheetDate,$id)
    {
        $this->sheetDate = $sheetDate;
        $this->id = $id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $sheet = new GoogleSheetsController();
        $sheet->connectGoogle();
        $sheet->inputRow(SipOutSetting::query()->where('project_id',$this->id)->first()->sheetId,$this->sheetDate,'leads');
    }
}
