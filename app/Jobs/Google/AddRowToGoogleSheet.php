<?php

namespace App\Jobs\Google;

use App\Http\Controllers\GoogleSheets\GoogleSheetsController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddRowToGoogleSheet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $sheetDate;
    private $sheetId;
    private $sheetName;

    public function __construct($sheetDate,$sheetId,$sheetName)
    {
        $this->sheetDate = $sheetDate;
        $this->sheetId = $sheetId;
        $this->sheetName = $sheetName;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $sheet = new GoogleSheetsController();
        $sheet->connectGoogle();
        $sheet->inputRow($this->sheetId,$this->sheetDate,$this->sheetName);
    }
}
