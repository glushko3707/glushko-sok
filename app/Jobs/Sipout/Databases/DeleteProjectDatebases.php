<?php

namespace App\Jobs\Sipout\Databases;

use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DeleteProjectDatebases implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $id;


    /**
     * Create a new job instance.
     */
    public function __construct($id)
    {
        $this->id = $id;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        ContactDatabase::query()->where('project_id',$this->id)->delete();
        CategoryDatabase::query()->where('project_id',$this->id)->delete();
    }
}
