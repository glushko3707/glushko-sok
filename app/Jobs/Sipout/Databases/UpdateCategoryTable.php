<?php

namespace App\Jobs\Sipout\Databases;

use App\Models\BlackList;
use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateCategoryTable implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    public $category;

    /**
     * Create a new job instance.
     */
    public function __construct($id,$category)
    {
        $this->id = $id;
        $this->category = $category;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $BlackList = BlackList::query()->where('project_id',$this->id)->pluck('phone');

        ContactDatabase::query()
            ->where([
                ['project_id', '=', $this->id],
                ['category_databases_id', '=', $this->category]
            ])
            ->whereIn('phone',$BlackList)
            ->delete();

        $categoryDatabase = CategoryDatabase::query()->find($this->category);


        $ContactDatabaseUsedCount = ContactDatabase::query()
            ->where([
                ['project_id', '=', $this->id],
                ['category_databases_id', '=', $this->category],
                ['used', '=', 1]
            ])
            ->count();


        $ContactDatabaseCount = ContactDatabase::query()
            ->where([
                ['project_id', '=', $this->id],
                ['category_databases_id', '=', $this->category]
            ])
            ->count();

        $categoryDatabase->update([
            'used_count' => $ContactDatabaseUsedCount,
            'count' => $ContactDatabaseCount,
        ]);

    }
}
