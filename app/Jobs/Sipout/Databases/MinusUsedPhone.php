<?php

namespace App\Jobs\Sipout\Databases;

use App\Jobs\Sipout\MinusSecondStep;
use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MinusUsedPhone implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $category;
    private $id;
    private $minus;

    public function __construct($id,$category,$minus)
    {
        $this->id = $id;
        $this->category = $category;
        $this->minus = $minus;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $minusPhone = CategoryDatabase::query()
            ->where('project_id', $this->id)
            ->whereIn('category_name', $this->minus)
            ->pluck('id');

        $categoryDatabase_id = CategoryDatabase::query()
            ->where('project_id', $this->id)
            ->where('category_name', $this->category)
            ->first()->id;


        $ContactDatabasePhone = ContactDatabase::query()->where('project_id',$this->id)->whereIn('category_databases_id',$minusPhone)->pluck('phone');
        $forSecond = $ContactDatabasePhone->chunk(10000);
        foreach ($forSecond as $minusSecondStep){
            MinusSecondStep::dispatch($minusSecondStep,$this->id,$categoryDatabase_id)->onQueue('low');
        }

    }
}
