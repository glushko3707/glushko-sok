<?php

namespace App\Jobs\Sipout\Databases;

use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateUsedPhone implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $id;
    public $categoryCleanUsed_id;

    public function __construct($id,$categoryCleanUsed_id)
    {
        $this->id = $id;
        $this->categoryCleanUsed_id = $categoryCleanUsed_id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        ContactDatabase::query()
            ->where([
                ['category_databases_id', '=', $this->categoryCleanUsed_id]
            ])
            ->update(['used' => 0]);
    }
}
