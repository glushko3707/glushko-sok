<?php

namespace App\Jobs\Sipout;

use App\Http\Controllers\Service\CheckRussiaPhoneController;
use App\Http\Controllers\Sipout\ContactDatabaseController;
use App\Models\BlackList;
use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use App\Services\Sipout\Service;
use App\Traits\SipOut;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddPhone implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use SipOut;

    private $category;
    private $id;
    private $phoneWeb;
    private $minus;

    /**
     * Create a new job instance.
     */
    public function __construct($phoneWeb,$id,$category,$minus)
    {
        $this->category = $category;
        $this->id = $id;
        $this->phoneWeb = $phoneWeb;
        $this->minus = $minus;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $ContactDatabaseController  = new ContactDatabaseController(new Service());
        $ContactDatabaseController->addPhoneToDatabase($this->phoneWeb,$this->id,$this->category,$this->minus);
    }
}
