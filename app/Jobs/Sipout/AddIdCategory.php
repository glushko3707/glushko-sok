<?php

namespace App\Jobs\Sipout;

use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddIdCategory implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $id;
    public $category;

    /**
     * Create a new job instance.
     */
    public function __construct($id,$category)
    {
        $this->id = $id;
        $this->category = $category;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
         $id  = $this->id ;
         ContactDatabase::query()->where('project_id',$id)->whereNull('category_databases_id')->update(['category_databases_id' => $this->category->id]);

    }
}
