<?php

namespace App\Jobs\Sipout;

use App\Http\Controllers\Sipout\SipoutStatisticController;
use App\Services\Sipout\Service;
use App\Traits\SipOut;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateProject implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use SipOut;

    private $id;


    /**
     * Create a new job instance.
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
       $SipoutStatisticController = new SipoutStatisticController(new Service());
        $SipoutStatisticController->sipoutStatisticUpdateStore($this->id);
    }

}
