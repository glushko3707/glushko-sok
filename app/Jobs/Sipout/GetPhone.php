<?php

namespace App\Jobs\Sipout;

use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use App\Traits\SipOut;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class GetPhone implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use SipOut;

    private $category_id;
    private $id;
    private $count;
    private $countFile;


    /**
     * Create a new job instance.
     */
    public function __construct($category_id,$count,$countFile,$id)
    {
        $this->category_id = $category_id;
        $this->id = $id;
        $this->count = $count;
        $this->countFile = $countFile;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $category_id = $this->category_id;
        $count = $this->count;
        $id = $this->id;
        $countFile = $this->countFile;

        $this->getPhone($category_id,$count,$countFile, $id);

    }
}
