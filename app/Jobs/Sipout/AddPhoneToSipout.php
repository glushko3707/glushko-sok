<?php

namespace App\Jobs\Sipout;

use App\Traits\SipOut;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddPhoneToSipout implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use SipOut;

    private $count;
    private $category_id;
    private $call_id;
    private $id;

    /**
     * Create a new job instance.
     */
    public function __construct($count,$category_id,$call_id,$id)
    {
        $this->count = $count;
        $this->category_id = $category_id;
        $this->call_id = $call_id;
        $this->id = $id;
    }
    /**
     * Execute the job.
     */
    public function handle(): void
    {
       $this->addPhoneSipout($this->count,$this->category_id,$this->call_id,$this->id);
    }

}
