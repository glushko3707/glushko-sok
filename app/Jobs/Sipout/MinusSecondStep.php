<?php

namespace App\Jobs\Sipout;

use App\Jobs\Sipout\Databases\UpdateCategoryTable;
use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class MinusSecondStep implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */

    private $ContactDatabasePhone;
    private $id;
    private $categoryDatabase_id;

    public function __construct($ContactDatabasePhone,$id,$categoryDatabase_id)
    {
        $this->ContactDatabasePhone = $ContactDatabasePhone;
        $this->id = $id;
        $this->categoryDatabase_id = $categoryDatabase_id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
       ContactDatabase::query()->where('project_id',$this->id)->where('category_databases_id','=',$this->categoryDatabase_id)->whereIn('phone',$this->ContactDatabasePhone)->update(['used' => 1]);
       UpdateCategoryTable::dispatch($this->id,$this->categoryDatabase_id)->onQueue('low');
    }
}
