<?php

namespace App\Jobs\Sipout;

use App\Models\Sipout\CategoryDatabase;
use App\Models\Sipout\ContactDatabase;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

class DeletePhoneDatabase implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $category_id;
    private $id;

    /**
     * Create a new job instance.
     */
    public function __construct($category_id,$id)
    {
        $this->category_id = $category_id;
        $this->id = $id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        ContactDatabase::query()->where('project_id',$this->id)->where('category_databases_id',$this->category_id)->delete();
    }
}
