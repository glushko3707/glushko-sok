<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Spatie\RateLimitedMiddleware\RateLimited;

class TestJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $maks;
    public $tries = 7;

    /**
     * Create a new job instance.
     */
    public function __construct($maks)
    {
        $this->maks = $maks;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $response = Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->post('https://webhook.site/02552419-81e1-4b3a-9329-30f09d1bfc38');
    }

    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->allow(1)
            ->everySeconds(60)
            ->releaseAfterMinutes(rand(1,10));
        return [$rateLimitedMiddleware];

    }

}
