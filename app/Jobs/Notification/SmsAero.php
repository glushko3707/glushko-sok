<?php

namespace App\Jobs\Notification;

use App\Http\Controllers\Notification\Services\SmsController;
use App\Models\Notification\SmsAeroHistory;
use App\Traits\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SmsAero\SmsaeroApiV2;
use Spatie\RateLimitedMiddleware\RateLimited;

class SmsAero implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Notification;

    public $data;
    public $tries = 3;

    /**
     * Create a new job instance.
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $data = $this->data;

        $send = new SmsController();
        $send->sendSmsAeroJob($data);
    }

    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->allow(500)
            ->everySeconds(5)
            ->releaseAfterSeconds(rand(1,5));
        return [$rateLimitedMiddleware];
    }

}
