<?php

namespace App\Jobs\Notification;

use App\Traits\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\RateLimitedMiddleware\RateLimited;

class SendMessageWhatsUp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Notification;

    public $text;
    public $phone;
    public $instance_id;
    public $id;
    /**
     * Create a new job instance.
     */
    public function __construct($text,$phone,$instance_id,$id)
    {
        $this->text = $text;
        $this->phone = $phone;
        $this->instance_id = $instance_id;
        $this->id = $id;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->sendWaMessage($this->text,$this->phone,$this->instance_id,$this->id);
    }

    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->allow(1)
            ->everySeconds(1)
            ->releaseAfterMinutes(1);
        return [$rateLimitedMiddleware];
    }


}
