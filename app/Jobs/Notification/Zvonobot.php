<?php

namespace App\Jobs\Notification;

use App\Models\Notification\ZvonobotHistory;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Spatie\RateLimitedMiddleware\RateLimited;

class Zvonobot implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;
    public $tries = 7;
    public $id;

    /**
     * Create a new job instance.
     */
    public function __construct($id,$data)
    {

        $this->data = $data;
        $this->id = $id;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $response = Http::withHeaders([
            "content-type" => "application/json",
            'accept' => 'application/json',
        ])->post('https://lk.zvonobot.ru/apiCalls/create',$this->data);

        $status = $response->json()['status'];
        $phone = $this->data['phone'];

        ZvonobotHistory::query()->insert([
            'project_id' => $this->id,
            'phone' => $phone,
            'status' => $status,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->allow(99)
            ->everySeconds(60)
            ->releaseAfterMinutes(rand(1,5));
        return [$rateLimitedMiddleware];
    }

}
