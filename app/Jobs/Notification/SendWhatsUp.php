<?php

namespace App\Jobs\Notification;

use App\Http\Controllers\Notification\Wa\WaMessageController;
use App\Traits\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\RateLimitedMiddleware\RateLimited;

class SendWhatsUp implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Notification;
    /**
     * Create a new job instance.
     */
    public $text;
    public $phone;
    public $project;



    public function __construct($text,$phone,$project)
    {
        $this->text = $text;
        $this->phone = $phone;
        $this->project = $project;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $WaMessageController = new WaMessageController();
        $WaMessageController->sendWaJob($this->text,$this->phone,$this->project);
    }

    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->allow(1)
            ->everySeconds(1)
            ->releaseAfterMinutes(1);
        return [$rateLimitedMiddleware];
    }
}
