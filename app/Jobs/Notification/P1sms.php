<?php

namespace App\Jobs\Notification;

use App\Traits\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\RateLimitedMiddleware\RateLimited;

class P1sms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    use Notification;

    public $id;
    public $date;
    public $tries = 3;

    /**
     * Create a new job instance.
     */
    public function __construct($date,$id)
    {
        $this->date = $date;
        $this->id = $id;

    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $this->sendP1smsJob($this->date,$this->id);
    }

    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->allow(99)
            ->everyMinutes(1)
            ->releaseAfterMinutes(rand(1,3));
        return [$rateLimitedMiddleware];
    }

}
