<?php

namespace App\Jobs\Yandex;

use App\Http\Controllers\Yandex\Metrika\YandexConversionController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\RateLimitedMiddleware\RateLimited;

class MetrikaOfflineConversonUpload implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public $id; public $phone; public $goal;

    public function __construct($id, $phone, $goal)
    {
       $this->id = $id;
       $this->phone = $phone;
       $this->goal = $goal;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $yandexConversionController = new YandexConversionController();
        $yandexConversionController->addConversionToGoal($this->id, $this->phone, $this->goal);
    }

    public function middleware()
    {
        $rateLimitedMiddleware = (new RateLimited())
            ->key($this->id)
            ->allow(100)
            ->everySeconds(60)
            ->releaseAfterSeconds(60);
        return [$rateLimitedMiddleware];
    }

}
