<?php

namespace App\Jobs\HW;

use App\Http\Controllers\HelloWorld\UpdateCostController;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class UpdateFacebookStat implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public int $id;
    public array $account;
    public string $startDate;

    public function __construct($id,$account,$startDate)

    {
        $this->id = $id;
        $this->account = $account;
        $this->startDate = $startDate;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        (new UpdateCostController())->updateAccountFacebookHw($this->id,$this->account,$this->startDate);
    }
}
