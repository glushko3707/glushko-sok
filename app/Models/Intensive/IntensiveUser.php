<?php

namespace App\Models\Intensive;

use App\Models\Telegram\LinkInvitePhone;
use App\Models\Telegram\UserChannel;
use App\Models\Webinar\LeadForWeb;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntensiveUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id', 'intensive_list_id', 'phone','telegram_username', 'telegram_id',
        'chat_main_active', 'chat_second_active',
        'meet_dateTime', 'payment_method',
        'feedback_1', 'feedback_2',
        'comment_actual', 'faculty_name', 'name',
        'score', 'lesson_1_visit',
        'lesson_2_visit', 'meet_visit',
        'homeWork_1', 'homeWork_2',
    ];

    public function getLeadsForWebs()
    {
        return $this->hasOne(LeadForWeb::class,'phone','phone')
            ->orderByRaw('IF(telegram_id IS NOT NULL AND telegram_id != "", 0, 1)') // Проверяем, что значение не NULL и не пустая строка
            ->latest();
    }

    public function getUserChannel()
    {
        return $this->hasMany(UserChannel::class,'telegram_id','telegram_id')
            ->orderByRaw('IF(telegram_id IS NOT NULL AND telegram_id != "", 0, 1)') // Проверяем, что значение не NULL и не пустая строка
            ->latest();

    }

    public function getLinkMainChat(){
        return $this->hasMany(LinkInvitePhone::class,'phone','phone')
            ->orderByRaw('IF(telegram_id IS NOT NULL AND telegram_id != "", 0, 1)') // Проверяем, что значение не NULL и не пустая строка
            ->latest();
    }
    public function getAnketa(){
        return $this->hasOne(GoogleForm::class,'phone','phone')->latest();
    }


}
