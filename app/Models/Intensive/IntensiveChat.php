<?php

namespace App\Models\Intensive;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntensiveChat extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id','chat_telegram_id','intensive_list_id','chat_name','chat_link'
    ];


    public function intensiveList()
    {
        return $this->belongsToMany(IntensiveList::class, 'intensive_list_id');
    }

    public function getIntensiveUser()
    {
        return $this->hasMany(IntensiveUser::class, 'intensive_list_id',);
    }

    public function getIntensiveUserChatName()
    {
        return $this->hasMany(IntensiveUser::class, 'faculty_name','chat_name');
    }


}
