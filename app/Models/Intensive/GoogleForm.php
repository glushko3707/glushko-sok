<?php

namespace App\Models\Intensive;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoogleForm extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone',
        'project_id',
        'date',
        'answers',
    ];

}

