<?php

namespace App\Models\Intensive;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IntensiveList extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','date_intensive','main_chat_id','salebot_tariff_id','link_zoom_meet_1','link_zoom_meet_2','link_invite',
        ];


    public function getIntensiveChats()
    {
        return $this->hasMany(IntensiveChat::class,'intensive_list_id');
    }

    public function getUsers()
    {
        return $this->hasMany(IntensiveUser::class,'intensive_list_id');
    }




}
