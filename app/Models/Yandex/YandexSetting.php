<?php

namespace App\Models\Yandex;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class YandexSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'access_token','expires_in','project_id','login'
    ];
}
