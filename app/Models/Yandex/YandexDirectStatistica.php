<?php

namespace App\Models\Yandex;

use App\Models\Lead\Ukolova;
use App\Models\Notification\TildaLead;
use App\Models\Payment;
use App\Models\Webinar\WebinarUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class YandexDirectStatistica extends Model
{
    use HasFactory;
    protected $fillable = [
        'Date',
        'CampaignName',
        'CampaignId',
        'AdGroupName',
        'AdGroupId',
        'AdId',
        'Clicks',
        'Impressions',
        'Cost','project_id',
        'AccountName',
    ];

    public function getLeads(): HasMany
    {
        return $this->hasMany(TildaLead::class,'utm_content','AdId');
    }

    public function getLeadsCampaign(): HasMany
    {
        return $this->hasMany(TildaLead::class,'utm_campaign','CampaignId');
    }


    public function getWebinarUser(): hasManyThrough
    {
        return $this->hasManyThrough(WebinarUser::class, TildaLead::class,
            'utm_content',
            'phone',
            'AdId',
            'phone');
    }

    public function getWebinarUserCampaign(): hasManyThrough
    {
        return $this->hasManyThrough(WebinarUser::class, TildaLead::class,
            'utm_campaign',
            'phone',
            'CampaignId',
            'phone');
    }




    public function getPayment(): hasManyThrough
    {
        return $this->hasManyThrough(Payment::class, Ukolova::class,
            'utm_content',
            'phone',
            'AdId',
            'phone');
    }
}

