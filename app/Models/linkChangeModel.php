<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class linkChangeModel extends Model
{
    use HasFactory;

    protected $fillable = [
        'utm_source', 'project_id', 'linkName', 'link',
    ];

}
