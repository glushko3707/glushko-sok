<?php

namespace App\Models\Analytica;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnalyticaSuccessDeal extends Model
{
    use HasFactory;
    protected $fillable = [
        'lead_id', 'project_id',  'name','date_creation','utm_source',
        'utm_medium','utm_campaign','utm_content','utm_term','reason_refusal',
        'date_first_lesson','date_first_lesson','language','responsible','address',
        'amount','More_information','passed_vu','passed_vu','Directions','country'
    ];
}
