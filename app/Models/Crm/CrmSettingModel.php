<?php

namespace App\Models\Crm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CrmSettingModel extends Model
{
    use HasFactory;

    protected $fillable = [
      'project_id',
      'crm_type','settings','statuses'
    ];
}
