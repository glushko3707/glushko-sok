<?php

namespace App\Models\HW;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HwDataAnalytica extends Model
{
    use HasFactory;
    protected $fillable = [
        'count_leads',
        'date',
        'utm_source',
        'utm_campaign',
        'utm_content',
        'utm_medium',
        'utm_term',
        'impressions',
        'clicks',
        'cost',
        'channel',
    ];
}
