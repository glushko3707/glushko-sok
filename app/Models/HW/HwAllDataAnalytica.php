<?php

namespace App\Models\HW;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HwAllDataAnalytica extends Model
{
    use HasFactory;
    protected $fillable = [
        'count_leads',
        'date_create',
        'utm_source',
        'utm_campaign',
        'utm_content',
        'utm_medium',
        'utm_term',
        'all_money',
        'new_money',
        'repeat_money',
        'write_lessons',
        'complete_lessons',
        'paid_trial',
        'paid_subscribe',
        'sales_subscribe',
        'control_payment',
        'impressions',
        'clicks',
        'cost',
    ];
}
