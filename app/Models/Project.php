<?php

namespace App\Models;

use App\Models\Intensive\IntensiveChat;
use App\Models\Intensive\IntensiveList;
use App\Models\Notification\NotificationSetting;
use App\Models\Service\Module;
use App\Models\Service\PermissionModule;
use App\Models\Sipout\ContactDatabase;
use App\Models\Sipout\SipOutSetting;
use App\Models\Sipout\TestPhone;
use App\Models\Telegram\TelegramSetting;
use App\Models\Waba\wabaSetting;
use App\Models\Waba\WabaTemplates;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Project extends Model
{
    use HasFactory;
    protected $fillable = [
        'name','active',
    ];

    public function getSipProject(): HasOne
    {
        return $this->hasOne(SipOutSetting::class,'project_id');
    }

    public function getTestPhone(): HasMany
    {
        return $this->hasMany(TestPhone::class,'project_id');
    }

    public function getNotificationSetting(): HasOne
    {
        return $this->hasOne(NotificationSetting::class,'project_id');
    }

    public function contactDatabases(): HasMany
    {
        return $this->hasMany(ContactDatabase::class,'project_id','id');
    }

    public function getPermissionModule(): HasMany
    {
        return $this->hasMany(PermissionModule::class,'project_id','id');
    }

    public function getPermissionModuleFull(): HasManyThrough
    {
        return $this->hasManyThrough(Module::class, PermissionModule::class,
            'project_id', // Foreign key on the cars table...
            'id', // Foreign key on the owners table...
            'id', // Local key on the mechanics table...
            'module_id'); // Local key on the cars table...
    }


    public function getSmsSetting(): HasOne
    {
        return $this->hasOne(SmsSetting::class,'project_id');
    }

    public function telegramSettingModel(): HasOne
    {
        return $this->hasOne(TelegramSetting::class,'project_id');
    }

    public function telegramConnect(): HasOne
    {
        return $this->hasOne(GlushkoTelegramConnect::class,'project_id');
    }

    public function getWabaSetting(): HasOne
    {
        return $this->hasOne(wabaSetting::class,'project_id');
    }

    public function getIntensiveList(): hasMany
    {
        return $this->hasMany(IntensiveList::class,'project_id');
    }

    public function getChatIntensiveList(): hasMany
    {
        return $this->hasMany(IntensiveChat::class,'project_id');
    }

    public function getWabaTemplates(): hasMany
    {
        return $this->hasMany(WabaTemplates::class,'project_id');
    }





}
