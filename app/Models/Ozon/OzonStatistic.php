<?php

namespace App\Models\Ozon;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OzonStatistic extends Model
{
    use HasFactory;

    protected $fillable = ['ad_id','impressions','title', 'date', 'clicks', 'moneySpent', 'avgBid', 'project_id'];
}
