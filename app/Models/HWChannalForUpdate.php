<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HWChannalForUpdate extends Model
{
    use HasFactory;
    protected $fillable = [
        'currency',
        'active',
        'utm_medium',
        'utm_source',
        'sheetName',
        'channel',
        'account_id'
    ];
}
