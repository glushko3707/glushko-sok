<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TelegramSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'sheet_id','project_id','salebot_api_key','chat_id','telegram_api_key','telegram_bot_name'
    ];
}
