<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LinkInvitePhone extends Model
{
    use HasFactory;
    protected $connection = 'mysql'; // Здесь указываем используемое соединение

    protected $fillable = [
        'project_id','phone','link_name','link','activate','telegram_id','telegram_username'
    ];
}
