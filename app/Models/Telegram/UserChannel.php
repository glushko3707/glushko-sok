<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserChannel extends Model
{
    protected $connection = 'telegram';

    protected $fillable = [
        'channel_id','project_id','link','active',
        'telegram_username','fullName','telegram_id','link_name',
    ];

    public function getLinkInvitePhone()
    {
        return $this->hasone(LinkInvitePhone::class,'link_name','link_name');
    }

    use HasFactory;
}
