<?php

namespace App\Models\Telegram;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalebotUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'salebot_id', 'name', 'project_id','phone', 'telegram_id','pipelineName'
    ];

}
