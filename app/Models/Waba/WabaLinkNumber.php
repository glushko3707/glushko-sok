<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WabaLinkNumber extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id', 'waba_intance_id', 'phone',
    ];

}
