<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class WabaIntance extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id', 'count_today','phone','name_space','waba_phone_id','active','owner_business_id','name_waba','waba_id'
    ];

    public function getTemplates(): HasMany
    {
        return $this->hasMany(WabaTemplates::class,'waba_intance_id');
    }

    public function getSettingWaba(): BelongsTo
    {
        return $this->belongsTo(wabaSetting::class,'project_id','project_id');
    }


}
