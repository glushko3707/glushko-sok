<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class wabaSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'welcomeWaba', 'project_id','welcome_template_name','token',
    ];
}
