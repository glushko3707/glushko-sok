<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WabaVariable extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id','type','waba_template_id','priority','value','count_variable',
    ];
}
