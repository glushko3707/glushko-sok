<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class WabaMessage extends Model
{
    use HasFactory;

    protected $fillable = [
      'project_id','callback','text','url_media','url_type'
    ];

    public function getButtons(): HasMany
    {
        return $this->HasMany(WabaButton::class,'waba_message_id');
    }
}
