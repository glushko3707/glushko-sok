<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WabaHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id', 'phone', 'type', 'message',   'status',  'w_id', "display_phone",
    ];

}
