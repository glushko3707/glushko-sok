<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WabaButton extends Model
{
    use HasFactory;

    protected $fillable = [
        'text','project_id','url','waba_message_id','callback'
    ];
}
