<?php

namespace App\Models\Waba;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WabaQueue extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id', 'dateSend', 'waba_intance_id', 'waba_templates_id', 'phone', 'variable'
    ];

}
