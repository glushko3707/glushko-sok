<?php

namespace App\Models\Waba;

use App\Models\Notification\WebSms;
use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WabaTemplates extends Model
{
    use HasFactory;

    protected $fillable = [
      'project_id', 'name', 'pipeline_step_id', 'url_media', 'url_type', 'waba_intance_id', 'waba_intance_id','code','reserve_sms_id','text'
    ];

    public function getWabaIntance()
    {
        return $this->hasOne(WabaIntance::class,'id','waba_intance_id');
    }

    public function getSetting()
    {
        return $this->hasOne(wabaSetting::class,'project_id','project_id');
    }

    public function getReserveSms()
    {
        return $this->hasOne(WebSms::class,'id','reserve_sms_id');
    }

    public function getProject()
    {
        return $this->hasOne(Project::class,'id','project_id');
    }

    public function getVariable(){
        return $this->hasMany(WabaVariable::class,'waba_template_id');

    }




}
