<?php

namespace App\Models\Facebook;

use App\Models\Lead\Ukolova;
use App\Models\Notification\TildaLead;
use App\Models\Payment;
use App\Models\Webinar\WebinarUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class FacebookStat extends Model
{
    use HasFactory;

    protected $fillable = [
'date', 'project_id', 'account_id', 'account_name', 'campaign_name', 'campaign_id', 'adset_id',
'ad_id',
'adset_name',
'ad_name',
'spend',
'impressions',
'clickAll',
'clickLink',
'lead',
'target_1_name',
'target_1_count',
'target_2_name',
'target_2_count',
    ];


    public function getLeadsUkolova(): HasMany
    {
        return $this->hasMany(Ukolova::class,'utm_content','ad_id');
    }

    public function getWebinarUserUkolova(): hasManyThrough
    {
        return $this->hasManyThrough(WebinarUser::class, Ukolova::class,
            'utm_content',
            'phone',
            'ad_id',
            'phone');
    }

    public function getLeads(): HasMany
    {
        return $this->hasMany(TildaLead::class,'utm_content','ad_id');
    }

    public function getWebinarUser(): hasManyThrough
    {
        return $this->hasManyThrough(WebinarUser::class, TildaLead::class,
            'utm_content',
            'phone',
            'ad_id',
            'phone');
    }


    public function getPayment(): hasManyThrough
    {
        return $this->hasManyThrough(Payment::class, Ukolova::class,
            'utm_content',
            'phone',
            'ad_id',
            'phone');
    }




}
