<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'service','project_id',
        'smsAero_key','smsAeroEmail','smsAeroSenderName',
        'p1smsApiKey','p1smsSender','welcomeSms',
    ];

}
