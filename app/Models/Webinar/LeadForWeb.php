<?php

namespace App\Models\Webinar;

use App\Models\Notification\TildaLead;
use App\Models\Notification\Variable;
use App\Models\Payment;
use App\Models\Telegram\UserChannel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

class LeadForWeb extends Model
{
    use HasFactory;
    protected $fillable = [
      'project_id', 'dateWeb', 'phone','updated_at', 'created_at',
      'utm_source','utm_medium', 'utm_campaign','utm_term','utm_content','attempt','date','user_name','telegram_id','telegram_username',
    ];

    public function getVariable(): HasMany
    {
        return $this->hasMany(Variable::class,'lead_for_web_id');
    }


    public function getTelegramUsers()
    {
        return $this->hasOne(UserChannel::class,'telegram_id','telegram_id');
    }

    public function getTildaLeads()
    {
        return $this->hasOne(TildaLead::class,'phone','phone');
    }

    public function getPayments()
    {
        return $this->hasOne(Payment::class,'phone','phone');
    }

    public function getWebinarUser()
    {
        return $this->hasOne(WebinarUser::class,'phone','phone');
    }





}
