<?php

namespace App\Models\Webinar;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebinarUser extends Model
{
    use HasFactory;
    protected $fillable = [
        'date','project_id','updated_at', 'created_at',
        'phone','username','utm_source' , 'utm_campaign', 'ssId', 'amoDealId',
        'buttons', 'banners', 'timeWeb','warm', 'utm_medium', 'utm_content'
    ];
}
