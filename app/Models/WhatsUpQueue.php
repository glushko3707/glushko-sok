<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WhatsUpQueue extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','phone','instance_id','text','time',
    ];
}
