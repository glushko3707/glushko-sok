<?php

namespace App\Models\Notification\Wa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebWaNumber extends Model
{
    use HasFactory;
    protected $fillable = [
        'instance_id','project_id','active','count','instance_phone'
    ];
}
