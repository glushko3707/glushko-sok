<?php

namespace App\Models\Notification\Wa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebWa extends Model
{
    use HasFactory;
    protected $fillable = [
      'button','urlButton',
      'text','footer','footer','type','project_id'
    ];
}
