<?php

namespace App\Models\Notification\Wa;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WaSendMessage extends Model
{
    use HasFactory;

    protected $fillable = [
      'phone','instance_id','project_id'
    ];


}
