<?php

namespace App\Models\Notification;

use App\Models\LeadCrm\LeadCrm;
use App\Models\Payment;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewLeadWeb extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone','project_id','addWeb',
        'utm_source','utm_medium',
        'created_at','updated_at','date'
    ];

    public function getCrm()
    {
        return $this->hasOne(LeadCrm::class,'new_lead_web_id','id');
    }



}
