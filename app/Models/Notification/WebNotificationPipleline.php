<?php

namespace App\Models\Notification;

use App\Models\Notification\Wa\WebWa;
use App\Models\Waba\WabaTemplates;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebNotificationPipleline extends Model
{
    use HasFactory;
    protected $fillable = [
      'project_id','content_id','content_type','priority','timeSend',
      'active','type',
    ];

    public function getSms()
    {
        return $this->hasOne(WebSms::class,'id','content_id');
    }

    public function getCall()
    {
        return $this->hasOne(WebCall::class,'id','content_id');
    }

    public function getWhatsUp()
    {
        return $this->hasOne(WebWa::class,'id','content_id');
    }

    public function getWabaTemplate()
    {
        return $this->hasMany(WabaTemplates::class,'name','content_id');
    }

}
