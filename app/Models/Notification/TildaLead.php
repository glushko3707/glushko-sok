<?php

namespace App\Models\Notification;

use App\Models\Payment;
use App\Models\Webinar\WebinarUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class TildaLead extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone', 'name', 'project_id','email','value_1',
        'utm_source', 'utm_medium', 'utm_campaign', 'utm_content', 'utm_term',
        'updated_at','created_at',
        '_ym_uid','_fbp','_fbc','_yclid','url'
    ];

    public function getWebinarUser(): hasOne
    {
        return $this->hasOne(WebinarUser::class,
            'phone',
            'phone');
    }

    public function getPayments()
    {
        return $this->hasMany(Payment::class,'phone','phone');
    }

}
