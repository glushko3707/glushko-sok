<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    use HasFactory;
    protected $fillable = [
        'variable_name', 'variable_value', 'project_id', 'lead_for_web_id',
    ];

}
