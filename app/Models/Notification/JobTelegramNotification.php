<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobTelegramNotification extends Model
{
    use HasFactory;
    public $guarded = [];

}
