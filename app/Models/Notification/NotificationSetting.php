<?php

namespace App\Models\Notification;

use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class NotificationSetting extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id','welcomeSms','dateWeb',
        'autoWeb','smsAero_key','smsAeroEmail','bizon_key',
        'smsAeroSenderName','wapico_key','zvonobotPhoneOutput','welcomeWa','welcomeWaba',
    ];

    public function getNotificationSetting(): BelongsTo
    {
        return $this->belongsTo(Project::class,'id','project_id');
    }

}
