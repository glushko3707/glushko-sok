<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebSms extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','text','type','timeSend',
    ];
}
