<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsAeroHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id', 'phone',
        'cost','status',
        'created_at','updated_at','text'
    ];
}
