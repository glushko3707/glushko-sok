<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebCall extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id','campaign_id','timeSend',
    ];

}
