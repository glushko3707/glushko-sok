<?php

namespace App\Models\Notification;

use App\Models\Webinar\LeadForWeb;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebNotificationSchedule extends Model
{
    use HasFactory;
    public $guarded = [];


    public function getPipelineStep()
    {
        return $this->hasOne(WebNotificationPipleline::class,'id','step_id');
    }

    public function getLeads()
    {
        return $this->hasMany(LeadForWeb::class,'phone','phone');
    }

}
