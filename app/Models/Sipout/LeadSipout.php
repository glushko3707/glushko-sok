<?php

namespace App\Models\Sipout;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadSipout extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','phone','utm_source','utm_medium',
        'created_at', 'updated_at',
    ];
}
