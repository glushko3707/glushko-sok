<?php

namespace App\Models\Sipout;

use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ContactDatabase extends Model
{
    use HasFactory;

    protected $fillable = [
        'phone','project_id','created_at','updated_at','category_databases_id',
    ];

    public function project(): BelongsTo
    {
        return $this->belongsTo(Project::class,'id','project_id');
    }

}
