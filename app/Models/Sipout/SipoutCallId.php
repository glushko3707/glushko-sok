<?php

namespace App\Models\Sipout;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SipoutCallId extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','date','phone','callId','money','timePhone','comment','spamCef','timeVoice',
    ];
}
