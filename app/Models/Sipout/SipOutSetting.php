<?php

namespace App\Models\Sipout;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SipOutSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id','api_key','sheetId','timeDifferent',
    ];
}

