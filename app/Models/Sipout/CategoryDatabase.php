<?php

namespace App\Models\Sipout;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryDatabase extends Model
{
    use HasFactory;

    protected $fillable = [
     'project_id','category_name','count','used_count',
    ];

}
