<?php

namespace App\Models;

use App\Models\Lead\Ukolova;
use App\Models\Notification\NewLeadWeb;
use App\Models\Notification\TildaLead;
use App\Models\Webinar\LeadForWeb;
use App\Models\Webinar\WebinarUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        'project_id', 'phone', 'orderId', 'status','sum','product','date', 'updated_at', 'created_at',
        'telegram_platform', 'platform_id','lead_id','lead_date'
    ];


    public function webinarUsers()
    {
        return $this->hasMany(WebinarUser::class,'phone','phone');
    }

    public function getUkolovaLead()
    {
        return $this->hasMany(Ukolova::class,'phone','phone');
    }

    public function getLeadForWeb()
    {
        return $this->hasMany(LeadForWeb::class,'phone','phone');
    }

    public function getLead()
    {
        return $this->hasMany(NewLeadWeb::class,'phone','phone');
    }

    public function getTildaLead()
    {
        return $this->hasMany(TildaLead::class,'phone','phone');
    }

}
