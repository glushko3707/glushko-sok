<?php

namespace App\Models\Lead;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
    use HasFactory;

    protected $fillable = [
      'phone','project_id','name','telegram_id','email',
        'utm_source','utm_medium','utm_campaign','utm_term','utm_content',
    ];
}
