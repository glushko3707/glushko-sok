<?php

namespace App\Models\Lead;

use App\Models\Facebook\FacebookStat;
use App\Models\Payment;
use App\Models\Webinar\WebinarUser;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Ukolova extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'phone', 'web_name', 'user_name', 'dateWeb', 'timeWeb', 'project_id','country',
        '_fbp', '_fbc', 'turnover', 'role',
        'utm_source', 'utm_medium', 'utm_content', 'utm_campaign', 'utm_term',
    ];


    public function getPayments(): HasMany
    {
        return $this->hasMany(Payment::class,'phone','phone');
    }

    public function getVistitorWebinar(): HasMany
    {
        return $this->hasMany(WebinarUser::class,'phone','phone');
    }


}
