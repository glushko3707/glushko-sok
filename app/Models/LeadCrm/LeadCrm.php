<?php

namespace App\Models\LeadCrm;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LeadCrm extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id','comment','dateTask','manager','new_lead_web_id',
        'status','target','task','tryCall','user_name'
    ];

    protected $primaryKey = 'new_lead_web_id';

}
