<?php

namespace App\Models\Smartsender;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class SmartsenderLeads extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id', 'ssId', 'utm_source', 'fullName',
        'utm_source', 'utm_medium', 'telegram_username','active',
    ];

}
