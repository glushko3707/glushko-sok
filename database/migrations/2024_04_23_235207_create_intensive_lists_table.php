<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('intensive_lists', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->date('date_intensive');
            $table->string('main_chat_id');
            $table->string('salebot_tariff_id')->nullable();
            $table->string('link_zoom_meet_1')->nullable();
            $table->string('link_zoom_meet_2')->nullable();
            $table->string('link_invite')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('intensive_lists');
    }
};
