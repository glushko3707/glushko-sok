<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sms_settings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->string('service')->nullable();
            $table->string('smsAero_key')->nullable();
            $table->string('smsAeroEmail')->nullable();
            $table->string('smsAeroSenderName')->nullable();
            $table->string('p1smsApiKey')->nullable();
            $table->string('p1smsSender')->nullable();
            $table->boolean('welcomeSms')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sms_settings');
    }
};
