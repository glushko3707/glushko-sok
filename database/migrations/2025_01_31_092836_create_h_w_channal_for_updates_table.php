<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('h_w_channal_for_updates', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('currency', 4);
            $table->boolean('active');
            $table->string('utm_medium');
            $table->string('utm_source');
            $table->string('sheetName');
            $table->string('channel');
            $table->string('account_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('h_w_channal_for_updates');
    }
};
