<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('webinar_users', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('project_id');
            $table->date('date');
            $table->string('phone')->nullable();
            $table->string('username')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('ssId')->nullable();
            $table->string('amoDealId')->nullable();
            $table->string('buttons')->nullable();
            $table->string('banners')->nullable();
            $table->integer('timeWeb')->nullable();
            $table->string('warm')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('webinar_users');
    }
};
