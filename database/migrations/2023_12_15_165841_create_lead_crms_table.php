<?php

use App\Models\Notification\NewLeadWeb;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('lead_crms', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('project_id');
            $table->foreignIdFor(NewLeadWeb::class)->constrained()->cascadeOnDelete();
            $table->string('comment')->nullable();;
            $table->string('task')->nullable();;
            $table->date('dateTask')->nullable();;
            $table->string('status')->default('Новый лид');;
            $table->boolean('target')->default(1);
            $table->string('manager')->nullable();
            $table->integer('tryCall')->default(0);
            $table->string('user_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('lead_crms');
    }
};
