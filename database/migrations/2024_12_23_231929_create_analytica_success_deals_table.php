<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('analytica_success_deals', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->string('lead_id')->nullable();
            $table->string('name')->nullable();
            $table->dateTime('date_creation')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_medium')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('utm_content')->nullable();
            $table->string('utm_term')->nullable();
            $table->string('reason_refusal')->nullable();
            $table->dateTime('date_first_lesson')->nullable();
            $table->string('language')->nullable();
            $table->string('responsible')->nullable();
            $table->string('address')->nullable();
            $table->string('amount')->nullable();
            $table->string('More_information')->nullable();
            $table->string('passed_vu')->nullable();
            $table->string('Directions')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('analytica_success_deals');
    }
};
