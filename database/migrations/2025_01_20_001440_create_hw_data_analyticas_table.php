<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hw_data_analyticas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->string('utm_source',100)->nullable();
            $table->string('utm_campaign',100)->nullable();
            $table->string('utm_content',100)->nullable();
            $table->string('utm_medium',100)->nullable();
            $table->string('utm_term')->nullable();
            $table->string('date')->nullable();
            $table->decimal('cost', 10, 2)->default(0);
            $table->string('channel',100)->nullable();
            $table->integer('count_leads')->default(0);
            $table->integer('impressions')->default(0);
            $table->integer('clicks')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hw_data_analyticas');
    }
};
