<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ozon_statistics', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->string('ad_id');
            $table->string('title')->nullable();
            $table->string('date');
            $table->string('clicks');
            $table->decimal('moneySpent',10, 2)->nullable();
            $table->decimal('avgBid', 10, 2)->nullable();
            $table->integer('impressions')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ozon_statistics');
    }
};
