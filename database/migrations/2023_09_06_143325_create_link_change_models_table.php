<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('link_change_models', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('utm_source')->nullable();
            $table->integer('project_id');
            $table->string('linkName');
            $table->string('link');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('link_change_models');
    }
};
