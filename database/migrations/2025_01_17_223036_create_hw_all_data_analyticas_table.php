<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('hw_all_data_analyticas', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('date_create')->nullable();
            $table->string('utm_source')->nullable();
            $table->string('utm_campaign')->nullable();
            $table->string('utm_content')->nullable();
            $table->string('utm_medium')->nullable();
            $table->string('date')->nullable();
            $table->integer('count_leads')->default(0);
            $table->decimal('all_money', 10, 2)->default(0);
            $table->decimal('new_money', 10, 2)->default(0);
            $table->decimal('repeat_money', 10, 2)->default(0);
            $table->integer('write_lessons')->default(0);
            $table->integer('complete_lessons')->default(0);
            $table->integer('paid_trial')->default(0);
            $table->integer('paid_subscribe')->default(0);
            $table->integer('sales_subscribe')->default(0);
            $table->integer('control_payment')->default(0);
            $table->integer('impressions')->default(0);
            $table->integer('clicks')->default(0);
            $table->decimal('cost', 10, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('hw_all_data_analyticas');
    }
};
