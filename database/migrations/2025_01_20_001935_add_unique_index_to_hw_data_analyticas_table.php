<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('hw_data_analyticas', function (Blueprint $table) {
            $table->unique(
                [
                    'project_id',
                    'date',
                    'utm_source',
                    'utm_medium',
                    'utm_campaign',
                    'utm_content',
                    'channel'
                ],
                'unique_hw_analytica_data'
            )->lengths([null, null, 100, 100, 100, 100, 100]); // Ограничение длины индекса
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('hw_data_analyticas', function (Blueprint $table) {
            //
        });
    }
};
