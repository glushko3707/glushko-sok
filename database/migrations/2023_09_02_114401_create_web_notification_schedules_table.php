<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('web_notification_schedules', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->time('timeSend');
            $table->integer('lead_id')->nullable();
            $table->integer('project_id');
            $table->integer('step_id');
            $table->string('phone');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('web_notification_schedules');
    }
};
