<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('facebook_stats', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->string('date');
            $table->string('account_id')->nullable();
            $table->string('account_name')->nullable();
            $table->string('campaign_id')->nullable();
            $table->string('campaign_name')->nullable();
            $table->string('adset_id')->nullable();
            $table->string('ad_id')->nullable();
            $table->string('adset_name')->nullable();
            $table->string('ad_name')->nullable();
            $table->decimal('spend', 8, 2)->default(0);
            $table->integer('impressions')->default(0);
            $table->integer('clickAll')->default(0);
            $table->integer('clickLink')->default(0);
            $table->integer('lead')->default(0);
            $table->string('target_1_name')->nullable();
            $table->integer('target_1_count')->default(0);
            $table->string('target_2_name')->nullable();
            $table->integer('target_2_count')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('facebook_stats');
    }
};
