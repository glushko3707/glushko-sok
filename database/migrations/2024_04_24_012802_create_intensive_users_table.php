<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('intensive_users', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->foreignIdFor(\App\Models\Project::class)->constrained()->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\Intensive\IntensiveList::class)->constrained()->cascadeOnDelete();
            $table->integer('intensive_chat_id')->nullable();
            $table->dateTime('meet_dateTime')->nullable();
            $table->string('name')->nullable();
            $table->string('telegram_username')->nullable();
            $table->string('telegram_id')->nullable();
            $table->string('phone')->nullable();
            $table->boolean('chat_main_active')->default(0);
            $table->boolean('chat_second_active')->default(0);
            $table->string('payment_method')->nullable();
            $table->string('feedback_1')->nullable();
            $table->string('feedback_2')->nullable();
            $table->string('comment_actual')->nullable();
            $table->string('faculty_name')->nullable();
            $table->integer('score')->nullable();
            $table->boolean('lesson_1_visit')->default(0);
            $table->boolean('lesson_2_visit')->default(0);
            $table->boolean('meet_visit')->default(0);
            $table->boolean('homeWork_1')->default(0);
            $table->boolean('homeWork_2')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('intensive_users');
    }
};
