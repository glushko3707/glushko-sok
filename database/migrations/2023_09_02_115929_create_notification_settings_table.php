<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('notification_settings', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->boolean('welcomeSms')->default(false);
            $table->integer('project_id');
            $table->date('dateWeb')->nullable();
            $table->boolean('autoWeb')->nullable();
            $table->string('smsAero_key')->nullable();
            $table->string('smsAeroEmail')->nullable();
            $table->string('smsAeroSenderName')->nullable();
            $table->string('wapico_key')->nullable();
            $table->string('zvonobot_key')->nullable();
            $table->string('zvonobotPhoneOutput')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('notification_settings');
    }
};
