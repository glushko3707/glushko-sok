<?php

use App\Http\Controllers\Auth\TelegramAuthController;
use App\Http\Controllers\Crm\Amo\AmoController;
use App\Http\Controllers\HelloWorld\ReportController;
use App\Http\Controllers\HelloWorld\UpdateCostController;
use App\Http\Controllers\KassaController;
use App\Http\Controllers\Notification\TestController;
use App\Http\Controllers\Payment\PaymentController;
use App\Http\Controllers\ProfileController;
use App\Livewire\Counter;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/projects', [ProfileController::class, 'project'])->name('projects');
});

Route::get('/installment/{sum}', [PaymentController::class,'installmentIndex',])->name('installmentIndex.index');
Route::get('/payment/tbank/{id}', [PaymentController::class,'tbankIndex',])->name('tbank.index');
Route::get('/payment/tbank/store/{id}', [PaymentController::class,'tbankStore',])->name('tbank.store');

Route::get('/payment/{id}', [PaymentController::class,'view',])->name('paymentController.index');
Route::post('/payment/{id}/store', [PaymentController::class,'paymentControllerStore',])->name('paymentControllerStore');
Route::delete('/payment/{id}/delete', [PaymentController::class,'paymentControllerDelete',])->name('paymentControllerDelete');
Route::patch('/payment/{id}/update', [PaymentController::class,'paymentControllerUpdate',])->name('paymentControllerUpdate');

Route::get('/payment/add/{id}', [PaymentController::class,'viewAdd',])->name('viewAdd.index');
Route::post('/payment/add/{id}/store', [PaymentController::class,'paymentAddControllerStore',])->name('paymentAddControllerStore');

// удалить
Route::get('/ukolova/getrl', [\App\Http\Controllers\Tilda\Ukolova::class,'getLeads',]);

Route::match(['get', 'post'],'/ukolova/report/leads', [App\Http\Controllers\Analytyca\UkolovaPowerBi::class, 'index']);


Route::get('/prodamus/response/{id}', [\App\Http\Controllers\Payment\ProdamusController::class,'response',])->name('response.store');

Route::match(['get', 'post'],'/tilda/payment/form/{id}', [\App\Http\Controllers\Payment\ProdamusController::class,'form']);
Route::match(['get', 'post'],'/api/tilda/payment/form/store/{id}', [\App\Http\Controllers\Payment\ProdamusController::class,'formStore'])->name('formStore');

require __DIR__.'/auth.php';

Route::get('register/telegram', [TelegramAuthController::class, 'storeTelegram']);

Route::get('/payment', [ProfileController::class, 'paymentService'])->name('paymentService');


Route::get('/counter', Counter::class);

Route::get('/diagnostic',
    function () {
        return redirect(\route('calculator.diagnostic'));
    }
);


Route::get('/calculator/diagnostic', [\App\Http\Controllers\Service\CalculatorController::class,'diagnostic',])->name('calculator.diagnostic');
Route::get('/calculator/webinar', [\App\Http\Controllers\Service\CalculatorController::class,'webinar',])->name('calculator.webinar');
Route::get('/calculator/threeDay', [\App\Http\Controllers\Service\CalculatorController::class,'threeDay',])->name('calculator.threeDay');


Route::get('/connect/yKassa/{sum}/{id}', [KassaController::class,'connect',]);
Route::get('yKassa/tilda/request/{id}', [KassaController::class,'tildaRequest',])->name('tildaRequest');
Route::get('/kurs/{sum}/{id}', [KassaController::class,'connectKurs',]);

Route::post('/api/yKassa/{id}/{sum}', [KassaController::class,'index',]);

Route::post('/api/yKassa/notifications', [KassaController::class,'notifications',]);
Route::post('/api/tbank/notifications/{id}', [KassaController::class,'tbankNotifications',])->name('tbankNotifications');


Route::match(['get', 'post'],'api/connect/yKassa/{id}/store', [KassaController::class,'connectStore',])->name('connectStore');
Route::post('api/kurs/yKassa/{id}/store', [KassaController::class,'connectStoreKurs',])->name('connectStoreKurs');

// платежи воронка



Route::get('/api/yandex/oauth/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'oauth',])->name('oauthYandex.index');
Route::get('/api/yandex/oauth/redirect/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'oauthRedirect',]);
Route::get('/api/yandex/oauth/redirect/handle/store/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'oauthRedirectHandle',])->name('oauthRedirectHandle');

Route::get('/api/yandex/segment/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'segment',]);
Route::get('/api/yandex/segment/add/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'segmentAdd',]);

Route::get('/api/yandex/getCampaignIds/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'getCampaignIds',]);
Route::get('/api/yandex/getReport/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'getReport',]);
Route::get('/yandex/getReportCSV/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'getReportCSVIndex',])->name('getReportCSV.index');
Route::post('/api/yandex/getReportCSV/store/{id}', [\App\Http\Controllers\Yandex\Direct\DirectController::class,'getReportCSVStore',])->name('getReportCSV.store');

Route::get('/yandex/reaching/{id}', [\App\Http\Controllers\Notification\TrafficController::class,'trafficAnalyticaYandex',])->name('trafficAnalyticaYandex.index');

Route::get('/yandex/conversion/add/{id}/{phone}/{goals}', [\App\Http\Controllers\Yandex\Metrika\YandexConversionController::class, 'addConversionToGoal'])->name('addConvertionToGoal.index');


Route::match(['get', 'post'],'/api/saleBot/lead/phone/{id}', [\App\Http\Controllers\saleBot\SalebotLeadController::class,'phoneWebinarLead']);
Route::match(['get', 'post'],'/api/tilda/ukolova/lead/{id}', [\App\Http\Controllers\Tilda\TildaController::class,'LeadTildaAnalyticUkolova']);
Route::match(['get', 'post'],'/api/tilda/traffic/lead/{id}', [\App\Http\Controllers\Tilda\TildaController::class,'LeadTildaAnalytic']);
Route::match(['get', 'post'],'/api/getcource/payment/{id}', [\App\Http\Controllers\Getcource\PaymentController::class,'NewPaymentGetcource']);

Route::match(['get', 'post'],'/api/tilda/payment/form/{id}', [\App\Http\Controllers\Getcource\PaymentController::class,'NewPaymentTilda'])->name('NewPaymentTilda');
Route::match(['get', 'post'],'/api/tilda/payment/{id}', [\App\Http\Controllers\Getcource\PaymentController::class,'NewPaymentTilda']);


// интенсив

// ссылка для пользователей
Route::get('/kupr/user/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveUserController::class,'userIndex'])->name('userIndex');

Route::get('/kupr/setIntensiveNotification/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveUserController::class,'setIntensiveNotification'])->name('setIntensiveNotification');


Route::get('/int/kupr/admin/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveUserController::class,'adminIndex'])->name('IntensiveAdminIndex');
Route::get('/intensive/kupr/admin/delete/user/{user_intensive_id}', [\App\Http\Controllers\Intensive\IntensiveUserController::class,'deleteUserStore'])->name('deleteUserStore');

Route::get('/int/kupr/update/admin/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveUserController::class,'adminIndexUpdate'])->name('adminIndexUpdate');
Route::get('/int/kupr/distributedByFaculty/store/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveUserController::class,'distributedByFaculty'])->name('distributedByFaculty');

Route::get('/intensive/list/{id}', [\App\Http\Controllers\Intensive\ListController::class,'listIndex'])->name('IntensiveListIndex');
Route::get('/intensive/list/setting/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\ListController::class,'listSettingIndex'])->name('listSettingIndex');

Route::get('/intensive/users/add/store/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveUserController::class,'addUserStore'])->name('addUserStore');


Route::get('/intensive/chat/create/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveCreateController::class,'IntensiveChatCreatesIndex'])->name('IntensiveChatCreatesIndex');
Route::post('/intensive/chat/create/store/{intensive_id}/{id}', [\App\Http\Controllers\Intensive\IntensiveCreateController::class,'IntensiveChatCreatesStore'])->name('IntensiveChatCreatesStore');

Route::get('/intensive/setting/{id}', [\App\Http\Controllers\Intensive\IntensiveSettingController::class,'IntensiveSettingIndex'])->name('IntensiveSettingIndex');
Route::get('/intensive/create/{id}', [\App\Http\Controllers\Intensive\IntensiveCreateController::class,'IntensiveCreatesIndex'])->name('IntensiveCreatesIndex');
Route::post('/intensive/create/store/{id}', [\App\Http\Controllers\Intensive\IntensiveCreateController::class,'IntensiveCreatesStore'])->name('IntensiveCreatesStore');

Route::match(['get', 'post'],'/api/intensive/googleForm/webhook/{id}', [\App\Http\Controllers\Intensive\IntensiveSettingController::class,'GoogleFormWebhook'])->name('GoogleFormWebhook');

//Route::get('/intensive/setting/{id}', [\App\Http\Controllers\Intensive\IntensiveSettingController::class,'IntensiveSettingIndex']);


// уколова трафик

Route::get('/target/reaching/{id}', [\App\Http\Controllers\Notification\TrafficController::class,'trafficAnalyticaTarget',])->name('trafficAnalyticaTarget.index');
Route::get('/target/noadmin/reaching/{id}', [\App\Http\Controllers\Notification\TrafficController::class,'trafficAnalyticaTargetNoAdmin',])->name('trafficAnalyticaTargetNoAdmin.index');
Route::get('/target/leads/ukolova/{id}', [\App\Http\Controllers\Notification\TrafficController::class,'targetLeads',])->name('targetLeadsUkolova.index');
Route::get('/target/statistic/update/store/{id}', [\App\Http\Controllers\Notification\TrafficController::class,'targetUpdateStatStore',])->name('targetUpdateStat.store');

Route::get('/facebook/admin/add/{id}/{businessId}/{access_token}', [\App\Http\Controllers\Facebook\FbAdminController::class,'addAdminToBm',])->name('addAdminToBm.index');
Route::get('/facebook/admin/add/account/{id}', [\App\Http\Controllers\Facebook\FbAdminController::class,'addAccountAdFacebookIndex',])->name('addAccountAdFacebook.index');
Route::get('/facebook/admin/delete/account/{id}', [\App\Http\Controllers\Facebook\FbAdminController::class,'deleteAccountAdFacebook',])->name('deleteAccountAdFacebook.index');
Route::post('/api/facebook/admin/add/account/store/{id}', [\App\Http\Controllers\Facebook\FbAdminController::class,'addAccountAdFacebookStore',])->name('addAccountAdFacebook.store');
Route::post('/api/facebook/admin/delete/account/store/{id}', [\App\Http\Controllers\Facebook\FbAdminController::class,'deleteAccountAdFacebookStore',])->name('deleteAccountAdFacebook.delete');


Route::get('/target/leads/{id}', [\App\Http\Controllers\Notification\TrafficController::class,'targetLeads',])->name('targetLeads.index');

Route::get('/target/setting/{id}', [\App\Http\Controllers\Facebook\SettingController::class,'index',])->name('SettingController.index');
Route::post('/target/setting/store/{id}', [\App\Http\Controllers\Facebook\SettingController::class,'storeSettingFacebook',])->name('storeSettingFacebook.store');

Route::get('/sipout/document/{id}', [\App\Http\Controllers\Service\PDFOverlayController::class,'indexOverlayPngOnPdfs',])->name('sipoutDocument.index');
Route::match(['get', 'post'],'/sipout/document/store/{id}', [\App\Http\Controllers\Service\PDFOverlayController::class,'overlayPngOnPdfs',])->name('sipoutDocument.store');
Route::match(['get', 'post'],'/sipout/document/get/{id}', [\App\Http\Controllers\Service\PDFOverlayController::class,'getPDFFile',])->name('getPDFFile.store');

//Route::match(['get', 'post'],'/user/info/get/{id}', [\App\Http\Controllers\Service\PDFOverlayController::class,'getPDFFile',])->name('getPDFFile.store');

// crm интеграции

Route::get('/AmoCrm/redirection/{id}', [AmoController::class, 'oAuthIndex'])->name('AmooAuth.index'); // добавление проекта запрос
Route::get('/AmoCrm/test/{id}', [AmoController::class, 'findContactFromPhone'])->name('oAuthTest.index'); // добавление проекта запрос
Route::get('/AmoCrm/pipeline/setting/{id}', [AmoController::class, 'pipelineSetting'])->name('pipelineSetting.index'); // добавление проекта запрос


Route::match(['get', 'post'],'/newLead', [\App\Http\Controllers\Analytyca\HwAddLead::class, 'analyticsNewLead'])->prefix('api/analytics');
Route::match(['get', 'post'],'/target_1', [\App\Http\Controllers\Analytyca\HwAddLead::class, 'target_1'])->prefix('api/analytics');
Route::match(['get', 'post'],'/target_2', [\App\Http\Controllers\Analytyca\HwAddLead::class, 'target_2'])->prefix('api/analytics');
Route::match(['get', 'post'],'/successDeal', [\App\Http\Controllers\Analytyca\HwAddLead::class, 'successDeal'])->prefix('api/analytics');
Route::match(['get', 'post'],'/unfulfilled', [\App\Http\Controllers\Analytyca\HwAddLead::class, 'unfulfilled'])->prefix('api/analytics');

Route::match(['get', 'post'],'/hw/updateStat', [UpdateCostController::class, 'hwUpdateCostMy']);
Route::match(['get', 'post'],'/hw/allStat/get', [UpdateCostController::class, 'getAllStatFromGoogleSheet']);


Route::match(['get', 'post'],'/api/getWebhooked',function (){return 200;});

Route::match(['get', 'post'],'/ozon/getCompanies', [\App\Http\Controllers\Ozon\OzonController::class, 'getCompanies']);
Route::match(['get', 'post'],'/ozon/getStatistics', [\App\Http\Controllers\Ozon\OzonController::class, 'getStatistics']);
Route::match(['get', 'post'],'/ozon/getReport', [\App\Http\Controllers\Ozon\OzonController::class, 'updateStatisticaHw']);

