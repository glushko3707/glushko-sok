<?php

use App\Http\Controllers\Notification\NotificationWebPhoneInsert;
use App\Http\Controllers\Notification\PiplelineController;
use App\Http\Controllers\Notification\ViewController;
use App\Http\Controllers\Notification\Wa\WaViewController;
use App\Http\Controllers\PaymentServiceController;
use App\Http\Controllers\Webinar\Bizon\BizonController;
use App\Http\Controllers\Webinar\LeadStatController;
use Illuminate\Support\Facades\Route;



Route::get('/api/newAll/{id}', [\App\Http\Controllers\Notification\LeadController::class,'newLead',])->name('newAll.index');




Route::group(['middleware' => ['auth']], function () {
    Route::get('/webinar/notification/{project_id}', [ViewController::class, 'pipelineNotificztion',])->name('pipelineNotificztion');
    Route::get('/webinar/createNotification/{project_id}', [ViewController::class, 'createNotification',])->name('createNotification.index');
    Route::post('/webinar/createNotification/store/{project_id}', [ViewController::class, 'createNotificationStore',])->name('createNotification.store');

    // запланированные уведомления
    Route::get('/webinar/planned/{project_id}', [ViewController::class, 'plannedNotificztion',])->name('plannedNotificztion');
    Route::delete('/webinar/planned/delete/{id}', [ViewController::class, 'plannedNotificztionDelete',])->name('plannedNotificztion.delete');
    Route::match(['get', 'post'],'/webinar/planned/action/{id}', [ViewController::class, 'plannedNotificztionAction',])->name('plannedNotificztion.action');
    Route::post('/webinar/changeDateWeb/{id}', [NotificationWebPhoneInsert::class, 'changeDateWeb',])->name('changeDateWeb');
//    Route::get('/webinar/planned/save/{id}', [ViewController::class, 'saveFile',])->name('saveFile.store');
    Route::get('/webinar/save/{project_id}', [ViewController::class, 'downloadShedule',])->name('downloadShedule');
    Route::get('/webinar/addWebLead/{project_id}', [ViewController::class, 'addWebLead',])->name('addWebLead');

    Route::get('/notification/insert/{id}', [NotificationWebPhoneInsert::class,'insertForm',])->name('addNotificationWeb.index');
    Route::post('/api/notification/insert/{id}/store', [NotificationWebPhoneInsert::class,'insertFormStore',])->name('notificationWeb');
    Route::post('/notification/insertNewLead/{id}/store', [NotificationWebPhoneInsert::class,'insertNewLeadStore',])->name('insertNewLeadStore');

    Route::get('/telegram/project/get', [PiplelineController::class,'getProject',])->name('getProjectTelegram.store');


    // настройки вебинарной воронки

    Route::get('/web/service/setting/{id}', [ViewController::class,'serviceSetting',])->name('serviceSetting.index');
    Route::post('/web/service/setting/store/{id}', [ViewController::class,'serviceSettingStore',])->name('serviceSetting.store');


    Route::post('/webinar/setting/autoWebActive/store/{id}', [\App\Http\Controllers\Notification\WebinarSettingController::class,'autoWebActive',])->name('autoWebActive.store');
    Route::get('/webinar/setting/zvonobotKey/store/{id}', [\App\Http\Controllers\Notification\WebinarSettingController::class,'zvonobotKey',])->name('zvonobotKey.store');

    // Звонобот
    Route::post('/webinar/zvonobot/zvonobotPhoneOutput/store/{id}', [\App\Http\Controllers\Notification\WebinarSettingController::class,'zvonobotPhoneOutput',])->name('zvonobotPhoneOutput.store');
    Route::post('/webinar/zvonobot/zvonobotPhoneOutput/test/store/{id}', [\App\Http\Controllers\Notification\WebinarSettingController::class,'zvonobotPhoneOutputTest',])->name('zvonobotPhoneOutputTest.store');


    Route::get('/sipOut/service/setting/{id}', [ViewController::class,'sipOutServiceSetting',])->middleware('permission')->name('sipOutServiceSetting.index');
    Route::get('/web/project/delete/{id}', [ViewController::class,'projectStoreDelete',])->name('projectStoreDelete.delete');

    Route::get('/web/linkChange/setting/{id}', [ViewController::class,'linkSetting',])->name('linkSetting.index');
    Route::post('/web/linkChange/linkChangeSetting/{id}', [ViewController::class,'linkChangeSetting',])->name('linkChangeSetting.store');


    Route::get('/service/module/{id}', [\App\Http\Controllers\Service\ModuleController::class,'serviceModuleIndex',])->name('serviceModule.index');
    Route::patch('/service/module/store/{id}', [\App\Http\Controllers\Service\ModuleController::class,'serviceModuleStore',])->name('serviceModule.store');
    Route::get('/service/payment/{id}', [PaymentServiceController::class,'paymentServiceIndex',])->name('paymentService.index');

    Route::get('/service/promoCode/active', [PaymentServiceController::class,'promoCodeActive',])->name('promoCodeActive.store');


});
Route::post('/api/webinar/notification/store/{project_id}', [ViewController::class, 'notificationStore',])->name('notificationStore');
Route::post('/api/webinar/notification/store/wa/{project_id}', [ViewController::class, 'notificationWaStore',])->name('notificationWaStore');

Route::delete('/api/webinar/createNotification/delete/{project_id}', [ViewController::class, 'createNotificationDelete',])->name('createNotification.delete');

Route::post('/api/tilda/new_leads/{id}', [\App\Http\Controllers\Tilda\TildaController::class, 'newLead']);


// тесты
Route::get('/webinar/notification/testPlanned/{id}', [\App\Http\Controllers\Notification\TestController::class, 'testPlanned',]);
Route::get('/webinar/notification/schedules_id/{schedules_id}', [\App\Http\Controllers\Notification\TestController::class, 'testCompleteAction',]);
Route::get('/webinar/notification/testRate/{id}', [\App\Http\Controllers\Notification\TestController::class, 'testRate',]);


Route::get('/webinar/notification/testKernels/{id}', [\App\Http\Controllers\Notification\TestController::class, 'testKernels',]);
Route::get('/test/addNotification/{phone}/{project_id}', [\App\Http\Controllers\Notification\TestController::class, 'testAddNotification',]);
Route::get('/webinar/notification/test/smsWelcomeTest', [\App\Http\Controllers\Notification\TestController::class, 'sendWelcomeSmsAeroTest',]);


// отче по вебинару
Route::post('/api/bizon/enterRoom/{id}', [BizonController::class,'enterRoom',])->name('enterRoom.store');
Route::post('/api/bizon/apiSetting/{id}', [BizonController::class,'bizonApiSetting',])->name('bizonApiSetting.store');

Route::match(['get', 'post'],'/api/notification/add/{id}', [NotificationWebPhoneInsert::class,'addNotificationRequest',])->name('addNotificationRequest.store');



Route::get('/webinar/stat/{id}', [BizonController::class,'WebinarStat',])->name('webinarStat.index');
Route::patch('/webinar/stat/update/{id}', [BizonController::class,'WebinarStatUpdate',])->name('webinarStat.update');
Route::get('/webinar/stat/updateUtm/{id}', [BizonController::class,'WebinarStatUpdateUtm',])->name('WebinarStatUpdateUtm.update');

Route::get('/webinar/reaching/{id}', [\App\Http\Controllers\Notification\TrafficController::class,'WebinarReaching',])->name('webinarReaching.index');


Route::get('/traffic/stat/{id}', [BizonController::class,'trafficStat',])->name('trafficStat.index');
Route::get('/traffic/stat/{id}/save', [BizonController::class,'trafficStatSave',])->name('trafficStatSave.index');


Route::get('/lead/tilda/{id}', [BizonController::class,'tildaLeadStat',])->name('tildaLeadStat.index');
Route::get('/lead/tilda/{id}/save', [BizonController::class,'tildaLeadStatSave',])->name('tildaLeadStatSave.index');

Route::get('/lead/stat/{id}', [LeadStatController::class,'leadStat',])->name('leadStat.index');

// Бизон
Route::post('/bizon/after/{id}', [BizonController::class, 'after',]);
Route::match(['get', 'post'],'/bizon/webinar/list/{id}', [BizonController::class, 'getWebinarLint',])->name('getWebinarLint.store');
Route::match(['get', 'post'],'/bizon/webinar/update/{id}/{webinar_id}', [BizonController::class, 'webinarIdUpdateIndex',])->name('webinar.update');


// whats up
Route::post('/api/wa/token/update/{id}', [WaViewController::class,'waTokenUpdate',])->name('waTokenUpdate.store');

Route::get('/wa/instance/{id}', [WaViewController::class,'instanceSetting',])->name('instanceSetting.index');
Route::post('/wa/instance/update/{id}', [WaViewController::class,'instanceUpdate',])->name('instanceSetting.update');
Route::get('/wa/instance/check/{id}/{instance_id}', [WaViewController::class,'instanceCheck',])->name('instanceCheck.store');
Route::post('/api/wa/instance/getQr/{id}/{instance_id}', [WaViewController::class,'instanceGetQr',])->name('instanceGetQr.store');
Route::post('/api/wa/instance/test/message/{id}', [WaViewController::class,'instanceTestMessage',])->name('instanceTestMessage.store');


Route::get('/wa/addInstance/{id}', [WaViewController::class,'addInstance',])->name('addInstance.index');
Route::post('/api/wa/addInstance/store/{id}', [WaViewController::class,'addInstanceStore',])->name('addInstance.store');

Route::post('/api/wa/addInstance/database/store/{id}', [WaViewController::class,'addInstanceDatabaseStore',])->name('addInstanceDatabase.store');
Route::post('/api/wa/webhook/{id}/{instance_id}', [WaViewController::class,'waWebhook',])->name('waWebhook.store');

Route::get('/wa/SendMessage/black/{id}', [WaViewController::class,'SendWaMessageOne',])->name('SendWaMessage.index');
Route::post('/wa/SendMessage/black/store/{id}', [WaViewController::class,'SendWaMessageStore',])->name('SendWaMessage.store');

Route::get('/waba/setting/{id}', [\App\Http\Controllers\Waba\SettingController::class,'settingWaba',])->name('settingWaba.index');

Route::get('/waba/register/{id}', [\App\Http\Controllers\Waba\SettingController::class,'facebookEnter',])->name('facebookEnter.index');
//Route::match(['get', 'post'],'/api/waba/saveToken/{id}', [\App\Http\Controllers\Waba\SettingController::class,'saveToken',])->name('saveToken.store');
Route::get('/waba/setBM/{id}', [\App\Http\Controllers\Waba\SettingController::class,'setBM',])->name('setBM.index');
Route::match(['get', 'post'],'/api/waba/setWabaId/{id}', [\App\Http\Controllers\Waba\SettingController::class,'setWabaId',])->name('setBM.store');
Route::match(['get', 'post'],'/waba/setWabaId/store/{id}', [\App\Http\Controllers\Waba\SettingController::class,'setWabaIdStore',])->name('setWabaId.store');
Route::match(['get', 'post'],'/waba/phone/certificate/update/store/{id}/{phone_id}', [\App\Http\Controllers\Waba\SettingController::class,'certificateUpdatePhone',])->name('certificateUpdatePhone.store');
Route::match(['get', 'post'],'/waba/phone/verificationCode/{id}', [\App\Http\Controllers\Waba\SettingController::class,'verificationCode',])->name('verificationCode.index');
Route::match(['get', 'post'],'/waba/phone/verificationCode/store/{id}', [\App\Http\Controllers\Waba\SettingController::class,'verificationCodeStore',])->name('verificationCode.store');
Route::match(['get', 'post'],'/waba/templates/{id}', [\App\Http\Controllers\Waba\TemplatesController::class,'TemplatesControllerIndex',])->name('templatesController.index');


Route::match(['get', 'post'],'/waba/redirect/oauth', [\App\Http\Controllers\Waba\SettingController::class,'exchangeToken',])->name('exchangeToken.store');
Route::post('/api/waba/token/setting/{project_id}', [\App\Http\Controllers\Waba\SettingController::class, 'wabaToken',])->name('wabaToken.store');


Route::get('/waba/SendMessage/{id}', [\App\Http\Controllers\Waba\SendMessageController::class,'SendMessageWabaIndex',])->name('SendMessageWaba.index');
Route::post('/api/waba/send/store/{id}', [\App\Http\Controllers\Waba\SendMessageController::class,'SendMessageWabaStore',])->name('SendMessageWaba.store');
Route::get('/api/waba/SendMessage/update/store/{id}', [\App\Http\Controllers\Waba\TemplatesController::class,'updateStore',])->name('updateWabaTemplate.store');
Route::get('/waba/response/{id}', [\App\Http\Controllers\Waba\WebhookController::class,'responseSetting',])->name('responseSetting.index');
Route::post('/api/waba/save/media/{id}', [\App\Http\Controllers\Waba\WabaMessageController::class,'saveMedia',])->name('saveMedia.store');
Route::post('/api/waba/delete/media/{id}', [\App\Http\Controllers\Waba\WabaMessageController::class,'deleteMedia',])->name('deleteMedia.store');


Route::get('/waba/message/{id}', [\App\Http\Controllers\Waba\WabaMessageController::class,'index',])->name('WabaMessageController.index');
Route::post('/waba/message/sendOne/{id}', [\App\Http\Controllers\Waba\WabaMessageController::class,'sendMessageStore',])->name('sendMessageStore.store');

Route::get('/wa/addInstance/delete/{id}/{instance_id}', [WaViewController::class,'deleteInstance',])->name('instance.delete');

Route::get('/api/waba/test/{waba_templates_id}', [\App\Http\Controllers\Waba\SendMessageController::class,'testMessage',])->name('testMessage.store');

Route::match(['get', 'post'],'/api/waba/webhook/{id}', [\App\Http\Controllers\Waba\WebhookController::class,'webhook',])->name('sendMessage.store');
Route::match(['get', 'post'],'/api/getDateWeb/{id}', [PiplelineController::class,'getDateWeb',])->name('getDateWeb.store');


Route::match(['get', 'post'],'/api/test/request', [\App\Http\Controllers\Notification\TestController::class,'requestTest',]);
Route::match(['get', 'post'],'/api/test/drag/request', [\App\Http\Controllers\Notification\TestController::class,'requestDrag',]);

