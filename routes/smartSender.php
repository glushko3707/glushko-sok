<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Projects\ProjectController;
use App\Http\Controllers\Smartsender\SmartsenderLeadsController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;



Route::post('/api/smartsender/newLead/store/{id}', [SmartsenderLeadsController::class,'newLeads',]);
Route::post('/api/smartsender/active/store/{id}', [SmartsenderLeadsController::class,'activeSmartsender',]);

