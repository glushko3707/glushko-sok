<?php

use App\Http\Controllers\LeadCrm\LeadCrmController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Sipout\Admin\StatAllProject;
use App\Http\Controllers\Sipout\ContactDatabaseController;
use App\Http\Controllers\Sipout\LeadController;
use App\Http\Controllers\Sipout\SipoutSettingController;
use App\Http\Controllers\Sipout\SipoutStatisticController;
use App\Http\Controllers\Sipout\WelcomeMessage;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/sipout/leads/client/{client_rand_id}', [LeadCrmController::class,'sipoutLeadsClientIndex',])->name('sipoutLeadsClientIndex');

Route::post('/api/crm/lead/update/store', [LeadCrmController::class,'crmLeadStore',])->name('crmLeadStore.store');

Route::middleware('auth')->group(function () {

// работа с базой данных номеров
Route::get('/contactDatabase/{id}', [ContactDatabaseController::class, 'contactDatabaseIndex',])->middleware('permission')->name('contactDatabase.index');
Route::post('/contactDatabase/cleanUsed/{id}', [ContactDatabaseController::class, 'contactDatabaseCleanUsed',])->name('contactDatabaseCleanUsed.index');
Route::get('/contactDatabase/updateCategory/{id}/{category_id}', [ContactDatabaseController::class, 'updateCategory',])->name('updateCategory.index');

Route::get('/contactDatabase/blackList/{id}', [ContactDatabaseController::class, 'blackListIndex',])->name('blackList.index');
Route::post('/contactDatabase/blackList/store/{id}', [ContactDatabaseController::class, 'blackListStore',])->name('blackList.store');


Route::post('/contactDatabase/save/{id}', [ContactDatabaseController::class, 'contactDatabaseStore',])->name('contactDatabase.store');
Route::get('/contactDatabase/add/{id}', [ContactDatabaseController::class, 'contactAddDatabaseIndex',])->middleware('permission')->name('contactAddDatabase.index');
Route::post('/contactDatabase/add/store/{id}', [ContactDatabaseController::class, 'contactAddDatabaseStore',])->name('contactAddDatabase.store');
Route::post('/contactDatabase/minus/store/{id}', [ContactDatabaseController::class, 'contactMinusDatabaseStore',])->name('contactMinusDatabase.store');
Route::post('/file/minus/store/{id}', [ContactDatabaseController::class, 'fileMinusStore',])->name('fileMinus.store');

Route::delete('/api/contactDatabase/delete/store/{id}', [ContactDatabaseController::class, 'contactDatabaseDelete',])->name('contactDatabase.delete');

Route::post('/api/contactDatabase/get/store/{id}', [ContactDatabaseController::class, 'getContactDatabase',])->name('getContactDatabase.store');
Route::post('/api/contactDatabase/get/delete/{id}', [ContactDatabaseController::class, 'getContactDatabaseDelete',])->name('getContactDatabase.delete');


Route::post('/api/contactDatabase/get/info/{id}', [ContactDatabaseController::class, 'getInfoContactDatabase',])->name('getInfoContactDatabase.store');
Route::post('/api/add/sipout/contact/{id}', [ContactDatabaseController::class, 'addSipoutContact',])->name('addSipoutContact.store');

// Настройки СипАута
Route::get('/sipoutSetting/{id}', [SipoutSettingController::class,'sipoutSettingIndex',])->middleware('permission')->name('sipoutSetting.index');
Route::post('/sipoutSetting/store/{id}', [SipoutSettingController::class,'sipoutSettingStore',])->name('sipoutSetting.store');

// лиды CRM
Route::get('/sipout/leads/{id}', [LeadCrmController::class,'sipoutLeadsIndex',])->middleware(['permission','payment'])->name('sipoutLeads.index');

Route::post('/sipout/leads/download/{id}', [LeadCrmController::class,'sipoutLeadsGet',])->name('sipoutLeadsGet.store');


// Welcome Sipout SMS
Route::get('/sipout/welcome/message/{id}', [WelcomeMessage::class,'index',])->name('WelcomeMessage.index');

Route::post('/api/sms/service/smsAero/setting/{project_id}', [\App\Http\Controllers\Notification\Services\SmsController::class, 'smsAeroSetting',])->name('smsAeroSetting.store');

Route::patch('/api/welcome/sms/active/{project_id}', [WelcomeMessage::class, 'firstSmsActive',])->name('firstSmsActive.store');
Route::post('/api/welcome/sms/update/text/{project_id}', [WelcomeMessage::class, 'firstSmsUpdateText',])->name('firstSmsUpdateText.store');

Route::patch('/api/welcome/wa/active/{project_id}', [WelcomeMessage::class, 'firstWaActive',])->name('firstWaActive.store');
Route::post('/api/welcome/wa/update/text/{project_id}', [WelcomeMessage::class, 'firstWaUpdateText',])->name('firstWaUpdateText.store');

Route::patch('/api/welcome/waba/active/{project_id}', [WelcomeMessage::class, 'firstWabaActive',])->name('firstWabaActive.store');
Route::post('/api/welcome/waba/update/text/{project_id}', [WelcomeMessage::class, 'firstWabaUpdateText',])->name('firstWabaUpdateText.store');



// Страница статистики
Route::get('sipout/statistic/{id}', [SipoutStatisticController::class,'sipoutStatisticIndex',])->middleware(['permission','payment'])->name('sipoutStatistic.index');
Route::post('api/sipout/getCall/{id}', [SipoutStatisticController::class,'getCallStatisticIndex',])->name('getCallStatistic.index');

Route::get('sipout/getAllCall/{id}', [SipoutStatisticController::class,'getSipoutPhone',])->name('getSipoutPhone.store');

// Загрузка новых данных
Route::get('sipout/statistic/update/store/{id}', [SipoutStatisticController::class,'sipoutStatisticUpdateStore',])->name('sipoutStatisticUpdate.index');
Route::post('sipout/statistic/uploadReport/store/{id}', [SipoutStatisticController::class,'sipoutStatisticUploadReportStore',])->name('sipoutStatisticUploadReport.index');

Route::delete('sipout/callId/delete/{id}', [SipoutStatisticController::class,'callIdDelete',])->name('callId.delete');


// обновление номера телефона и комментариев
Route::post('/api/sipoutCallIdUpdateComment/{id}', [SipoutStatisticController::class,'sipoutCallIdUpdateComment',])->name('sipoutCallIdUpdateComment.store');
Route::post('/api/sipoutCallIdUpdateOffer/{id}', [SipoutStatisticController::class,'sipoutCallIdUpdateOffer',])->name('sipoutCallIdUpdateOffer.store');

});

// загрузка новых лидов
Route::get('/api/newLeadsSipOut/{id}', [LeadController::class,'newLeadsSipOutStore',])->name('newLeadsSipOut.store');
Route::post('/api/tilda/new_leads/{id}', [\App\Http\Controllers\Tilda\TildaController::class, 'newLead']);
Route::get('/api/newLeadsSipOut/stopList/{id}', [LeadController::class,'stopListStore',])->name('stopListStore.store');
Route::post('/api/tilda/leads/{id}', [\App\Http\Controllers\Tilda\TildaController::class, 'newLeadAllProject']);


// Администратор
Route::get('/sipout/admin/stat/{id}', [StatAllProject::class,'StatAllProject',])->name('StatAllProject.index');


//удалить
Route::get('/test/waba/failed/{w_id}', [\App\Http\Controllers\Waba\WebhookController::class,'filedWabaSmsSend',]);
