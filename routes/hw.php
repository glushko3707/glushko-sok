<?php

use Illuminate\Support\Facades\Route;


Route::get('/hw/account/update/table/list', [\App\Http\Controllers\HelloWorld\UpdateCostController::class,'listUpdateIndex'])->name('hw.account.update.table.list');
Route::match(['get', 'post'],'/hw/account/update/table/list/all/update/store', [\App\Http\Controllers\HelloWorld\UpdateCostController::class,'listUpdateStore'])->name('hw.listUpdateStore');


Route::get('/hw/account/update/table/date/{account_update_id}', [\App\Http\Controllers\HelloWorld\UpdateCostController::class,'listUpdateDateIndex'])->name('hw.account.update.table.date');
Route::match(['get', 'post'],'/hw/account/update/table/date/store/{account_update_id}', [\App\Http\Controllers\HelloWorld\UpdateCostController::class,'listUpdateDateStore'])->name('hw.account.update.table.date.store');

Route::match(['get', 'post'],'/hw/report/asdasd/{report}', [App\Http\Controllers\HelloWorld\ReportController::class, 'index'])->middleware(['auth']);
