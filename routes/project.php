<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Projects\ProjectController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;


// создание новых проектов
Route::get('/project/create/index', [ProjectController::class, 'createProjectIndex',])->middleware('permission')->name('createProject.index');
Route::post('/api/project/create/store', [ProjectController::class, 'createProjectStore',])->middleware('prevent_multiple_requests')->name('createProject.store');
Route::get('/project/destroy/store/{id}', [ProjectController::class, 'destroyProjectStore',])->name('project.destroy');

// доступны проекты (нужен)
Route::get('/project/permission/index', [ProjectController::class, 'permissionProjectIndex',])->name('permissionProject.index');
Route::get('/project/permission/get', [ProjectController::class, 'getPermissionProject',])->middleware('auth')->name('getPermissionProject.index');
Route::post('/project/permission/get/json', [ProjectController::class, 'getPermissionProjectJson',])->name('getPermissionProjectJson.index');
Route::get('/project/module/get/{id}', [ProjectController::class, 'getPermissionModule',])->name('getPermissionProjectModule.index');
