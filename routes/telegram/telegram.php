<?php

use App\Http\Controllers\saleBot\SalebotCrmController;
use App\Http\Controllers\Telegram\LinkInvitePhoneController;
use Illuminate\Support\Facades\Route;



Route::match(['get', 'post'],'/api/telegram/link/create/{id}', [LinkInvitePhoneController::class,'createInviteLink',])->name('createInviteLinkTelegram');
Route::match(['get', 'post'],'/api/getLinkInvite/{id}', [LinkInvitePhoneController::class,'getLinkSite',])->name('getEmptyLink.store');

Route::get('/api/saleBot/text/input', [\App\Http\Controllers\saleBot\TextController::class,'textInput'])->name('SalebotTextInput.index');
Route::post('/api/saleBot/text/input/store', [\App\Http\Controllers\saleBot\TextController::class,'textInputStore'])->name('SalebotTextInput.store');

Route::match(['get', 'post'],'/api/salebot/newUser/{project_id}', [\App\Http\Controllers\Telegram\TelegramMemberController::class,'newUserSalebot',]);

Route::match(['get', 'post'],'/api/salebot/join_chat_member/{id}', [\App\Http\Controllers\Telegram\TelegramMemberController::class,'joinMember',])->name('joinMember.store');
Route::match(['get', 'post'],'/api/intensive/getNext/{id}', [\App\Http\Controllers\Intensive\IntensiveSettingController::class,'getNextIntensive',])->name('getNextIntensive.store');

Route::match(['get', 'post'],'/api/salebot/sheet/add/{id}', [\App\Http\Controllers\saleBot\SalebotAddToSheet::class,'webhookFromSalebot',]);


Route::match(['get', 'post'],'/api/enterTelegramId/{id}', [\App\Http\Controllers\Telegram\TelegramMemberController::class,'enterTelegramId',]);

Route::match(['get', 'post'],'/telegram/ads/treiner/create', function () {
    return view('service.telegramAds.TelegramAds');
});

Route::match(['get', 'post'],'/telegram/ads/treiner/statistic', function () {
    return view('service.telegramAds.TelegramAdsStatistic');
});


Route::match(['get'],'/fortuna/{id}', [\App\Http\Controllers\TelegramApps\FortunaController::class,'index']);


Route::match(['get'],'/fortuna/telegramSetting/{id}', [\App\Http\Controllers\Telegram\TelegramSetting::class,'telegramSetting'])->name('telegramSetting.index');


Route::match(['get','post'],'/api/crm/create/{id}', [SalebotCrmController::class,'addToCrm']);
